<?php
/**
 * @file
 * field_bases.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function field_bases_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_message_status'.
  $permissions['create field_message_status'] = array(
    'name' => 'create field_message_status',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create field_message_view_count'.
  $permissions['create field_message_view_count'] = array(
    'name' => 'create field_message_view_count',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_message_status'.
  $permissions['edit field_message_status'] = array(
    'name' => 'edit field_message_status',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_message_view_count'.
  $permissions['edit field_message_view_count'] = array(
    'name' => 'edit field_message_view_count',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_message_status'.
  $permissions['edit own field_message_status'] = array(
    'name' => 'edit own field_message_status',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_message_view_count'.
  $permissions['edit own field_message_view_count'] = array(
    'name' => 'edit own field_message_view_count',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_message_status'.
  $permissions['view field_message_status'] = array(
    'name' => 'view field_message_status',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_message_view_count'.
  $permissions['view field_message_view_count'] = array(
    'name' => 'view field_message_view_count',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_message_status'.
  $permissions['view own field_message_status'] = array(
    'name' => 'view own field_message_status',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_message_view_count'.
  $permissions['view own field_message_view_count'] = array(
    'name' => 'view own field_message_view_count',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  return $permissions;
}
