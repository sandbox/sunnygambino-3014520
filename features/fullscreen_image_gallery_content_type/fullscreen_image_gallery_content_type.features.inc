<?php
/**
 * @file
 * fullscreen_image_gallery_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function fullscreen_image_gallery_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function fullscreen_image_gallery_content_type_node_info() {
  $items = array(
    'fullscreen_image_gallery' => array(
      'name' => t('Fullscreen Image Gallery'),
      'base' => 'node_content',
      'description' => t('Image gallery with Fullscreen Images'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
