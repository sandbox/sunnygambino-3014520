<?php
/**
 * @file
 * q_a_drag_drop_no_order.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function q_a_drag_drop_no_order_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function q_a_drag_drop_no_order_node_info() {
  $items = array(
    'qadraganddrop' => array(
      'name' => t('Q&A - Drag & Drop no order'),
      'base' => 'node_content',
      'description' => t('Create a list of answers (max. 3) that needs to be dragged and dropped without a specific order'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
