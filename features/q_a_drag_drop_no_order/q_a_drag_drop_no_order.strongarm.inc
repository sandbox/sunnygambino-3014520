<?php
/**
 * @file
 * q_a_drag_drop_no_order.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function q_a_drag_drop_no_order_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__qadraganddrop';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'body' => array(
          'weight' => 0,
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '9',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__qadraganddrop'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_qadraganddrop_pattern';
  $strongarm->value = '';
  $export['pathauto_node_qadraganddrop_pattern'] = $strongarm;

  return $export;
}
