<?php
/**
 * @file
 * q_a_sortable_list.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function q_a_sortable_list_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function q_a_sortable_list_node_info() {
  $items = array(
    'qasortable' => array(
      'name' => t('Q&A - Sortable List'),
      'base' => 'node_content',
      'description' => t('Create a list in order that needs to be sorted in correct order'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
