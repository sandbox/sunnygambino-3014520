<?php
/**
 * @file
 * fulscreen_image.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function fulscreen_image_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-fullscreenimage-field_bg_colour'.
  $field_instances['node-fullscreenimage-field_bg_colour'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Use this option to set a background colour. You can use the HEX colour code e.g. #ffffff, the RGB colour code e.g. rgb(255,255,255), or simply enter a colour name e.g. white, black, red, green, blue. If your image is smaller than the slide itself, this is the colour that will show around the image.
',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_colour',
    'label' => 'Enter a background colour',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 7,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_fsimage'.
  $field_instances['node-fullscreenimage-field_fsimage'] = array(
    'bundle' => 'fullscreenimage',
    'deleted' => 0,
    'description' => 'Upload the image to be used for this slide. The target image size is 1366 pixels wide and 768 pixels high (a 16:9 aspect ratio) which will fill the available space on the slide. Larger images will be resized to fit (some cropping may occur). You may upload smaller images - these will be centred (both horizontally and vertically) in the middle of the slide. Please be aware that the left and right edges of full screen images will appear to be cropped if the slide is viewed on a screen with a narrower 4:3 aspect ratio - an iPad for example. We recommend the image file size should be between 200KB and 400KB.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => 'fullscreen_image',
        ),
        'type' => 'image',
        'weight' => 2,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_fsimage',
    'label' => 'Fullscreen image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '20MB',
      'max_resolution' => '1366x768',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'large',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_master_presentation'.
  $field_instances['node-fullscreenimage-field_master_presentation'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_master_presentation',
    'label' => 'Master presentation',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'custom_display_fields' => array(),
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_migrate_original_nid'.
  $field_instances['node-fullscreenimage-field_migrate_original_nid'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This field shows you the original node ID before the migration',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_migrate_original_nid',
    'label' => 'Original NID',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 50,
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 10,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_scale_for_ipad'.
  $field_instances['node-fullscreenimage-field_scale_for_ipad'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'On widescreen (16:9) images, the left & right edges of the image will be cropped when viewed on an iPad. This is because the iPad\'s screen ratio is 4:3 - a squarer, less rectangular shape. To prevent this, select the \'Scale for iPad\' checkbox. In this case, on the iPad, the slide will show a letter-boxed version of the image - with back bars at the top and the bottom of the screen. Therefore the whole of the image will still be visible. N.B. This feature will work on images with aspect ratios other than 16:9 - but you may get unexpected results.
',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_scale_for_ipad',
    'label' => 'Scale for iPad',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_scale_to_fit'.
  $field_instances['node-fullscreenimage-field_scale_to_fit'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => 'If you choose select the \'Scale to fit\' option, then images smaller than the default (1366 x 768 pixels) will be scaled up to fit the available space in the slide. Please note that scaling the image up will inevitably lessen the quality to some degree, so you may see some pixilation. Some cropping of the image may also occur. N.B. This feature will work on images with aspect ratios other than 16:9 - but you may get unexpected results.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_scale_to_fit',
    'label' => 'Scale to fit',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 1,
      ),
      'type' => 'options_onoff',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_subtitle'.
  $field_instances['node-fullscreenimage-field_subtitle'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'If you wish to add a caption to your image - insert your text here. Leave this field blank if no caption is required. 
',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_subtitle',
    'label' => 'Caption',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => -3,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_tag'.
  $field_instances['node-fullscreenimage-field_tag'] = array(
    'bundle' => 'fullscreenimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Chapters are used to navigate quickly to slides in the presenter app. You can set a slide as a chapter marker simply by entering a name.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tag',
    'label' => 'Chapter marker',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => -4,
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-fullscreenimage-field_thumbnail'.
  $field_instances['node-fullscreenimage-field_thumbnail'] = array(
    'bundle' => 'fullscreenimage',
    'deleted' => 0,
    'description' => 'Leave this option blank if you would like the system to automatically generate a thumbnail for you. However, if you would like to upload your own specific thumbnail, please do so here. The image you upload should be at least 1024 pixels wide by 768 pixels high. Images larger than 1024x768 pixels will be resized. We recommend the image file size should be between 200KB and 300KB. ',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file_entity',
        'settings' => array(
          'file_view_mode' => 'default',
        ),
        'type' => 'file_rendered',
        'weight' => 1,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_thumbnail',
    'label' => 'Slide thumbnail',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'jpg jpeg png gif',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '10MB',
      'max_resolution' => '1024x768',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 1,
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Caption');
  t('Chapter marker');
  t('Chapters are used to navigate quickly to slides in the presenter app. You can set a slide as a chapter marker simply by entering a name.');
  t('Enter a background colour');
  t('Fullscreen image');
  t('If you choose select the \'Scale to fit\' option, then images smaller than the default (1366 x 768 pixels) will be scaled up to fit the available space in the slide. Please note that scaling the image up will inevitably lessen the quality to some degree, so you may see some pixilation. Some cropping of the image may also occur. N.B. This feature will work on images with aspect ratios other than 16:9 - but you may get unexpected results.');
  t('If you wish to add a caption to your image - insert your text here. Leave this field blank if no caption is required. 
');
  t('Leave this option blank if you would like the system to automatically generate a thumbnail for you. However, if you would like to upload your own specific thumbnail, please do so here. The image you upload should be at least 1024 pixels wide by 768 pixels high. Images larger than 1024x768 pixels will be resized. We recommend the image file size should be between 200KB and 300KB. ');
  t('Master presentation');
  t('On widescreen (16:9) images, the left & right edges of the image will be cropped when viewed on an iPad. This is because the iPad\'s screen ratio is 4:3 - a squarer, less rectangular shape. To prevent this, select the \'Scale for iPad\' checkbox. In this case, on the iPad, the slide will show a letter-boxed version of the image - with back bars at the top and the bottom of the screen. Therefore the whole of the image will still be visible. N.B. This feature will work on images with aspect ratios other than 16:9 - but you may get unexpected results.
');
  t('Original NID');
  t('Scale for iPad');
  t('Scale to fit');
  t('Slide thumbnail');
  t('This field shows you the original node ID before the migration');
  t('Upload the image to be used for this slide. The target image size is 1366 pixels wide and 768 pixels high (a 16:9 aspect ratio) which will fill the available space on the slide. Larger images will be resized to fit (some cropping may occur). You may upload smaller images - these will be centred (both horizontally and vertically) in the middle of the slide. Please be aware that the left and right edges of full screen images will appear to be cropped if the slide is viewed on a screen with a narrower 4:3 aspect ratio - an iPad for example. We recommend the image file size should be between 200KB and 400KB.');
  t('Use this option to set a background colour. You can use the HEX colour code e.g. #ffffff, the RGB colour code e.g. rgb(255,255,255), or simply enter a colour name e.g. white, black, red, green, blue. If your image is smaller than the slide itself, this is the colour that will show around the image.
');

  return $field_instances;
}
