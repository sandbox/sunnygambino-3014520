<?php
/**
 * @file
 * at_a_glance.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function at_a_glance_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|ataglance|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select Background',
    'weight' => '7',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select Background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_details|node|ataglance|form';
  $field_group->group_name = 'group_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Details',
    'weight' => '2',
    'children' => array(
      0 => 'field_contact_information',
      1 => 'field_facilities',
      2 => 'field_hotel_brand',
      3 => 'field_hotel_rating',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-details field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_details|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_first_column_style|node|ataglance|form';
  $field_group->group_name = 'group_first_column_style';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_2';
  $field_group->data = array(
    'label' => 'First Column Style',
    'weight' => '24',
    'children' => array(
      0 => 'field_column_bg_colour',
      1 => 'field_column_bold',
      2 => 'field_column_text_colour',
      3 => 'field_column_width',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-first-column-style field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_first_column_style|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header_row_style|node|ataglance|form';
  $field_group->group_name = 'group_header_row_style';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_3';
  $field_group->data = array(
    'label' => 'Header Row Style',
    'weight' => '15',
    'children' => array(
      0 => 'field_header_bold',
      1 => 'field_header_text_colour',
      2 => 'field_row_bg_colour',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-header-row-style field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_header_row_style|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image1|node|ataglance|form';
  $field_group->group_name = 'group_image1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_2';
  $field_group->data = array(
    'label' => '',
    'weight' => '17',
    'children' => array(
      0 => 'field_image_1',
      1 => 'field_image_1_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image1 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image1|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image2|node|ataglance|form';
  $field_group->group_name = 'group_image2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_2';
  $field_group->data = array(
    'label' => '',
    'weight' => '18',
    'children' => array(
      0 => 'field_image_2',
      1 => 'field_image_2_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image2 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image2|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image3|node|ataglance|form';
  $field_group->group_name = 'group_image3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_2';
  $field_group->data = array(
    'label' => '',
    'weight' => '19',
    'children' => array(
      0 => 'field_image_3',
      1 => 'field_image_3_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image3 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image3|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image4|node|ataglance|form';
  $field_group->group_name = 'group_image4';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_2';
  $field_group->data = array(
    'label' => '',
    'weight' => '20',
    'children' => array(
      0 => 'field_image_4',
      1 => 'field_image_4_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image4 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image4|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_image5|node|ataglance|form';
  $field_group->group_name = 'group_image5';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_tab_2';
  $field_group->data = array(
    'label' => '',
    'weight' => '21',
    'children' => array(
      0 => 'field_image_5',
      1 => 'field_image_5_text',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => '',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-image5 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_image5|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|ataglance|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Information',
    'weight' => '9',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|ataglance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tab_1|node|at_a_glance|form';
  $field_group->group_name = 'group_tab_1';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Overview',
    'weight' => '3',
    'children' => array(
      0 => 'field_bedrooms',
      1 => 'field_closest_airports',
      2 => 'field_closest_railway',
      3 => 'field_from_bristol',
      4 => 'field_from_edinburgh',
      5 => 'field_from_london',
      6 => 'field_from_manchester',
      7 => 'field_information',
      8 => 'field_max_capacity',
      9 => 'field_meeting_event_rooms',
      10 => 'field_overview_image',
      11 => 'field_parking',
      12 => 'field_restaurants',
      13 => 'field_suites',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Overview',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-tab-1 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_tab_1|node|at_a_glance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tab_2|node|at_a_glance|form';
  $field_group->group_name = 'group_tab_2';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Gallery',
    'weight' => '4',
    'children' => array(
      0 => 'group_image1',
      1 => 'group_first_column_style',
      2 => 'group_image2',
      3 => 'group_image3',
      4 => 'group_image4',
      5 => 'group_image5',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Gallery',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-tab-2 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_tab_2|node|at_a_glance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_tab_3|node|at_a_glance|form';
  $field_group->group_name = 'group_tab_3';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Capacity Chart',
    'weight' => '5',
    'children' => array(
      0 => 'field_table',
      1 => 'group_header_row_style',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Capacity Chart',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-tab-3 field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_tab_3|node|at_a_glance|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|ataglance|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'ataglance';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '8',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_thumbnail|node|ataglance|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Capacity Chart');
  t('Details');
  t('First Column Style');
  t('Gallery');
  t('Header Row Style');
  t('Node Information');
  t('Overview');
  t('Select Background');
  t('Thumbnail');

  return $field_groups;
}
