<?php
/**
 * @file
 * at_a_glance.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function at_a_glance_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function at_a_glance_node_info() {
  $items = array(
    'ataglance' => array(
      'name' => t('At a glance'),
      'base' => 'node_content',
      'description' => t('Template that will show details about each Hotel ‘at a glance’'),
      'has_title' => '1',
      'title_label' => t(' Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
