<?php
/**
 * @file
 * image_gallry_with_captions.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function image_gallry_with_captions_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_caption'.
  $field_bases['field_caption'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_caption',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 120,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_image_gallery'.
  $field_bases['field_image_gallery'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_image_gallery',
    'field_permissions' => array(
      'type' => 0,
    ),
    'indexes' => array(
      'revision_id' => array(
        0 => 'revision_id',
      ),
      'value' => array(
        0 => 'value',
      ),
    ),
    'locked' => 0,
    'module' => 'field_collection',
    'settings' => array(
      'hide_blank_items' => 1,
      'hide_initial_item' => 0,
      'path' => '',
    ),
    'translatable' => 0,
    'type' => 'field_collection',
  );

  return $field_bases;
}
