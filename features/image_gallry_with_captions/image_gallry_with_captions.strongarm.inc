<?php
/**
 * @file
 * image_gallry_with_captions.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function image_gallry_with_captions_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__imagegallerycaptions';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '7',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__imagegallerycaptions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_options_imagegallerycaptions';
  $strongarm->value = array(
    0 => 'main-menu',
  );
  $export['menu_options_imagegallerycaptions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'menu_parent_imagegallerycaptions';
  $strongarm->value = 'main-menu:0';
  $export['menu_parent_imagegallerycaptions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_imagegallerycaptions';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_imagegallerycaptions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_preview_imagegallerycaptions';
  $strongarm->value = '1';
  $export['node_preview_imagegallerycaptions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_submitted_imagegallerycaptions';
  $strongarm->value = 1;
  $export['node_submitted_imagegallerycaptions'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_imagegallerycaptions_pattern';
  $strongarm->value = '';
  $export['pathauto_node_imagegallerycaptions_pattern'] = $strongarm;

  return $export;
}
