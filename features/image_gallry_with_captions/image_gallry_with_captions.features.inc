<?php
/**
 * @file
 * image_gallry_with_captions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function image_gallry_with_captions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function image_gallry_with_captions_node_info() {
  $items = array(
    'imagegallerycaptions' => array(
      'name' => t('Image gallery with captions'),
      'base' => 'node_content',
      'description' => t('Image Gallery with captions'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
