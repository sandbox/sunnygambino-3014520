<?php
/**
 * @file
 * story.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function story_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__story';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'body' => array(
          'weight' => 0,
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__story'] = $strongarm;

  return $export;
}
