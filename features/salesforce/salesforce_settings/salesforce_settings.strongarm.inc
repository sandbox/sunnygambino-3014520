<?php
/**
 * @file
 * salesforce_settings.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function salesforce_settings_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_consumer_key';
  $strongarm->value = '3MVG9HxRZv05HarTt6K8uKMXpEyDstIxMQw8YjNI9Wi7Ry.EVExxcxAsatbx1mozIrJI0rSHxVcqvsz7wIFfS';
  $export['salesforce_consumer_key'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_consumer_secret';
  $strongarm->value = '8995432640216035481';
  $export['salesforce_consumer_secret'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_endpoint';
  $strongarm->value = 'https://login.salesforce.com';
  $export['salesforce_endpoint'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_identity';
  $strongarm->value = array(
    'id' => 'https://login.salesforce.com/id/00D20000000pKalEAE/00520000004nYSEAA2',
    'asserted_user' => TRUE,
    'user_id' => '00520000004nYSEAA2',
    'organization_id' => '00D20000000pKalEAE',
    'username' => 'zoltan@breadcreative.com',
    'nick_name' => 'zoltan1.4449957896729045E12',
    'display_name' => 'Zoltán Horváth',
    'email' => 'zoltan@breadcreative.com',
    'email_verified' => TRUE,
    'first_name' => 'Zoltán',
    'last_name' => 'Horváth',
    'timezone' => 'Europe/London',
    'photos' => array(
      'picture' => 'https://c.eu7.content.force.com/profilephoto/005/F',
      'thumbnail' => 'https://c.eu7.content.force.com/profilephoto/005/T',
    ),
    'addr_street' => NULL,
    'addr_city' => NULL,
    'addr_state' => NULL,
    'addr_country' => 'GB',
    'addr_zip' => NULL,
    'mobile_phone' => NULL,
    'mobile_phone_verified' => FALSE,
    'is_lightning_login_user' => FALSE,
    'status' => array(
      'created_date' => NULL,
      'body' => NULL,
    ),
    'urls' => array(
      'enterprise' => 'https://eu7.salesforce.com/services/Soap/c/{version}/00D20000000pKal',
      'metadata' => 'https://eu7.salesforce.com/services/Soap/m/{version}/00D20000000pKal',
      'partner' => 'https://eu7.salesforce.com/services/Soap/u/{version}/00D20000000pKal',
      'rest' => 'https://eu7.salesforce.com/services/data/v{version}/',
      'sobjects' => 'https://eu7.salesforce.com/services/data/v{version}/sobjects/',
      'search' => 'https://eu7.salesforce.com/services/data/v{version}/search/',
      'query' => 'https://eu7.salesforce.com/services/data/v{version}/query/',
      'recent' => 'https://eu7.salesforce.com/services/data/v{version}/recent/',
      'tooling_soap' => 'https://eu7.salesforce.com/services/Soap/T/{version}/00D20000000pKal',
      'tooling_rest' => 'https://eu7.salesforce.com/services/data/v{version}/tooling/',
      'profile' => 'https://eu7.salesforce.com/00520000004nYSEAA2',
      'feeds' => 'https://eu7.salesforce.com/services/data/v{version}/chatter/feeds',
      'groups' => 'https://eu7.salesforce.com/services/data/v{version}/chatter/groups',
      'users' => 'https://eu7.salesforce.com/services/data/v{version}/chatter/users',
      'feed_items' => 'https://eu7.salesforce.com/services/data/v{version}/chatter/feed-items',
      'feed_elements' => 'https://eu7.salesforce.com/services/data/v{version}/chatter/feed-elements',
    ),
    'active' => TRUE,
    'user_type' => 'STANDARD',
    'language' => 'en_US',
    'locale' => 'en_GB',
    'utcOffset' => 0,
    'last_modified_date' => '2017-09-08T13:44:54.000+0000',
  );
  $export['salesforce_identity'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_instance_url';
  $strongarm->value = 'https://eu7.salesforce.com';
  $export['salesforce_instance_url'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'salesforce_refresh_token';
  $strongarm->value = '5Aep861JmND5bFIsacVsSX_aKdjU3tSYHcnhI_MR9A4CgnTcibBpcn0q0m.1ZQU7.zvmib1_hdMOrZ98ZC_yHg1';
  $export['salesforce_refresh_token'] = $strongarm;

  return $export;
}
