<?php
/**
 * @file
 * salesforce_account_history.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function salesforce_account_history_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function salesforce_account_history_node_info() {
  $items = array(
    'sf_account_hitory' => array(
      'name' => t('Salesforce Account History'),
      'base' => 'node_content',
      'description' => t('Salesforce account history with Task and Event objects	'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
