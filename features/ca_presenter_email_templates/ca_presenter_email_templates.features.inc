<?php
/**
 * @file
 * ca_presenter_email_templates.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function ca_presenter_email_templates_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
