<?php
/**
 * @file
 * data_table.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function data_table_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|datatable|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'datatable';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '7',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => 'Option 1. Choose a background image.<br />
Option 2. Enter a background colour. Please note, in order to use a background colour, you need to select "No Image" in Option 1. ',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|datatable|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_first_column_style|node|datatable|form';
  $field_group->group_name = 'group_first_column_style';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'datatable';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'First Column Style',
    'weight' => '6',
    'children' => array(
      0 => 'field_column_bg_colour',
      1 => 'field_column_text_colour',
      2 => 'field_column_width',
      3 => 'field_column_bold',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'First Column Style',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-first-column-style field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_first_column_style|node|datatable|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_header_row_style|node|datatable|form';
  $field_group->group_name = 'group_header_row_style';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'datatable';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Header Row Style',
    'weight' => '4',
    'children' => array(
      0 => 'field_header_text_colour',
      1 => 'field_row_bg_colour',
      2 => 'field_header_bold',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Header Row Style',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-header-row-style field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_header_row_style|node|datatable|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|datatable|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'datatable';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '9',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|datatable|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|datatable|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'datatable';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '8',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|datatable|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('First Column Style');
  t('Header Row Style');
  t('Node information');
  t('Select background');
  t('Thumbnail');

  return $field_groups;
}
