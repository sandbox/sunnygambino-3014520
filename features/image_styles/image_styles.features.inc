<?php
/**
 * @file
 * image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: app_thumbnails.
  $styles['app_thumbnails'] = array(
    'label' => 'App thumbnails',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 480,
          'height' => 360,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: fullscreen_image.
  $styles['fullscreen_image'] = array(
    'label' => 'Fullscreen image (1366x768)',
    'effects' => array(
      4 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1366,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => -10,
      ),
      5 => array(
        'name' => 'image_crop',
        'data' => array(
          'width' => 1366,
          'height' => 768,
          'anchor' => 'center-center',
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: main_page_thumbnails.
  $styles['main_page_thumbnails'] = array(
    'label' => 'Main page thumbnails (240x180)',
    'effects' => array(
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 240,
          'height' => 180,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: viewer_thumbnails.
  $styles['viewer_thumbnails'] = array(
    'label' => 'Viewer thumbnails',
    'effects' => array(
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 200,
          'height' => 150,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
