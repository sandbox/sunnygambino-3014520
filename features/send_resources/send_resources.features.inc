<?php
/**
 * @file
 * send_resources.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function send_resources_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function send_resources_node_info() {
  $items = array(
    'sendresources' => array(
      'name' => t('Send Resources'),
      'base' => 'node_content',
      'description' => t('A template for sending presentations and documents'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
