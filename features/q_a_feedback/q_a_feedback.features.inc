<?php
/**
 * @file
 * q_a_feedback.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function q_a_feedback_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function q_a_feedback_node_info() {
  $items = array(
    'qafeedback' => array(
      'name' => t('Q&A - Feedback'),
      'base' => 'node_content',
      'description' => t('A template for open-ended questions.'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
