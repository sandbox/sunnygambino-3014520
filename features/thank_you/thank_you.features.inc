<?php
/**
 * @file
 * thank_you.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function thank_you_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function thank_you_node_info() {
  $items = array(
    'thankyou' => array(
      'name' => t('Thank you'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
