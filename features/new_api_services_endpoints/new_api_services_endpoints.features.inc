<?php
/**
 * @file
 * new_api_services_endpoints.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function new_api_services_endpoints_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
}
