<?php
/**
 * @file
 * new_api_services_endpoints.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function new_api_services_endpoints_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'v3';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'v3';
  $endpoint->authentication = array(
    'services' => 'services',
  );
  $endpoint->server_settings = array(
    'formatters' => array(
      'json' => TRUE,
      'bencode' => FALSE,
      'jsonp' => FALSE,
      'php' => FALSE,
      'xml' => FALSE,
    ),
    'parsers' => array(
      'application/json' => TRUE,
      'application/x-www-form-urlencoded' => TRUE,
      'multipart/form-data' => TRUE,
      'application/xml' => FALSE,
      'text/xml' => FALSE,
    ),
  );
  $endpoint->resources = array(
    'devicekey' => array(
      'actions' => array(
        'device' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'document_categories' => array(
      'actions' => array(
        'categories' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'documents' => array(
      'actions' => array(
        'list' => array(
          'enabled' => '1',
        ),
        'book' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
        'sync' => array(
          'enabled' => '1',
        ),
        'count' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'error' => array(
      'actions' => array(
        'submit' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'events' => array(
      'actions' => array(
        'push' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'messages' => array(
      'actions' => array(
        'inbox' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
        'unread' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'presentation_categories' => array(
      'actions' => array(
        'categories' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'presentations' => array(
      'actions' => array(
        'list' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
        'sync' => array(
          'enabled' => '1',
        ),
        'count' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'slides' => array(
      'actions' => array(
        'list' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'system' => array(
      'actions' => array(
        'connect' => array(
          'enabled' => '1',
        ),
        'get_variable' => array(
          'enabled' => '1',
        ),
        'set_variable' => array(
          'enabled' => '1',
        ),
        'del_variable' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'user' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
        'create' => array(
          'enabled' => '1',
        ),
        'update' => array(
          'enabled' => '1',
        ),
        'delete' => array(
          'enabled' => '1',
        ),
        'index' => array(
          'enabled' => '1',
        ),
      ),
      'actions' => array(
        'login' => array(
          'enabled' => '1',
          'settings' => array(
            'services' => array(
              'resource_api_version' => '1.0',
            ),
          ),
        ),
        'logout' => array(
          'enabled' => '1',
          'settings' => array(
            'services' => array(
              'resource_api_version' => '1.0',
            ),
          ),
        ),
        'token' => array(
          'enabled' => '1',
        ),
        'request_new_password' => array(
          'enabled' => '1',
        ),
        'user_pass_reset' => array(
          'enabled' => '1',
        ),
        'register' => array(
          'enabled' => '1',
        ),
      ),
      'targeted_actions' => array(
        'cancel' => array(
          'enabled' => '1',
        ),
        'password_reset' => array(
          'enabled' => '1',
        ),
        'resend_welcome_email' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['v3'] = $endpoint;

  return $export;
}
