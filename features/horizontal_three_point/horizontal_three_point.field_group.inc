<?php
/**
 * @file
 * horizontal_three_point.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function horizontal_three_point_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_animation|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_animation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Animation',
    'weight' => '6',
    'children' => array(
      0 => 'field_animation_speed',
      1 => 'field_animation_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Animation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-animation field-group-fieldset',
        'description' => 'Please select an animation type and speed (milliseconds).',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_animation|node|horizontalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '7',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => 'Option 1. Choose a background image.<br />
Option 2. Enter a background colour. Please note, in order to use a background colour, you need to select "No Image" in Option 1.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|horizontalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '9',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|horizontalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_one|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_point_one';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point one',
    'weight' => '3',
    'children' => array(
      0 => 'field_left_content',
      1 => 'field_left_image',
      2 => 'field_left_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Point one',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-point-one field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_point_one|node|horizontalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_three|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_point_three';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point three',
    'weight' => '5',
    'children' => array(
      0 => 'field_right_content',
      1 => 'field_right_image',
      2 => 'field_right_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Point three',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-point-three field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_point_three|node|horizontalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_two|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_point_two';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point two',
    'weight' => '4',
    'children' => array(
      0 => 'field_middle_content',
      1 => 'field_middle_image',
      2 => 'field_middle_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Point two',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-point-two field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_point_two|node|horizontalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|horizontalthreepoint|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'horizontalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '8',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|horizontalthreepoint|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Animation');
  t('Node information');
  t('Point one');
  t('Point three');
  t('Point two');
  t('Select background');
  t('Thumbnail');

  return $field_groups;
}
