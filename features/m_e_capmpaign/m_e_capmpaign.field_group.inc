<?php
/**
 * @file
 * m_e_capmpaign.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function m_e_capmpaign_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|mandeoffer|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'mandeoffer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '3',
    'children' => array(
      0 => 'field_bg_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 0,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => 'Option 1. Choose a background image.<br />
Option 2. Enter a background colour. Please note, in order to use a background colour, you need to select "No Image" in Option 1. ',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|mandeoffer|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|mandeoffer|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'mandeoffer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '5',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_node_info|node|mandeoffer|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|mandeoffer|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'mandeoffer';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '4',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_thumbnail|node|mandeoffer|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Node information');
  t('Select background');
  t('Thumbnail');

  return $field_groups;
}
