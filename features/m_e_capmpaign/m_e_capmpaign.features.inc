<?php
/**
 * @file
 * m_e_capmpaign.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function m_e_capmpaign_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function m_e_capmpaign_node_info() {
  $items = array(
    'mandeoffer' => array(
      'name' => t('M & E Campaign'),
      'base' => 'node_content',
      'description' => t('Meetings & Events offer template'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
