<?php
/**
 * @file
 * one_row_image.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function one_row_image_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-onerowimage-field_bg_colour'.
  $field_instances['node-onerowimage-field_bg_colour'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Use this option to set a background colour. You can use the HEX colour code e.g. #ffffff, the RGB colour code e.g. rgb(255,255,255), or simply enter a colour name e.g. white, black, red, green, blue.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_colour',
    'label' => '3. Enter a background colour',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 3,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 13,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_bg_content'.
  $field_instances['node-onerowimage-field_bg_content'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choose an image from the selection box below. Use the buttons to refine your choice and the horizontal scroll bar to see what is available. Click the thumbnail to see a larger version. Click the checkbox (top right of the thumbnail) to select. Alternatively, select ‘No Image’ and choose another option below.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_content',
    'label' => '1. Choose a background image',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 2,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_bg_image'.
  $field_instances['node-onerowimage-field_bg_image'] = array(
    'bundle' => 'onerowimage',
    'deleted' => 0,
    'description' => 'Use this option to upload your own background image. The target image size is 1366 pixels wide and 768 pixels high (a 16:9 aspect ratio) which will fill the available space on the slide. Larger images will be resized to fit (some cropping may occur). Please be aware that the left and right edges of the image will appear to be cropped if the slide is viewed on a screen with a narrower 4:3 aspect ratio - an iPad for example. We recommend the image file size should be between 200KB and 400KB.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_image',
    'label' => '2. Upload a background image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '10MB',
      'max_resolution' => '1366x768',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'large',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_colour_scheme'.
  $field_instances['node-onerowimage-field_colour_scheme'] = array(
    'bundle' => 'onerowimage',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_colour_scheme',
    'label' => 'Scheme',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_image'.
  $field_instances['node-onerowimage-field_image'] = array(
    'bundle' => 'onerowimage',
    'deleted' => 0,
    'description' => 'If you wish to use your own image for this slide, please upload it here. The ideal image size should be 1366 pixels wide. Please be aware that the left and right edge of the image will appear to be cropped if the slide is viewed on a screen that is NOT widescreen - an iPad for example. You can vary the height of the image to allow for more, or less text - as you require. The maximum height of the image is 600 pixels, the minimum is 100 pixels. Larger images will be resized to fit (some cropping may occur). Smaller images will be rejected. We recommend the image file size should be between 200KB and 300KB.
PLEASE NOTE: You can choose not to upload a foreground image here - and use a background image instead. Click on the thumbnail (top right of this page) to see examples the various layout styles that are possible with this template.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '10MB',
      'max_resolution' => '1366x600',
      'min_resolution' => '1366x100',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 6,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_layout_vertical'.
  $field_instances['node-onerowimage-field_layout_vertical'] = array(
    'bundle' => 'onerowimage',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 2,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_layout_vertical',
    'label' => 'Layout',
    'required' => 1,
    'settings' => array(
      'select2_integration' => array(
        'styling' => array(
          'dropdownAutoWidth' => 0,
          'placeholder' => '',
        ),
        'use_select2' => 0,
      ),
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'use_select2' => FALSE,
      ),
      'type' => 'options_select',
      'weight' => 4,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_left_content'.
  $field_instances['node-onerowimage-field_left_content'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_left_content',
    'label' => 'Content',
    'required' => 0,
    'settings' => array(
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'weight' => -1,
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 5,
      ),
      'type' => 'text_textarea',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_master_presentation'.
  $field_instances['node-onerowimage-field_master_presentation'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'hidden',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_master_presentation',
    'label' => 'Master presentation',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'custom_display_fields' => array(),
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 11,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_migrate_original_nid'.
  $field_instances['node-onerowimage-field_migrate_original_nid'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'This field shows you the original node ID before the migration',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_migrate_original_nid',
    'label' => 'Original NID',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 50,
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 12,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_slide_main_title'.
  $field_instances['node-onerowimage-field_slide_main_title'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The slide title will be displayed in the content of your slide. Leave blank for slides where a title is not required.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_slide_main_title',
    'label' => 'Slide title',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => -4,
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_tag'.
  $field_instances['node-onerowimage-field_tag'] = array(
    'bundle' => 'onerowimage',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Chapters are used to navigate quickly to slides in the presenter app. You can set a slide as a chapter marker simply by entering a name.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tag',
    'label' => 'Chapter marker',
    'required' => FALSE,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => -3,
    'widget' => array(
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-onerowimage-field_thumbnail'.
  $field_instances['node-onerowimage-field_thumbnail'] = array(
    'bundle' => 'onerowimage',
    'deleted' => 0,
    'description' => 'Leave this option blank if you would like the system to automatically generate a thumbnail for you. However, if you would like to upload your own specific thumbnail, please do so here. The image you upload should be at least 1024 pixels wide by 768 pixels high. Images larger than 1024x768 pixels will be resized. We recommend the image file size should be between 200KB and 300KB. ',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'file_entity',
        'settings' => array(
          'file_view_mode' => 'default',
        ),
        'type' => 'file_rendered',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_thumbnail',
    'label' => 'Slide thumbnail',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'jpg jpeg png gif',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '10MB',
      'max_resolution' => '1024x768',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'weight' => 1,
    'widget' => array(
      'active' => 1,
      'module' => 'file',
      'settings' => array(
        'progress_indicator' => 'throbber',
      ),
      'type' => 'file_generic',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('1. Choose a background image');
  t('2. Upload a background image');
  t('3. Enter a background colour');
  t('Chapter marker');
  t('Chapters are used to navigate quickly to slides in the presenter app. You can set a slide as a chapter marker simply by entering a name.');
  t('Choose an image from the selection box below. Use the buttons to refine your choice and the horizontal scroll bar to see what is available. Click the thumbnail to see a larger version. Click the checkbox (top right of the thumbnail) to select. Alternatively, select ‘No Image’ and choose another option below.');
  t('Content');
  t('If you wish to use your own image for this slide, please upload it here. The ideal image size should be 1366 pixels wide. Please be aware that the left and right edge of the image will appear to be cropped if the slide is viewed on a screen that is NOT widescreen - an iPad for example. You can vary the height of the image to allow for more, or less text - as you require. The maximum height of the image is 600 pixels, the minimum is 100 pixels. Larger images will be resized to fit (some cropping may occur). Smaller images will be rejected. We recommend the image file size should be between 200KB and 300KB.
PLEASE NOTE: You can choose not to upload a foreground image here - and use a background image instead. Click on the thumbnail (top right of this page) to see examples the various layout styles that are possible with this template.');
  t('Image');
  t('Layout');
  t('Leave this option blank if you would like the system to automatically generate a thumbnail for you. However, if you would like to upload your own specific thumbnail, please do so here. The image you upload should be at least 1024 pixels wide by 768 pixels high. Images larger than 1024x768 pixels will be resized. We recommend the image file size should be between 200KB and 300KB. ');
  t('Master presentation');
  t('Original NID');
  t('Scheme');
  t('Slide thumbnail');
  t('Slide title');
  t('The slide title will be displayed in the content of your slide. Leave blank for slides where a title is not required.');
  t('This field shows you the original node ID before the migration');
  t('Use this option to set a background colour. You can use the HEX colour code e.g. #ffffff, the RGB colour code e.g. rgb(255,255,255), or simply enter a colour name e.g. white, black, red, green, blue.');
  t('Use this option to upload your own background image. The target image size is 1366 pixels wide and 768 pixels high (a 16:9 aspect ratio) which will fill the available space on the slide. Larger images will be resized to fit (some cropping may occur). Please be aware that the left and right edges of the image will appear to be cropped if the slide is viewed on a screen with a narrower 4:3 aspect ratio - an iPad for example. We recommend the image file size should be between 200KB and 400KB.');

  return $field_instances;
}
