<?php
/**
 * @file
 * one_row_image.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function one_row_image_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function one_row_image_node_info() {
  $items = array(
    'onerowimage' => array(
      'name' => t('One row & image'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
