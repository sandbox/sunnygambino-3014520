<?php
/**
 * @file
 * ca_presenter_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function ca_presenter_views_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
