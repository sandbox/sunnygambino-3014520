<?php
/**
 * @file
 * footer_block.features.fe_block_boxes.inc
 */

/**
 * Implements hook_default_fe_block_boxes().
 */
function footer_block_default_fe_block_boxes() {
  $export = array();

  $fe_block_boxes = new stdClass();
  $fe_block_boxes->info = 'Provide footer information';
  $fe_block_boxes->format = 'full_html';
  $fe_block_boxes->machine_name = 'footer_block';
  $fe_block_boxes->body = '<div class="left">
©Companyapp. All rights reserved.
</div>
<div class="right">
<a href="mailto:support@companyapp.co.uk">support@companyapp.co.uk</a>
</div>';

  $export['footer_block'] = $fe_block_boxes;

  return $export;
}
