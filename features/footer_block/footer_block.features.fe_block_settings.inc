<?php
/**
 * @file
 * footer_block.features.fe_block_settings.inc
 */

/**
 * Implements hook_default_fe_block_settings().
 */
function footer_block_default_fe_block_settings() {
  $export = array();

  $export['version'] = '2.0';

  $export['block-footer_block'] = array(
    'cache' => -1,
    'custom' => 0,
    'machine_name' => 'footer_block',
    'module' => 'block',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'hidden',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'presentation_admin' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_admin',
        'weight' => 0,
      ),
      'presentation_artotel' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_artotel',
        'weight' => 0,
      ),
      'presentation_bread2' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_bread2',
        'weight' => 0,
      ),
      'presentation_companyapp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_companyapp',
        'weight' => 0,
      ),
      'presentation_companyapp3' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_companyapp3',
        'weight' => 0,
      ),
      'presentation_companyapp4' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_companyapp4',
        'weight' => 0,
      ),
      'presentation_countryinns' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_countryinns',
        'weight' => 0,
      ),
      'presentation_edwardian' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_edwardian',
        'weight' => 0,
      ),
      'presentation_keune2' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_keune2',
        'weight' => 0,
      ),
      'presentation_marriott' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott',
        'weight' => 0,
      ),
      'presentation_marriott_golf' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott_golf',
        'weight' => 0,
      ),
      'presentation_marriott_international' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott_international',
        'weight' => 0,
      ),
      'presentation_marriott_meetings' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott_meetings',
        'weight' => 0,
      ),
      'presentation_mayfair' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_mayfair',
        'weight' => 0,
      ),
      'presentation_mda' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_mda',
        'weight' => 0,
      ),
      'presentation_parkinn' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_parkinn',
        'weight' => 0,
      ),
      'presentation_parkinn_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_parkinn_2018',
        'weight' => 0,
      ),
      'presentation_parkplaza' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_parkplaza',
        'weight' => 0,
      ),
      'presentation_prizeotel' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_prizeotel',
        'weight' => 0,
      ),
      'presentation_radisson' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radisson',
        'weight' => 0,
      ),
      'presentation_radisson_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radisson_2018',
        'weight' => 0,
      ),
      'presentation_radisson_collection' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radisson_collection',
        'weight' => 0,
      ),
      'presentation_radissonblu' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonblu',
        'weight' => 0,
      ),
      'presentation_radissonbluedwardian' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonbluedwardian',
        'weight' => 0,
      ),
      'presentation_radissonred' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonred',
        'weight' => 0,
      ),
      'presentation_radissonred_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonred_2018',
        'weight' => 0,
      ),
      'presentation_rhg' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_rhg',
        'weight' => 0,
      ),
      'presentation_sats' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_sats',
        'weight' => 0,
      ),
      'presentation_schweppes' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_schweppes',
        'weight' => 0,
      ),
      'presentation_standardlife_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_standardlife_2018',
        'weight' => 0,
      ),
    ),
    'title' => '',
    'visibility' => 0,
  );

  $export['masquerade-masquerade'] = array(
    'cache' => -1,
    'custom' => 0,
    'delta' => 'masquerade',
    'module' => 'masquerade',
    'node_types' => array(),
    'pages' => '',
    'roles' => array(),
    'themes' => array(
      'adminimal' => array(
        'region' => 'hidden',
        'status' => 1,
        'theme' => 'adminimal',
        'weight' => 0,
      ),
      'bartik' => array(
        'region' => '',
        'status' => 0,
        'theme' => 'bartik',
        'weight' => 0,
      ),
      'presentation_admin' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_admin',
        'weight' => 0,
      ),
      'presentation_artotel' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_artotel',
        'weight' => 0,
      ),
      'presentation_bread2' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_bread2',
        'weight' => 0,
      ),
      'presentation_companyapp' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_companyapp',
        'weight' => 0,
      ),
      'presentation_companyapp3' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_companyapp3',
        'weight' => 0,
      ),
      'presentation_companyapp4' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_companyapp4',
        'weight' => 0,
      ),
      'presentation_countryinns' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_countryinns',
        'weight' => 0,
      ),
      'presentation_edwardian' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_edwardian',
        'weight' => 0,
      ),
      'presentation_keune2' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_keune2',
        'weight' => 0,
      ),
      'presentation_marriott' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott',
        'weight' => 0,
      ),
      'presentation_marriott_golf' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott_golf',
        'weight' => 0,
      ),
      'presentation_marriott_international' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott_international',
        'weight' => 0,
      ),
      'presentation_marriott_meetings' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_marriott_meetings',
        'weight' => 0,
      ),
      'presentation_mayfair' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_mayfair',
        'weight' => 0,
      ),
      'presentation_mda' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_mda',
        'weight' => 0,
      ),
      'presentation_parkinn' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_parkinn',
        'weight' => 0,
      ),
      'presentation_parkinn_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_parkinn_2018',
        'weight' => 0,
      ),
      'presentation_parkplaza' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_parkplaza',
        'weight' => 0,
      ),
      'presentation_prizeotel' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_prizeotel',
        'weight' => 0,
      ),
      'presentation_radisson' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radisson',
        'weight' => 0,
      ),
      'presentation_radisson_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radisson_2018',
        'weight' => 0,
      ),
      'presentation_radisson_collection' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radisson_collection',
        'weight' => 0,
      ),
      'presentation_radissonblu' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonblu',
        'weight' => 0,
      ),
      'presentation_radissonbluedwardian' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonbluedwardian',
        'weight' => 0,
      ),
      'presentation_radissonred' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonred',
        'weight' => 0,
      ),
      'presentation_radissonred_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_radissonred_2018',
        'weight' => 0,
      ),
      'presentation_rhg' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_rhg',
        'weight' => 0,
      ),
      'presentation_sats' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_sats',
        'weight' => 0,
      ),
      'presentation_schweppes' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_schweppes',
        'weight' => 0,
      ),
      'presentation_standardlife_2018' => array(
        'region' => 'footer',
        'status' => 1,
        'theme' => 'presentation_standardlife_2018',
        'weight' => 0,
      ),
    ),
    'title' => '<none>',
    'visibility' => 0,
  );

  return $export;
}
