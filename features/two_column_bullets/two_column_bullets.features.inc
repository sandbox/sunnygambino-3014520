<?php
/**
 * @file
 * two_column_bullets.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function two_column_bullets_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function two_column_bullets_node_info() {
  $items = array(
    'twocolumnbullets' => array(
      'name' => t('Two column bullets'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
