<?php
/**
 * @file
 * ca_presenter_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function ca_presenter_rules_default_rules_configuration() {
  $items = array();
  $items['rules_send_custom_email'] = entity_import('rules_config', '{ "rules_send_custom_email" : {
      "LABEL" : "Send custom email",
      "PLUGIN" : "action set",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : {
        "user" : { "label" : "User", "type" : "user" },
        "subject" : { "label" : "Subject", "type" : "text" },
        "body" : { "label" : "Body", "type" : "text" }
      },
      "ACTION SET" : [
        { "mail" : {
            "to" : [ "user:mail" ],
            "subject" : [ "subject" ],
            "message" : [ "body" ],
            "from" : [ "site:mail" ],
            "language" : [ "user:language" ]
          }
        }
      ]
    }
  }');
  return $items;
}
