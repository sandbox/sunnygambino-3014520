<?php
/**
 * @file
 * two_column.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function two_column_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|twocolumn|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twocolumn';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '6',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => 'Option 1. Choose a background image.<br />
Option 2. Enter a background colour. Please note, in order to use a background colour, you need to select "No Image" in Option 1. ',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|twocolumn|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_colour_scheme|node|twocolumn|form';
  $field_group->group_name = 'group_colour_scheme';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twocolumn';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Colour scheme',
    'weight' => '5',
    'children' => array(
      0 => 'field_colour_scheme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Colour scheme',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-colour-scheme field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_colour_scheme|node|twocolumn|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_font_colour|node|twocolumn|form';
  $field_group->group_name = 'group_font_colour';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twocolumn';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Font Colour Scheme',
    'weight' => '3',
    'children' => array(
      0 => 'field_colour_scheme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Font Colour Scheme',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-font-colour field-group-fieldset',
        'description' => '<br />
This option allows you to change the default font colour for this template. This can be useful if the default font colour does not work well over the background you have chosen. If the default is a dark coloured font - the reverse will be a light coloured font - and vice versa.',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_font_colour|node|twocolumn|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|twocolumn|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twocolumn';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '8',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|twocolumn|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|twocolumn|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'twocolumn';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '7',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|twocolumn|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Colour scheme');
  t('Font Colour Scheme');
  t('Node information');
  t('Select background');
  t('Thumbnail');

  return $field_groups;
}
