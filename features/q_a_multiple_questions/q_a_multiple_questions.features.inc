<?php
/**
 * @file
 * q_a_multiple_questions.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function q_a_multiple_questions_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function q_a_multiple_questions_node_info() {
  $items = array(
    'qamultiple' => array(
      'name' => t('Q&A - Multiple Questions'),
      'base' => 'node_content',
      'description' => t('A template for asking multiple questions with one correct answer'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
