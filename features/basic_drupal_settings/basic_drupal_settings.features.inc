<?php
/**
 * @file
 * basic_drupal_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function basic_drupal_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}
