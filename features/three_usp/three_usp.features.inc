<?php
/**
 * @file
 * three_usp.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function three_usp_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function three_usp_node_info() {
  $items = array(
    'usp' => array(
      'name' => t('Three USP'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
