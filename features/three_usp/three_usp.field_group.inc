<?php
/**
 * @file
 * three_usp.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function three_usp_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_animation|node|usp|form';
  $field_group->group_name = 'group_animation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Animation',
    'weight' => '7',
    'children' => array(
      0 => 'field_animation_speed',
      1 => 'field_animation_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Animation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-animation field-group-fieldset',
        'description' => 'Please select an animation type and speed (milliseconds).',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_animation|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|usp|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '8',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_image',
      2 => 'field_bg_usp',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => '<br />
<strong>Option 1.</strong> Choose a background image.<br />
<strong>Option 2.</strong> Upload a background image. Please note, in order to use your own image, you need to select "No Image" in option 1.<br />
<strong>Option 3.</strong> Enter a background colour. The colour will be applied behind any background image. You must select "No Image" in option 1.<br />
Please note: certain background / content combinations may not work as well as others. Please preview your choice to check readability of text etc.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_font_colour|node|usp|form';
  $field_group->group_name = 'group_font_colour';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Font Colour Scheme',
    'weight' => '2',
    'children' => array(
      0 => 'field_colour_scheme',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Font Colour Scheme',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-font-colour field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_font_colour|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|usp|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Information',
    'weight' => '10',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|usp|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '9',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_usp_one|node|usp|form';
  $field_group->group_name = 'group_usp_one';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'USP one',
    'weight' => '2',
    'children' => array(
      0 => 'field_left_image',
      1 => 'field_usp_one',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-usp-one field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_usp_one|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_usp_three|node|usp|form';
  $field_group->group_name = 'group_usp_three';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'USP three',
    'weight' => '4',
    'children' => array(
      0 => 'field_right_image',
      1 => 'field_usp_three',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-usp-three field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_usp_three|node|usp|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_usp_two|node|usp|form';
  $field_group->group_name = 'group_usp_two';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'usp';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'USP two',
    'weight' => '3',
    'children' => array(
      0 => 'field_middle_image',
      1 => 'field_usp_two',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-usp-two field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_usp_two|node|usp|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Animation');
  t('Font Colour Scheme');
  t('Node Information');
  t('Select background');
  t('Thumbnail');
  t('USP one');
  t('USP three');
  t('USP two');

  return $field_groups;
}
