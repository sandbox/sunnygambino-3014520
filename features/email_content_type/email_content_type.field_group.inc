<?php
/**
 * @file
 * email_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function email_content_type_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cc_group|node|email|form';
  $field_group->group_name = 'group_cc_group';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Additional recipients',
    'weight' => '4',
    'children' => array(
      0 => 'field_bcc_email',
      1 => 'field_cc_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Additional recipients',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'additional-recipients',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_cc_group|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_email_attachments|node|email|form';
  $field_group->group_name = 'group_email_attachments';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Attachments',
    'weight' => '6',
    'children' => array(
      0 => 'field_email_documents',
      1 => 'field_email_presentations',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Attachments',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'attachments',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_email_attachments|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_email_details|node|email|form';
  $field_group->group_name = 'group_email_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Email details',
    'weight' => '5',
    'children' => array(
      0 => 'field_email_message',
      1 => 'field_email_subject',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Email details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'email-details',
        'description' => '',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_email_details|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_email_settings|node|email|form';
  $field_group->group_name = 'group_email_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '9',
    'children' => array(
      0 => 'field_email_note',
      1 => 'field_email_notifications',
      2 => 'field_email_test',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Settings',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'settings',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_email_settings|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_email_sign_off|node|email|form';
  $field_group->group_name = 'group_email_sign_off';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Email sign off',
    'weight' => '7',
    'children' => array(
      0 => 'field_email_signature',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-email-sign-off field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_email_sign_off|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|email|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '14',
    'children' => array(
      0 => 'field_migrate_original_nid',
      1 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_recipient_details|node|email|form';
  $field_group->group_name = 'group_recipient_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Recipient details',
    'weight' => '2',
    'children' => array(
      0 => 'field_recipients_email',
      1 => 'field_recipients_name',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Recipient details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'recipient-details',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_recipient_details|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|node|email|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '8',
    'children' => array(
      0 => 'field_email_note',
      1 => 'field_email_notifications',
      2 => 'field_email_test',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Settings',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'settings',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_settings|node|email|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_your_details|node|email|form';
  $field_group->group_name = 'group_your_details';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'email';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Your details',
    'weight' => '3',
    'children' => array(
      0 => 'field_from_name',
      1 => 'field_reply_email',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Your details',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'your-details',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_your_details|node|email|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Additional recipients');
  t('Attachments');
  t('Email details');
  t('Email sign off');
  t('Node information');
  t('Recipient details');
  t('Settings');
  t('Your details');

  return $field_groups;
}
