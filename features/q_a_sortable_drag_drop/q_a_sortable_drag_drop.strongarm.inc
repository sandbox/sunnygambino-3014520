<?php
/**
 * @file
 * q_a_sortable_drag_drop.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function q_a_sortable_drag_drop_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'field_bundle_settings_node__qasortabledrop';
  $strongarm->value = array(
    'view_modes' => array(),
    'extra_fields' => array(
      'form' => array(
        'body' => array(
          'weight' => 0,
        ),
        'title' => array(
          'weight' => '0',
        ),
        'path' => array(
          'weight' => '9',
        ),
      ),
      'display' => array(),
    ),
  );
  $export['field_bundle_settings_node__qasortabledrop'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_node_qasortabledrop_pattern';
  $strongarm->value = '';
  $export['pathauto_node_qasortabledrop_pattern'] = $strongarm;

  return $export;
}
