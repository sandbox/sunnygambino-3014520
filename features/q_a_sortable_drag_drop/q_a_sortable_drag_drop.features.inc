<?php
/**
 * @file
 * q_a_sortable_drag_drop.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function q_a_sortable_drag_drop_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function q_a_sortable_drag_drop_node_info() {
  $items = array(
    'qasortabledrop' => array(
      'name' => t('Q&A - Sortable Drag & Drop'),
      'base' => 'node_content',
      'description' => t('Create a list of answers that needs to be dragged and dropped in order'),
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
