<?php
/**
 * @file
 * migration_settings.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function migration_settings_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "migrate" && $api == "migrate") {
    return array("version" => "1");
  }
}
