<?php
/**
 * @file
 * ca_presenter_main_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function ca_presenter_main_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu_categories:categories.
  $menu_links['main-menu_categories:categories'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'categories',
    'router_path' => 'categories',
    'link_title' => 'Categories',
    'options' => array(
      'identifier' => 'main-menu_categories:categories',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: main-menu_documents:documents.
  $menu_links['main-menu_documents:documents'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'documents',
    'router_path' => 'documents',
    'link_title' => 'Documents',
    'options' => array(
      'identifier' => 'main-menu_documents:documents',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: main-menu_emails:emails.
  $menu_links['main-menu_emails:emails'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'emails',
    'router_path' => 'emails',
    'link_title' => 'Emails',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'emails',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_emails:emails',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: main-menu_messages:messages.
  $menu_links['main-menu_messages:messages'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'messages',
    'router_path' => 'messages',
    'link_title' => 'Messages',
    'options' => array(
      'identifier' => 'main-menu_messages:messages',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: main-menu_presentations:presentations.
  $menu_links['main-menu_presentations:presentations'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'presentations',
    'router_path' => 'presentations',
    'link_title' => 'Presentations',
    'options' => array(
      'identifier' => 'main-menu_presentations:presentations',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 1,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: main-menu_reports:reports.
  $menu_links['main-menu_reports:reports'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'reports',
    'router_path' => 'reports',
    'link_title' => 'Reports',
    'options' => array(
      'attributes' => array(
        'style' => 'reports',
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'main-menu_reports:reports',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
  );
  // Exported menu link: main-menu_users:users-admin.
  $menu_links['main-menu_users:users-admin'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'users-admin',
    'router_path' => 'users-admin',
    'link_title' => 'Users',
    'options' => array(
      'identifier' => 'main-menu_users:users-admin',
    ),
    'module' => 'system',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: user-menu_my-account:myaccount.
  $menu_links['user-menu_my-account:myaccount'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'myaccount',
    'router_path' => 'myaccount',
    'link_title' => 'My account',
    'options' => array(
      'attributes' => array(
        'class' => array(
          0 => 'account',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'user-menu_my-account:myaccount',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: user-menu_payers:users-admin/payers.
  $menu_links['user-menu_payers:users-admin/payers'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'users-admin/payers',
    'router_path' => 'users-admin/payers',
    'link_title' => 'Payers',
    'options' => array(
      'attributes' => array(),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
      'identifier' => 'user-menu_payers:users-admin/payers',
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: user-menu_user-account:user.
  $menu_links['user-menu_user-account:user'] = array(
    'menu_name' => 'user-menu',
    'link_path' => 'user',
    'router_path' => 'user',
    'link_title' => 'User account',
    'options' => array(
      'alter' => TRUE,
      'identifier' => 'user-menu_user-account:user',
    ),
    'module' => 'system',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Categories');
  t('Documents');
  t('Emails');
  t('Messages');
  t('My account');
  t('Payers');
  t('Presentations');
  t('Reports');
  t('User account');
  t('Users');

  return $menu_links;
}
