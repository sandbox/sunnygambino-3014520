<?php
/**
 * @file
 * section_title.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function section_title_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function section_title_node_info() {
  $items = array(
    'sectiontitle' => array(
      'name' => t('Section title'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Slide name (for system reference only)'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
