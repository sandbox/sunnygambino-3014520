<?php
/**
 * @file
 * presentation_and_client_logo.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function presentation_and_client_logo_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_bg_colour'.
  $field_instances['node-presentation_title_client_logo-field_bg_colour'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Use this option to set a background colour. You can use the HEX colour code e.g. #ffffff, the RGB colour code e.g. rgb(255,255,255), or simply enter a colour name e.g. white, black, red, green, blue. ',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 6,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_colour',
    'label' => '3. Enter a background colour',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_bg_image'.
  $field_instances['node-presentation_title_client_logo-field_bg_image'] = array(
    'bundle' => 'presentation_title_client_logo',
    'deleted' => 0,
    'description' => 'Use this option to upload your own background image. The recommended image size is 1366 pixels wide and 768 pixels high. Please note: at this ‘widescreen’ aspect ratio - you will only see the full width of the image when the viewer is in ‘fullscreen’ mode, not when the viewer is in ‘standard’ mode.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 5,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_image',
    'label' => '2. Upload a background image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '20 MB',
      'max_resolution' => '1366x768',
      'min_resolution' => '',
      'resup' => 0,
      'resup_autostart' => 0,
      'resup_max_filesize' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'manualcrop_crop_info' => TRUE,
        'manualcrop_default_crop_area' => TRUE,
        'manualcrop_enable' => FALSE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => TRUE,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => FALSE,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 6,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_bg_section'.
  $field_instances['node-presentation_title_client_logo-field_bg_section'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Choose an image from the selection box below. Use the buttons to refine your choice and the horizontal scroll bar to see what is available. Click the thumbnail to see a larger version. Click the checkbox (top right of the thumbnail) to select. Alternatively, select ‘No Image’ and choose another option below.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_bg_section',
    'label' => '1. Choose a background image',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_colour_scheme'.
  $field_instances['node-presentation_title_client_logo-field_colour_scheme'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 7,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_colour_scheme',
    'label' => 'Font colour',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_editable_title'.
  $field_instances['node-presentation_title_client_logo-field_editable_title'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => array(
      0 => array(
        'value' => 0,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'list',
        'settings' => array(),
        'type' => 'list_default',
        'weight' => 8,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_editable_title',
    'label' => 'Editable text checkbox',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(
        'display_label' => 0,
      ),
      'type' => 'options_onoff',
      'weight' => 3,
    ),
  );

  // Exported field_instance: 'node-presentation_title_client_logo-field_image'.
  $field_instances['node-presentation_title_client_logo-field_image'] = array(
    'bundle' => 'presentation_title_client_logo',
    'deleted' => 0,
    'description' => 'Please upload a client logo for this slide. Larger images will be resized to fit the space available (some cropping may occur). We recommend the image file size should be between 100KB and 200KB.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 12,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_image',
    'label' => 'Upload client logo',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'png gif jpg jpeg',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '5MB',
      'max_resolution' => '400x400',
      'min_resolution' => '',
      'resup' => 0,
      'resup_autostart' => 0,
      'resup_max_filesize' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'manualcrop_crop_info' => TRUE,
        'manualcrop_default_crop_area' => TRUE,
        'manualcrop_enable' => FALSE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => TRUE,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => FALSE,
        'preview_image_style' => 'medium',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 7,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_master_presentation'.
  $field_instances['node-presentation_title_client_logo-field_master_presentation'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'entityreference',
        'settings' => array(
          'bypass_access' => FALSE,
          'link' => FALSE,
        ),
        'type' => 'entityreference_label',
        'weight' => 1,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_master_presentation',
    'label' => 'Master presentation',
    'required' => 0,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'entityreference',
      'settings' => array(
        'match_operator' => 'CONTAINS',
        'path' => '',
        'size' => 60,
      ),
      'type' => 'entityreference_autocomplete',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_migrate_original_nid'.
  $field_instances['node-presentation_title_client_logo-field_migrate_original_nid'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_migrate_original_nid',
    'label' => 'Original NID',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_slide_main_title'.
  $field_instances['node-presentation_title_client_logo-field_slide_main_title'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The slide title will be displayed in the content of your slide. Leave blank for slides where a title is not required.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 11,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_slide_main_title',
    'label' => 'Slide title',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 1,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_subtitle'.
  $field_instances['node-presentation_title_client_logo-field_subtitle'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The sub title will be displayed in the content of your slide. Usually in a smaller font than the title. Leave this field blank for slides where a sub title is not required.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 10,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_subtitle',
    'label' => 'Subtitle',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-presentation_title_client_logo-field_tag'.
  $field_instances['node-presentation_title_client_logo-field_tag'] = array(
    'bundle' => 'presentation_title_client_logo',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'Chapters are used to navigate quickly to slides in the presenter app. You can set a  slide as a chapter marker simply by entering a name.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 9,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_tag',
    'label' => 'Chapter marker',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'node-presentation_title_client_logo-field_thumbnail'.
  $field_instances['node-presentation_title_client_logo-field_thumbnail'] = array(
    'bundle' => 'presentation_title_client_logo',
    'deleted' => 0,
    'description' => 'Leave this option blank if you would like the system to automatically generate a thumbnail for you. However, if you would like to upload your own specific thumbnail, please do so here. The image you upload should be at least 1024 pixels wide by 768 pixels high. Images larger than 1024x768 pixels will be resized. We recommend the image file size should be between 200KB and 300KB. ',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 3,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_thumbnail',
    'label' => 'Slide thumbnail',
    'required' => 0,
    'settings' => array(
      'alt_field' => 0,
      'default_image' => 0,
      'file_directory' => '',
      'file_extensions' => 'jpg jpeg png gif',
      'filefield_paths' => array(
        'active_updating' => 0,
        'file_name' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '[file:ffp-name-only-original].[file:ffp-extension-original]',
        ),
        'file_path' => array(
          'options' => array(
            'pathauto' => 0,
            'slashes' => 0,
            'transliterate' => 0,
          ),
          'value' => '',
        ),
        'redirect' => FALSE,
        'retroactive_update' => 0,
      ),
      'filefield_paths_enabled' => 1,
      'max_filesize' => '10MB',
      'max_resolution' => '1024x768',
      'min_resolution' => '',
      'resup' => 0,
      'resup_autostart' => 0,
      'resup_max_filesize' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'manualcrop_crop_info' => TRUE,
        'manualcrop_default_crop_area' => TRUE,
        'manualcrop_enable' => FALSE,
        'manualcrop_inline_crop' => FALSE,
        'manualcrop_instant_crop' => FALSE,
        'manualcrop_instant_preview' => TRUE,
        'manualcrop_keyboard' => TRUE,
        'manualcrop_maximize_default_crop_area' => FALSE,
        'manualcrop_require_cropping' => array(),
        'manualcrop_styles_list' => array(),
        'manualcrop_styles_mode' => 'include',
        'manualcrop_thumblist' => FALSE,
        'preview_image_style' => 'thumbnail',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 4,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('1. Choose a background image');
  t('2. Upload a background image');
  t('3. Enter a background colour');
  t('Chapter marker');
  t('Chapters are used to navigate quickly to slides in the presenter app. You can set a  slide as a chapter marker simply by entering a name.');
  t('Choose an image from the selection box below. Use the buttons to refine your choice and the horizontal scroll bar to see what is available. Click the thumbnail to see a larger version. Click the checkbox (top right of the thumbnail) to select. Alternatively, select ‘No Image’ and choose another option below.');
  t('Editable text checkbox');
  t('Font colour');
  t('Leave this option blank if you would like the system to automatically generate a thumbnail for you. However, if you would like to upload your own specific thumbnail, please do so here. The image you upload should be at least 1024 pixels wide by 768 pixels high. Images larger than 1024x768 pixels will be resized. We recommend the image file size should be between 200KB and 300KB. ');
  t('Master presentation');
  t('Original NID');
  t('Please upload a client logo for this slide. Larger images will be resized to fit the space available (some cropping may occur). We recommend the image file size should be between 100KB and 200KB.');
  t('Slide thumbnail');
  t('Slide title');
  t('Subtitle');
  t('The slide title will be displayed in the content of your slide. Leave blank for slides where a title is not required.');
  t('The sub title will be displayed in the content of your slide. Usually in a smaller font than the title. Leave this field blank for slides where a sub title is not required.');
  t('Upload client logo');
  t('Use this option to set a background colour. You can use the HEX colour code e.g. #ffffff, the RGB colour code e.g. rgb(255,255,255), or simply enter a colour name e.g. white, black, red, green, blue. ');
  t('Use this option to upload your own background image. The recommended image size is 1366 pixels wide and 768 pixels high. Please note: at this ‘widescreen’ aspect ratio - you will only see the full width of the image when the viewer is in ‘fullscreen’ mode, not when the viewer is in ‘standard’ mode.');

  return $field_instances;
}
