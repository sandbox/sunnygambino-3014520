<?php
/**
 * @file
 * email_template.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function email_template_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function email_template_node_info() {
  $items = array(
    'email_template' => array(
      'name' => t('Email template'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
