<?php
/**
 * @file
 * payer.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function payer_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function payer_node_info() {
  $items = array(
    'payer' => array(
      'name' => t('Payer'),
      'base' => 'node_content',
      'description' => t('This content store who responsible payer for a group'),
      'has_title' => '1',
      'title_label' => t('Contact name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
