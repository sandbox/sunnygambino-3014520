<?php
/**
 * @file
 * taxonomies.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function taxonomies_taxonomy_default_vocabularies() {
  return array(
    'content_types' => array(
      'name' => 'Content types',
      'machine_name' => 'content_types',
      'description' => 'Add categories to content types',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
    'document_category' => array(
      'name' => 'Document category',
      'machine_name' => 'document_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => '',
      'weight' => -9,
    ),
    'presentation_category' => array(
      'name' => 'Presentation category',
      'machine_name' => 'presentation_category',
      'description' => '',
      'hierarchy' => 1,
      'module' => '',
      'weight' => -10,
    ),
    'primary_statuses' => array(
      'name' => 'Primary statuses',
      'machine_name' => 'primary_statuses',
      'description' => NULL,
      'hierarchy' => 0,
      'module' => '',
      'weight' => 0,
    ),
    'secondary_statuses' => array(
      'name' => 'Secondary statuses',
      'machine_name' => 'secondary_statuses',
      'description' => NULL,
      'hierarchy' => 0,
      'module' => '',
      'weight' => 0,
    ),
  );
}
