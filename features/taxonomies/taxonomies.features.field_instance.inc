<?php
/**
 * @file
 * taxonomies.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function taxonomies_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-content_types-field_taxonomy_class'.
  $field_instances['taxonomy_term-content_types-field_taxonomy_class'] = array(
    'bundle' => 'content_types',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_taxonomy_class',
    'label' => 'Class',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-content_types-field_taxonomy_content_type'.
  $field_instances['taxonomy_term-content_types-field_taxonomy_content_type'] = array(
    'bundle' => 'content_types',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The content type\'s name what stored in the database. Known as machine name.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'entityreference_rendered_widget' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_taxonomy_content_type',
    'label' => 'Content type machine name',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 5,
    ),
  );

  // Exported field_instance: 'taxonomy_term-content_types-field_taxonomy_icon'.
  $field_instances['taxonomy_term-content_types-field_taxonomy_icon'] = array(
    'bundle' => 'content_types',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'HTML for the icon. Uses font-awesome. Example: <code>fa fa-check</code>',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_taxonomy_icon',
    'label' => 'Icon',
    'required' => 0,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 60,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Class');
  t('Content type machine name');
  t('HTML for the icon. Uses font-awesome. Example: <code>fa fa-check</code>');
  t('Icon');
  t('The content type\'s name what stored in the database. Known as machine name.');

  return $field_instances;
}
