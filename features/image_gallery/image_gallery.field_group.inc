<?php
/**
 * @file
 * image_gallery.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function image_gallery_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|imagegallery|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'imagegallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '4',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => '<br />
<strong>Option 1.</strong> Choose a background image.<br />
<strong>Option 2.</strong> Upload a background image. Please note, in order to use your own image, you need to select "No Image" in option 1.<br />
Please note: certain background / content combinations may not work as well as others. Please preview your choice to check readability of text etc.',
      ),
      'formatter' => 'collapsible',
    ),
  );
  $field_groups['group_background|node|imagegallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|imagegallery|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'imagegallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Information	',
    'weight' => '6',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'field_resources',
      3 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node Information	',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|imagegallery|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|imagegallery|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'imagegallery';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '5',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|imagegallery|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Node Information	');
  t('Select background');
  t('Thumbnail');

  return $field_groups;
}
