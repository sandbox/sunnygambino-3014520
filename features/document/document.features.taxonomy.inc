<?php
/**
 * @file
 * document.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function document_taxonomy_default_vocabularies() {
  return array(
    'document_category' => array(
      'name' => 'Document category',
      'machine_name' => 'document_category',
      'description' => '',
      'hierarchy' => 0,
      'module' => '',
      'weight' => -9,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'primary_statuses' => array(
      'name' => 'Primary statuses',
      'machine_name' => 'primary_statuses',
      'description' => NULL,
      'hierarchy' => 0,
      'module' => '',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
    'secondary_statuses' => array(
      'name' => 'Secondary statuses',
      'machine_name' => 'secondary_statuses',
      'description' => NULL,
      'hierarchy' => 0,
      'module' => '',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
