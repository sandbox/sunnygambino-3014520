<?php
/**
 * @file
 * crm_contacts.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function crm_contacts_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function crm_contacts_node_info() {
  $items = array(
    'crm_contacts' => array(
      'name' => t('CRM contacts'),
      'base' => 'node_content',
      'description' => t('Contacts from 3rd party CRM systems like Salesforce.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
