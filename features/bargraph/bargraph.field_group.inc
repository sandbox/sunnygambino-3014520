<?php
/**
 * @file
 * bargraph.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function bargraph_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|bargraph|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '7',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-background field-group-fieldset',
        'description' => 'Option 1. Choose a background image.<br />
Option 2. Enter a background colour. Please note, in order to use a background colour, you need to select "No Image" in Option 1.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|bargraph|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_chart_data|node|bargraph|form';
  $field_group->group_name = 'group_chart_data';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Bar data',
    'weight' => '6',
    'children' => array(
      0 => 'field_chart_data',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Bar data',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-chart-data field-group-fieldset',
        'description' => 'Using the following fields you can add bar data to dynamically generate a bar graph.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_chart_data|node|bargraph|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_chart_x_axis|node|bargraph|form';
  $field_group->group_name = 'group_chart_x_axis';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Chart x-axis',
    'weight' => '3',
    'children' => array(
      0 => 'field_chart_x_axis_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-chart-x-axis field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_chart_x_axis|node|bargraph|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_chart_y_axis|node|bargraph|form';
  $field_group->group_name = 'group_chart_y_axis';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Chart y-axis',
    'weight' => '4',
    'children' => array(
      0 => 'field_chart_y_axis_title',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-chart-y-axis field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_chart_y_axis|node|bargraph|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|bargraph|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '9',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|bargraph|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = TRUE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_settings|node|bargraph|form';
  $field_group->group_name = 'group_settings';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Settings',
    'weight' => '6',
    'children' => array(
      0 => 'field_chart_value_suffix',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-settings field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_settings|node|bargraph|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|bargraph|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'bargraph';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '8',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-thumbnail field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|bargraph|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Bar data');
  t('Chart x-axis');
  t('Chart y-axis');
  t('Node information');
  t('Select background');
  t('Settings');
  t('Thumbnail');

  return $field_groups;
}
