<?php
/**
 * @file
 * device_details_report.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function device_details_report_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'device_report';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Device report';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Device report';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['access']['perm'] = 'view device_details report';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['reset_button'] = TRUE;
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'input_required' => 0,
      'text_input_required' => array(
        'text_input_required' => array(
          'value' => 'Select any filter and click on Apply to see results',
          'format' => 'root_user',
        ),
      ),
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'secondary_collapse_override' => '0',
    ),
    'name' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
    'rid' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'autosubmit' => 0,
        'is_secondary' => 0,
        'any_label' => '- Any Role -',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
          ),
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
        ),
      ),
    ),
  );
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'created' => 'created',
    'field_secondary_name' => 'field_secondary_name',
    'field_device_details' => 'field_device_details',
    'name' => 'name',
  );
  $handler->display->display_options['style_options']['default'] = 'created';
  $handler->display->display_options['style_options']['info'] = array(
    'created' => array(
      'sortable' => 1,
      'default_sort_order' => 'desc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_secondary_name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'field_device_details' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'name' => array(
      'sortable' => 1,
      'default_sort_order' => 'asc',
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Relationship: Content: Author */
  $handler->display->display_options['relationships']['uid']['id'] = 'uid';
  $handler->display->display_options['relationships']['uid']['table'] = 'node';
  $handler->display->display_options['relationships']['uid']['field'] = 'uid';
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['date_format'] = 'long';
  $handler->display->display_options['fields']['created']['second_date_format'] = 'long';
  /* Field: Content: App version */
  $handler->display->display_options['fields']['field_app_version']['id'] = 'field_app_version';
  $handler->display->display_options['fields']['field_app_version']['table'] = 'field_data_field_app_version';
  $handler->display->display_options['fields']['field_app_version']['field'] = 'field_app_version';
  /* Field: Content: Model */
  $handler->display->display_options['fields']['field_device_model']['id'] = 'field_device_model';
  $handler->display->display_options['fields']['field_device_model']['table'] = 'field_data_field_device_model';
  $handler->display->display_options['fields']['field_device_model']['field'] = 'field_device_model';
  /* Field: Content: Platform */
  $handler->display->display_options['fields']['field_app_platform']['id'] = 'field_app_platform';
  $handler->display->display_options['fields']['field_app_platform']['table'] = 'field_data_field_app_platform';
  $handler->display->display_options['fields']['field_app_platform']['field'] = 'field_app_platform';
  /* Field: Content: Device details */
  $handler->display->display_options['fields']['field_device_details']['id'] = 'field_device_details';
  $handler->display->display_options['fields']['field_device_details']['table'] = 'field_data_field_device_details';
  $handler->display->display_options['fields']['field_device_details']['field'] = 'field_device_details';
  $handler->display->display_options['fields']['field_device_details']['empty'] = 'Unknown';
  /* Field: User: Name */
  $handler->display->display_options['fields']['name']['id'] = 'name';
  $handler->display->display_options['fields']['name']['table'] = 'users';
  $handler->display->display_options['fields']['name']['field'] = 'name';
  $handler->display->display_options['fields']['name']['relationship'] = 'uid';
  $handler->display->display_options['fields']['name']['label'] = 'Username';
  /* Field: Content: Secondary name */
  $handler->display->display_options['fields']['field_secondary_name']['id'] = 'field_secondary_name';
  $handler->display->display_options['fields']['field_secondary_name']['table'] = 'field_data_field_secondary_name';
  $handler->display->display_options['fields']['field_secondary_name']['field'] = 'field_secondary_name';
  $handler->display->display_options['fields']['field_secondary_name']['label'] = 'Event';
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'event' => 'event',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Content: App version (field_app_version) */
  $handler->display->display_options['filters']['field_app_version_value']['id'] = 'field_app_version_value';
  $handler->display->display_options['filters']['field_app_version_value']['table'] = 'field_data_field_app_version';
  $handler->display->display_options['filters']['field_app_version_value']['field'] = 'field_app_version_value';
  $handler->display->display_options['filters']['field_app_version_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_app_version_value']['group'] = 1;
  $handler->display->display_options['filters']['field_app_version_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_app_version_value']['expose']['operator_id'] = 'field_app_version_value_op';
  $handler->display->display_options['filters']['field_app_version_value']['expose']['label'] = 'App version';
  $handler->display->display_options['filters']['field_app_version_value']['expose']['operator'] = 'field_app_version_value_op';
  $handler->display->display_options['filters']['field_app_version_value']['expose']['identifier'] = 'field_app_version_value';
  $handler->display->display_options['filters']['field_app_version_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    8 => 0,
    10 => 0,
    12 => 0,
    9 => 0,
    11 => 0,
    7 => 0,
    13 => 0,
  );
  /* Filter criterion: Content: Model (field_device_model) */
  $handler->display->display_options['filters']['field_device_model_value']['id'] = 'field_device_model_value';
  $handler->display->display_options['filters']['field_device_model_value']['table'] = 'field_data_field_device_model';
  $handler->display->display_options['filters']['field_device_model_value']['field'] = 'field_device_model_value';
  $handler->display->display_options['filters']['field_device_model_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_device_model_value']['group'] = 1;
  $handler->display->display_options['filters']['field_device_model_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_device_model_value']['expose']['operator_id'] = 'field_device_model_value_op';
  $handler->display->display_options['filters']['field_device_model_value']['expose']['label'] = 'Model';
  $handler->display->display_options['filters']['field_device_model_value']['expose']['operator'] = 'field_device_model_value_op';
  $handler->display->display_options['filters']['field_device_model_value']['expose']['identifier'] = 'field_device_model_value';
  $handler->display->display_options['filters']['field_device_model_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    8 => 0,
    10 => 0,
    12 => 0,
    9 => 0,
    11 => 0,
    7 => 0,
    13 => 0,
  );
  /* Filter criterion: Content: Platform (field_app_platform) */
  $handler->display->display_options['filters']['field_app_platform_value']['id'] = 'field_app_platform_value';
  $handler->display->display_options['filters']['field_app_platform_value']['table'] = 'field_data_field_app_platform';
  $handler->display->display_options['filters']['field_app_platform_value']['field'] = 'field_app_platform_value';
  $handler->display->display_options['filters']['field_app_platform_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_app_platform_value']['group'] = 1;
  $handler->display->display_options['filters']['field_app_platform_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_app_platform_value']['expose']['operator_id'] = 'field_app_platform_value_op';
  $handler->display->display_options['filters']['field_app_platform_value']['expose']['label'] = 'Platform';
  $handler->display->display_options['filters']['field_app_platform_value']['expose']['operator'] = 'field_app_platform_value_op';
  $handler->display->display_options['filters']['field_app_platform_value']['expose']['identifier'] = 'field_app_platform_value';
  $handler->display->display_options['filters']['field_app_platform_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    8 => 0,
    10 => 0,
    12 => 0,
    9 => 0,
    11 => 0,
    7 => 0,
    13 => 0,
  );
  /* Filter criterion: User: Name */
  $handler->display->display_options['filters']['uid']['id'] = 'uid';
  $handler->display->display_options['filters']['uid']['table'] = 'users';
  $handler->display->display_options['filters']['uid']['field'] = 'uid';
  $handler->display->display_options['filters']['uid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['uid']['value'] = '';
  $handler->display->display_options['filters']['uid']['group'] = 1;
  $handler->display->display_options['filters']['uid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['operator_id'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['label'] = 'Username';
  $handler->display->display_options['filters']['uid']['expose']['operator'] = 'uid_op';
  $handler->display->display_options['filters']['uid']['expose']['identifier'] = 'uid';
  $handler->display->display_options['filters']['uid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['uid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    4 => 0,
  );
  /* Filter criterion: User: Roles */
  $handler->display->display_options['filters']['rid']['id'] = 'rid';
  $handler->display->display_options['filters']['rid']['table'] = 'users_roles';
  $handler->display->display_options['filters']['rid']['field'] = 'rid';
  $handler->display->display_options['filters']['rid']['relationship'] = 'uid';
  $handler->display->display_options['filters']['rid']['group'] = 1;
  $handler->display->display_options['filters']['rid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['rid']['expose']['operator_id'] = 'rid_op';
  $handler->display->display_options['filters']['rid']['expose']['operator'] = 'rid_op';
  $handler->display->display_options['filters']['rid']['expose']['identifier'] = 'rid';
  $handler->display->display_options['filters']['rid']['expose']['remember'] = TRUE;
  $handler->display->display_options['filters']['rid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    5 => 0,
    6 => 0,
    4 => 0,
  );

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['path'] = 'device-report';
  $export['device_report'] = $view;

  return $export;
}
