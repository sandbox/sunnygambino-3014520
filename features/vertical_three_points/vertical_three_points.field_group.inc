<?php
/**
 * @file
 * vertical_three_points.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function vertical_three_points_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_animation|node|verticalthreepoint|default';
  $field_group->group_name = 'group_animation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Animation',
    'weight' => '1',
    'children' => array(
      0 => 'field_animation_speed',
      1 => 'field_animation_type',
      2 => 'field_animation_speed',
      3 => 'field_animation_type',
      4 => 'field_animation_speed',
      5 => 'field_animation_type',
      6 => 'field_animation_speed',
      7 => 'field_animation_type',
      8 => 'field_animation_speed',
      9 => 'field_animation_type',
      10 => 'field_animation_speed',
      11 => 'field_animation_type',
      12 => 'field_animation_speed',
      13 => 'field_animation_type',
      14 => 'field_animation_speed',
      15 => 'field_animation_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_animation|node|verticalthreepoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_animation|node|verticalthreepoint|form';
  $field_group->group_name = 'group_animation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Animation',
    'weight' => '6',
    'children' => array(
      0 => 'field_animation_speed',
      1 => 'field_animation_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Animation',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Please select an animation type and speed (milliseconds).',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_animation|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_animation|node|verticalthreepoint|teaser';
  $field_group->group_name = 'group_animation';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Animation',
    'weight' => '1',
    'children' => array(
      0 => 'field_animation_speed',
      1 => 'field_animation_type',
      2 => 'field_animation_speed',
      3 => 'field_animation_type',
      4 => 'field_animation_speed',
      5 => 'field_animation_type',
      6 => 'field_animation_speed',
      7 => 'field_animation_type',
      8 => 'field_animation_speed',
      9 => 'field_animation_type',
      10 => 'field_animation_speed',
      11 => 'field_animation_type',
      12 => 'field_animation_speed',
      13 => 'field_animation_type',
      14 => 'field_animation_speed',
      15 => 'field_animation_type',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_animation|node|verticalthreepoint|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|verticalthreepoint|default';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '2',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
      2 => 'field_bg_colour',
      3 => 'field_bg_content',
      4 => 'field_bg_colour',
      5 => 'field_bg_content',
      6 => 'field_bg_colour',
      7 => 'field_bg_content',
      8 => 'field_bg_colour',
      9 => 'field_bg_content',
      10 => 'field_bg_colour',
      11 => 'field_bg_content',
      12 => 'field_bg_colour',
      13 => 'field_bg_content',
      14 => 'field_bg_colour',
      15 => 'field_bg_content',
      16 => 'field_bg_colour',
      17 => 'field_bg_content',
      18 => 'field_bg_colour',
      19 => 'field_bg_content',
      20 => 'field_bg_colour',
      21 => 'field_bg_content',
      22 => 'field_bg_colour',
      23 => 'field_bg_content',
      24 => 'field_bg_colour',
      25 => 'field_bg_content',
      26 => 'field_bg_colour',
      27 => 'field_bg_content',
      28 => 'field_bg_colour',
      29 => 'field_bg_content',
      30 => 'field_bg_colour',
      31 => 'field_bg_content',
      32 => 'field_bg_colour',
      33 => 'field_bg_content',
      34 => 'field_bg_colour',
      35 => 'field_bg_content',
      36 => 'field_bg_colour',
      37 => 'field_bg_content',
      38 => 'field_bg_colour',
      39 => 'field_bg_content',
      40 => 'field_bg_colour',
      41 => 'field_bg_content',
      42 => 'field_bg_colour',
      43 => 'field_bg_content',
      44 => 'field_bg_colour',
      45 => 'field_bg_content',
      46 => 'field_bg_colour',
      47 => 'field_bg_content',
      48 => 'field_bg_colour',
      49 => 'field_bg_content',
      50 => 'field_bg_colour',
      51 => 'field_bg_content',
      52 => 'field_bg_colour',
      53 => 'field_bg_content',
      54 => 'field_bg_colour',
      55 => 'field_bg_content',
      56 => 'field_bg_colour',
      57 => 'field_bg_content',
      58 => 'field_bg_colour',
      59 => 'field_bg_content',
      60 => 'field_bg_colour',
      61 => 'field_bg_content',
      62 => 'field_bg_colour',
      63 => 'field_bg_content',
      64 => 'field_bg_colour',
      65 => 'field_bg_content',
      66 => 'field_bg_colour',
      67 => 'field_bg_content',
      68 => 'field_bg_colour',
      69 => 'field_bg_content',
      70 => 'field_bg_colour',
      71 => 'field_bg_content',
      72 => 'field_bg_colour',
      73 => 'field_bg_content',
      74 => 'field_bg_colour',
      75 => 'field_bg_content',
      76 => 'field_bg_colour',
      77 => 'field_bg_content',
      78 => 'field_bg_colour',
      79 => 'field_bg_content',
      80 => 'field_bg_colour',
      81 => 'field_bg_content',
      82 => 'field_bg_colour',
      83 => 'field_bg_content',
      84 => 'field_bg_colour',
      85 => 'field_bg_content',
      86 => 'field_bg_colour',
      87 => 'field_bg_content',
      88 => 'field_bg_colour',
      89 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_background|node|verticalthreepoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|verticalthreepoint|form';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '8',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Select background',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => 'Option 1. Choose a background image.<br />
Option 2. Enter a background colour. The colour will be applied behind any background image. You must select "No Image" in option 1.<br />
Please note: certain background / content combinations may not work as well as others. Please preview your choice to check readability of text etc.',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups['group_background|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_background|node|verticalthreepoint|teaser';
  $field_group->group_name = 'group_background';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Select background',
    'weight' => '2',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_bg_content',
      2 => 'field_bg_colour',
      3 => 'field_bg_content',
      4 => 'field_bg_colour',
      5 => 'field_bg_content',
      6 => 'field_bg_colour',
      7 => 'field_bg_content',
      8 => 'field_bg_colour',
      9 => 'field_bg_content',
      10 => 'field_bg_colour',
      11 => 'field_bg_content',
      12 => 'field_bg_colour',
      13 => 'field_bg_content',
      14 => 'field_bg_colour',
      15 => 'field_bg_content',
      16 => 'field_bg_colour',
      17 => 'field_bg_content',
      18 => 'field_bg_colour',
      19 => 'field_bg_content',
      20 => 'field_bg_colour',
      21 => 'field_bg_content',
      22 => 'field_bg_colour',
      23 => 'field_bg_content',
      24 => 'field_bg_colour',
      25 => 'field_bg_content',
      26 => 'field_bg_colour',
      27 => 'field_bg_content',
      28 => 'field_bg_colour',
      29 => 'field_bg_content',
      30 => 'field_bg_colour',
      31 => 'field_bg_content',
      32 => 'field_bg_colour',
      33 => 'field_bg_content',
      34 => 'field_bg_colour',
      35 => 'field_bg_content',
      36 => 'field_bg_colour',
      37 => 'field_bg_content',
      38 => 'field_bg_colour',
      39 => 'field_bg_content',
      40 => 'field_bg_colour',
      41 => 'field_bg_content',
      42 => 'field_bg_colour',
      43 => 'field_bg_content',
      44 => 'field_bg_colour',
      45 => 'field_bg_content',
      46 => 'field_bg_colour',
      47 => 'field_bg_content',
      48 => 'field_bg_colour',
      49 => 'field_bg_content',
      50 => 'field_bg_colour',
      51 => 'field_bg_content',
      52 => 'field_bg_colour',
      53 => 'field_bg_content',
      54 => 'field_bg_colour',
      55 => 'field_bg_content',
      56 => 'field_bg_colour',
      57 => 'field_bg_content',
      58 => 'field_bg_colour',
      59 => 'field_bg_content',
      60 => 'field_bg_colour',
      61 => 'field_bg_content',
      62 => 'field_bg_colour',
      63 => 'field_bg_content',
      64 => 'field_bg_colour',
      65 => 'field_bg_content',
      66 => 'field_bg_colour',
      67 => 'field_bg_content',
      68 => 'field_bg_colour',
      69 => 'field_bg_content',
      70 => 'field_bg_colour',
      71 => 'field_bg_content',
      72 => 'field_bg_colour',
      73 => 'field_bg_content',
      74 => 'field_bg_colour',
      75 => 'field_bg_content',
      76 => 'field_bg_colour',
      77 => 'field_bg_content',
      78 => 'field_bg_colour',
      79 => 'field_bg_content',
      80 => 'field_bg_colour',
      81 => 'field_bg_content',
      82 => 'field_bg_colour',
      83 => 'field_bg_content',
      84 => 'field_bg_colour',
      85 => 'field_bg_content',
      86 => 'field_bg_colour',
      87 => 'field_bg_content',
      88 => 'field_bg_colour',
      89 => 'field_bg_content',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_background|node|verticalthreepoint|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|verticalthreepoint|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node Information',
    'weight' => '10',
    'children' => array(
      0 => 'field_master_presentation',
      1 => 'field_migrate_original_nid',
      2 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Node Information',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_node_info|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_one|node|verticalthreepoint|default';
  $field_group->group_name = 'group_point_one';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point one',
    'weight' => '-2',
    'children' => array(
      0 => 'field_left_content',
      1 => 'field_left_image',
      2 => 'field_left_subtitle',
      3 => 'field_left_content',
      4 => 'field_left_image',
      5 => 'field_left_subtitle',
      6 => 'field_left_content',
      7 => 'field_left_image',
      8 => 'field_left_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_one|node|verticalthreepoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_one|node|verticalthreepoint|form';
  $field_group->group_name = 'group_point_one';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point one',
    'weight' => '3',
    'children' => array(
      0 => 'field_left_content',
      1 => 'field_left_image',
      2 => 'field_left_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_one|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_one|node|verticalthreepoint|teaser';
  $field_group->group_name = 'group_point_one';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point one',
    'weight' => '-2',
    'children' => array(
      0 => 'field_left_content',
      1 => 'field_left_image',
      2 => 'field_left_subtitle',
      3 => 'field_left_content',
      4 => 'field_left_image',
      5 => 'field_left_subtitle',
      6 => 'field_left_content',
      7 => 'field_left_image',
      8 => 'field_left_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_one|node|verticalthreepoint|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_three|node|verticalthreepoint|default';
  $field_group->group_name = 'group_point_three';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point three',
    'weight' => '0',
    'children' => array(
      0 => 'field_right_content',
      1 => 'field_right_image',
      2 => 'field_right_subtitle',
      3 => 'field_right_content',
      4 => 'field_right_image',
      5 => 'field_right_subtitle',
      6 => 'field_right_content',
      7 => 'field_right_image',
      8 => 'field_right_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_three|node|verticalthreepoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_three|node|verticalthreepoint|form';
  $field_group->group_name = 'group_point_three';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point three',
    'weight' => '5',
    'children' => array(
      0 => 'field_right_content',
      1 => 'field_right_image',
      2 => 'field_right_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_three|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_three|node|verticalthreepoint|teaser';
  $field_group->group_name = 'group_point_three';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point three',
    'weight' => '0',
    'children' => array(
      0 => 'field_right_content',
      1 => 'field_right_image',
      2 => 'field_right_subtitle',
      3 => 'field_right_content',
      4 => 'field_right_image',
      5 => 'field_right_subtitle',
      6 => 'field_right_content',
      7 => 'field_right_image',
      8 => 'field_right_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_three|node|verticalthreepoint|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_two|node|verticalthreepoint|default';
  $field_group->group_name = 'group_point_two';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point two',
    'weight' => '-1',
    'children' => array(
      0 => 'field_middle_content',
      1 => 'field_middle_image',
      2 => 'field_middle_subtitle',
      3 => 'field_middle_content',
      4 => 'field_middle_image',
      5 => 'field_middle_subtitle',
      6 => 'field_middle_content',
      7 => 'field_middle_image',
      8 => 'field_middle_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_two|node|verticalthreepoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_two|node|verticalthreepoint|form';
  $field_group->group_name = 'group_point_two';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point two',
    'weight' => '4',
    'children' => array(
      0 => 'field_middle_content',
      1 => 'field_middle_image',
      2 => 'field_middle_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_two|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_point_two|node|verticalthreepoint|teaser';
  $field_group->group_name = 'group_point_two';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Point two',
    'weight' => '-1',
    'children' => array(
      0 => 'field_middle_content',
      1 => 'field_middle_image',
      2 => 'field_middle_subtitle',
      3 => 'field_middle_content',
      4 => 'field_middle_image',
      5 => 'field_middle_subtitle',
      6 => 'field_middle_content',
      7 => 'field_middle_image',
      8 => 'field_middle_subtitle',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_point_two|node|verticalthreepoint|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|verticalthreepoint|default';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '3',
    'children' => array(
      0 => 'field_thumbnail',
      1 => 'field_thumbnail',
      2 => 'field_thumbnail',
      3 => 'field_thumbnail',
      4 => 'field_thumbnail',
      5 => 'field_thumbnail',
      6 => 'field_thumbnail',
      7 => 'field_thumbnail',
      8 => 'field_thumbnail',
      9 => 'field_thumbnail',
      10 => 'field_thumbnail',
      11 => 'field_thumbnail',
      12 => 'field_thumbnail',
      13 => 'field_thumbnail',
      14 => 'field_thumbnail',
      15 => 'field_thumbnail',
      16 => 'field_thumbnail',
      17 => 'field_thumbnail',
      18 => 'field_thumbnail',
      19 => 'field_thumbnail',
      20 => 'field_thumbnail',
      21 => 'field_thumbnail',
      22 => 'field_thumbnail',
      23 => 'field_thumbnail',
      24 => 'field_thumbnail',
      25 => 'field_thumbnail',
      26 => 'field_thumbnail',
      27 => 'field_thumbnail',
      28 => 'field_thumbnail',
      29 => 'field_thumbnail',
      30 => 'field_thumbnail',
      31 => 'field_thumbnail',
      32 => 'field_thumbnail',
      33 => 'field_thumbnail',
      34 => 'field_thumbnail',
      35 => 'field_thumbnail',
      36 => 'field_thumbnail',
      37 => 'field_thumbnail',
      38 => 'field_thumbnail',
      39 => 'field_thumbnail',
      40 => 'field_thumbnail',
      41 => 'field_thumbnail',
      42 => 'field_thumbnail',
      43 => 'field_thumbnail',
      44 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_thumbnail|node|verticalthreepoint|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|verticalthreepoint|form';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '9',
    'children' => array(
      0 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Thumbnail',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => '',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_thumbnail|node|verticalthreepoint|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_thumbnail|node|verticalthreepoint|teaser';
  $field_group->group_name = 'group_thumbnail';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'verticalthreepoint';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Thumbnail',
    'weight' => '3',
    'children' => array(
      0 => 'field_thumbnail',
      1 => 'field_thumbnail',
      2 => 'field_thumbnail',
      3 => 'field_thumbnail',
      4 => 'field_thumbnail',
      5 => 'field_thumbnail',
      6 => 'field_thumbnail',
      7 => 'field_thumbnail',
      8 => 'field_thumbnail',
      9 => 'field_thumbnail',
      10 => 'field_thumbnail',
      11 => 'field_thumbnail',
      12 => 'field_thumbnail',
      13 => 'field_thumbnail',
      14 => 'field_thumbnail',
      15 => 'field_thumbnail',
      16 => 'field_thumbnail',
      17 => 'field_thumbnail',
      18 => 'field_thumbnail',
      19 => 'field_thumbnail',
      20 => 'field_thumbnail',
      21 => 'field_thumbnail',
      22 => 'field_thumbnail',
      23 => 'field_thumbnail',
      24 => 'field_thumbnail',
      25 => 'field_thumbnail',
      26 => 'field_thumbnail',
      27 => 'field_thumbnail',
      28 => 'field_thumbnail',
      29 => 'field_thumbnail',
      30 => 'field_thumbnail',
      31 => 'field_thumbnail',
      32 => 'field_thumbnail',
      33 => 'field_thumbnail',
      34 => 'field_thumbnail',
      35 => 'field_thumbnail',
      36 => 'field_thumbnail',
      37 => 'field_thumbnail',
      38 => 'field_thumbnail',
      39 => 'field_thumbnail',
      40 => 'field_thumbnail',
      41 => 'field_thumbnail',
      42 => 'field_thumbnail',
      43 => 'field_thumbnail',
      44 => 'field_thumbnail',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(),
    ),
  );
  $field_groups['group_thumbnail|node|verticalthreepoint|teaser'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Animation');
  t('Node Information');
  t('Point one');
  t('Point three');
  t('Point two');
  t('Select background');
  t('Thumbnail');

  return $field_groups;
}
