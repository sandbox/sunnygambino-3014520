<?php
/**
 * @file
 * ca_presenter_user_profile.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function ca_presenter_user_profile_taxonomy_default_vocabularies() {
  return array(
    'organisational_structure' => array(
      'name' => 'Organisational Structure',
      'machine_name' => 'organisational_structure',
      'description' => 'Creating a taxonomy of the company\'s structure
   for use in permissions',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}
