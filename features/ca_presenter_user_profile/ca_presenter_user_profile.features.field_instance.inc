<?php
/**
 * @file
 * ca_presenter_user_profile.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function ca_presenter_user_profile_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'user-user-field_user_groups'.
  $field_instances['user-user-field_user_groups'] = array(
    'bundle' => 'user',
    'default_value' => array(
      0 => array(
        'tid' => 1,
      ),
    ),
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'taxonomy',
        'settings' => array(),
        'type' => 'taxonomy_term_reference_link',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'user',
    'field_name' => 'field_user_groups',
    'label' => 'User groups',
    'required' => 0,
    'settings' => array(
      'user_register_form' => 1,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_buttons',
      'weight' => 51,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('User groups');

  return $field_instances;
}
