<?php
/**
 * @file
 * device_key.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function device_key_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function device_key_node_info() {
  $items = array(
    'device_key' => array(
      'name' => t('Device key'),
      'base' => 'node_content',
      'description' => '',
      'has_title' => '1',
      'title_label' => t('Name'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
