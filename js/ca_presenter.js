/**
 * CA Presenter
 * Copyright 2018 Companyapp
 */

/**
 * ca_presenter
 * @author Abdul Sadik Yalcin
 * @author Zoltan Horvath
 */

(function ($) {
    Drupal.behaviors.myBehavior = {
      attach: function (context, settings) {
        var self = this;
        var hasRun = false;
        //Publishing script autostart on change
        $(".view-id-presentations tr select,.view-id-documents_1 tr select").change(function() {
          var action = $(this).find('option:selected').attr('id');
          var pid = $(this).find('option:selected').attr('value');
          switch (action) {
            case 'published':
              location.href = '/presentations/'+pid+'/published';
              break;
            case 'published_web':
              location.href = '/presentations/'+pid+'/published_web';
              break;
            case 'create_pdf':
              location.href = '/presentations/'+pid+'/create_pdf';
              break;
            case 'not_published':
              location.href = '/presentations/'+pid+'/not_published';
              break;
            case 'archived':
              location.href = '/presentations/'+pid+'/archived';
              break;
           }
        });
        $(".share-link").click(function(e) {
          e.preventDefault();
          var link = $(this).attr('data-share-link');
          clipboard.copyToClipboardDialog(decodeURIComponent(Drupal.settings.baseUrl+link),Drupal.t('Share presentation'));
        });
        if($('#nodeaccess-grants-form').length > 0) {
          if($('#nodeaccess-grants-form').find('table:nth-child(2)').length > 0) {
            $('#nodeaccess-grants-form').find('table:nth-child(2)').find('th:nth-child(2)').html('<label><input type="checkbox" id="view" class="view_select_all select_all" />'+Drupal.t('View')+'</label>');
            $('#nodeaccess-grants-form').find('table:nth-child(2)').find('th:nth-child(3)').html('<label><input type="checkbox" id="update" class="update_select_all select_all" />'+Drupal.t('Edit')+'</label>');
            $('#nodeaccess-grants-form').find('table:nth-child(2)').find('th:nth-child(4)').html('<label><input type="checkbox" id="delete" class="delete_select_all select_all" />'+Drupal.t('Delete')+'</label>');
          } else
          if($('#nodeaccess-grants-form').find('table:nth-child(1)').length > 0) {
            $('#nodeaccess-grants-form').find('table:nth-child(1)').find('th:nth-child(2)').html('<label><input type="checkbox" id="view" class="view_select_all select_all" />'+Drupal.t('View')+'</label>');
            $('#nodeaccess-grants-form').find('table:nth-child(1)').find('th:nth-child(3)').html('<label><input type="checkbox" id="update" class="update_select_all select_all" />'+Drupal.t('Edit')+'</label>');
            $('#nodeaccess-grants-form').find('table:nth-child(1)').find('th:nth-child(4)').html('<label><input type="checkbox" id="delete" class="delete_select_all select_all" />'+Drupal.t('Delete')+'</label>');
          }
          $('.select_all').unbind('click').bind("click",function(e) {
            var clicked = $(this);
            $('#nodeaccess-grants-form').find('table:nth-child(3)').find('.form-checkbox').each(function(i,v) {
              if($(v).attr('id').indexOf(clicked.attr('id')) !== -1) {
                if($(v).prop('checked') === true) {
                  $(v).prop('checked',false);
                } else {
                  $(v).prop('checked',true);
                }
              }
            });
          });
        }
        //WYSIWYG settings
        var is_user = false;
        var is_editor = false;
        var is_admin = false;
        if(Drupal.settings.userid == '1') {
          is_admin = true;
        } else {
          $.each(Drupal.settings.user_roles, function(i,v) {
            if(v.indexOf('Editor') || v.indexOf('editor')) {
  	      is_editor = true;
            } else
            if((v.indexOf('User') || v.indexOf('user')) && v.indexOf('authenticated') == -1) {
	      is_user = true;
            }
          });
        }
        $('.filter-wrapper .filter-list option').each(function(i,v) {
          self.removeEditor(v);
        });
        $('.filter-wrapper .filter-list option').each(function(i,v) {
          var elem = this;
          if(is_editor === true && $(v).attr('value').indexOf('editor') !== -1) {
            setTimeout(function() {
              $(v).prop('selected','selected');
              self.setEditor(v);
              $(elem).parent().trigger('change');
            },1500);
          } else
          if(is_user === true && $(v).attr('value').indexOf('user') !== -1) {
            setTimeout(function() {
              $(v).prop('selected','selected');
              self.setEditor(v);
              $(elem).parent().trigger('change');

            },1500);
          } else if(is_admin === true && $(v).attr('value').indexOf('root') !== -1){
            setTimeout(function() {
              $(v).prop('selected','selected');
              self.setEditor(v);
            },1500);
          }
        });


        var pdftemplate = [];
        var exporttemplate = [];

        if( $('.upload-presentation').length <= 0 ) {
          if($('li.from-pdf').length > 0) { pdftemplate.push( $('li.from-pdf')[0].outerHTML ); $('li.from-pdf').remove(); }
          if($('li.from-images').length > 0) { pdftemplate.push( $('li.from-images')[0].outerHTML ); $('li.from-images').remove(); }

          if(pdftemplate.length > 0){
            $('<li class="upload-presentation"><a href="#">' + Drupal.t('Upload Presentation') + '</a></li>').appendTo('ul.tabs');
            $( '<ul>' + pdftemplate.join('') + '</ul>' ).appendTo('.upload-presentation');
          }
        }

        if( $('.import-or-export-presentation').length <= 0 ) {
          if($('li.import').length > 0) { exporttemplate.push( $('li.import')[0].outerHTML ); $('li.import').remove(); }
          if( $('li.export').length > 0 ) { exporttemplate.push( $('li.export')[0].outerHTML ); $('li.export').remove(); }

          if(exporttemplate.length > 0){
            $('<li class="import-or-export-presentation"><a href="#">' + Drupal.t('Import/Export') + '</a></li>').appendTo('ul.tabs');
            $( '<ul>' + exporttemplate.join('') + '</ul>' ).appendTo('.import-or-export-presentation');
          }
        }

        if( $('#video_helper').length ) {
          if( $('ul.tabs').length ) {
            $('#video_helper').appendTo('ul.tabs');
          } else {
            $('<ul class="tabs"></ul>').appendTo('#tabs-wrapper');
            $('#video_helper').appendTo('ul.tabs');
          }
          $('#video_helper').css('display', '');
        }

        $('#views-form-presentations-presentations-list fieldset#edit-select').hide();
        $('#views-form-presentations-presentations-list .views-field-views-bulk-operations').hide();
        $('#views-form-presentations-presentations-list .vbo-select').hide();
        $('#toolbar').click(function(e) {
          $('#views-form-presentations-presentations-list fieldset#edit-select').toggle();
          $('#views-form-presentations-presentations-list .views-field-views-bulk-operations').toggle();
          $('#views-form-presentations-presentations-list .vbo-select').toggle();
        });
	//Media library preview
	$('.view-media-default').find('.views-field-rendered').find('a').on("click",function() {
          var l = $(this).attr("href");
	  console.log("link : ",l);
	  window.previewModal.init();
	  $("#slide_example_frame").attr("src",l);
	  $('#slide_example_stame').contents().find('img').attr('width','500px');
	  $('.preview-modal').css('display','table');
	  $('.preview-modal').css('opacity','1');
	  $('a#close-slide-preview').unbind('click').on('click',function() {
	    $("#slide_example_frame").attr("src",'about:blank');
	    $('.preview-modal').css('display','none');
	    $('.preview-modal').css('opacity','0');
	  });
	  return false;
	});
        if (window.top != window.self)  {
          self.specialLinks();
        }
      },
      setEditor: function(v) {
        $(v).parent().trigger('change');
        return;
      },
      removeEditor: function(v) {
        $(v).removeAttr('selected');
        return false;
      },
      specialLinks: function() {
	    // Listen for all links, not just those currently in the DOM
	    $(document).on('click', 'a', function(e){
	      var href = this.href,
		  dataIndex = $(this).attr('data-index');

	      // gotoSlide
	      if ( href.indexOf('//gotoSlide') > -1 || href.indexOf('//gotoslide') > -1 || href.indexOf('//goToSlide') >-1 ) {
		self.autoPlay = false;
		self.goToSlide(href);
		return false;
	      }

	      // reloadSlide
	      if ( href.indexOf('//reloadSlide') > -1 || href.indexOf('//reloadslide') > -1 ) {
		self.autoPlay = false;
		self.reloadSlide(href, iframe.id);
		return false;
	      }

	      // openDocument
	      if ( href.indexOf('//opendocument') > -1 || href.indexOf('//openDocument') > -1 ) {
		self.openDocument(href);
		return false;
	      }

	      // openExternal
	      if ( href.indexOf('//openExternal') > -1 ) {
		e.preventDefault();
		var link = href;
		link = link.split('link=');
		link = link[1];
		var win = window.open(link, '_blank');
	      }

	      if ( dataIndex !== null && dataIndex == 'nextSlide' ) {
		self.autoPlay = false;
		self.slider.trigger('next.owl.carousel');
	      }

	      if ( dataIndex !== null && dataIndex == 'prevSlide' ) {
		self.autoPlay = false;
		self.slider.trigger('prev.owl.carousel');
	      }
	    });
      }
    };
})(jQuery);

jQuery(document).ready(function($) {
  //Library slides loader
  if(jQuery('.view-display-id-library_slides_page').length > 0) {
    var loader = document.getElementById('html-loader');
    var run = 0;
    jQuery(document).ajaxStart(function () {
      //ajax request went so show the loading image
      jQuery(loader).show();
    })
    .ajaxStop(function () {
      //got response so hide the loading image
      jQuery(loader).hide();
      $('html').animate({
         scrollTop: $('.view-display-id-library_slides_page').offset().top
      }, 1000);
    });
  }

   //jQuery(window).on('resize', window.previewModal.init);
    jQuery('.slide-preview').load(function(e) {
      if($(this).attr('src').indexOf('undefined') !== -1) {
          return false;
      }
      //If iframe src if empty do not show it
      if($(e.currentTarget).attr('src') === '') {
          return false;
      }
      // Blocks page from scrolling to top when clicked on hash href
      e.preventDefault();
      var clone = jQuery(this).clone();
      var top_page = window.document;
      var container = jQuery(top_page).find('body').find('.slide-preview-container');
      var iframe = jQuery(top_page).find('body').find('.preview-item');
      iframe.html(clone);
      jQuery(container).css({ 'display' : 'table' });
      jQuery(container).animate({
        'opacity' : '1'
      }, 300);
      jQuery(iframe).animate({
        'opacity' : '1'
      }, 300);
      jQuery('a#close-slide-preview').on('click', function(e){
        // Blocks page from scrolling to top when clicked on hash href
        e.preventDefault();
        jQuery(container,iframe).animate({
          'opacity' : '0'
        }, 300, function(){
          jQuery(container,iframe).css({ 'display' : 'none' });
        });
      });
      /**
       * Toggle preview to widescreen
       */
      $('.slide-preview-toggle a').unbind('click').on('click', function(event){
          event.preventDefault();

          $('.slide-preview-container').toggleClass('widescreen');
          var frame = $('.preview-iframe')[0].contentWindow || $('.preview-iframe')[0].contentDocument || $('.preview-iframe')[0].contentWindow.document;
          frameBody = frame.window.document.body;
          frameBody.classList.toggle('widescreen');


          if(typeof frame.backgroundEvent !== 'undefined'){
            frame.backgroundEvent();
          }
      });
      var widescreen = false;
      var frame = $('.preview-iframe')[0].contentWindow || $('.preview-iframe')[0].contentDocument || $('.preview-iframe')[0].contentWindow.document;
      frameBody = frame.window.document.body;
      frameBody.classList.toggle('widescreen');
      $(frameBody).find('#image-gallery-with-captions .swipe').css('visibility','visible');
      $.each(frameBody.classList, function(i,v) {
        if(v == 'widescreen') widescreen = true;
      });
      $('.slide-preview-container').removeClass('widescreen');
      if(widescreen) {
          $('.slide-preview-container').addClass('widescreen');
      }
    });

    /**
     * Properties
     */
    window.slider = {};
    window.el = {};
    window.default_alert = window.alert;
    window.top_page;

    /**
     * provides system for detecting ctrl-c (clipboard copy)
     * @see https://stackoverflow.com/questions/2903991/how-to-detect-ctrlv-ctrlc-using-javascript
     * @type {{}}
     */
    window.clipboard = {};
    window.clipboard.copyHandler = {};
    /*
     * keyboard key mapping for convenience
     * @type {{ctrlKey: number, cmdKey: number, cKey: number}}
     */
    window.clipboard.copyHandler.keys = {
        ctrlKey : 17,
        cmdKey : 91,
        cKey : 67
    };
    window.clipboard.copyHandler.ctrlDown = false;
    window.clipboard.copyHandler.handlers = {};

    /**
     * Over writes alert box
     */
    window.alert = function(message) {
        // Only overwrite on back-end
        if( window.location.href.indexOf('presentations') > -1 ||
            window.location.href.indexOf('documents') > -1 ||
            window.location.href.indexOf('emails') > -1 ||
            window.location.href.indexOf('emails2') > -1 ||
            window.location.href.indexOf('messages') > -1 ||
            window.location.href.indexOf('reports') > -1) {

            var thetitle = 'Presentation Link';
            if (message.indexOf("This browser restriction") !== -1) {
                thetitle = 'Please Note';
            }

            $('<div />').text(message).dialog({
                dialogClass: 'no-close',
                modal:true,
                title: thetitle,
                buttons: {
                    'OK':function(){
                        $(this).dialog('close');
                    }
                },
                close:function(){ jQuery(this).dialog('destroy').remove(); }
            });
        } else {
            window.default_alert(message);
        }
    };

    /**
     * initialises a jquery ui dialog box with the ability to copy to clipboard
     * @param copyValue
     * @param title
     */
    window.clipboard.copyToClipboardDialog = function(copyValue, title) {
        //Remove all existing dialogs first
	$('.ui-dialog').remove();
        title = (typeof title === 'undefined') ? 'Copy to clipboard' : title;

        var inputSelector = 'js-copy-to-clipboard',
        html = '<div class="description dialog-description">'+Drupal.t('Click the Copy button to save the text into your clipboard. Then use CTRL.+V to paste the text into your email client.')+'</div><textarea readonly id="clipboard-target" value="' + copyValue + ' " rows="5" cols="50">'+copyValue+'</textarea>',
        clipboard,
        dialogArgs = {
            dialogClass: 'no-close',
            modal: true,
            title: title,
            width: 500,
            buttons: {},
        },
        copyPasteHandler = function (e) {
            if (e.which === 17) {
                var $target = $('#clipboard-target');
                $target.select();
            }
        };

        if (typeof Clipboard !== 'undefined' && Clipboard.isSupported()) {
            // clipboard is supported we set up a copy to clipboard dialogue

            dialogArgs.closeHandler = function () {
                $(this).dialog('destroy').remove();
                clipboard.destroy();
            };

            html += '<button id="copied">'+Drupal.t('Copied')+'</button><button id="' + inputSelector + '" data-clipboard-target="#clipboard-target">'+Drupal.t('Copy')+'</button>';

            $('<div />').html(html).dialog(dialogArgs);

            clipboard = new Clipboard('#' + inputSelector);
            clipboard.on('success', function (e) {
                $('#copied').addClass('copied-to-cb').show(1000);
                setTimeout(function () {
                    $('#copied').removeClass('copied-to-cb').hide(1000);
                }, 3000);
            });
        } else {
            // Clipboard is not supported we implement a workaround
            // mobiles will not be able to do this maybe they have access to clipboard
            html = '<p> Click ctrl+c / cmd+c to copy link to clipboard</p>' + html + '<span class="clipboard-message"></span>';
            dialogArgs.closeHandler = function () {
                $(this).dialog('destroy').remove();
                $('body').off('keydown', copyPasteHandler);
                window.clipboard.copyHandler.destroy();
            };

            $('body').on('keydown', copyPasteHandler);

            window.clipboard.copyHandler.init(function (e) {
                var $input = $('.clipboard-message');
                $input.addClass('copied-to-cb');
                setTimeout(function () {
                    $input.removeClass('copied-to-cb');
                }, 3000);
            });

            $('<div />').html(html).dialog(dialogArgs);
        }
    };

    window.clipboard.copyHandler.handlers.ctrlKeyDownHandler = function(e) {
        var keys = window.clipboard.copyHandler.keys;
        if (e.keyCode === keys.ctrlKey || e.keyCode === keys.cmdKey) {
            window.clipboard.copyHandler.ctrlDown = true;
        }
    };

    /**
     * event callback detects ctrl key being raised
     * @param e
     */
    window.clipboard.copyHandler.handlers.ctrlKeyUpHandler = function(e) {
        var keys = window.clipboard.copyHandler.keys;
        if (e.keyCode === keys.ctrlKey || e.keyCode === keys.cmdKey) {
            window.clipboard.copyHandler.ctrlDown = false;
        }
    };

    /**
     * event callback detecting ctrl - paste key keydown combination
     * @param e
     * @param cb
     */
    window.clipboard.copyHandler.handlers.pasteKeyHandler = function(e, cb) {
        var keys = window.clipboard.copyHandler.keys;
        if (window.clipboard.copyHandler.ctrlDown && (e.keyCode === keys.cKey)) {
            cb(e);
        }
    };

    /**
     * initialise listeners
     * @param {function} cb
     */
    window.clipboard.copyHandler.init = function(cb) {
        var ctrlKeyDownHandler = window.clipboard.copyHandler.handlers.ctrlKeyDownHandler,
        ctrlKeyUpHandler = window.clipboard.copyHandler.handlers.ctrlKeyUpHandler,
        pasteKeyHandler = window.clipboard.copyHandler.handlers.pasteKeyHandler;

        $(document).on('keydown', ctrlKeyDownHandler).on('keyup', ctrlKeyUpHandler);

        $('body').on('keydown', function (e) {
            pasteKeyHandler(e, cb);
        });
    };

    /**
     * remove key listeners for listening for ctrl-copy
     */
    window.clipboard.copyHandler.destroy = function () {
        var ctrlKeyDownHandler = window.clipboard.copyHandler.handlers.ctrlKeyDownHandler,
        ctrlKeyUpHandler = window.clipboard.copyHandler.handlers.ctrlKeyUpHandler,
        pasteKeyHandler = window.clipboard.copyHandler.handlers.pasteKeyHandler;

        $(document).off('keydown', ctrlKeyDownHandler).off('keyup', ctrlKeyUpHandler);

        $('body').off('keydown', function (e) {
            pasteKeyHandler(e, cb);
        });
    };

    /**
     * Template preview functionality
     */
    $('a.slide-example').on('click', function(event){
        event.preventDefault();

        var slideid = $(this).attr('data-sid');
        var $container = $('.preview-frame[data-sid="' + slideid + '"] .outer-container');
        var $frame = $('.preview-frame[data-sid="' + slideid + '"] iframe');

        openPopup($container, $frame);
        initSlider(slideid);
    });

    /**
     * Slide preview functionality - singular actual slides
     */
    $('a.slide-preview-bind').on('click', function(e){
      e.preventDefault();
      console.log('clicked');
      var sid = $(this).attr('data-sid');
      var pid = $(this).attr('data-pid');
      $('iframe#slide_example_frame, iframe.slide-preview').attr('src','/node/'+sid+'/'+pid+'?view=1&presentation_id='+pid+'&slide_example=1');
      $('iframe#slide_example_frame, iframe.slide-preview').load(function() {
        var clone = $(this).clone();
        var top_page = window.document;
        var container = $(top_page).find('body').find('.outer-container');
        var iframe = $(top_page).find('body').find('.preview-item');
        iframe.html(clone);
        $(container).css({ 'display' : 'table' });
        $(container).animate({
          'opacity' : '1'
        }, 300);
        $(iframe).animate({
          'opacity' : '1'
        }, 300);
      });
    });

    /**
     * Toggle preview to widescreen
     */
    $('.slide-example-toggle a').on('click', function(event){
        event.preventDefault();

        var slideid = $(this).attr('data-slideid');
        $('.outer-container').toggleClass('widescreen');

        if( $('.outer-container').hasClass('widescreen') ){
            toggleBG(slideid, true);
        } else {
          toggleBG(slideid);
        }
    });

    $('.popup-close.slide-example-close a').on('click', function(e){
        event.preventDefault();

        var slideid = $(this).attr('data-slideid');

        $('.owl-item iframe').each(function(){
          $(this).remove();
        });

        slider[slideid].trigger('destroy.owl.carousel');
        delete slider[slideid];

        $('.outer-container').animate({
            'opacity' : '0'
        }, 300, function(){
            $('.outer-container').css({ 'display' : 'none' });

        });
    });

    $('.prev-slide-button').on('click', function(event){
        event.preventDefault();

        var id = $(this).attr('data-slideid');
        slider[id].trigger('prev.owl.carousel');
    });
    $('.next-slide-button').on('click', function(event){
        event.preventDefault();
        var id = $(this).attr('data-slideid');
        slider[id].trigger('next.owl.carousel');
    });

    /**
     * Adds promote to library value to the original field
     */
    $('#custom_promote').unbind('click').bind('click',function() {
        $('#edit-status').attr('checked',this.checked);
    });

    if($('#custom_promote').length > 0 && $('#edit-status').length > 0) {
        $('#edit-status').bind('click',function() {
            $('#custom_promote').attr('checked',this.checked);
        });
    }

    /**
     * Hides error messages when icon clicked
     *
     * DEBUG ONLY
     */
    $('.messages__icon').on('click', function(event){
        event.preventDefault();
        $(this).parent().hide();
        //var r = confirm('Please note the error and report it before you close this :)');
        //if(r === true) {
            //$(this).parent().hide();
        //}
    });

    /**
     * Performs delete on slides edit page
     */
    if($('#edit-actionca-presenter-detach-slide').length > 0) {
        $('.status-block-delete a').unbind('click').bind('click',function() {
            $('.views-field-views-bulk-operations input').each(function(i,v) {
                $(this).attr('checked',false);
            });
            $(this).parent().parent().parent().find('.views-field-views-bulk-operations input').attr('checked','true');
            $(this).parent().parent().parent().find('.views-field-views-bulk-operations input').prop('checked',true);
            $('#edit-actionca-presenter-detach-slide').trigger('click');
            return false;
        });
    }

    /**
     * Presentation page theme selector
     */
    if($('#edit-field-theme-und-0-value').length > 0) {
        $('#edit-field-theme-custom').bind('change',function() {
            var value = jQuery('#edit-field-theme-custom').val();
            $('#edit-field-theme-und-0-value').val(value);
        });
    }

    /**
     * Searches slides on library page
     */
    /*top_page = window.parent.document;
    var path = window.location.pathname;
    path = path.split('/');
    if(path[1] == 'search-slides-form') {
        $('.form-submit').bind('click',function(e) {
            var title = $('.form-text').val();
            $('#selected_library_slides', top_page).attr('src','/search-slides/' + title);
            var loader = window.parent.document.getElementById('html-loader');
            var loaderText = window.parent.document.getElementById('loader-text');
            $(loaderText).text('Loading slides. Please wait...');
            $(loaderText).removeClass('hidden');
            $(loader).show();
        });
    }*/

    $('#edit-actionca-presenter-attach-slide').on('click', function(){
        var loader = window.parent.document.getElementById('html-loader');
        var loaderText = window.parent.document.getElementById('loader-text');
        $(loaderText).text('Adding slides. Please wait...');
        $(loaderText).removeClass('hidden');
        $(loader).show();
    });

    /**
     * Redirect for library page
     */
    $('#selected_library_slides').load(function(){
        window.localStorage.removeItem('selected_html');
        $(window.parent.document).find('#entity_selector_frame').contents().find('#edit-submit-entity-selector').trigger('click');
        var iframe = jQuery(this);

        //redirection
        var path = window.location.pathname;
        path = path.split('/');
        var message_elem = jQuery('.messages--status',iframe.contents());
        if(message_elem.length > 0) {
            var message = jQuery('.messages--status',iframe.contents()).html().indexOf(Drupal.t('Insert slides'));
            if(message > -1) {
              window.location = '/presentations/' + path[2] + '/edit-slides';
            }
        } else if( $('#views-form-presentations-library-slides-page',iframe.contents()).length > 0 || $('#views-form-presentations-page-8',iframe.contents()).length > 0 ) {
            var loader = window.parent.document.getElementById('html-loader');
            var loaderText = window.parent.document.getElementById('loader-text');
            $(loader).hide();
            $(loaderText).text('');
            $(loaderText).addClass('hidden');
        } else {
            var loader = window.parent.document.getElementById('html-loader');
            var loaderText = window.parent.document.getElementById('loader-text');
            $(loaderText).text('Adding slides. Please wait...');
            $(loaderText).removeClass('hidden');
            $(loader).show();
        }
    });


    /**
     * Animate popup view
     *
     * @param object $container - Frame object
     * @param object $frame - IFrame object
     */
    function openPopup($container, $frame){
        $container.css({ 'display' : 'table' });
        $container.animate({
            'opacity' : '1'
        }, 300);
        $frame.animate({
            'opacity' : '1'
        }, 300);
    };

    /**
     * Initialises slider instance
     *
     * @param int slideid
     */
    function initSlider(slideid){
        slider[slideid] = $('.owl-carousel[data-slideid="'+ slideid +'"]');
        slider[slideid].owlCarousel({
            loop: false,
            items: 1,
            touchDrag: false,
            mouseDrag: false,
            freeDrag: false,
            onChanged: function(event) {
                var index = event.item.index + 1;
                if(index == 1){
                    $('.prev-slide-button[data-slideid="'+ slideid +'"]').hide();
                } else {
                    $('.prev-slide-button[data-slideid="'+ slideid +'"]').show();
                }

                if(index == event.item.count){
                    $('.next-slide-button[data-slideid="'+ slideid +'"]').hide()
                } else {
                    $('.next-slide-button[data-slideid="'+ slideid +'"]').show();
                }
            },
            onTranslated: function(event){
                var $faker = $('.owl-item.active .frame-faker');
                var $current = $('.owl-item.active');
                var id = $faker.attr('data-id');
                var src = $faker.attr('data-src');
                loadFrame(id, src, $current);
            },
            onInitialized: function(event){
                var $faker = $('.owl-item.active .frame-faker');
                var $current = $('.owl-item.active .item');
                var id = $faker.attr('data-id');
                var src = $faker.attr('data-src');
                console.log('Params: ', id, ' ', src);
                loadFrame(id, src, $current);
            }
        });
    };

    /**
     * Loads iframe by changing src attribute
     *
     * @param int id - The id of the iframe
     * @param string src - The source of the iframe
     * @param object $current Frame object
     */
    function loadFrame(id, src, $current){
        if(id){
            if($('#slide-' + id).length < 1){
                $('<iframe scrolling="no" id="slide-' + id + '" class="slide-preview preview-iframe loading" src="' + src + '" onload="window.previewModal.frameLoaded(this);"></iframe>').appendTo($current);

                $('#slide-' + id).on('load', function(event){
                    console.log('completed loading frame: ', id);
                    $('.loading-icon[data-id="' + id + '"]').hide();
                    console.log('yo : ',$(this));
      		    var widescreen = false;
		    var frame = $(this)[0].contentWindow || $(this)[0].contentDocument || $(this)[0].contentWindow.document;
		    frameBody = frame.window.document.body;
		    frameBody.classList.toggle('widescreen');
		    $(frameBody).find('#image-gallery-with-captions .swipe').css('visibility','visible');
		    $.each(frameBody.classList, function(i,v) {
		      if(v == 'widescreen') widescreen = true;
		    });
		    $('.slide-preview-container').removeClass('widescreen');
		    if(widescreen) {
		      $('.slide-preview-container').addClass('widescreen');
		    }
                });
            } else {
                console.log('Frame already exists: ', $('#slide-' + id).length);
            }
        } else {
          console.log('ID is not defined: ', id);
          alert('Sorry, we can\'t display this slide.');
        }
    };

    /**
     * Toggles preview slide background to widescreen and 4:3
     *
     * @param int slideid - Slide ID
     * @param boolean - Add
     */
    function toggleBG(slideid, add){
        var containerid = slideid;
        var frames = $('div[data-slideid="' + slideid + '"] iframe');
        for(var i = 0; i < frames.length; i++){
            var frame = $('#' + frames[i].id)[0].contentWindow || $('#' + frames[i].id)[0].contentDocument || $('#' + frames[i].id)[0].contentWindow.document;
            frameBody = frame.window.document.body;

            //toggleclass is not realiable in this case due to speed of events
            if(add){
                $(frameBody).addClass('widescreen');
            } else {
                $(frameBody).removeClass('widescreen');
            }

            if(typeof frame.backgroundEvent !== 'undefined'){
              frame.backgroundEvent();
            }
        }

        if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
            var evt = document.createEvent('UIEvents');
            evt.initUIEvent('resize', true, false, window, 0);
            window.dispatchEvent(evt);
        } else {
            window.dispatchEvent(new Event('resize'));
        }
    };

});
