var merge = require('easy-pdf-merge');
var fs = require('fs');

var output_path = process.argv[2];
var images = process.argv[3];

output_path = JSON.parse(output_path);
images = JSON.parse(images);
if(images.length === 1) {
    return console.log(JSON.stringify({
      'status': 'after_pdf_creation',
      'output_path': images[0],
      'images': images,
    }));
}

merge(images,output_path,function(err){
  if(err)
    return console.log(err);
    console.log(JSON.stringify({
      'status': 'after_pdf_creation',
      'output_path': output_path,
      'images': images,
    }));
});
