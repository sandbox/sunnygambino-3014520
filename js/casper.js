const path = require('path');
const _ = require('lodash');
const Promise = require('bluebird');
const fs = require('fs-extra');
const scrape = require('website-scraper');
var cookie = process.argv[2];
var url = process.argv[3];
var pid = process.argv[4];
var sid = process.argv[5]
var _path = process.argv[6]
var type = process.argv[7]
const options = {
  urls: [url],
  directory: _path + '/' +sid,
  request: {
    headers : {
      'Cookie' : cookie
    }
  },
  defaultFilename : sid+'.html',
  subdirectories: [
    {directory: 'system/files/'+sid+'/', extensions: ['.jpg','.png', '.svg', '.js', '.css', '.woff', '.abf','.acfm','.afm','.amfm','.bdf','.cha','.chr','.compositefont','.dfont','.eot','.etx','.euf','.f3f','.fea','.ffil','.fnt','.fon','.fot','.gdr','.gf','.glif','.gxf','.lwfn','.mcf','.mf','.mxf','.nftr','.odttf','.otf','.pcf','.pfa','.pfb','.pfm','.pfr','.pk','.pmt','.sfd','.sfp','.suit','.t65','.tfm','.ttc','.tte','.ttf','.txf','.ufo','.vfb','.vlw','.vnf','.woff','.woff2','.xfn','.xft','.ytf','.mp4','.mp3','.gif','.ico','.js.txt', '.min.js.txt', '.css.txt', '.min.css.txt', '.txt', '.jpeg']},
  ]
};

scrape(options, (error, result) => {
	/* some code here */
	//console.log('error : ',error);
	//console.log('result : ',result);
});
