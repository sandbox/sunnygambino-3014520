jQuery(document).ready(function() {
  jQuery('img, .view-icon').bind('click',function() {
    var clone = jQuery(this).clone();
    var top_page = window.parent.document;
    var container = jQuery(top_page).find('#preview-popup').find('.outer-container');
    var iframe = jQuery(top_page).find('#preview-popup').find('#slide_example_frame');
    setTimeout(function() {
      var sid = jQuery(this).parent().parent().parent()
            .find('.views-field-views-bulk-operations')
            .find('input').val();
      var pid = window.parent.location.pathname;
      pid = pid.split('/');
      pid = pid[2];
      if(!pid || !sid) {
        return false;
      }
      var src = '/presentation-viewer/' + pid + '/slide/' + sid;
      iframe.attr('src', src);
      jQuery(container).css({ 'display' : 'table' });
      jQuery(container).animate({
        'opacity' : '1'
      }, 300);
    },300);
  });
});
