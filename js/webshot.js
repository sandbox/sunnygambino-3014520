var webshot = require('webshot');
var cookies = JSON.parse(process.argv[2]);
console.log('cookies : ', cookies);
var options = JSON.parse(process.argv[3]);
console.log('options : ',options);
var settings = JSON.parse(process.argv[4]);
console.log('settings : ',settings);
var webshot_options = {
  cookies : cookies,
  renderDelay : 2000,
  timeout : 10000,
  phantomConfig: {
      'ignore-ssl-errors': 'true',
  },
  quality: 100,
  streamType: settings.streamtype,
  screenSize: {
    width: settings.width, 
    height: settings.height
  },
   shotSize: {
    width: settings.width,
    height: settings.height
  },
  userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36'
};
webshot(options['url'], options['export_path'], webshot_options, function(err) {
  console.log('webshot error : ',err);
  if(err === null) {
    console.log('thumbnail url : ',options['url']);
    console.log('thumbnail export_path : ',options['export_path']);
  } else {
    console.log('error : ',err);
  }
});
