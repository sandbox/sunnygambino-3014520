<?php


class CAPresenterPDFConverter {
	/**
	 * defines content type
	 */
	const THEME_PDF  ='presentation_pdf_ppt';
	const MODULE_NAME = 'ca_presenter_pdf_uploader';
	const FIELD_PDF_FILE = 'field_pdf_uploads';
	const FIELD_PDF_PAGE_RANGE = 'field_pdf_page_range';
	const FIELD_CONVERTED_IMAGES = 'field_converted_images';
	const PDF_PRESENTATION_FORM_ID = 'presentation_from_pdf_node_form';

	const SLIDE_HEIGHT = 768;
	const SLIDE_WIDTH = 1024;

	public $file;
	public $num_of_pages;
	public $parent_node;
	public $nids;
	public $pages;
        public $advanced_slide_type = 'fullscreenimage';
        public $field_pdf_image = 'field_fsimage';
	private $first_slide_thumbnail = NULL;

	/**
	 * provides path to the node pdf converter
	 * @return string
	 */
	static function NODE_PAGE_IMAGE_EXECUTABLE() {
		return drupal_realpath(drupal_get_path('module', self::MODULE_NAME)) . '/js/get-page-image.js';
	}

        public function set_slide_type($type) {
          $this->advanced_slide_type = $type;
        }

        public function set_field_type($type) {
          $this->field_pdf_image = $type;
        }

	/**
	 * provides path to the node pdf page counter
	 * @return string
	 */
	static function NODE_PAGE_COUNTER_EXECUTABLE() {
		return drupal_realpath(drupal_get_path('module', self::MODULE_NAME)) . '/js/get-pages.js';
	}


	/**
	 * gets the number of slides (pages) in a pdf
	 * @return null |int $ret
	 */

	private function get_number_of_slides_in_pdf () {
		// execute node script that will stdout a json array of the image paths
		$cmd = 'node ' . self::NODE_PAGE_COUNTER_EXECUTABLE() . ' "' . $this->file_path . '"';
		exec($cmd, $ret);
		$pages = NULL;
		if (isset($ret) && is_array($ret) && sizeof($ret) > 0) {
			$ret = json_decode($ret[0], TRUE);
			//error handling from the node cli processes
			if (isset($ret['error'])) {
				$message = (isset($ret['message'])) ? $ret['message'] : 'Could not generate presentation from pdf';
				$watchdog_message = (isset($ret['stderr'])) ? $ret['stderr'] : $message . '. ' . $ret['stderr'];
				drupal_set_message($message, 'error');
				watchdog('error', $watchdog_message);
				return null;
			}
			// return image paths
			$pages = (isset($ret['pages'])) ? (int) $ret['pages'] : NULL;
		}
		return  $pages;
	}

	/**
	 *
	 * @param string $url
	 * @return string
	 */
	private function generate_html_body  ($url) {
		$html = '<div id="image-slide-container"><img alt="" src="' .
		        $url . '" border="0" hspace="0" vspace="0"/></div>';
		return $html;
	}

	/**
	 * generates an advanced slide with a page from the pdf embedded and returns the node nid
	 * @param $page_number
	 * @return null | string $nid
	 */

	public function create_advanced_slide($page_number) {
		$image_path = $this->get_page_image_from_pdf($page_number);
		$nid = $this->create_advanced_slide_node($page_number, $image_path);
//		unlink($image_path);
		$this->nids[] = $nid;
		return $nid;
	}

	/**
	 * generates an image for a given pdf page
	 * @param $page_number
	 * @return null | string $ret
	 */
	private function get_page_image_from_pdf($page_number) {
		// execute node script that will stdout a json array of the image paths
		$ret = NULL;
		$cmd = 'node ' . self::NODE_PAGE_IMAGE_EXECUTABLE() . ' "' . $this->file_path .'"' . ' ' . $page_number;
		exec($cmd, $ret);
		if (isset($ret) && is_array($ret) && sizeof($ret) > 0) {
			$ret = json_decode($ret[0], TRUE);
			//error handling from the node cli processes
			if (isset($ret['error'])) {
				$message = (isset($ret['message'])) ? $ret['message'] : 'Could not generate presentation from pdf';
				$watchdog_message = (isset($ret['stderr'])) ? $ret['stderr'] : $message . '. ' . $ret['stderr'];
				drupal_set_message($message, 'error');
				watchdog('error', $watchdog_message);
				return null;
			} else if (isset($ret['image_path'])) {
				return $ret['image_path'];
			}
		}
		// further error handling in case of nodejs / cli misconfiguration
		drupal_set_message('error', 'Could not generate presentation from pdf (cli)');
		watchdog('error', 'error generating presentation from pdf (potential node problem)');
		return $ret;
	}

	/**
	 * creates an advanced slide node for attaching to presentation node
	 * @param int $page_number
	 * @param string $image_path
	 * @return null | string $nid
	 */
	private function create_advanced_slide_node ($page_number, $image_path) {
		global $user;
		$uri_dir = 'private://'. $user->uid;
		$nid = NULL;
		$fn = basename($image_path);
		$fn_parts = explode('.', $fn);
		if (file_prepare_directory($uri_dir, FILE_CREATE_DIRECTORY)) {
			$image = file_get_contents($image_path);
			$converted_image = file_save_data($image, $uri_dir . '/' . basename($fn));
			$drupal_image_file = (array) $converted_image;
			$drupal_image_file['display'] = 1;
			$node = new stdClass();
			$node->type = $this->advanced_slide_type;
			node_object_prepare($node);
			$node->title = 'Slide ' . ((int)$page_number + 1);
			$node->{$this->field_pdf_image} = [LANGUAGE_NONE => [$drupal_image_file]];
			$url = file_create_url($drupal_image_file['uri']);
			$node->body = [LANGUAGE_NONE => [ 0 => ['value' => self::generate_html_body($url) , 'format' => 'full_html']]];
                        $node->field_body = [LANGUAGE_NONE => [ 0 => ['value' => self::generate_html_body($url) , 'format' => 'full_html']]];
			// set association with master node
			$node->field_master_presentation = array(LANGUAGE_NONE => array(0 => array('target_id' => (int)$this->parent_node->nid)));
			$node->field_js_editor = array(LANGUAGE_NONE => array(0 => array('value' => 'jQuery(document).ready(function() {  jQuery("#blank-slide").addClass("image-uploader"); });')));
			node_save($node);
			$nid = $node->nid;
      $presentation = node_load($this->parent_node->nid);
      _create_thumbnail_main($presentation, $node, 10);
		} else {
			drupal_set_message('error', 'Unable to generate presentation slides from pdf');
		}

		return $nid;
	}


	/**
	 * @param null | array $nids
	 * @return mixed
	 */
	public function attach_slides_to_presentation ($nids = NULL) {
		/**
		 * @var object $node
		 */
		$node = $this->parent_node;
		$this->nids = ($nids !== NULL) ? $nids : $this->nids;
		if ($this->nids !== NULL) {
			// attaching prepared thumbnail to presentation node
			if (isset($this->first_slide_thumbnail)) {
				$node->field_cover_thumbnail = $this->first_slide_thumbnail;
			}
			foreach ($nids as $nid) {
				$node->field_slides[LANGUAGE_NONE][] = [
					'target_id' => $nid
				];
			}

		}

		node_save($node);
    global $subdomain;
    global $published_not_published;
    $node_wrapper = entity_metadata_wrapper('node', $node);
    $slides = $node_wrapper->field_slides->value();
    $export_path = 'private://' . $subdomain . '/ca_presenter/presentations/' . $node->nid;
    //@_create_thumbnails($node,$slides,$export_path, FALSE);
    @_generate_book_json($node,$node_wrapper,$slides,$published_not_published->name, FALSE);
		return $node;
	} 

	/**
	 * attach the presentation container node to the instance
	 * @param null | mixed $node
	 */
	public function attach_parent_node ($node = NULL) {
		$this->parent_node = $node;
	}

	public function  __construct($file, $parent_node = NULL, $page_range = NULL) {
    //Cancel button fix
    if(!$file) {
      drupal_goto('/');
    }
		$this->parent_node = $parent_node;
		$this->file = $file;
		@$this->uri = $file->uri;
		$this->file_path = drupal_realpath($this->uri);
		$this->pages_in_pdf = self::get_number_of_slides_in_pdf();

		if ($page_range !== NULL) {
			$this->pages = explode(',', $page_range);
			foreach ($this->pages as &$page) {
			  $page = (int) $page-1;
      }
			$this->num_of_pages = sizeof ($this->pages);
		} else {
			$this->num_of_pages = $this->pages_in_pdf;
			$this->pages = range(0, $this->num_of_pages);
		}
	}

}
