var argv = process.argv;

var $pdf_fn = argv[2];

var PDFImage = require("pdf-image").PDFImage;

var pdfImage = new PDFImage($pdf_fn, {
    graphicsMagick : true,
});


pdfImage.numberOfPages()
    .then(function(numOfPages) {
        // define the callback for each page creation
       console.log(JSON.stringify({'pages' : numOfPages}));
    })
    .catch(function(err) {
        console.log(JSON.stringify(err));
    });

