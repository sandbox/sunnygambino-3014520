var argv = process.argv;
var fs = require('fs');


// third cli arg is the file path and name, could make this more robust
var pdf_fn = argv[2];
var page_number = argv[3];

var output_dir = '/tmp';

var PDFImage = require("pdf-image").PDFImage;

var theOptions = {
    outputDirectory : output_dir,
    convertExtension : 'png'
}
theOptions.convertOptions = {};
theOptions.convertOptions["-quality"] =  "100";
theOptions.convertOptions["-flatten"] =  "";
theOptions.convertOptions["-sharpen"] =  "0x1.0";
theOptions.convertOptions["-density"] =  "300";
theOptions.convertOptions["-resize"] =  "1024x768^";

var pdfImage = new PDFImage(pdf_fn, theOptions);

pdfImage.convertPage(page_number)
    .then(function (imagePath) {
        console.log(JSON.stringify({image_path : imagePath}));
    })
    .catch(function(err) {
        console.log(JSON.stringify(err));
    });
