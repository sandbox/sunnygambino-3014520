<?php

class CAViewer {


  /**
   * checks to see whether user has access to a presentation or documents
   * via email reference key
   *
   * @param string $aid attachment id
   * @param string $ref
   * @param NULL | string $eid
   * @param bool $isDocument
   *
   * @return bool
   */
  static function check_presentation_reference ($aid, $ref, $eid = NULL, $isDocument = FALSE) {
    static $access = NULL; // only want to do heavy lifting of checking once
    if ($access === NULL) {
      $access = FALSE;
      $table = 'field_data_field_reference';
      $reference_column = 'field_reference_value';
      $emails = FALSE;
      // old emails will not have an eid attached
      // so will need two queries
      $emails = self::get_email_node($ref, $eid);

      // did the email node load correctly
      if ($emails !== FALSE) {
        foreach($emails as $email) {

          // get the node's references, and attachments to verify against
          // get queries

          // are we checking the reference for a document or a presentation
          $field = ($isDocument) ? 'field_email_documents' : 'field_email_presentations';

          $email_node_ref = (isset($email->field_reference[LANGUAGE_NONE])) ?
            $email->field_reference[LANGUAGE_NONE][0]['value'] : NULL;

          $attachments = (isset($email->{$field}[LANGUAGE_NONE])) ?
            $email->{$field}[LANGUAGE_NONE][0]['value'] : NULL;

 
          if ($email_node_ref !== NULL && $attachments !== NULL) {
            $attachments = explode(',', $attachments);
            $is_presentation_matched = in_array($aid, $attachments);
            $is_ref_matched = ($ref === $email_node_ref);
            if ($is_presentation_matched && $is_ref_matched) {
              $access = TRUE;
  	    }
          }
        }
      }
    }

    return $access;
  }

  /**
   * loads up email node from reference and or entity id
   * @param $ref
   * @param null $eid
   *
   * @return bool|mixed
   */

  static function get_email_node ($ref, $eid = NULL) {
    $table = 'field_data_field_reference';
    $reference_column = 'field_reference_value';
    $email = FALSE;
    //if ($eid !== NULL) {
      //$email = node_load($eid);
    //} else {
      $query = db_select($table, 't');
      $query->condition($reference_column, $ref);
      $query->fields('t', ['entity_id']);
      $results = $query->execute()->fetchAll();
      $eid = (isset($results[0]->entity_id)) ? $results[0]->entity_id : NULL;
      $nids = array();
      foreach($results as $result) {
        $nids[] = $result->entity_id;
      }
      $email = node_load_multiple($nids);
    //}
    return $email;
  }
}
