<?php

class CADevice{

  /**
   * entity
   * @var stdClass
   */
  private $entity = NULL;

  /**
   * handle the problem if no entity found in database
   * @param $name
   *
   * @return null
   */
  public function __get($name) {

    // we return NULL to all properties if entity is NULL
    if ($this->entity === NULL) {
      return NULL;
    } else {
      return $this->{$name};
    }

  }


  /**
   * updates app info field
   * @param $app_info
   */
  public function set_app_info($app_info = NULL) {
    // field_device_key_app_info
    if ($this->entity !== NULL && $app_info !== NULL) {
      // keep as JSON as dont think you can serialise and store in textfield
      $this->entity->field_device_key_app_info->set($app_info);
      $this->entity->save();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * updates last accessed field
   *
   * @return bool
   */
  public function set_access() {
    $updated = time();
    if ($this->entity !== NULL) {
      $this->entity->field_device_key_access_code->set($updated);
      $this->entity->save();
      return TRUE;
    }
    return FALSE;
  }

  /**
   * checks to see whether the account logged in is owner
   * @return bool
   */
  public function is_owner () {
    global $user;
    $is_owner = FALSE;
    if($this->entity !== NULL) {
      $current_user = $this->entity->field_device_key_user->value();
      $is_owner = ($current_user->uid === $user->uid);
    }

    return $is_owner;
  }


  /**
   * checks to see if the entity exists
   * @return bool
   */
  public function exists () {
    $exists = ($this->entity !== NULL);
    return $exists;
  }

  public function __construct($access_code, $account) {

    $this->access_code = $access_code;
    $this->account = $account;

    // load up the device key in question
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node', '=')
      ->entityCondition('bundle', 'device_key', '=')
      ->propertyCondition('status', NODE_PUBLISHED)
      ->fieldCondition('field_device_key_user',
        'target_id', $account->uid, '=')
      ->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT')
      ->fieldCondition('field_device_key_access_code', 'value',
        $access_code, '=');

    $results = $query->execute();

    if (count($results['node']) > 0) {
      $result = array_pop($results['node']);
      $nid = (isset($result->nid)) ? $result->nid : NULL;
      $this->entity = ($nid !== NULL) ?
        entity_metadata_wrapper('node', $nid) : NULL;
    }
  }
}