window.run = false;

(function ($) {
    Drupal.behaviors.background_processes = {
        attach: function (context, settings) {
            var self = this;
            self.interval = 0;
            $('.view-presentations', context).once('background_processes', function () {
                self.getBatch();
                setTimeout(function() {self.refreshBatch()},1500);
            });
        },

        getBatch: function() {
            var self = this;
            $.ajax({
                url: '/presentations/status',
                type: 'get',
                dataType: 'json',
                beforeSend: function() {
                },
                success: function (response) {
                    console.log('background_processes response',response);
                    var batch_nids = response;
                    var site_nids = self.getPresentationIDs();
                    //var nids = self.filterNids(site_nids,batch_nids);
                    var nids = Object.keys(response);
                    self.markPresentation(nids,response);
                },
                error: function(e) {
                    console.log('background_processes error',e);
                    $('tr.publishing').removeClass('publishing');
                    $('.background-process-overlay').each(function(){
                        $(this).remove();
                    });

                }
            });
        },

        refreshBatch: function() {
            var self = this;
            $.ajax({
                url: '/presentations/status',
                type: 'get',
                dataType: 'json',
                beforeSend: function() {
                },
                success: function (response) {
                    if(response.length === 0) {
                        clearInterval(self.interval);
                        $('.info-wrapper').each(function() {
                            $(this).remove();
                        });

                        $('tr.publishing').find('.background-process-overlay').each(function() {
                            $(this).remove();
                        });

                        $('tr.publishing').each(function() {
                            $(this).removeClass('publishing');
                        });

                        return false;
                    };

                    console.log('background_processes response',response);
                    var batch_nids = response;
                    var site_nids = self.getPresentationIDs();
                    var nids = Object.keys(response);
                    self.markPresentation(nids,response);
                    $('.info-wrapper').each(function() {
                        $(this).remove();
                    });

                    $('.ref-inf').each(function(i,v) {
                        $(nids).each(function(i2,v2) {
                            if($(v).attr('title') == v2) {
                                self.addInfo(response,v2);
                            }
                        });
                    });
                    if(self.interval == 0) {
                        self.interval = setInterval(function() {self.refreshBatch();},5000);
                    }
                },
                error: function(e) {
                    console.log('background_processes error',e);
                }
            });
        },

        getPresentationIDs: function() {
            var nids = new Array();
            $('body').find('.ref-inf').each(function(i,v) {
                nids.push($(v).attr('title'));
            });
            return nids;
        },

        filterNids: function(site_nids,batch_nids) {
            var nids = [];

            $(batch_nids).each(function(i,v) {
                if($.inArray(v[v.length-1],site_nids) !== -1) {
                    nids.push(v[v.length-1]);
                }
            });

            return nids;
        },

        markPresentation: function(nids,response) {
            if(nids.length < 1){
                return;
            }

            var self = this;
            $('tr.publishing').each(function(idx,val) {
                var row = $(this);

                row.find('.background-process-overlay').each(function() {
                    if($(this).html().length == 0) {
                        $(this).remove();
                        row.removeClass('publishing');
                    }
                });
            });

            $('.ref-inf').each(function(i,v) {
                $(nids).each(function(i2,v2) {
                    if($(v).attr('title') == v2) {
                        $(v).closest('tr').addClass('publishing');

                        var $row = $(v).closest('tr');
                        var $management = $(v).closest('tr').find('.management');
                        var width = $management.outerWidth();
                        var height = $row.outerHeight();

                        var html = self.getMarkup($(v).closest('tr'),v2);
                        if(typeof html !== "undefined") {
                            $(v).closest('tr').append(html);
                            self.addInfo(response,v2);
                        }

                        $('.background-process-overlay').each(function(){
                          var $parent = $(this).parent('tr');
                          $(this).css({
                            'width' : width + 'px',
                            'height' : height + 'px',
                            'left' : ($row.width() - width ) + 10 + 'px'
                          })
                        });
                    }
                });
            });
        },

        getMarkup: function(element,pid) {
            var html = '<td class="background-process-overlay" data-presentation-id="'+pid+'">'+
                          '<div class="background-process-info-wrapper" data-presentation-id="'+pid+'">'+'</div>'+
                       '</td>';

            if($(element).find('.background-process-overlay').length === 0) {
                return html;
            }
            return;
        },

        cloneRow: function(element){

            var $copy = $(element).find('.existing-data');

            $(element).find('td.views-field-thumbnail').clone().prependTo( $copy );
            $(element).find('td.views-field-thumbnail').addClass('hide-element');

            $(element).find('td.views-field-title').clone().prependTo( $copy );
            $(element).find('td.views-field-title').addClass('hide-element');

            $(element).find('td.presentation-status-section').clone().prependTo( $copy )
            $(element).find('td.presentation-status-section').addClass('hide-element');

            $(element).find('td.management').clone().prependTo( $copy );
            $(element).find('td.management').addClass('hide-element');

            $copy.addClass('copied');
        },

        addInfo: function(response,pid) {

            var self = this;
            $('.info-wrappper').remove();
            var html = '';
            $('tr').find('.background-process-info-wrapper').each(function(i,v) {
                var parentelement = $(this).parent();
                var row = $(parentelement).parent();
                if($(v).attr('data-presentation-id') == pid) {
                    $(response).each(function(i2,v2) {
                        if(v2[pid][5] == $(v).attr('data-presentation-id')) {
                            var batchid = v2[pid][0];
                            var progressValue = v2[pid][1];
                            var progressText = v2[pid][2];
                            var start = v2[pid][3];
                            var end = v2[pid][4];
                            var id = v2[pid][5];

                            progressValue = parseInt(progressValue.replace('%', '')).toFixed();
                            progressText = progressText.replace('<br/>', ' ');
                            progressText = progressText.replace('<em', '<br /><em');

                            html += '<div class="progress-wrapper" class="clearfix">';
                              html += '<div class="progress-action">';
                                html += '<div class="current-value">Publishing: ' + progressValue + '%</div>';
                                html += '<div class="abort-wrapper">';
                                  html += '<div data-bid="'+v2[pid][0]+'" class="abort-button">Abort <i class="fa fa-ban"></i></div>';
                                html += '</div>';
                              html += '</div>';
                              html += '<div class="progress-bar">';
                                html += '<div class="done" style="width: ' + progressValue + '%;"></div>';
                                html += '<div class="remaining" style="width:' + (100 - progressValue) + '%;"></div>';
                              html += '</div>';
                              html += '<div class="info-wrapper">';
                                html += progressText;
                              html += '</div>';
                            html += '</div>';
                        }
                    });
                    $(v).html(html);
                    self.bindStop();
                }
            });
        },

        bindStop: function() {
            var self = this;
            $('.abort-button').each(function(i,v){
                $(v).bind('click',function(e) {
                    var bid = $(this).attr('data-bid');
                    $.ajax({
                        url: '/presentations/status_delete',
                        type: 'post',
                        data: {bid},
                        dataType: 'json',
                        beforeSend: function() {
                        },
                        success: function (response) {
                            console.log(response);
                        },
                        error: function(e) {
                            console.log('background_processes error',e);
                        }
                    });
                });
            });
        }
    }
}(jQuery));
