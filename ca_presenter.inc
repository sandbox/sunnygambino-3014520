<?php
function get_slide_types() {
    $sql = "SELECT data FROM {field_config} where field_name = 'field_slides'";
    $query = db_query($sql);
    $results = FALSE;
    if(isset($query)) {
      if($query->rowCount() === 1) {
        $results = $query->fetchAll();
      }
    }
    return $results;
}

function get_slide_content_types() {
  $cid = 'ca_slide_content_types';
  $slide_content_types = cache_get($cid);

  if (!$slide_content_types) {

    //Make list of slide types
    $slide_types = '';
    $content_types_vocab = variable_get('ca_presenter_content_types_vocab', 0);
    if ($content_types_vocab == 0) {
      return ['error, no content_types vocab available'];
    }
    $node_types = node_type_get_types();
    $sql = "SELECT data FROM {field_config} where field_name = 'field_slides'";
    $query = db_query($sql);
    if (isset($query)) {
      if ($query->rowCount() === 1) {
        $results = $query->fetchAll();
        $slide_types = unserialize($results[0]->data)['settings']['handler_settings']['target_bundles'];
      }
    }
    $slide_content_types = [];
    foreach ($node_types as $node_type) {
      if (in_array($node_type->type, $slide_types)) {
        $query2 = new EntityFieldQuery();
        $query2->entityCondition('entity_type', 'taxonomy_term')
          ->propertyCondition('vid', $content_types_vocab)
          ->fieldCondition('field_taxonomy_content_type', 'value', $node_type->type);

        $terms = $query2->execute();

        $tids = [];
        foreach ($terms as $term) {
          $tids = array_keys($term);
        }

        $additional_infos = [];
        foreach ($tids as $tid) {
          $vocab = taxonomy_term_load($tid);
          $category = $vocab->name;
        }

        $slide_content_types[$node_type->type] = $node_type->name;

      }
      cache_set($cid, $slide_content_types);
    }
    // @todo Is this request static enough to cache as it appears to be called twice on
    // form loads
  } else {
    $slide_content_types = $slide_content_types->data;
  }

  return $slide_content_types;
}

/*function get_slide_content_vocabularies(){

  $type = 'content_types';
  $vocabulary = taxonomy_vocabulary_machine_name_load($type);
  $tree = taxonomy_get_tree($vocabulary->vid);
  $vocabularies = array();
  $slides = array();

  foreach ($tree as $term) {
    $id = $term->tid;
    $category = $term->name;
    $fields = taxonomy_term_load($id);
    $content_types = field_get_items('taxonomy_term', $fields, 'field_taxonomy_content_type');

    foreach( $content_types as $type ) {
      $vocabularies[] = array(
        'id' => $id,
        'category' => $category,
        'type' => $type['value']
      );

    }
  }
  return $vocabularies;
}*/

function FileSizeConvert($bytes)
{
    $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

    foreach($arBytes as $arItem)
    {
        if($bytes >= $arItem["VALUE"])
        {
            $result = $bytes / $arItem["VALUE"];
            $result = str_replace(".", "," , strval(round($result, 2)))." ".$arItem["UNIT"];
            break;
        }
    }
    return $result;
}

?>
