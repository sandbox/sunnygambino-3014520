<?php
/**
 * @file
 * image_uploader_content_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function image_uploader_content_type_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_advanced|node|image_uploader|form';
  $field_group->group_name = 'group_advanced';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_uploader';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Advanced',
    'weight' => '10',
    'children' => array(
      0 => 'field_bg_colour',
      1 => 'field_cover_thumbnail',
      2 => 'field_css_editor',
      3 => 'field_disable_nav_back',
      4 => 'field_disable_nav_forward',
      5 => 'field_js_editor',
      6 => 'field_resource_advanced',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-advanced field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_advanced|node|image_uploader|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_node_info|node|image_uploader|form';
  $field_group->group_name = 'group_node_info';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_uploader';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Node information',
    'weight' => '11',
    'children' => array(
      0 => 'field_archive_hash',
      1 => 'field_last_updated_timestamp',
      2 => 'field_last_updated_uid',
      3 => 'field_migrate_original_nid',
      4 => 'field_primary_statuses',
      5 => 'field_secondary_statuses',
      6 => 'field_status',
      7 => 'path',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-node-info field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_node_info|node|image_uploader|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_options|node|image_uploader|form';
  $field_group->group_name = 'group_options';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_uploader';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Options',
    'weight' => '4',
    'children' => array(
      0 => 'field_disable_navbar',
      1 => 'field_exclude_sendable',
      2 => 'field_fullscreen',
      3 => 'field_viewer_size',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-options field-group-fieldset',
        'required_fields' => 1,
        'id' => '',
      ),
    ),
  );
  $field_groups['group_options|node|image_uploader|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_slides|node|image_uploader|form';
  $field_group->group_name = 'group_slides';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'image_uploader';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Slides',
    'weight' => '9',
    'children' => array(
      0 => 'field_slides',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Slides',
      'instance_settings' => array(
        'required_fields' => 1,
        'id' => '',
        'classes' => 'group-slides field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'collapsed',
    ),
  );
  $field_groups['group_slides|node|image_uploader|form'] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Advanced');
  t('Node information');
  t('Options');
  t('Slides');

  return $field_groups;
}
