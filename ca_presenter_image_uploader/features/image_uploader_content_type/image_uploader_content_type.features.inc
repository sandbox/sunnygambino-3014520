<?php
/**
 * @file
 * image_uploader_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function image_uploader_content_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function image_uploader_content_type_node_info() {
  $items = array(
    'image_uploader' => array(
      'name' => t('image uploader'),
      'base' => 'node_content',
      'description' => t('Create a presentation from set of images'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
