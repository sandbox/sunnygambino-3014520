<?php
/**
 * @file
 * image_uploader_content_type.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function image_uploader_content_type_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_slide_type'.
  $permissions['create field_slide_type'] = array(
    'name' => 'create field_slide_type',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit field_slide_type'.
  $permissions['edit field_slide_type'] = array(
    'name' => 'edit field_slide_type',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_slide_type'.
  $permissions['edit own field_slide_type'] = array(
    'name' => 'edit own field_slide_type',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view field_slide_type'.
  $permissions['view field_slide_type'] = array(
    'name' => 'view field_slide_type',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_slide_type'.
  $permissions['view own field_slide_type'] = array(
    'name' => 'view own field_slide_type',
    'roles' => array(),
    'module' => 'field_permissions',
  );

  return $permissions;
}
