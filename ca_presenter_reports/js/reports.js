(function ($) {
  Drupal.behaviors.ca_presenter_reports = {
    attach: function (context, settings) {
      this.filters();
    },
    filters: function() {
      $('button.report-filter-button.apply').on('click',function(e) {
        var name = $('input.name-filter').val();
        var category = $('select.category-filter').val();
        $('tr').each(function(i,v) {
          $(this).show();
        });
        $('tr').each(function(i,v) {
          if(
              (
                typeof $(this).attr('data-name') !== 'undefined' && 
                $(this).attr('data-name').toLowerCase().indexOf(name.toLowerCase()) === -1 && 
                name.length > 0
              ) || (
                typeof $(this).attr('data-category') !== 'undefined' && 
                $(this).attr('data-category') !== category && 
                category.length > 0
              )
            ) {
            $(this).hide();
          }
        });
      });
      $('button.report-filter-button.clear').on('click',function(e) {
        $('input.name-filter').val('');
        $('select.category-filter').val('');
        $('tr').each(function(i,v) {
          $(this).show();
        });
      });
    }
  };
})(jQuery);
