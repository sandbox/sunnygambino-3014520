<?php
function ca_presenter_reports_views_pre_execute(&$view) {
   if($view->name=="reports" && $view->current_display == 'presentation_avg_view_time') {
     $query = $view->build_info['query'];
     $query->addExpression('SEC_TO_TIME(ROUND(AVG(R2.field_received_value - R.field_received_value)))','avarage_view_time');
     $query->leftJoin('field_data_field_received','R','node.nid = R.entity_id');
     $query->innerJoin('field_data_field_received','R2','R2.revision_id = (R.revision_id + 1)');
     $view->build_info['query'] = $query;
      return $view;
   }
   if($view->name=="reports" && $view->current_display == 'slide_avg_view_time') {
     $query = $view->build_info['query'];
     $query->addExpression('SEC_TO_TIME(ROUND(AVG(R2.field_received_value - R.field_received_value)))','avarage_view_time');
     $query->leftJoin('field_data_field_received','R','node.nid = R.entity_id');
     $query->innerJoin('field_data_field_received','R2','R2.revision_id = (R.revision_id + 1)');
     $view->build_info['query'] = $query;
      return $view;
   }
   if($view->name=="reports" && $view->current_display == 'presentation_sum_view_time') {
     $query = $view->build_info['query'];
     $query->addExpression('SEC_TO_TIME(ROUND(SUM(R2.field_received_value - R.field_received_value)))','sum_view_time');
     $query->leftJoin('field_data_field_received','R','node.nid = R.entity_id');
     $query->innerJoin('field_data_field_received','R2','R2.revision_id = (R.revision_id + 1)');
     $view->build_info['query'] = $query;
      return $view;
   }
   if($view->name=="reports" && $view->current_display == 'slide_sum_view_time') {
     $query = $view->build_info['query'];
     $query->addExpression('SEC_TO_TIME(ROUND(SUM(R2.field_received_value - R.field_received_value)))','sum_view_time');
     $query->leftJoin('field_data_field_received','R','node.nid = R.entity_id');
     $query->innerJoin('field_data_field_received','R2','R2.revision_id = (R.revision_id + 1)');
     $view->build_info['query'] = $query;
      return $view;
   }
   if($view->name=="reports" && $view->current_display == 'slide_sum_view_time') {
     $query = $view->build_info['query'];
     $query->addExpression('SEC_TO_TIME(ROUND(SUM(R2.field_received_value - R.field_received_value)))','sum_view_time');
     $query->leftJoin('field_data_field_received','R','node.nid = R.entity_id');
     $query->innerJoin('field_data_field_received','R2','R2.revision_id = (R.revision_id + 1)');
     $view->build_info['query'] = $query;
      return $view;
   }
   if($view->name=="reports" && $view->current_display == 'session_sum_view_time') {
     $query = $view->build_info['query'];
     $query->addExpression('SEC_TO_TIME(ROUND(SUM(R2.field_received_value - R.field_received_value)))','sum_view_time');
     $query->leftJoin('field_data_field_received','R','node.nid = R.entity_id');
     $query->innerJoin('field_data_field_received','R2','R2.revision_id = (R.revision_id + 1)');
     $view->build_info['query'] = $query;
      return $view;
   }
}
