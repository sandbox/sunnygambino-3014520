<?php
/**
 * VBO actions - Thumbnail generation etc...
 */
function ca_presenter_action_info() {
  return [
    'ca_presenter_generate_thumbnail' => [
      'type' => 'node',
      'label' => t('Generate Thumbnail'),
      'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
    ],
    'ca_presenter_clone_slide' => [
      'type' => 'node',
      'label' => t('Clone'),
      'behavior' => ['changes_property'],
      'configurable' => TRUE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
      'permissions' => ['administer site configuration'],
    ],
    'ca_presenter_reset_access' => [
      'type' => 'node',
      'label' => t('Change access'),
      'behavior' => ['changes_property'],
      'configurable' => TRUE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
      'permissions' => ['administer site configuration'],
    ],
    'ca_presenter_detach_slide' => [
      'type' => 'node',
      'label' => t('Delete'),
      'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
    ],
    'ca_presenter_delete_presentation' => [
      'type' => 'node',
      'label' => t('Delete Presentation'),
      'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
      'permissions' => ['administer site configuration'],
    ],
    'ca_presenter_regenerate_book' => [
      'type' => 'node',
      'label' => t('Regenerate book JSON'),
      'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
      'permissions' => ['administer site configuration'],
    ],
    'ca_presenter_attach_slide' => [
      'type' => 'node',
      'label' => t('Insert slides'),
      //'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
    ],
    'ca_presenter_verify_aws_ses' => [
      'type' => 'user',
      'label' => t('Verify emails on AWS SES'),
      //'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
    ],
    'ca_presenter_draft_slide' => [
      'type' => 'node',
      'label' => t('Draft slide'),
      //'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
    ],
    'ca_presenter_undraft_slide' => [
      'type' => 'node',
      'label' => t('Undraft slide'),
      //'behavior' => ['changes_property'],
      'configurable' => FALSE,
      'vbo_configurable' => FALSE,
      'triggers' => ['any'],
    ],
  ];
}
/**
 * VBO function for generating thumbnails
 *
 * @param $node
 * @param $context
 */
function ca_presenter_generate_thumbnail(&$node, $context) {
  $pid = $context['view_info']['arguments'][0];
  $presentation = node_load($pid);
  _create_thumbnail_main($presentation, $node, 10);
}

/**
 * VBO actions - Slide detach
 *
 * @param $node
 * @param $context
 */
function ca_presenter_detach_slide(&$node, $context) {
  $pid = $context['view_info']['arguments'][0];
  $presentation = node_load($pid);
  $slides = $presentation->field_slides['und'];

  foreach ($slides as $key => $slide) {
    if ($slide['target_id'] == $node->nid) {
      unset($slides[$key]);
    }
  }
  $s = [];
  foreach ($slides as $key => $slide) {
    if (!empty($slide['target_id']) && $slide['target_id'] != '') {
      $s[] = $slide;
    }
  }
  $slides = $s;
  $presentation->field_slides['und'] = array();
  $presentation->field_slides['und'] = $slides;
  node_save($presentation);
  $status = $presentation->filed_primary_statuses['und'][0]['tid'];
  $node_wrapper = entity_metadata_wrapper('node', $presentation);
  $slides = $node_wrapper->field_slides->value();
  _generate_book_json($presentation, $node_wrapper, $slides, $status, $changed = TRUE);
}

/**
 * VBO actions - Presentation delete
 *
 * @param $node
 * @param $context
 */
function ca_presenter_delete_presentation(&$node, $context) {
  $pid = $context['node']->nid;
  $presentation = node_load($pid);
  $slides = $presentation->field_slides['und'];

  foreach ($slides as $key => $slide) {
    if($slide->field_master_presentation[LANGUAGE_NONE][0]['target_id'] == $pid)
      node_delete($slide['target_id']);
  }
  node_delete($pid);
}

/**
 * VBO actions - Regenerate book JSON
 *
 * @param $node
 * @param $context
 */
function ca_presenter_regenerate_book(&$node, $context) {
  $pid = $context['node']->nid;
  $presentation = node_load($pid);

  $node_wrapper = entity_metadata_wrapper('node', $presentation);
  $slides = $node_wrapper->field_slides->value();
  @_generate_book_json_main($presentation,$node_wrapper,$slides,  taxonomy_term_load($node_wrapper->field_primary_statuses->value()),TRUE,TRUE);

}

/**
 * VBO actions - Slide attach
 *
 * @param $node
 * @param $context
 *
 * @return bool
 */
function ca_presenter_attach_slide(&$node, $context) {
  global $subdomain;
  $referer = $_SERVER['HTTP_REFERER'];
  $url_path = parse_url($referer,PHP_URL_PATH);
  $exploded_url_path = explode('/',$url_path);
  $pid = $exploded_url_path[2];
  $position = $exploded_url_path[6];
  if(isset($_SESSION) && isset($_SESSION['_add_slide_from_library']['pid']))
    $pid = $_SESSION['_add_slide_from_library']['pid'];
  if(isset($_SESSION) && isset($_SESSION['_add_slide_from_library']['position']))
    $position = $_SESSION['_add_slide_from_library']['position'];
  $presentation = node_load($pid);
  if (!$presentation || $presentation->type != 'presentation') {
    return FALSE;
  }
  $_SESSION['_add_slide_from_library']['slides'][] = $node->nid;
  //When batch finished, save all slides to presentation
  if ($context['progress']['current'] == $context['progress']['total']) {
    $_SESSION['_add_slide_from_library']['slides'] = array_unique($_SESSION['_add_slide_from_library']['slides']);
    $export_path = 'private://' . $subdomain . '/ca_presenter/presentations/' . $presentation->nid;
    $thumbnail_dir = drupal_realpath($export_path) . '/__thumbnails';
    if(!is_dir($thumbnail_dir)) {
      mkdir($thumbnail_dir,0775);
    }
    $target_ids = [];
    foreach ($_SESSION['_add_slide_from_library']['slides'] as $new_slide) {
      $target_ids[] = [
        'target_id' => $new_slide,
      ];
      $slide_instance = node_load($new_slide);
      $library_presentation_path = drupal_realpath('private://'.$subdomain.'/ca_presenter/presentations/'.$slide_instance->field_master_presentation[LANGUAGE_NONE][0]['target_id']);
      $library_slide_thumbnail_path = $library_presentation_path . '/__thumbnails/'.$slide_instance->nid.'.jpg';
      copy($library_slide_thumbnail_path,$thumbnail_dir.'/'.$slide_instance->nid.'.jpg');
    }
    if (!isset($presentation->field_slides['und'])) {
      $presentation->field_slides['und'] = [];
    }
    array_splice($presentation->field_slides['und'], $position, 0, $target_ids);
    node_save($presentation);
    $status = $presentation->field_primary_statuses['und'][0]['tid'];
    $node_wrapper = entity_metadata_wrapper('node', $presentation);
    $slides = $node_wrapper->field_slides->value();
    @_generate_book_json($presentation, $node_wrapper, $slides, $status, $changed = TRUE);
    return TRUE;
  }
  return FALSE;
}

/**
 * VBO actions - Slide clone
 *
 * @param $node
 * @param $context
 */
function ca_presenter_clone_slide(&$node, $context) {
  global $user, $subdomain, $published_full;
  //fix for motherfucker advanced slides
  if(isset($context['node']->field_resource_advanced) && isset($context['node']->field_resource_advanced[LANGUAGE_NONE])) {
    foreach($context['node']->field_resource_advanced[LANGUAGE_NONE] as $k => $v) {
      $fid = $context['node']->field_resource_advanced[LANGUAGE_NONE][$k]['fid'];
      $context['node']->field_resource_advanced[LANGUAGE_NONE][$k] = array('fid' => $fid, 'display' => '1');
    }
  }

  $pid = $context['view_info']['arguments'][0];
  $sid = $node->nid;
  $presentation = node_load($pid);
  $new_presentation_title = $context['presentation_title'];
  $new_presentation_theme = $context['presentation_theme'];
  $new_presentation_category = $context['presentation_category'];

  $existing_presentation_id = $context['presentation'];
  $existing_presentation_slide_placement = $context['slide_placement'];
  $existing_presentation_slide_number = $context['slide_number'];

  $original_pid = $context['presentation_id'];

  $mode = '';
  if ($new_presentation_title != '' || $new_presentation_theme != ''
    || $new_presentation_category != '') {
    $mode = 'new';
  }

  if ($existing_presentation_id != '' && $existing_presentation_slide_placement != '') {
    $mode = 'existing';
    $new_slide = $context['node'];
    $new_slide->is_new = TRUE;
    $new_slide->field_master_presentation['und'][0]['target_id'] = $existing_presentation_id;
    unset($new_slide->nid);
    unset($new_slide->vid);
    unset($new_slide->tnid);
    node_save($new_slide);
    $existing_presentation = node_load($existing_presentation_id);
    _create_thumbnail_main($existing_presentation, $new_slide, 10);
    $_SESSION['clone_slides'][] = $new_slide->nid;
  }

  //Clone into new presentation
  if ($mode == 'new') {
    $new_presentation = NULL;
    $node_wrapper = entity_metadata_wrapper('node', node_load($context['clone_new_presentation']));
    $new_slide = $context['node'];
    $new_slide->is_new = TRUE;
    if ($node_wrapper->nid->value() !== NULL) {
      $new_slide->field_master_presentation['und'][0]['target_id'] = $node_wrapper->nid->value();
    }
    unset($new_slide->nid);
    unset($new_slide->vid);
    unset($new_slide->tnid);
    node_save($new_slide);
    $_SESSION['clone_slides'][] = $new_slide->nid;

    //Recreate the slide
    $node_wrapper->field_slides[] = $new_slide;
    if ($new_presentation_theme != '') {
      $node_wrapper->field_theme = $new_presentation_theme;
    }
    if ($new_presentation_category != '') {
      $node_wrapper->field_taxonomy = $new_presentation_category;
    }
    $node_wrapper->save();
    //Just to be sure reload this new presentation
    $new_presentation = node_load($node_wrapper->nid->value());
    $slides = $node_wrapper->field_slides->value();
    $status = $new_presentation->field_primary_statuses['und'][0]['tid'];
    _generate_book_json($new_presentation, $node_wrapper, $slides, $status, $changed = TRUE);
    _create_thumbnail_main($new_presentation, $new_slide, 10);
  }

  if ($mode == 'existing' && ($context['progress']['current'] == $context['progress']['total'])) {
    $presentation = node_load($existing_presentation_id);
    if (!isset($presentation->field_slides['und'])) {
      $presentation->field_slides['und'] = [];
    }
    if ($existing_presentation_slide_placement == '1') { //place slides to the beginning
      $beginning = [];
      foreach ($_SESSION['clone_slides'] as $slide_id) {
        $beginning[] = ['target_id' => $slide_id];
      }
      $slides = $presentation->field_slides['und'];
      foreach ($slides as $k => $v) {
        $beginning[] = $v;
      }
      $presentation->field_slides['und'] = $beginning;
      node_save($presentation);
      $presentation = node_load($presentation->nid,NULL,TRUE);
    }
    if ($existing_presentation_slide_placement == '2') { //place slides to the end
      $end = $presentation->field_slides['und'];
      foreach ($_SESSION['clone_slides'] as $slide_id) {
        $end[] = ['target_id' => $slide_id];
      }
      // @todo work out what is meant by adding something to node wrapper is meant to do
      //  $node_wrapper->field_slides[] = $node;
      $presentation->field_slides['und'] = $end;
      node_save($presentation);
      $presentation = node_load($presentation->nid,NULL,TRUE);
    }
    if ($existing_presentation_slide_placement == '3') { //place slides after a specified slide
      $begin = [];
      for ($i = 0; $i < $existing_presentation_slide_number; $i++) {
        $begin[] = $presentation->field_slides['und'][$i];
      }
      foreach ($_SESSION['clone_slides'] as $slide_id) {
        $begin[] = ['target_id' => $slide_id];
      }
      for ($k = $i; $k < count($presentation->field_slides['und']); $k++) {
        $begin[] = $presentation->field_slides['und'][$k];
      }
      $presentation->field_slides['und'] = $begin;
      node_save($presentation);
      $presentation = node_load($presentation->nid,NULL,TRUE);
    }

    $primary_status =
      isset($presentation->field_primary_statuses[LANGUAGE_NONE][0]['tid']) ?
        $presentation->field_primary_statuses[LANGUAGE_NONE][0]['tid'] :
        NULL;
    if ($primary_status !== NULL &&
      $published_full->tid === $primary_status) {
      // do publishing routine
      // the export resources function has been extrapolated
      // so can do a single page without batch
      $status = $presentation->filed_primary_statuses['und'][0]['tid'];
      $node_wrapper = entity_metadata_wrapper('node', $presentation);
      $slides = $node_wrapper->field_slides->value();
      _generate_book_json($presentation, $node_wrapper, $slides, $status, $changed = TRUE);
      _export_resource($presentation, $node);

      $export_path = 'private://' . $subdomain . '/ca_presenter/presentations/' . $presentation->nid;

      _build_archive($presentation, $export_path);
      _create_thumbnail_main($presentation, $slides, 10);
      $node_wrapper->field_last_updated_timestamp->set(time());
      $node_wrapper->save();
    } else {
      $status = $presentation->filed_primary_statuses['und'][0]['tid'];
      $node_wrapper = entity_metadata_wrapper('node', $presentation);
      $slides = $node_wrapper->field_slides->value();
      _generate_book_json($presentation, $node_wrapper, $slides, $status, $changed = TRUE);
      $node_wrapper->field_last_updated_timestamp->set(time());
      $node_wrapper->save();
    }
  }
}

/**
 * VBO actions - Reset nodeaccess
 *
 * @param $node
 * @param $context
 */
function ca_presenter_reset_access(&$node, $context) {
  ob_start();
  print '<pre>';
  var_dump($context['rid']);
  print '<pre>';
  $r = ob_get_clean();
  watchdog('reset_access',$r);
  foreach($context['rid'] as $rid => $values) {
    if($values['rid']['double_check'] == '1') {
      watchdog('reset_access','Change this role : ' . $rid);
      db_delete('nodeaccess')
        ->condition('nid', $node->nid)
        ->condition('realm', 'nodeaccess_rid')
        ->condition('gid',$rid)
        ->execute();

      db_insert('nodeaccess')
        ->fields(array(
          'nid' => $node->nid,
          'gid' => $rid,
          'realm' => 'nodeaccess_rid',
          'grant_view' => $values['rid']['view'],
          'grant_update' => $values['rid']['edit'],
          'grant_delete' => $values['rid']['delete'],
        ))
        ->execute();
    }
  }
}

function ca_presenter_reset_access_form($settings, &$form_state) {
  $data = array();
  $form = array();
  $form['rid'] = array('#tree' => TRUE);
  $help = '
    <h1>Change accesses on multiple presentations</h1>
    <p>You have been selected a few presentations so here you have to decide what do you want to change in terms of access.</p>
    <p>Select roles by clicking on fieldsets. If you want to change accesses in terms of this role, please tick "<b>Do you really want to change permission for authenticated user role?</b>" checkbox first. 
    Select permission what you want to add to this role. You can select from view/edit/delete access. If you leave view/edit/delete empty then you will revoke all the access to presentations.
    </p>
    <p>So in one sentence: you will change access permissions for presentations what you have selected before.</p>
  ';
  $form['rid']['#prefix'] = $help;
  //Load all roles
  $sql = "SELECT * FROM role as r";
  $q = db_query($sql);
  foreach($q->fetchAll() as $res) {
    $form['rid'][$res->rid]['rid'] = array(
      '#type' => 'fieldset',
      '#title' => $res->name,
      '#collapsible' => TRUE, 
      '#collapsed' => TRUE
    );
    $form['rid'][$res->rid]['rid']['double_check'] = array(
      '#type' => 'checkbox',
      '#title' => '<b>'. t('Do you really want to change permission for') . ' ' . $res->name . ' ' . t('role') . '?' . '</b>'
    );
    $form['rid'][$res->rid]['rid']['view'] = array(
      '#type' => 'checkbox',
      '#title' => 'view'
    );
    $form['rid'][$res->rid]['rid']['edit'] = array(
      '#type' => 'checkbox',
      '#title' => 'edit'
    );
    $form['rid'][$res->rid]['rid']['delete'] = array(
      '#type' => 'checkbox',
      '#title' => 'delete'
    );
  }
  return $form;
}

function ca_presenter_reset_access_submit($form,$form_state) {
  return($form_state['values']);
}

function ca_presenter_verify_aws_ses(&$user, $context) {
  sleep(3);
  ca_presenter_emails_aws_check($user->mail);
}

function ca_presenter_draft_slide(&$node,$context) {
  global $user;
  $pid = $context['view_info']['arguments'][0];
  $presentation = node_load($pid);
  if(!$presentation || $presentation->type != 'presentation')
    return FALSE;
  if($presentation->uid != $user->uid)
    return FALSE;
  $drafts = $presentation->field_draft_slides[LANGUAGE_NONE][0]['value'];
  $draft_slides = [];
  if(strlen($drafts) > 0) {
    $draft_slides = (array)json_decode($drafts);
  }
  array_push($draft_slides,$node->nid);
  $presentation->field_draft_slides[LANGUAGE_NONE][0]['value'] = json_encode($draft_slides);
  node_save($presentation);
  if($context['progress']['current'] == $context['progress']['total']) {
    $status = $presentation->filed_primary_statuses['und'][0]['tid'];
    $node_wrapper = entity_metadata_wrapper('node', $presentation);
    $slides = $node_wrapper->field_slides->value();
    _generate_book_json($presentation, $node_wrapper, $slides, $status, $changed = TRUE);
  }
  return TRUE;
}

function ca_presenter_undraft_slide(&$node,$context) {
  global $user;
  $pid = $context['view_info']['arguments'][0];
  $presentation = node_load($pid);
  if(!$presentation || $presentation->type != 'presentation')
    return FALSE;
  if($presentation->uid != $user->uid)
    return FALSE;
  $drafts = $presentation->field_draft_slides[LANGUAGE_NONE][0]['value'];
  $draft_slides = [];
  if(strlen($drafts) > 0) {
    $draft_slides = (array)json_decode($drafts);
  }
  if (($key = array_search($node->nid, $draft_slides)) !== FALSE) {
    unset($draft_slides[$key]);
  }
  $draft_slides = array_values($draft_slides);
  $presentation->field_draft_slides[LANGUAGE_NONE][0]['value'] = json_encode($draft_slides);
  node_save($presentation);
  if($context['progress']['current'] == $context['progress']['total']) {
    $status = $presentation->filed_primary_statuses['und'][0]['tid'];
    $node_wrapper = entity_metadata_wrapper('node', $presentation);
    $slides = $node_wrapper->field_slides->value();
    _generate_book_json($presentation, $node_wrapper, $slides, $status, $changed = TRUE);
  }
  return TRUE;
}
