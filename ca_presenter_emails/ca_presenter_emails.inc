<?php
  function ca_presenter_emails_email_access() {
    return TRUE;
  }
  /**
   * 
   * @global type $user
   * @param type $u A user id
   * @return boolean
   */
  function getTemplate($u = FALSE) {
    if($u !== FALSE) {
      $current_user = user_load($u);
    } else {
      global $user;
      $current_user = user_load($user->uid);
    }
    $selected_template = FALSE;
    if(isset($current_user->profile_email_template) && $current_user->profile_email_template != '') {
      $selected_template = node_load($current_user->profile_email_template);
    }
    if($selected_template === FALSE) {
      $nodes = db_query('SELECT nid FROM {node} WHERE type =  :type', array(
          ':type' => 'email_template',
        ))->fetchAll();
      $templates = array();
      foreach($nodes as $node) {
        $templates[] = node_load($node->nid);
      }
      //TODO: Make fail safe solution with ca_presenter admin settings.
      //      Setup a default email template and chack is there any email template content exists.
      $selected_template = $templates[0];
      //
    }
    if($selected_template === FALSE) {
      drupal_set_message(t('No email template found in the system. Please create one!','error'));
      return FALSE;
    }
    $email_body_template = (isset($selected_template->field_email_body_template['und'])) ? $selected_template->field_email_body_template['und'][0]['value'] : '';
    $email_body_text_template = (isset($selected_template->field_email_body_template_text['und'])) ? $selected_template->field_email_body_template_text['und'][0]['value'] : '';
    $email_item_template = (isset($selected_template->field_email_pres_item_template['und'])) ? $selected_template->field_email_pres_item_template['und'][0]['value'] : '';
    $email_item_text_template = (isset($selected_template->field_email_pres_item_template['und'])) ? $selected_template->field_email_pres_item_template_t['und'][0]['value'] : '';
    $email_item_separator_template = (isset($selected_template->field_email_pres_item_sep['und'])) ? $selected_template->field_email_pres_item_sep['und'][0]['value'] : '';
    $email_item_separator_text_template = (isset($selected_template->field_email_pres_item_sep_t['und'])) ? $selected_template->field_email_pres_item_sep_t['und'][0]['value'] : '';
    $email_document_item_template = (isset($selected_template->field_email_doc_item_template['und'])) ? $selected_template->field_email_doc_item_template['und'][0]['value'] : '';
    $email_document_item_text_template = (isset($selected_template->field_email_doc_item_template_t['und'])) ? $selected_template->field_email_doc_item_template_t['und'][0]['value'] : '';
    $email_document_item_separator_template = (isset($selected_template->field_email_doc_item_sep['und'])) ? $selected_template->field_email_doc_item_sep['und'][0]['value'] : '';
    $email_document_item_separator_text_template = (isset($selected_template->field_email_doc_item_sep_t['und'])) ? $selected_template->field_email_doc_item_sep_t['und'][0]['value'] : '';

    return array(
      'body_html' => $email_body_template,
      'body_text' => $email_body_text_template,
      'presentation_item_html' => $email_item_template,
      'presentation_item_text' => $email_item_text_template,
      'presentation_item_separator_html' => $email_item_separator_template,
      'presentation_item_separator_text' => $email_item_separator_text_template,
      'document_item_html' => $email_document_item_template,
      'document_item_text' => $email_document_item_text_template,
      'document_item_separator_html' => $email_document_item_separator_template,
      'document_item_separator_text' => $email_document_item_separator_text_template
    );
  }

  function buildTemplate($template_infos,$node) {
    $user = user_load($node->uid);

    // some profile fields may not be an available on all systems
    $firstname_txt = (isset($user->profile_firstname) ? $user->profile_firstname : '');
    $lastname_txt  = (isset($user->profile_lastname)  ? $user->profile_lastname  : '');
    $email_txt     = (isset($user->mail)              ? $user->mail              : '');
    $jobtitle_txt  = (isset($user->profile_jobtitle)  ? $user->profile_jobtitle  : '');
    $address_txt   = (isset($user->profile_address)   ? $user->profile_address   : '');
    $url_txt       = (isset($user->profile_url)       ? $user->profile_url       : '');
    $phone_txt     = (isset($user->profile_phone)     ? $user->profile_phone     : '');

    //phone number
    if ($user->profile_phone != '') {
      $template_infos['body_html'] = preg_replace(array('/%phone\[([^\]]+)\]/'), array($user->profile_phone), $template_infos['body_html']);
      $template_infos['body_text'] = trim(drupal_html_to_text(preg_replace(array('/%phone\[([^\]]+)\]/'), array($user->profile_phone), $template_infos['body_text'])));
    } else {
      $template_infos['body_html'] = preg_replace(array('/%phone\[([^\]]+)\]/'), array('${1}'), $template_infos['body_html']);
      $template_infos['body_text'] = trim(drupal_html_to_text(preg_replace(array('/%phone\[([^\]]+)\]/'), array('${1}'), $template_infos['body_text'])));
    }

    //Website
    if ($user->profile_url != '') {
      $template_infos['body_html'] = preg_replace(array('/%url\[([^\]]+)\]/'), array($user->profile_url), $template_infos['body_html']);
      $template_infos['body_text'] = trim(drupal_html_to_text(preg_replace(array('/%url\[([^\]]+)\]/'), array($user->profile_url), $template_infos['body_text'])));
    } else {
      $template_infos['body_html'] = preg_replace(array('/%url\[([^\]]+)\]/'), array('${1}'), $template_infos['body_html']);
      $template_infos['body_text'] = trim(drupal_html_to_text(preg_replace(array('/%url\[([^\]]+)\]/'), array('${1}'), $template_infos['body_text'])));
    }

    //HTML template
    $template_infos['body_html'] = str_replace(
      array(
        '%message',
        '%signature',
        '%firstname',
        '%lastname',
        '%email',
        '%jobtitle',
        '%address',
        '%url',
        '%phone'
       ),
      array(
        (isset($node->field_email_message['und'][0]) ? $node->field_email_message['und'][0]['value'] : ''),
        (isset($node->field_email_signature['und'][0]['value']) ? $node->field_email_signature['und'][0]['value'] : ''),
        $firstname_txt,
        $lastname_txt,
        $email_txt,
        $jobtitle_txt,
        $address_txt,
        $url_txt,
        $phone_txt
      ),
      $template_infos['body_html']
    );


    $template_infos['body_text'] = str_replace(
      array(
        '%message',
        '%signature',
        '%firstname',
        '%lastname',
        '%email',
        '%jobtitle',
        '%address',
        '%url',
        '%phone'
      ),
      array(
        (isset($node->field_email_message['und'][0]) ? trim(drupal_html_to_text($node->field_email_message['und'][0]['value'])) : ''),
        (isset($node->field_email_signature['und'][0]) ? trim(drupal_html_to_text($node->field_email_signature['und'][0]['value'])) : ''),
        trim(drupal_html_to_text($firstname_txt)),
        trim(drupal_html_to_text($lastname_txt)),
        trim(drupal_html_to_text($email_txt)),
        trim(drupal_html_to_text($jobtitle_txt)),
        trim(drupal_html_to_text($address_txt)),
        trim(drupal_html_to_text($url_txt)),
        trim(drupal_html_to_text($phone_txt))
      ),
      $template_infos['body_text']
    );

    //Presentations
    $template_infos = buildPresentation($template_infos,$node);
    //Documents
    $template_infos = buildDocument($template_infos,$node);

    //Hostname
    $protocol = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on' ? 'https://' : 'http://');
    $template_infos['body_html'] = str_replace(array('%hostname'), array($protocol . $_SERVER['SERVER_NAME']), $template_infos['body_html']);
    $template_infos['body_text'] = str_replace(array('%hostname'), array($protocol . $_SERVER['SERVER_NAME']), $template_infos['body_text']);

    return $template_infos;
  }

  function buildPresentation($template_infos,$node) {
    global $base_url;
    global $user;
    global $subdomain;
    $viewer_path = variable_get('ca_presenter_viewer_path','/ca-viewer');
    if($viewer_path != '') {
      $viewer_path = $base_url.$viewer_path.'/';
    }
    $viewer_domain = $subdomain;
    $ids = array();
    if(count($node->field_email_presentations) > 0 && isset($node->field_email_presentations['und'][0])) {
      $ids = explode(',',$node->field_email_presentations['und'][0]['value']);
    }
    $presentation_items = '';
    foreach($ids as $selected_presentations) {
      if(strlen($selected_presentations) <= 0)
        continue;
      $presentation = node_load($selected_presentations);
      $presentation->nid = (int)$presentation->nid;
      $reference = $node->field_reference['und'][0]['value'];

        global $base_root;
        $fulldomain = parse_url($base_root);
        $splitdomain = explode(".", $fulldomain['host']);
        $subdomain = $splitdomain[0];
        $path = 'private://'.$subdomain.'/ca_presenter/presentations/'.$presentation->nid.'/__thumbnails/';
        $first_slide_id = null;
        if(isset($presentation->field_slides["und"])) {
          $first_slide_id = $presentation->field_slides["und"][0]["target_id"];
          $files = glob(drupal_realpath($path). '/'.$first_slide_id.'.jpg');
        }
        $user_thumbnail = null;
        if(isset($presentation->field_cover_thumbnail["und"]))
          $user_thumbnail = $presentation->field_cover_thumbnail["und"][0]["uri"];
        if(isset($user_thumbnail)) {
          $img_src = file_create_url($user_thumbnail);
        } else {
          if(isset($files) && count($files) > 0) {
            $file_name = explode('/', $files[0]);
            $file_name = $file_name[count($file_name)-1];
            $img_src = 'system/files/'.$subdomain.'/ca_presenter/presentations/'.$presentation->nid.'/__thumbnails/'.$file_name;
          } else {
            $img_src = '/sites/default/files/no-thumbnail.png';
          }
        }
        
        $img = url($img_src, array('absolute' => TRUE));

      $presentation_item = str_replace(
        array(
          '%thumbnail_url',
          '%icon',
          '%title',
          '%description',
          '%view_url',
        ),
        array(
          //url('presentation-viewer/' . $presentation->nid . '/thumbnail/' . (($reference != NULL) ? $reference . '/' : '') , array('absolute' => TRUE)),
          $img,
          $img,
          $presentation->title,
          (isset($presentation->body['und'][0]['value'])) ?  $presentation->body['und'][0]['value'] : '',
          ($viewer_path != '' && $viewer_domain != '') ?
	    url($viewer_path.'index.html',
	      array('query' =>
               array(
                 'domain' => $viewer_domain,
  	         'username' => $user->name,
                 'presentation_id' => $presentation->nid,
	         'ref' => $node->field_reference['und'][0]['value'],
                 'eid' => $node->nid
	       )
              ),
	      array(
  	        'absolute' => TRUE,
                'query' => 'x=%email'
  	      )
	    ) : url('presentation-viewer/' . $presentation->nid . '/' . (($node->field_reference['und'][0]['value'] != NULL) ? $node->field_reference['und'][0]['value'] . '/' : ''), array('absolute' => TRUE, 'query' => 'x=%email')),
        ),
        $template_infos['presentation_item_html']
      );
      $presentation_items .= $presentation_item . $template_infos['presentation_item_separator_html'];
    }
    if(!isset($presentation_items) || $presentation_items == '') {
      $template_infos['body_html'] = str_replace(array('%presentation_items'), array(''), $template_infos['body_html']);
      $template_infos['body_html'] = str_replace(array('%presentation_title'), array(''), $template_infos['body_html']);
    } else {
      $template_infos['body_html'] = str_replace(array('%presentation_items'), array($presentation_items), $template_infos['body_html']);
      $template_infos['body_html'] = str_replace(array('%presentation_title'), array('Presentations'), $template_infos['body_html']);
    }
    return $template_infos;
  }

  function buildDocument($template_infos, $node) {
  global $ca_presenter_module_path;
    $ids = array();
    $document_items = '';
    if(count($node->field_email_documents) > 0 && isset($node->field_email_documents['und'][0]))
      $ids = explode(',',$node->field_email_documents['und'][0]['value']);
    foreach($ids as $selected_documents) {
      if(!isset($selected_documents))
        continue;
      $document = node_load($selected_documents);
      if(!$document) continue;
      $document->nid = (int)$document->nid;
      $reference = $node->field_reference['und'][0]['value'];
      $to_email = '';
      $recipient_emails = array();
      $recipient_emails = explode(',',$node->field_recipients_email['und'][0]['value']);
      foreach($recipient_emails as $recipient_email) {
        $to_email .= ','.$recipient_email;
      }
      $to_email = ltrim($to_email, ',');
      $to_email = rtrim($to_email, ",");
      //Set document's icons
      $publish_mime_type = $document->field_file['und'][0]['filemime'];

      if ($publish_mime_type == 'application/pdf') {
        $icon = url($ca_presenter_module_path . '/images/email/pdf-icon.png', array('absolute' => TRUE));
      } else if ($publish_mime_type == 'application/msword') {
        $icon = url($ca_presenter_module_path . '/images/email/doc-icon.png', array('absolute' => TRUE));
      } else if ($publish_mime_type == 'application/vnd.openxmlformats-officedocument.wordprocessingml.document') {
        $icon = url($ca_presenter_module_path . '/images/email/doc-icon.png', array('absolute' => TRUE));
      } else if ($publish_mime_type == 'application/vnd.ms-excel') {
        $icon = url($ca_presenter_module_path . '/images/email/xls-icon.png', array('absolute' => TRUE));
      } else if ($publish_mime_type == 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet') {
        $icon = url($ca_presenter_module_path . '/images/email/xls-icon.png', array('absolute' => TRUE));
      } else if ($publish_mime_type == 'application/vnd.ms-powerpoint') {
        $icon = url($ca_presenter_module_path . '/images/email/ppt-icon.png', array('absolute' => TRUE));
      } else if ($publish_mime_type == 'application/vnd.openxmlformats-officedocument.presentationml.presentation') {
        $icon = url($ca_presenter_module_path . '/images/email/ppt-icon.png', array('absolute' => TRUE));
      } else {
        $icon = url($ca_presenter_module_path . '/images/email/generic-icon.png', array('absolute' => TRUE));
      }
      $body = '';
      if(isset($document->body['und'])) {
        $body = $document->body['und'][0]['value'];
      }
      $document_item = str_replace(
        array(
          '%icon',
          '%thumbnail_url',
          '%title',
          '%description',
          '%view_url',
        ),
        array(
          $icon,
          $icon,
          $document->title,
          $body,
          url('document-viewer/' . $document->nid . '/' . (($reference != NULL) ? $reference . '/' . $node->nid : ''), array('absolute' => TRUE)),
        ),
        $template_infos['document_item_html']
      );
      $document_items .= $document_item . $template_infos['document_item_separator_html'];
    }
    if(!isset($document_items) || $document_items === '') {
      $template_infos['body_html'] = str_replace(array('%documents_title'), array(''), $template_infos['body_html']);
      $template_infos['body_html'] = str_replace(array('%document_items'), array(''), $template_infos['body_html']);
    } else {
      $template_infos['body_html'] = str_replace(array('%documents_title'), array('Documents'), $template_infos['body_html']);
      $template_infos['body_html'] = str_replace(array('%document_items'), array($document_items), $template_infos['body_html']);
    }
    return $template_infos;
  }

  function ca_presenter_emails_admin_email_info($eid) {
    global $user;
    $path = drupal_get_normal_path('emails');
    $output = '';

    if ($path && $menu_item = menu_get_item($path)) {
      menu_set_item(NULL, $menu_item);
    }

    $email = node_load($eid);
    $to_names = array();
    if(isset($email->field_recipients_name['und']))
      $to_names = explode(",",$email->field_recipients_name['und'][0]['value']);
    $_to_name = array();
    foreach($to_names as $to_name) {
      $_to_name[] = $to_name;
    }
    $to_name = implode(',',$_to_name);
    $to_emails = explode(",",$email->field_recipients_email['und'][0]['value']);
    $_to_email = array();
    foreach($to_emails as $to_email) {
      $_to_email[] = $to_email;
    }
    $recipients = $to_email = $_to_email;
    $title = '<div class="header-icon"><i class="fa fa-info"></i></div>'.$to_name.' - '.format_date($email->created,'short').' | "'.$email->title.'"';
    drupal_set_title($title,PASS_THROUGH);

    // Recipients information @see: ca_presenter_emails_helpers.php
      $output .= getRecipientsInfo($recipients,$email);
    //

    // Notes @see: ca_presenter_emails_helpers.php
      $output .= getNotesInfo($email);
    //

    // Presentation attachments @see: ca_presenter_emails_helpers.php
    $output .= getPresentationsInfo($email);
    //

    // Documents attachments @see: ca_presenter_emails_helpers.php
    $output .= getDocumentsInfo($email);

    //Rendered message
    $output .= '<h3>Message</h3><iframe id="email_preview" style="width:100%;height:500px;" src="/emails/preview/'.$email->nid.'"></iframe>';
    return $output;
  }

/**
 *
 * email presentation stats page callback
 * @param $eid
 * @param $pid
 *
 * @return string
 */
  function ca_presenter_emails_email_presentation_stats($eid, $pid) {
    $path = drupal_get_normal_path('emails');
    $output = '';

    if ($path && $menu_item = menu_get_item($path)) {
      menu_set_item(NULL, $menu_item);
    }

    $presentation = node_load($pid);
    $email = node_load($eid);

    $reference = isset($email->field_reference[LANGUAGE_NONE][0]) ?
      $email->field_reference[LANGUAGE_NONE][0]['value'] : NULL;

    $to_name = $email->field_recipients_name['und'][0]['value'];

    drupal_set_title(t('<div class="header-icon">'
      . '<i class="fa fa-bar-chart-o"></i></div>%name (%time) "%subject"',
      array (
        '%name' => $to_name,
        '%time' => date('d M y - H:i', $email->created),
        '%subject' => $email->title
      )), PASS_THROUGH);

    // Presentation information

    $presentation_status = (isset($presentation->field_primary_statuses['und']['0']['tid'])) ?
      $presentation->field_primary_statuses['und']['0']['tid'] : NULL;

    $presentation_status = ($presentation_status !== NULL) ?
      taxonomy_term_load($presentation_status) : FALSE;


    $rows = array(
      array(
        'Ref',
        $presentation->nid,
      ),
      array(
        'Presentation',
        $presentation->title,
      ),
      array(
        'Status',
        ($presentation_status !== FALSE) ? $presentation_status->name : '',
      ),
    );
    $table = theme('table',array('header' => NULL, 'rows' => $rows));
    $output .= '<h3>Presentation information</h3>' . $table;


    // Views
    $header = array(
      array('data' => t('Recipients')),
      array('data' => t('First view')),
      array('data' => t('Last view')),
      array('data' => t('Views'), 'style' => 'text-align: center;')
    );

    // @todo add the piwik code to get stats

    $stats = CAPresenterTracking::get_visit_data($reference,
      CAPresenterTracking::VIEW_PRESENTATION, $pid);
    $rows = [];
    if (!empty($stats) > 0) {
        $row = array(
          array('data' => $to_name),
          array('data' => $stats['first_visit']),
          array('data' => $stats['last_visit']),
          array('data' => $stats['views'], 'align' => 'center'),
        );

        $rows[] = $row;

    } else {

      $rows[] = array(
        array (
        'data' => t('No view data available.'),
        'colspan' => '4',
        'class' => 'message')
      );
    }

    $table = theme('table', ['header' => $header, 'rows' => $rows], NULL);
    $output .= '<h3>Views</h3>' . $table;

    //

    $stats = CAPresenterTracking::get_slide_data($reference,$pid);

    // Stats

      $output .= '<h3>Slide views</h3>';
      $headers = [
        ['data' => t('Title')],
        ['data' => t('Visits')],
        ['data' => t('Average time on slide')],
      ];
      $rows = [];
      foreach ($stats as $slide) {
        $rows[] = [
          ['data' => $slide['title']],
          ['data' => $slide['visits']],
          ['data' => $slide['avg_time']],
        ];
      }
      $table = theme('table', ['header' => $headers, 'rows' => $rows]);
      $output .= $table;
      $node_wrapper = entity_metadata_wrapper('node', node_load($presentation->nid));
      $slides = $node_wrapper->field_slides->value();

    $header = array(
      array('data' => t('Visit date')),
      array('data' => t('Visit duration')),
      array('data' => t('Actions')),
      array('data' => t('IP')),
      array('data' => t('Location')),
      array('data' => t('OS')),
      array('data' => t('Browser')),
    );

    $stats = CAPresenterTracking::get_slide_detail_data($reference, $pid);
    $rows = _ca_presenter_emails_generate_slide_stat_detail_table($stats);
    $header = array(
      array('data' => t('Visit date')),
      array('data' => t('Visit duration')),
      array('data' => t('Actions')),
      array('data' => t('IP')),
      array('data' => t('Location')),
      array('data' => t('OS')),
      array('data' => t('Browser')),
    );

    $table = theme('table', array('header' => $header, 'rows' => $rows));

    $output .= '<h3>View report (web stats)</h3>' . $table;

    return $output;
  }

/**
 * generates the detailed stat report by slides for email attachments
 * @param $content
 *
 * @return array
 */
  function _ca_presenter_emails_generate_slide_stat_detail_table ($content) {
    $opts = [
      'scope' => 'footer',
      'group' => JS_LIBRARY,
      'requires_jquery' => TRUE
    ];
    drupal_add_js('misc/form.js', $opts);
    drupal_add_js('misc/collapse.js', $opts);
    if (is_array($content) && count($content) > 0) {
      if (isset($content['result']) && $content['result'] == 'error') {
        $rows[] = array(array('data' => $content['message'], 'colspan' => '7', 'class' => 'message'));
      } else {
        foreach($content as $item) {
          $row = array(
            date('d M y - H:i', $item['firstActionTimestamp']),
            $item['visitDurationPretty'],
            $item['actions'],
            $item['visitIp'],
            $item['location'],
            $item['operatingSystemName'],
            $item['browserName']
          );

          $rows[] = $row;

          $actionDetails = $item['actionDetails'];

          if (is_array($actionDetails)) {
            $sub_header = array(
              array('data' => t('Title')),
              array('data' => t('Time spent')),
            );

            $sub_rows = array();
            foreach ($actionDetails as $actionDetail) {
              $url = explode('/',$actionDetail['url']);
              $slide_id = $url[count($url)-2];
              $pid = explode('?', $url[count($url) -1]);
              $pid = $pid[0];
              $presentation = node_load($pid);
              $node_wrapper = entity_metadata_wrapper('node', $presentation);
              $slides = $node_wrapper->field_slides->value();
              foreach($slides as $slidek => $slidev) {
                if($slide_id == $slidev->nid) {
                  $sub_rows[] = array(
                    $actionDetail['pageTitle'],
                    isset($actionDetail['timeSpentPretty'])
                      ? $actionDetail['timeSpentPretty'] : '0s',
                  );
                }
              }
            }

            $sub_table = theme('table', array('header' => $sub_header, 'rows' => $sub_rows));

            $form = array();
            $form['fieldset'] = array(
              '#type' => 'fieldset',
              '#title' => t('Action details'),
              '#attributes' => [
                'class' => ['collapsible', 'collapsed']
              ]
            );

            $form['fieldset']['table'] = array(
              '#type' => 'markup',
              '#markup' => $sub_table,
            );
            $collapsible_table = drupal_render($form);
            $rows[] = array(
              array(
                'data' => $collapsible_table,
                'colspan' => '7',
              ),
            );
          }
        }
      }
    } else {
      $rows[] = array(array('data' => t('No statistical data available at this time.'), 'colspan' => '7', 'class' => 'message'));
    }
    return $rows;
  }

/**
 * generates stats summary page for attachment
 * @param $eid
 * @param $did
 *
 * @return string
 */
  function ca_presenter_emails_email_document_stats($eid, $did) {
    $path = drupal_get_normal_path('emails');

    if ($path && $menu_item = menu_get_item($path)) {
      menu_set_item(NULL, $menu_item);
    }

    $document = node_load($did);

    $email = node_load($eid);
    $email->eid = $email->nid;
    $to_name = $email->field_recipients_name['und'][0]['value'];
    $title  = $to_name . ' (' . date('d M y - H:i', $email->created) . ') ' . $email->title;
    drupal_set_title($title);

    // Document information
    //$document_status = ca_presenter_document_get_status($document);
    $document_status = (isset($document->field_primary_statuses['und'][0]['tid'])) ?
      $document->field_primary_statuses['und'][0]['tid'] : NULL;
    if ($document_status !== NULL) {
      $document_status = taxonomy_term_load($document_status);
    }

    $rows = array(
      array(
        'Ref',
        $document->nid,
      ),
      array(
        'Document',
        $document->title,
      ),
      array(
        'Status',
        $document_status->name,
      ),
    );

    $table = theme('table', [
      'header' => NULL,
      'rows' => $rows
    ]);

    $output = '<h3>Document information</h3>' . $table;


    // Downloads
    $header = array(
      array('data' => t('Recipient')),
      array('data' => t('First download time'), 'style' => 'text-align: center;'),
      array('data' => t('Downloaded times'), 'style' => 'text-align: center;'),
    );

    // getting document tracking stats

    $header = array(
      array('data' => t('Recipients')),
      array('data' => t('First view')),
      array('data' => t('Last view')),
      array('data' => t('Views'), 'style' => 'text-align: center;')
    );

    $reference = (isset($email->field_reference[LANGUAGE_NONE][0]['value'])) ?
      $email->field_reference[LANGUAGE_NONE][0]['value'] : NULL;

    $stats = ($reference !== NULL) ? CAPresenterTracking::get_visit_data($reference,
      CAPresenterTracking::VIEW_DOCUMENT, $did) : [];
    $rows = [];
    if (!empty($stats) > 0) {
      $row = array(
        array('data' => $to_name),
        array('data' => $stats['first_visit']),
        array('data' => $stats['last_visit']),
        array('data' => $stats['views'], 'align' => 'center'),
      );

      $rows[] = $row;

    } else {

      $rows[] = array(
        array (
          'data' => t('No download data available.'),
          'colspan' => '4',
          'class' => 'message')
      );
    }

    $table = theme('table', ['header' => $header, 'rows' => $rows]);
    $output .= '<h3>Downloads</h3>' . $table;

    return $output;
  }
?>
