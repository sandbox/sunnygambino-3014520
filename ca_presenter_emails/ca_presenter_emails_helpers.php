<?php
/**
 * renders recipient info area of email report
 * @param $recipients
 * @param $email
 *
 * @return string
 */
function getRecipientsInfo($recipients, $email) {
  $header = [
    ['data' => t('Email')],
    ['data' => t('Message ID')],
  ];

  $rows = [];

  if (count($recipients) > 0) {
    foreach ($recipients as $recipient) {
      $row = [
        ['data' => $recipient],
        ['data' => $email->nid],
      ];

      $rows[] = $row;
    }
  }
  if (empty($rows)) {
    $rows[] = [
      [
        'data' => t('No recipients available.'),
        'colspan' => '6',
        'class' => 'message',
      ],
    ];
  }
  $settings_form = drupal_get_form('_email_settings_form', $email->nid);
  $settings_form = render($settings_form);
  return $settings_form . '<h3>' . t('Recipient\'s information') . '</h3>' . theme('table', [
      'header' => $header,
      'rows' => $rows,
    ]);

}

/**
 * generates email notes section of email sent page
 * @param $email
 *
 * @return string
 */
function getNotesInfo($email) {
  $header = [
    ['data' => t('Email notes')],
  ];

  $row = [];

  $note = '';
  if (count($email->field_email_note) > 0) {
    $note = $email->field_email_note['und'][0]['value'];
  }
  if (strlen($note) > 0) {
    $row = [
      [
        [
          'data' => $note,
        ],
      ],
    ];
    return '<h3>Email notes</h3>' . theme('table', [
        'header' => $header,
        'rows' => $row,
      ]);
  }
  else {
    $rows[] = [
      [
        'data' => t('No presentations available.'),
        'colspan' => '6',
        'class' => 'message',
      ],
    ];
    return '<h3>Email notes</h3>' . theme('table', [
        'header' => $header,
        'rows' => $row,
      ]);
  }
}

/**
 * builds the presentation attachments section of the email stats page
 * @param \stdClass $email
 *
 * @return string
 */
function getPresentationsInfo($email) {
  global $user;
  global $subdomain;
  module_load_include('inc', 'ca_presenter_tracking', 'include/class');
  $header = [
    ['data' => t('Ref'), 'width' => '50'],
    ['data' => t('Title'), 'width' => '350'],
    ['data' => t('Status'), 'style' => 'text-align: center;'],
    ['data' => t('Views'), 'style' => 'text-align: center'],
    ['data' => t('View stats'), 'style' => 'text-align: center;'],
    ['data' => t('View link'), 'style' => 'text-align: center;'],
    ['data' => t('Open in viewer'), 'style' => 'text-align: center;'],
  ];

  $presentations = [];
  $ids = [];
  if (count($email->field_email_presentations) > 0) {
    $ids = explode(',', $email->field_email_presentations['und'][0]['value']);
  }
  $nids = [];
  foreach ($ids as $presentation) {
    $node = node_load((int)$presentation);
    if ($node) {
      $presentations[] = $node;
      $nids[] = $node->nid;
    }
  }
  $reference = $email->field_reference[LANGUAGE_NONE][0]['value'];
  $visits = CAPresenterTracking::get_aggregate_visits_by_reference($reference,
    $nids, CAPresenterTracking::VIEW_PRESENTATION);
  //TODO: implement viewer path in ca presenter admin page
  //$viewer_path = variable_get('ca_presenter_settings_viewer_path','');
  $viewer_path = variable_get('ca_presenter_viewer_path', '/ca-viewer');
  if ($viewer_path != '') {
    $viewer_path = $viewer_path . '/';
  }
  $viewer_domain = $subdomain;
  $rows = [];
  if (count($presentations) > 0) {
    foreach ($presentations as $presentation) {


      if (!$presentation->nid) {
        continue;
      }

      $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
      $get_link_url = $row[5]['data'] = '<a href="javascript:clipboard.copyToClipboardDialog(\'' .
        $protocol . $_SERVER['SERVER_NAME'] . $viewer_path . 'index.html' . '?domain=' . $viewer_domain .
        '&username=' . $user->name .
        '&session=' . session_id() .
        '&presentation_id=' . $presentation->nid .
        //'&ref=' . $email->field_reference['und'][0]['value'] .
        '&ref=' . _get_ref($presentation->nid).
        '&eid=' . $email->nid .
        '\', \'Presentation Link\');"><div class="icon"><i class="fa fa-link"></i></div></a>';
      $viewer_link = $row[6]['data'] =
        '<a href="' .
        url($viewer_path . 'index.html',
          [
            'query' =>
              [
                'domain' => $viewer_domain,
                'username' => $user->name,
                'session' => session_id(),
                'presentation_id' => $presentation->nid,
                //'ref' => $email->field_reference['und'][0]['value'],
                'ref' => _get_ref($presentation->nid),
                'eid' => $email->nid,
              ],
          ],
          [
            'absolute' => TRUE,
          ]
        ) . '" target="_blank"><div class="icon"><i class="fa fa-play"></i></div></a>';

      $nid = $presentation->nid;
      $presentation_status = (array) taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']);
      $row = [
        ['data' => $nid, 'class' => 'ref'],
        ['data' => $presentation->title, 'class' => 'title'],
        [
          'data' => $presentation_status['name'],
          'align' => 'center',
          'class' => 'status',
        ],
        [
          'data' => (isset($visits[$nid])) ? $visits[$nid] : '',
          'align' => 'center',
          'class' => 'visited',
        ],
        [
          'data' => l('<div class="icon"><i class="fa fa-bar-chart-o"></i></div>', 'emails/' . $email->nid . '/presentation/' . $presentation->nid . '/stats', ['html' => TRUE]),
          'align' => 'center',
          'class' => 'view-stats',
        ],
        [
          'data' => $get_link_url,
          'align' => 'center',
          'class' => 'view-link',
        ],
        [
          'data' => $viewer_link,
          'align' => 'center',
          'class' => 'open-in-viewer',
        ],
      ];

      $rows[] = $row;
    }
  }

  if (empty($rows)) {
    $rows[] = [
      [
        'data' => t('No presentations available.'),
        'colspan' => '6',
        'class' => 'message',
      ],
    ];
  }

  return '<h3>Presentation attachments</h3>' . theme('table', [
      'header' => $header,
      'rows' => $rows,
    ]);
}

/**
 * builds the document attachment section of email statistics page
 * @param \stdClass $email
 *
 * @return string
 */
function getDocumentsInfo($email) {
  $header = [
    ['data' => t('Ref'), 'width' => '50'],
    ['data' => t('Title'), 'width' => '350'],
    ['data' => t('Status'), 'style' => 'text-align: center;'],
    ['data' => t('Views'), 'style' => 'text-align: center;'],
    ['data' => t('View stats'), 'style' => 'text-align: center;'],
    ['data' => t('View link'), 'style' => 'text-align: center;'],
    ['data' => t('Open in viewer'), 'style' => 'text-align: center;'],
  ];

  $documents = [];
  $dids = [];
  if (count($email->field_email_documents) > 0) {
    $dids = explode(',', $email->field_email_documents['und'][0]['value']);
  }
  foreach ($dids as $document) {
    $documents[] = node_load($document);
  }

  // get the stats from piwik
  $reference = (isset($email->field_reference['und'][0]['value']))
    ? $email->field_reference['und'][0]['value'] : NULL;
  $stats = CAPresenterTracking::get_aggregate_visits_by_reference($reference,
    $dids, CAPresenterTracking::VIEW_DOCUMENT);

  $rows = [];
  if (count($documents) > 0) {

    foreach ($documents as $document) {
      if (!$document || !$document->nid || $document->type != 'document') {
        continue;
      }
      $status_tid = (isset($document->field_primary_statuses['und'][0]['tid']))
        ? $document->field_primary_statuses['und'][0]['tid'] : NULL;
      if ($status_tid !== NULL) {
        $document_status = (array) taxonomy_term_load($document->field_primary_statuses['und'][0]['tid']);
      } else {
        $document_status = FALSE;
      }
      $document_status_name = (!$document_status && isset($document_status['name']))
        ? '' : $document_status['name'];
      $row = [
        ['data' => $document->nid, 'class' => 'ref'],
        ['data' => $document->title, 'class' => 'title', 'class' => 'title'],
        [
          'data' => $document_status_name,
          'align' => 'center',
          'class' => 'status',
        ],
        [
          'data' => (isset($stats[$document->nid])) ? $stats[$document->nid] : 0,
          'align' => 'center',
          'class' => 'view-stats'
        ],
        [
          'data' => l('<div class="icon"><i class="fa fa-bar-chart-o"></i></div>', 'emails/' . $email->nid . '/document/' . $document->nid . '/stats', ['html' => TRUE]),
          'align' => 'center',
          'class' => 'view-stats',
        ],
        [
          'data' => '<a href="javascript:clipboard.copyToClipboardDialog(\'' . url('document-viewer/' . $document->nid . '/' . $email->field_reference['und'][0]['value'] . '/' . $email->nid, ['absolute' => TRUE]) . '\');"><div class="icon"><i class="fa fa-link"></i></div></a>',
          'align' => 'center',
          'class' => 'view-link',
        ],
        [
          'data' => l('<div class="icon"><i class="fa fa-play"></i></div>', url('document-viewer/' . $document->nid . '/' . $reference . '/' . $email->nid, ['absolute' => TRUE]), [
            'attributes' => ['target' => '_blank'],
            'html' => TRUE,
          ]),
          'align' => 'center',
          'class' => 'open-in-viewer',
        ],
      ];

      $rows[] = $row;
    }
  }

  if (empty($rows)) {
    $rows[] = [
      [
        'data' => t('No documents available.'),
        'colspan' => '6',
        'class' => 'message',
      ],
    ];
  }
  return '<h3>Document attachments</h3>' . theme('table', [
      'header' => $header,
      'rows' => $rows,
    ]);
}

function sendEmail($template, $node, $is_test = FALSE) {
  $mail = new PHPMailer();
  $mail->IsSMTP();
  $mail->Host = variable_get('smtp_host', '');
  $mail->SMTPDebug = variable_get('smtp_debugging', SMTP_LOGGING_ERRORS);
  $secure = variable_get('smtp_protocol','standard');
  if($secure !== 'standard') {
    $mail->SMTPSecure = $secure;
  }
  global $mail_log;
  $mail->Debugoutput = function ($str, $level) {
    $mail_log[] = $str;
  };
  $smtp_username = variable_get('smtp_username', '');
  $smtp_password = variable_get('smtp_password', '');
  if ($smtp_username != '' && $smtp_password != '') {
    $mail->SMTPAuth = TRUE;
    $mail->Username = $smtp_username;
    $mail->Password = $smtp_password;
  }
  $mail->IsHTML(TRUE);
  $mail->From = $node->field_reply_email['und'][0]['value'];
  if (isset($node->field_from_name['und'][0])) {
    $mail->FromName = $node->field_from_name['und'][0]['value'];
  }
  else {
    $mail->FromName = '';
  }
  if (isset($node->field_email_subject['und'][0])) {
    $mail->Subject = $node->field_email_subject['und'][0]['value'];
  }
  else {
    $mail->Subject = '';
  }
  if (isset($node->field_reply_email['und'][0]) && $node->field_reply_email['und'][0]['value'] != NULL) {
    $mail->AddReplyTo($node->field_reply_email['und'][0]['value'], $node->field_from_name['und'][0]['value']);
  }
  elseif (isset($node->field_reply_email['und'][0])) {
    $mail->AddReplyTo($node->field_from_email['und'][0]['value'], $node->field_from_name['und'][0]['value']);
  }
  $email_addresses = explode(",", $node->field_recipients_email['und'][0]['value']);
  //Send to test emails only
  if ($is_test && isset($node->field_email_test)
    && isset($node->field_email_test['und'][0])
    && count($node->field_email_test) > 0
    && $node->field_email_test['und'][0]['value']) {
    $email_addresses = $node->field_email_test;
  }
  elseif (isset($node->field_bcc_email) && isset($node->field_bcc_email['und'][0]) && count($node->field_bcc_email) > 0 && isset($node->field_bcc_email['und'][0]['value'])) {
    foreach (explode(",", trim($node->field_bcc_email['und'][0]['value'])) as $bcc) {
      if (is_array($bcc) && !isset($bcc['value'])) {
        continue;
      }
      $mail->AddBCC($bcc, '');
    }
  }
  //
  foreach ($email_addresses as $to_emails) {
    if (!isset($to_emails)) {
      continue;
    }
    //$mail_log = [];
    $to_email = $to_emails;
    if (is_array($to_email)) {
      if (!isset($to_email['value'])) {
        $to_email = array_pop($to_email);
      }
      $to_email = $to_email['value'];
    }
    $mail->ClearAddresses();
    $mail->AddAddress($to_email);
    //Add CCs
    if (isset($node->field_cc_email['und'][0]) && count($node->field_cc_email) > 0 && $node->field_cc_email['und'][0]['value'] != NULL) {
      $cc = explode(',', $node->field_cc_email['und'][0]['value']);
      foreach ($cc as $cc_value) {
        $mail->AddCC($cc_value);
      }
    }
    //
    $mail->Body = $template['body_html'];
    $mail->AltBody = $template['body_text'];
    $mail->CharSet = 'UTF-8';
    $result = $mail->Send();
    if (!$result) {
      drupal_set_message(t('Error: sorry, the email hasn\'t been sent through due to unknown error to ' . $to_email . ' address'), 'error');
    }
  }
  ob_start();
  var_dump($mail_log);
  $r = ob_get_clean();
  watchdog('mail',$r);

}
