<?php

class CAPresenterMailSystem extends SmtpMailSystem {

  const IS_HTML = 'is_html';

  /**
   * Concatenate and wrap the e-mail body for either
   * plain-text or HTML emails.
   *
   * @param $message
   *   A message array, as described in hook_mail_alter().
   *
   * @return
   *   The formatted $message.
   */
  public function format(array $message) {
    // we set a parameter to determine whether we want that
    // particular email to be html
    $this->AllowHtml = (variable_get('smtp_allowhtml', 0)) ? 1 : 0;
    watchdog('mail system',$this->AllowHtml);


    $message['body'] = implode("\n\n", $message['body']);
    if ($this->AllowHtml == 0) {
      // Convert any HTML to plain-text.
      $message['body'] = drupal_html_to_text($message['body']);
      // Wrap the mail body for sending.
      $message['body'] = drupal_wrap_mail($message['body']);
    } else {
      $message['headers']['Content-Type'] = 'text/html; charset=UTF-8; delsp=yes';
    }

    return $message;

  }
}
