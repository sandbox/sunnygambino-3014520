(function ($) {
    Drupal.behaviors.ca_presenter_emails = {
        attach: function(context, settings) {
            $('#popup_close').unbind('click').bind('click',function(e) {
                $('#popup_overlay, #preview_popup').hide();
                $('#preview_popup iframe').attr('src','about:blank');
            });
        }
    }
})(jQuery);