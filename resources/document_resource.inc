<?php

function ca_presenter_service_document_index($access_code) {
  global $published_full;
  global $published_web;
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'document')
    ->execute();
  $documents = node_load_multiple(array_keys($result['node']));

  $data = array();

  if (count($documents) > 0) {
    foreach ($documents as $document) {
      $status = taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']);
      if($status->tid =! $published_full->tid && $status->tid != $published_web->tid) {
        continue;
      }

      $document_file_field = file_load($document->field_file['und'][0]['fid']);

      $file_name = $document_file_field->filename;
      $mime_type = $document_file_field->filemime;

      $item = array(
        'id' => $document->nid,
        'title' => $document->title,
        'description' => $document->body['und'][0]['value'],
        'categories' => taxonomy_term_load($document->field_document_category['und'][0]['tid'])->name,
        'file_name' => $file_name,
        'mime_type' => $mime_type,
        'created' => format_date($document->created,'short'),
      );

      $data[] = $item;
    }
  }

  return $data;
}

function ca_presenter_service_document_download($access_code, $did) {
  $document = node_load($did);
  $download_url = file_create_url($document->field_file['und'][0]['uri']);
  $archive_size = $document->field_file['und'][0]['filesize'];

  return array(
    'download_url' => $download_url,
    'download_size' => $archive_size,
  );
}

function ca_presenter_service_document_update($access_code, $did, $remote_file_hash) {
  $file_path = ca_presenter_document_get_file_path($did);
  $server_file_hash = hash_file('md5', $file_path);

  if ($remote_file_hash != $server_file_hash) {
    $download_url = ca_presenter_document_get_archive_download_url($did);
    $archive_size = ca_presenter_document_get_archive_size($did);

    return array(
      'update_available' => 1,
      'download_url' => $download_url,
      'download_size' => $archive_size,
    );
  } else {
    return array(
      'update_available' => 0,
      'download_url' => NULL,
      'download_size' => NULL,
    );
  }
}

/**
 * Access.
 */
function ca_presenter_service_document_access($permission, $args = array()) {
  if (user_access('administer documents')) {
    return TRUE;
  }

  if (!user_access('access content')) {
    return FALSE;
  }

  module_load_include('inc', 'ca_presenter', 'ca_presenter_api/device_key_resource');
  module_load_include('inc', 'ca_presenter', 'includes/ca_device');
  global $user;
  $account = user_load($user->uid);
  if(isset($account->profile_cms) && $account->profile_cms == "1") {
    return FALSE;
  }

  $access_code = $args[0];
  $u = user_load($user->uid);
  if ((int)$u->uid !== 1) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'device_key');
    $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
    $result = $query->execute();
    $device = node_load_multiple(array_keys($result['node']));
    if (count($device) > 1) {
      return services_error(t('Invalid access code.'), 406);
    }
    $is_there_any_active = FALSE;
    foreach($device as $d) {
      if($d->status == 1) {
        $is_there_any_active = TRUE;
        $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
      }
    }
    if(!$is_there_any_active) {
      return services_error(t('Invalid access code.'), 406);
    }
  }

  switch ($permission) {
    case 'index':
      return user_access('access content');
      break;
    case 'download':
    case 'update':
      $did = $args[1];
      $document = ca_presenter_document_get_document($did);
      if ($document && $document->status == 1) {
        return TRUE;
      }
      break;
  }

  return FALSE;
}
