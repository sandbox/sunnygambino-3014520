<?php

function ca_presenter_service_message_retrieve($access_code, $start_time) {
  global $user;

  $user_roles = array_keys($user->roles);
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'message')
    ->propertyCondition('created', $start_time, '>' )
    ->execute();
  $messages = node_load_multiple(array_keys($result['node']));

  $data = array();

  if (count($messages) > 0) {
    foreach ($messages as $message) {
      if(count($message->field_message_view_count) == 0) {
        $message->field_message_view_count['und'][0]['value'] = 1;
      } else {
        $count = $message->field_message_view_count['und'][0]['value'];
        $count = (int)$count+1;
        $message->field_message_view_count['und'][0]['value'] = $count;
      }
      node_save($message);

      $item = array(
        'id' => $message->nid,
        'subject' => $message->title,
        'body' => $message->field_body['und'][0]['value'],
        'posted' => $message->created,
      );

      $data[] = $item;
    }
  }
  return $data;
}

/**
 * Access.
 */
function ca_presenter_service_message_access($permission, $args = array()) {
  global $user;

  if ($user->uid != 1) {
    $device_enabled = variable_get('ca_presenter_device_key', 1);
    if ($device_enabled == 1) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->entityCondition('bundle', 'device_key');
      $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
      $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
      $result = $query->execute();
      $device = node_load_multiple(array_keys($result['node']));
      if (count($device) > 1) {
        return services_error(t('Invalid access code.'), 406);
      }
      $is_there_any_active = FALSE;
      foreach($device as $d) {
        if($d->status == 1) {
          $is_there_any_active = TRUE;
          $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
        }
      }
      if(!$is_there_any_active) {
        return services_error(t('Invalid access code.'), 406);
      }
    }
  }

  if(!$user->uid) {
    return FALSE;
  }

  switch ($permission) {
    case 'retrieve':
      return TRUE;
      break;
  }

  return FALSE;
}
