<?php

function ca_presenter_service_lookandfeel_retrieve() {
  global $subdomain;
  $logo_image_path = drupal_realpath('private://'.$subdomain.'/ca_presenter/lookandfeel/logo.png');

  if (file_exists($logo_image_path)) {
    $logo_image_data = file_get_contents($logo_image_path);
  }

  return array(
    'logo' => base64_encode($logo_image_data),
  );
}

/**
 * Access.
 */
function ca_presenter_service_lookandfeel_access($permission, $args = array()) {
  global $user;

  if(!$user->uid) {
    return FALSE;
  }

  switch ($permission) {
    case 'retrieve':
      return TRUE;
      break;
  }

  return FALSE;
}
