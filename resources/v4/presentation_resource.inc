<?php
function ca_presenter_service_sync_v4($access_code, $app_info) {
  global $user;
  global $db_url;
  $mysqli = db_connect($db_url['default']);
  $user_roles = array_keys($user->roles);
  //Thumbail field for presentations
  $presentation_thumbnail_field_name = variable_get('ca_presenter_presentation_thumbnail_field', '');
  if($presentation_thumbnail_field_name == '')
    $presentation_thumbnail_field_name = 'field_cover_thumbnail';
  $presentation_category_vid = variable_get('ca_presenter_presentation_category_vocabulary','');
  if($presentation_category_vid == '') {
   $presentation_category_vid = '1';
  }
  $document_category_vid = variable_get('ca_presenter_document_category_vocabulary','');
  if($document_category_vid == '') {
   $document_category_vid = '3';
  }
  $desktop_help = variable_get('ca_presenter_help_desktop_content','');
  $mobile_help = variable_get('ca_presenter_help_mobile_content','');

  $presentation_categories = array();
  $document_categories = array();
  $query = "CALL get_categories(".$presentation_category_vid.");";
  if ($mysqli->multi_query($query)) { # I use multi_query because my stored procedure yields multiple results, instead of one for better performance   
    if ($result = $mysqli->store_result()) {
      while ($row = $result->fetch_array()) {
        $presentation_categories[] = $row;
      }
      $result->free();
      $mysqli->next_result();
    }
  }
  $query = "CALL get_categories(".$document_category_vid.");";
  if ($mysqli->multi_query($query)) { # I use multi_query because my stored procedure yields multiple results, instead of one for better performance   
    if ($result = $mysqli->store_result()) {
      while ($row = $result->fetch_array()) {
        $document_categories[] = $row;
      }
      $result->free();
      $mysqli->next_result();
    }
  }

  //Create query ro retrive node datas
  $sql = "
    SELECT 
      n.*,
      f.filepath AS thumbnail_path,
      f.filename AS thumbnail_name,
      slides.sid AS slide_id,
    FROM node as n
    LEFT JOIN content_type_presentation AS ctp ON(n.nid = ctp.nid)
    LEFT JOIN files AS f ON(ctp.".$presentation_thumbnail_field_name."_fid = f.fid)
    LEFT JOIN ca_presenter_presentation_slides AS slides ON(slides.pid = n.nid AND n.type = 'presentation')
    WHERE n.nid IS NOT NULL
    AND (
      1 = (SELECT 1 FROM node_access na WHERE na.nid = n.nid AND na.realm = 'nodeaccess_author' AND na.gid = '" . db_escape_string($user->uid) . "' AND (na.grant_view = 1 OR na.grant_update = 1) LIMIT 1)
      OR 1 = (SELECT 1 FROM node_access na WHERE na.nid = n.nid AND na.realm = 'nodeaccess_uid' AND na.gid = '" . db_escape_string($user->uid) . "' AND (na.grant_view = 1 OR na.grant_update = 1) LIMIT 1)
      OR 1 = (SELECT 1 FROM node_access na WHERE na.nid = n.nid AND na.realm = 'nodeaccess_rid' AND na.gid IN (" . implode(',' , $user_roles) . ") AND (na.grant_view = 1 OR na.grant_update = 1) LIMIT 1)
    )
    GROUP BY n.nid
  ";
  $results = db_query($sql);
  $return = array();
  $thumbnails = array();
  while($row = db_fetch_array($results)) {
    if($row['thumbnail_path'] != '' && file_exists($row['thumbnail_path'])) {
      $thumbnails[] = $row['thumbnail_path'];
    } elseif($row['slide_id'] != '' && file_exists('sites/default/files/ca_presenter/presentations/'.$row['nid'].'/__thumbnails/'.$row['slide_id'].'.jpg')) {
      $thumbnails[] = 'sites/default/files/ca_presenter/presentations/'.$row['nid'].'/__thumbnails/'.$row['slide_id'].'.jpg';
    }
    $return[$row['type']][$row['nid']] = $row;
  }
  $timestamp = time();
  $return['presentation_categories'] = $presentation_categories;
  $return['document_categories'] = $document_categories;
  $return['help']['desktop'] = $desktop_help;
  $return['help']['mobile'] = $mobile_help;
  return $return;
}

/**
 * Access.
 */
function ca_presenter_service_presentation_access_v4($permission, $args = array()) {
  global $user;
  $account = user_load($user->uid);
  if(isset($account->profile_cms) && $account->profile_cms == "1") {
    return FALSE;
  }
  if ($user->uid != 1) {
    $device_enabled = variable_get('ca_presenter_device_key', 1);

    if ($device_enabled) {
      $access_code = $args[0];
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->entityCondition('bundle', 'device_key');
      $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
      $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
      $result = $query->execute();
      $device = node_load_multiple(array_keys($result['node']));
      if (count($device) > 1) {
        return services_error(t('Invalid access code.'), 406);
      }
      $is_there_any_active = FALSE;
      foreach($device as $d) {
        if($d->status == 1) {
          $is_there_any_active = TRUE;
          $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
        }
      }
      if(!$is_there_any_active) {
        return services_error(t('Invalid access code.'), 406);
      }
    }
  }

  if (!ca_presenter_presentation_access('access')) {
    return services_error(t('Access denied.'), 403);
  }

  switch ($permission) {
    case 'index':
      return TRUE;

      break;

    case 'download':
    case 'update':
      $pid = $args[2];
      $presentation = ca_presenter_presentation_get_presentation($pid);

      if (!$presentation->nid) {
        return services_error(t('Invalid data supplied.'), 406);
      }

      if (!ca_presenter_presentation_access('view', $presentation->nid)) {
        return services_error(t('Access denied.'), 403);
      }

      $presentation_status = ca_presenter_presentation_get_status($presentation);

      if ($presentation_status['primary'] != 'PUBLISHED_1') {
        if ($presentation_status['primary'] == 'PUBLISHED_2') {
          return services_error(t("This content is currently published to view on the web only. To download, please visit the admin area and set its status to 'Published (Full)'."), 403);
        } else if ($presentation_status['primary'] == 'BUSY') {
          return services_error(t('This presentation is currently updating. Please try again later.'), 403);
        } else {
          return services_error(t('This presentation is not available.'), 403);
        }
      }

      return TRUE;

      break;
  }

  return FALSE;
}
/* creates a compressed zip file */
function create_zip($files = array(),$destination = '',$overwrite = false) {
	//if the zip file already exists and overwrite is false, return false
	if(file_exists($destination) && !$overwrite) { return false; }
	//vars
	$valid_files = array();
	//if files were passed in...
	if(is_array($files)) {
		//cycle through each file
		foreach($files as $file) {
			//make sure the file exists
			if(file_exists($file)) {
				$valid_files[] = $file;
			}
		}
	}
	//if we have good files...
	if(count($valid_files)) {
		//create the archive
		$zip = new ZipArchive();
		if($zip->open($destination,$overwrite ? ZIPARCHIVE::OVERWRITE : ZIPARCHIVE::CREATE) !== true) {
			return false;
		}
		//add the files
		foreach($valid_files as $file) {
			$zip->addFile($file,$file);
		}
		//debug
		//echo 'The zip archive contains ',$zip->numFiles,' files with a status of ',$zip->status;
		
		//close the zip -- done!
		$zip->close();
		
		//check to make sure the file exists
		return file_exists($destination);
	}
	else
	{
		return false;
	}
}
