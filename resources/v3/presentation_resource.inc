<?php

function ca_presenter_service_presentation_index_v3($access_code, $app_info) {
  $start = microtime();
  ini_set('max_execution_time', 240);
  global $user;
  global $published_full;
  global $base_root;
  global $subdomain;
  $sql = "
	SELECT  DISTINCT
		n.nid as id,
		n.title as title,
		n.uid as author_uid,
		n.status as status,
		fb.body_value as description,
		(
		    SELECT GROUP_CONCAT(td.name)
			FROM field_data_field_taxonomy as ft
		    LEFT JOIN taxonomy_term_data as td ON ft.field_taxonomy_tid = td.tid
		    WHERE n.nid = ft.entity_id
		    AND ft.field_taxonomy_tid IS NOT NULL
		)   as categories,
                (
		    CASE 
	              WHEN n.uid = ".$user->uid." && n.status = 0 THEN 'personal'
	              WHEN n.uid != ".$user->uid." && n.status = 0 THEN 'shared'
        	      WHEN n.status = 1 THEN 'library'
	              ELSE '' 
	              END
		) as `group`,
		u.name as author,
		n.created as created,
		n.changed as published,
		(SELECT '0') as execution_time,
		(
		    SELECT td.name
		    FROM field_data_field_primary_statuses as ps
		    LEFT JOIN taxonomy_term_data as td ON ps.field_primary_statuses_tid = td.tid
		    WHERE n.nid = ps.entity_id
		) as publishing_status,
                ah.field_archive_hash_value as content_hash
	FROM node n
	LEFT JOIN field_data_body as fb ON n.nid = fb.entity_id
	LEFT JOIN field_data_field_taxonomy as ft ON n.nid = ft.entity_id
	LEFT JOIN users as u ON n.uid = u.uid
	LEFT JOIN field_data_field_primary_statuses as fps ON n.nid = fps.entity_id
	LEFT JOIN field_data_field_archive_hash as ah ON n.nid = ah.entity_id
	LEFT JOIN taxonomy_term_data as td2 ON td2.tid = fps.field_primary_statuses_tid
        LEFT JOIN node_access na on n.nid = na.nid
	WHERE n.type = 'presentation' AND
	fps.field_primary_statuses_tid IS NOT NULL AND
	td2.description = 'PUBLISHED_1'
";
  watchdog('services',$sql);
  if(!user_access('administer users')) {
    $sql .= " AND
        (
          na.realm = 'nodeaccess_author' AND na.gid = '".$user->uid."' AND (na.grant_view = 1 OR na.grant_update = 1) OR
          na.realm = 'nodeaccess_uid' AND na.gid = '".$user->uid."' AND (na.grant_view = 1 OR na.grant_update = 1) OR
          na.realm = 'nodeaccess_rid' AND na.gid IN(".implode(',',array_keys($user->roles)).") AND (na.grant_view = 1 OR na.grant_update = 1)
        )
    ";
  }

  $results = db_query($sql);
  $result_set = $results->fetchAll();
  foreach($result_set as $result) {
    $result->categories = array($result->categories);
    $result->execution_time = microtime(true) - $start;
  } 
  return $result_set;
}
function ca_presenter_service_presentation_index_v4($access_code, $app_info, $pid = FALSE) {
  $start = microtime();
  ini_set('max_execution_time', 240);
  global $user;
  global $published_full;
  global $base_root;
  global $subdomain;
  $sql = "
	SELECT DISTINCT 
		n.nid as id,
		n.title as title,
		fb.body_value as description,
		(
		    SELECT GROUP_CONCAT(td.name)
			FROM field_data_field_taxonomy as ft
		    LEFT JOIN taxonomy_term_data as td ON ft.field_taxonomy_tid = td.tid
		    WHERE n.nid = ft.entity_id
		    AND ft.field_taxonomy_tid IS NOT NULL
		)   as categories,
                (
		    CASE 
	              WHEN n.uid = ".$user->uid." && n.status = 0 THEN 'personal'
	              WHEN n.uid != ".$user->uid." && n.status = 0 THEN 'shared'
        	      WHEN n.status = 1 THEN 'library'
	              ELSE '' 
	              END
		) as `group`,
		u.name as author,
		(SELECT 'string') as thumbnail_hash,
                ah.field_archive_hash_value as content_hash,
		n.created as created,
		n.changed as published,
		(SELECT 'string') as thumbnail_data,
		(SELECT 'string') as download_url,
		(SELECT 'string') as download_size,
                ah.field_archive_hash_value as archive_hash,
		(SELECT 0.0) as execution_time
	FROM node n
	LEFT JOIN field_data_body as fb ON n.nid = fb.entity_id
	LEFT JOIN field_data_field_taxonomy as ft ON n.nid = ft.entity_id
	LEFT JOIN users as u ON n.uid = u.uid
	LEFT JOIN field_data_field_primary_statuses as fps ON n.nid = fps.entity_id
	LEFT JOIN field_data_field_archive_hash as ah ON n.nid = ah.entity_id
	LEFT JOIN taxonomy_term_data as td2 ON td2.tid = fps.field_primary_statuses_tid
        LEFT JOIN node_access na on n.nid = na.nid
	WHERE n.type = 'presentation' AND
	fps.field_primary_statuses_tid IS NOT NULL AND
	td2.description = 'PUBLISHED_1'
";
  if(!user_access('administer users')) {
    $sql .= " AND
        (
          na.realm = 'nodeaccess_author' AND na.gid = '".$user->uid."' AND (na.grant_view = 1 OR na.grant_update = 1) OR
          na.realm = 'nodeaccess_uid' AND na.gid = '".$user->uid."' AND (na.grant_view = 1 OR na.grant_update = 1) OR
          na.realm = 'nodeaccess_rid' AND na.gid IN(".implode(',',array_keys($user->roles)).") AND (na.grant_view = 1 OR na.grant_update = 1)
        )
    ";
  }
  $results = db_query($sql);
  $result_set = $results->fetchAll();
  foreach($result_set as $result) {
    $result->categories = array($result->categories);
    $result->execution_time = microtime(true) - $start;

  global $base_root;
  $presentation = node_load($result->id);
  $fulldomain = parse_url($base_root);
  $splitdomain = explode(".", $fulldomain['host']);
  $subdomain = $splitdomain[0];
  $path = 'private://' . $subdomain . '/ca_presenter/presentations/' . $presentation->nid . '/__thumbnails/';
  $first_slide_id = NULL;
    $thumbnail_path = '';
    if (isset($presentation->field_slides["und"])) {
      $first_slide_id = $presentation->field_slides["und"][0]["target_id"];
      $files = glob(drupal_realpath($path) . '/' . $first_slide_id . '.jpg');
    }
    $user_thumbnail = NULL;
    if (isset($presentation->field_cover_thumbnail["und"])) {
      $user_thumbnail = drupal_realpath($presentation->field_cover_thumbnail["und"][0]["uri"]);
    }
    if (isset($user_thumbnail)) {
      $thumbnail_path = $user_thumbnail;
    }
    if (isset($files) && count($files) > 0) {
      $image = file_get_contents($files[0]);
      $scheme = variable_get('file_default_scheme','private');
      $file_storage_path = variable_get('file_'.$scheme.'_path');
      $image_path = drupal_realpath($scheme . '://' . $subdomain . '/ca_presenter/presentations/' . $presentation->nid . '/__thumbnails/' . $first_slide_id . '.jpg');
      $thumbnail_path = $image_path;
    }
    else {
      $thumbnail_path = drupal_realpath("/sites/default/files/no-thumbnail.png");
    }

    if (file_exists($thumbnail_path)) {
      $result->thumbnail_data = file_get_contents($thumbnail_path);
      $result->thumbnail_data = base64_encode($result->thumbnail_data);
      $result->thumbnail_hash = md5_file($thumbnail_path);
      $result->download_url = $base_root . '/presentations/' . $presentation->nid . '/download';
    }
  } 
  return $result_set;
}

/*function ca_presenter_service_presentation_index_v4($access_code, $app_info, $pid = FALSE) {
  $start = microtime();
  global $user;
  global $published_full;
  global $published_web;
  global $base_url;
  global $subdomain;
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'presentation');
  if(isset($pid) && $pid && $pid != '' && $pid*1 > 0) {
    $query->propertyCondition('nid',$pid);
  }
  $result = $query->execute();
  $presentations = node_load_multiple(array_keys($result['node']));

  $data = array();

  if (count($presentations) > 0) {
    foreach ($presentations as $presentation) {
      $presentation_status = taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']);
      $presentation_categories = taxonomy_term_load($presentation->field_taxonomy['und'][0]['tid'])->name;
      $presentation_group = ca_presenter_presentation_get_group($presentation);
      $node_user = user_load($presentation->uid);
      if($presentation_status->tid == $published_full->tid || $presentation_status->tid == $published_web->tid) {
        $thumbnail_path = ca_presenter_presentation_get_thumbnail_path($presentation);
        if (file_exists($thumbnail_path)) {
          $thumbnail_data = file_get_contents($thumbnail_path);
          $thumbnail_data = base64_encode($thumbnail_data);
          $thumbnail_hash = md5_file($thumbnail_path);
        }

        $download_url = url('/presentations/'.$presentation->nid.'/download',array('absolute' => TRUE));
        $file_path = drupal_realpath('private://'.$subdomain.'/ca_presenter/presentations/'.$presentation->nid.'.zip');
        $archive_size = filesize($file_path);
        $archive_hash = md5_file($file_path);

        $item = array(
          'id' => $presentation->nid,
          'title' => $presentation->title,
          'description' => $presentation->body['und'][0]['value'],
          'categories' => array($presentation_categories),
          'group' => ca_presenter_presentation_get_group($presentation),
          'author' => $node_user->name,
          'thumbnail_hash' => $thumbnail_hash,
          'content_hash' => $archive_hash,
          'created' => $presentation->created,
          'published' => format_date($presentation->created,'short'),
          'thumbnail_data' => $thumbnail_data,
          'download_url' => $download_url,
          'download_size' => $archive_size,
          'archive_hash' => $archive_hash,
          'execution_time' => microtime(true) - $start
        );

        $data[] = $item;
      }
    }
  }
  return $data;

}*/

function ca_presenter_service_presentation_thumbnail_v3($access_code, $app_info, $pids) {
  global $published_full;
  foreach ($pids as $pid) {
    $presentation = node_load($pid);

    if (!$presentation->nid) {
      return services_error(t('Invalid data supplied.'), 406);
    }

    $presentation_status = taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']);

    if($presentation_status->tid != $published_full->tid) {
      continue;
    }
    $thumbnail_path = ca_presenter_presentation_get_thumbnail_path($presentation);
    if (file_exists(drupal_realpath($thumbnail_path))) {
      //Check for base64 file
      $info = pathinfo(drupal_realpath($thumbnail_path));
      $base64_path = $info['dirname'] . '/../__base64/' . $info['filename'] . '.base64';
      $md5_path = $info['dirname'] . '/../__base64/' . $info['filename'] . '.md5';
      if(file_exists($base64_path)) {
	watchdog('services thumbnail','Hash files found. Continue with preserved datas.',NULL,WATCHDOG_INFO);
        $thumbnail_data = file_get_contents($base64_path);
        $thumbnail_hash = file_get_contents($md5_path);
      } else {
	watchdog('services thumbnail','Hash files NOT found. Continue with generation base64 and md5 datas.',NULL,WATCHDOG_WARNING);
        $thumbnail_data = file_get_contents($thumbnail_path);
        $thumbnail_data = base64_encode($thumbnail_data);
        $thumbnail_hash = md5_file($thumbnail_path);
      }
    }

    $item = array(
      'id' => $presentation->nid,
      'thumbnail_hash' => $thumbnail_hash,
      'thumbnail_data' => $thumbnail_data,
    );

    $data[] = $item;
  }
  return $data;
}

function ca_presenter_service_presentation_download_v3($access_code, $app_info, $pid) {
  global $base_url;
  global $subdomain;
  $download_url = url('/presentations/'.(int)$pid.'/download',array('absolute' => TRUE));
  $file_path = drupal_realpath('private://'.$subdomain.'/ca_presenter/presentations/'.(int)$pid.'.zip');
  $archive_size = filesize($file_path);
  $archive_hash = md5_file($file_path);

  return array(
    'download_url' => $download_url,
    'download_size' => $archive_size,
    'content_hash' => $archive_hash,
  );
}

function ca_presenter_service_presentation_update_v3($access_code, $app_info, $pid, $remote_files) {
  global $subdomain;
  if(!is_array($remote_files)) {
    $remote_files = json_decode($remote_files,TRUE);
  }
  $presentation = node_load($pid);
  $primary_status = taxonomy_term_load($presentation->field_primary_statuses[LANGUAGE_NONE][0]['tid'])->name;
  $secondary_status = taxonomy_term_load($presentation->field_secondary_statuses[LANGUAGE_NONE][0]['tid'])->name;

  $presentation_path = ca_presenter_presentation_get_path($presentation);
  $server_files = ca_presenter_presentation_get_files($presentation);

  $update_files = array();
  $delete_files = array();

  $download_size = 0;

  // Append and update files
  if (count($server_files > 0)) {
    foreach ($server_files as $server_file_id => $server_file) {
      if ($server_file['path'] == 'files.json') continue;

      if (!isset($remote_files[$server_file_id])) {
          $update_files[$server_file_id] = $server_file;
          $update_files[$server_file_id]['download_url'] = file_create_url('private://'. $subdomain . '/ca_presenter/presentations/' . $presentation->nid . '/' . $server_file['path']);
          $update_files[$server_file_id]['path'] = $server_file['path'];

          $download_size += $update_files[$server_file_id]['size'];
      } else {
        if ($remote_files[$server_file_id]['modified'] != $server_file['modified'] || $remote_files[$server_file_id]['size'] != $server_file['size']) {
          $update_files[$server_file_id] = $server_file;
          $update_files[$server_file_id]['download_url'] = file_create_url('private://'. $subdomain . '/ca_presenter/presentations/' . $presentation->nid . '/' . $server_file['path']);
          $update_files[$server_file_id]['path'] = $server_file['path'];

          $download_size += $update_files[$server_file_id]['size'];
        }
      }
    }
  }

  // Delete files
  if (count($remote_files) > 0) {
    foreach ($remote_files as $remote_file_id => $remote_file) {
      if (isset($remote_files['path']) && $remote_files['path'] == 'files.json') continue;

      if (!isset($server_files[$remote_file_id])) {
        $delete_files[$remote_file_id] = $remote_file;
      }
    }
  }

  // Files metadata
  if (count($update_files) > 0 || count($delete_files) > 0) {
    $file_id = md5('files.json');

    $update_files[$file_id] = $server_files[$file_id];
    $update_files[$file_id]['download_url'] = file_create_url('private://'. $subdomain . '/ca_presenter/presentations/' . $presentation->nid . '/' . $server_files[$file_id]['path']);
    $update_files[$file_id]['path'] = $server_files[$file_id]['path'];

    $download_size += $update_files[$file_id]['size'];
  }

  $archive_hash = file_create_url('private://'. $subdomain . '/ca_presenter/presentations/' . $presentation->nid . '/' . $server_files[$file_id]['path']);
  $archive_hash = md5_file(drupal_realpath('private://'.$subdomain.'/ca_presenter/presentations/'.$presentation->nid.'.zip'));
  if($secondary_status == "UPDATED") {
    return array(
      'download_size' => 0,
      'content_hash' => $archive_hash,
      'update_files' => $update_files,
      'delete_files' => $delete_files,
    );
  }

  return array(
    'download_size' => $download_size,
    'content_hash' => $archive_hash,
    'update_files' => $update_files,
    'delete_files' => $delete_files,
  );
}

/**
 * Access.
 *
 * @param $permission
 * @param array $args
 *
 * @return mixed
 */
function ca_presenter_service_presentation_access_v3($permission, $args = array()) {
  module_load_include('inc', 'ca_presenter', 'ca_presenter_api/device_key_resource');
  module_load_include('inc', 'ca_presenter', 'includes/ca_device');
  global $user;
  $account = user_load($user->uid);
  if(isset($account->profile_cms) && $account->profile_cms == "1") {
    return FALSE;
  }
  $access_code = $args[0];
  $device_enabled = variable_get('ca_presenter_device_key', 1);
  $u = user_load($user->uid);
  if ((int)$u->uid !== 1 && $device_enabled == 1) {
    $query = new EntityFieldQuery();
    $query->entityCondition('entity_type', 'node');
    $query->entityCondition('bundle', 'device_key');
    $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
    $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
    $result = $query->execute();
    $device = node_load_multiple(array_keys($result['node']));
    if (count($device) > 1) {
      return services_error(t('Invalid access code.'), 406);
    }
    $is_there_any_active = FALSE;
    foreach($device as $d) {
      if($d->status == 1) {
        $is_there_any_active = TRUE;
        $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
      }
    }
    if(!$is_there_any_active) {
      return services_error(t('Invalid access code.'), 406);
    }
  }

  switch ($permission) {
    case 'index':
      return TRUE;
      break;
    case 'download':
    case 'update':
      $pid = $args[2];
      $presentation = node_load($pid);

      if (!$presentation->nid) {
        return services_error(t('Invalid data supplied.'), 406);
      }
      return TRUE;
      break;
  }
  return FALSE;
}
