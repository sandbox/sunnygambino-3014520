<?php

function ca_presenter_service_event_push_v3($access_code, $app_info, $events) {
  global $user;
  //Get Salesforce contacts
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'crm_contacts')
    ->execute();
  $contact_nodes = array();
  if(isset($result['node'])) {
    $contact_nodes = node_load_multiple(array_keys($result['node'])); 
  }
  //End of SF contacts
  if(is_string($events)) {
    $test_events = json_decode($events,TRUE);
    if(is_array($test_events) && count($test_events) != 0) {
      $events = $test_events;
    }
  }
  if (!is_array($events) || count($events) == 0) {
    return array(
      'crm_contacts' => $contact_nodes
    );
  }

  $event_ids = array();
  foreach ($events as $event) {
    $crmid = FALSE;
    //Dev variables
    //$event['crm_id'] = '0032000001D8iAeAAJ';
    //*************
    $crmid = (!isset($event['crm_id'])) ? NULL : $event['crm_id'];
    $metadata = (!isset($event['metadata']) || $event['metadata'] == '') ? NULL : $event['metadata'];
    $session_note = '';
    if($event['type'] == 'SESSION_NOTES') {
      $session_notes = $event['metadata'];
      $event['name'] = nl2br($session_notes);
    }
    if ($metadata != NULL) {
      $array_data = json_decode($metadata, true);
      if(isset($array_data[0]))
        $array_data = $array_data[0]['data'];
      $array_data['crm_id'] = $crmid;
      if(!$array_data) {
	return array(
	  'error' => t('Cannot decode metadata as JSON. Metadata is: '.$metadata),
	  'crm_contacts' => $contact_nodes
	);
      }
      //Dev variables
      //$array_data['crm_id'] = '0032000001D8iAeAAJ';
      //*************
      $crmid = ($array_data['crm_id'] == NULL) ? NULL : $array_data['crm_id'];
      $note = ($array_data[0]['data'] == '') ? NULL : $array_data[0]['data'];

      if ($array_data == NULL) {
	return array(
	  'error' => t('Metadata is empty.'),
	  'crm_contacts' => $contact_nodes
	);
      }
      $flat_data = ca_presenter_event_array_flatten($array_data, array(), '', 0);

      if (!is_array($flat_data)) {
	return array(
	  'error' => t('Cannot flatten metadata array.'),
	  'crm_contacts' => $contact_nodes
	);
      }
    }
    $type = ($crmid == '') ? $event['type'] : 'CRM_'.$event['type'];
    //$type = $event['type'];
    $name = '';
    if(isset($event['name']) && !empty($event['name'])) {
      $name = $event['name'];
    }
    $presentation_id = (!isset($event['presentation_id']) || $event['presentation_id'] == '') ? NULL : $event['presentation_id'];
    $slide_id = (!isset($event['slide_id']) || $event['slide_id'] == '') ? NULL : $event['slide_id'];
    $document_id = (!isset($event['document_id']) || $event['document_id'] == '') ? NULL : $event['document_id'];
    $view_id = (!isset($event['view_id']) || $event['view_id'] == '') ? NULL : $event['view_id'];
    $session_id = (!isset($event['session_id']) || $event['session_id'] == '') ? NULL : $event['session_id'];
    $client_id = (!isset($event['client_id']) || $event['client_id'] == '') ? NULL : $event['client_id'];
    $client_name = (!isset($event['client_name']) || $event['client_name'] == '') ? NULL : $event['client_name'];
    $location_lat = (!isset($event['location_lat']) || $event['location_lat'] == '') ? NULL : $event['location_lat'];
    $location_lng = (!isset($event['location_lng']) || $event['location_lng'] == '') ? NULL : $event['location_lng'];
    $metadata = (!isset($event['metadata']) || $event['metadata'] == '') ? NULL : $event['metadata'];
    $viewer_mode = 0;
    $registered_time = $event['registered_time'];
    $received_time = time();
    if ($type == '' || (int)$registered_time == 0) {
      return array(
        'error' => 'Invalid data supplied. No type or regtime.',
        'crm_contacts' => $contact_nodes
      );
    }

    $new_event = new stdClass();
    $new_event->type = "event";
    node_object_prepare($new_event);
    $new_event->title = $name;
    if($name === NULL)
      $new_event->title = $type;
    $new_event->language = LANGUAGE_NONE;
    $new_event->uid = $user->uid;
    $new_event->status = 1;
    $new_event->promote = 0;
    if(NULL !== $presentation_id)
      $new_event->field_presentation_id['und'][0]['target_id'] = $presentation_id; 
    if(NULL !== $document_id)
      $new_event->field_document_id['und'][0]['target_id'] = $document_id;
    if(isset($event['metadata']))
      $new_event->field_metadata_value['und'][0]['value'] = $event['metadata'];
    $new_event->field_secondary_name['und'][0]['value'] = $type;
    if(isset($device))
      $new_event->field_device_id['und'][0]['value'] = $device;
    if(NULL !== $slide_id && !empty($slide_id))
      $new_event->field_slide_id['und'][0]['target_id'] = $slide_id;
    $new_event->field_view_id['und'][0]['value'] = $view_id;
    $new_event->field_session_id['und'][0]['value'] = $session_id;
    $new_event->field_client_id['und'][0]['value'] = $client_id;
    $new_event->field_client_name['und'][0]['value'] = $client_name;
    $new_event->field_location_lat['und'][0]['value'] = $location_lat;
    $new_event->field_location_lng['und'][0]['value'] = $location_lng;
    $new_event->field_viewer_mode['und'][0]['value'] = $viewer_mode;
    $new_event->field_received['und'][0]['value'] = $registered_time;
    $new_event->field_registered['und'][0]['value'] = time();
    
    node_save($new_event);
    $eid = $new_event->nid;

    if (!$eid) {
      return array(
        'error' => t('No event ID.'),
        'crm_contacts' => $contact_nodes
      );
    }
    if($crmid) {
      if(!$event_ids[$crmid]) {
        $event_ids[$crmid] = array();
      }
      array_push($event_ids[$crmid],$eid);
    }
  } //endforeach
    /*$crm_report_id = variable_get('ca_presenter_crm_report_id',0);
    if($crm_report_id == 0) {
      drupal_set_message(t('Cannot find CRM report ID. Please set it up <a href="/settings/reports">here</a>!'),'warning');
    }
    if(count($event_ids) > 0 && $crm_report_id != 0) {
      foreach($event_ids as $ci => $ei) { //crmid and event id
	$eids = array();
	if(count($ei) > 0) {
	  for($i=0;$i<count($ei);$i++) {
	    array_push($eids,$ei[$i]);
	    //Save session to DB
            //ca_presenter_session_insert($ci,$ei[$i]);
	  }
	}
        global $base_url;
        global $user;
	$new = new stdClass();
  	$new->type = "sf_account_hitory";
	$new->title = "Companyapp report at ".date('d/m/Y H:i:s');
	$new->uid = $user->uid;
	$new->field_sf_name_id['und'][0]['value'] = $ci;
	if(isset($note) && $note != '')
	  $new->field_sf_description['und'][0]['value'] = $note.PHP_EOL.PHP_EOL.'Presentation report: '.$base_url.'/reports/'.$crm_report_id.'/view?id='.implode($eids,',').'&crmid='.$ci.'&op=Show';
	else
	  $new->field_sf_description['und'][0]['value'] = 'Presentation report: '.$base_url.'/reports/'.$crm_report_id.'/view?id='.implode($eids,',').'&crmid='.$ci.'&op=Show';
	$new->field_sf_due_date_only['und'][0]['value'] = date('Y-m-d',$registered_time);
	$new->field_sf_subject['und'][0]['value'] = "Companyapp report at ".date('d/m/Y H:i:s');
	$new->field_sf_created_date['und'][0]['value'] = date('Y-m-dTH:i:s.uZ');
	$new->field_sf_due_date_time['und'][0]['value'] = date('Y-m-dTH:i:s.uZ');
	$new->field_sf_duration['und'][0]['value'] = ($received_time-$registered_time)*60;
	$new->field_sf_start_date_time['und'][0]['value'] = date('Y-m-dTH:i:s.uZ',$registered_time);
	$new->field_sf_status['und'][0]['value'] = 'Completed';
	node_save($new);

        $maps = salesforce_api_salesforce_field_map_load_by(array('drupal' => 'node_' . $new->type));
        $map = reset($maps);
        $salesforce = salesforce_api_id_load('node', $new->nid);
        $salesforce->name = $map->name;
        try {
          sf_node_export($new, $salesforce->name, $salesforce->sfid);
        } catch (Exception $e) {
          salesforce_api_log(SALESFORCE_LOG_SOME, 'Exception while attempting to export node: ' . $e->getMessage(), array(), WATCHDOG_ERROR, l('node ' . $new->nid, 'node/' . $new->nid));
        }
	$new = node_load(FALSE, NULL, TRUE);
      }
    }*/

  // Action
  //TODO: run action code
  //

  return array(
    'pushed' => count($events),
    'crm_contacts' => $contact_nodes
  );
}

/**
 * Access.
 */
function ca_presenter_service_event_access_v3($permission, $args = array()) {
  global $user;

  if ($user->uid != 1) {
    $device_enabled = variable_get('ca_presenter_device_key', 1);

    if ($device_enabled == 1) {
      $access_code = $args[0];
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->entityCondition('bundle', 'device_key');
      $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
      $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
      $result = $query->execute();
      $device = node_load_multiple(array_keys($result['node']));
      if (count($device) > 1) {
        return services_error(t('Invalid access code.'), 406);
      }
      $is_there_any_active = FALSE;
      foreach($device as $d) {
        if($d->status == 1) {
          $is_there_any_active = TRUE;
          $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
        }
      }
      if(!$is_there_any_active) {
        return services_error(t('Invalid access code.'), 406);
      }
    }
  }

  if (!user_is_logged_in()) {
    return services_error(t('Access denied.'), 403);
  }

  switch ($permission) {
    case 'push':
      return TRUE;

      break;
  }

  return FALSE;
}
