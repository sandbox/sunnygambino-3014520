<?php

function ca_presenter_service_document_index_v3($access_code, $app_info) {
  global $user;
  global $published_full;
  $account = user_load($user);
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'document')
    ->execute();
  $documents = node_load_multiple(array_keys($result['node']));

  $data = array();

  if (count($documents) > 0) {
    foreach ($documents as $document) {
      if ($document->field_primary_statuses[LANGUAGE_NONE][0]['tid'] != $published_full->tid || !node_access('view',$document,$account)) {
        continue;
      }
      $document_file_field = file_load($document->field_file['und'][0]['fid']);

      $publish_file_name = $document_file_field->filename;
      $publish_mime_type = $document_file_field->filemime;
      $archive_hash = md5_file($document_file_field->uri);
      if(isset($document->field_document_category['und'][0]['tid']) && $document->field_document_category['und'][0]['tid'] != '')
        $document_categories = taxonomy_term_load($document->field_document_category['und'][0]['tid'])->name;
      else
        $document_categories = '';

      $item = array(
        'id' => $document->nid,
        'title' => $document->title,
        'description' => (isset($document->body['und']) ? $document->body['und'][0]['value'] : ''),
        'categories' => $document_categories,
        'file_name' => $publish_file_name,
        'mime_type' => $publish_mime_type,
        'content_hash' => $archive_hash,
        'created' => $document->created,
        'published' => $document->changed,
      );

      $data[] = $item;
    }
  }

  return $data;
}

function ca_presenter_service_document_index_v4($access_code, $app_info, $did = FALSE) {
  global $published_full;
  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', 'node');
  $query->entityCondition('bundle', 'document');
  if($did && isset($did) && $did != '')
    $query->propertyCondition('nid',$did);
  $result = $query->execute();
  $documents = node_load_multiple(array_keys($result['node']));

  $data = array();

  if (count($documents) > 0) {
    foreach ($documents as $document) {
      if ($document->field_primary_statuses[LANGUAGE_NONE][0]['tid'] != $published_full->tid || !node_access('view',$document)) {
        continue;
      }
      $document_file_field = file_load($document->field_file['und'][0]['fid']);

      $publish_file_name = $document_file_field->filename;
      $publish_mime_type = $document_file_field->filemime;
      $archive_hash = md5_file($document->field_file['und'][0]['uri']);

      $item = array(
        'id' => $document->nid,
        'title' => $document->title,
        'description' => $document->body['und'][0]['value'],
        'categories' => array(taxonomy_term_load($document->field_document_category['und'][0]['tid'])->name),
        'file_name' => $publish_file_name,
        'mime_type' => $publish_mime_type,
        'content_hash' => $archive_hash,
        'created' => $document->created,
        'published' => $document->changed,
      );

      $data[] = $item;
    }
  }

  return $data;
}


function ca_presenter_service_document_download_v3($access_code, $app_info, $did) {
  $document = node_load($did);
  //$download_url = file_create_url($document->field_file['und'][0]['uri']);
  $download_url = url('/documents/'.$did.'/download_zip',array('absolute' => TRUE));
  $archive_size = $document->field_file['und'][0]['filesize'];
  $archive_hash = md5_file($document->field_file['und'][0]['uri']);
  $document_file_field = file_load($document->field_file['und'][0]['fid']);
  $publish_file_name = $document_file_field->filename;
  $publish_mime_type = $document_file_field->filemime;

  return array(
    'download_url' => $download_url,
    'download_size' => $archive_size,
    'content_hash' => $archive_hash,
    'file_name' => $publish_file_name,
    'mime_type' => $publish_mime_type,
  );
}


/**
 * Access.
 */
function ca_presenter_service_document_access_v3($permission, $args = array()) {
  global $user;
  $account = user_load($user->uid);
  if(isset($account->profile_cms) && $account->profile_cms == "1") {
    return FALSE;
  }

  if ($user->uid != 1) {
    $device_enabled = variable_get('ca_presenter_device_key', 1);

    if ($device_enabled == 1) {
      $access_code = $args[0];
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->entityCondition('bundle', 'device_key');
      $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
      $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
      $result = $query->execute();
      $device = node_load_multiple(array_keys($result['node']));
      if (count($device) > 1) {
        return services_error(t('Invalid access code.'), 406);
      }
      $is_there_any_active = FALSE;
      foreach($device as $d) {
        if($d->status == 1) {
          $is_there_any_active = TRUE;
          $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
        }
      }
      if(!$is_there_any_active) {
        return services_error(t('Invalid access code.'), 406);
      }
    }
  }

  switch ($permission) {
    case 'index':
      return TRUE;

      break;

    case 'download':
    case 'update':
      global $published_full;
      global $published_web;
      $did = $args[2];
      $document = node_load($did);

      if (!$document->nid) {
        return services_error(t('Invalid data supplied.'), 406);
      }

      $document_status = $document->status;
      $status = taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']);
      if($status->tid =! $published_full->tid && $status->tid != $published_web->tid) {
        return services_error(t("This content is currently published to view on the web only. To download, please visit the admin area and set its status to 'Published (Full)'."), 403);
      }

      return TRUE;

      break;
  }

  return FALSE;
}
