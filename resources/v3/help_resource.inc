<?php

function ca_presenter_service_help_retrieve_v3($app_info) {
  global $base_url;
  list($app_type, $app_device) = explode('.', $app_info['type']);
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'help')
    ->execute();
  $nodes = node_load_multiple(array_keys($result['node']));
  $help_content = t('No help available');
  if(empty($nodes) || !isset($nodes)) {
    return array(
      'content' => $help_content,
    );
  }
  foreach($nodes as $n) {
    if ($app_type == 'desktop' && $n->field_help_mobile['und'][0]['value'] == "0") {
      $help_content = $n->field_help_body['und'][0]['value'];
    }
    if ($app_type == 'mobile' && $n->field_help_mobile['und'][0]['value'] == "1") {
      $help_content = $n->field_help_body['und'][0]['value'];
    }
  }

  $help_content = str_replace('[site-url]',$base_url,$help_content);

  return array(
    'content' => $help_content,
  );
}

/**
 * Access.
 */
function ca_presenter_service_help_access_v3($permission, $args = array()) {
  return TRUE;
  global $user;

  if (!user_is_logged_in()) {
    return services_error(t('Access denied.'), 403);
  }

  switch ($permission) {
    case 'retrieve':
      return TRUE;

      break;
  }

  return FALSE;
}
