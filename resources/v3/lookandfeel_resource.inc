<?php

function ca_presenter_service_lookandfeel_retrieve_v3($app_info) {
  global $subdomain;
  list($app_type, $app_device) = explode('.', $app_info['type']);
  $logo_image_path = drupal_realpath('private://'.$subdomain.'/ca_presenter/lookandfeel/logo.png');

  if (file_exists($logo_image_path)) {
    $logo_image_data = file_get_contents($logo_image_path);
  }

  return array(
    'logo' => base64_encode($logo_image_data),
  );
}

/**
 * Access.
 */
function ca_presenter_service_lookandfeel_access_v3($permission, $args = array()) {
  global $user;

  if (!user_is_logged_in()) {
    return services_error(t('Access denied.'), 403);
  }

  switch ($permission) {
    case 'retrieve':
      return TRUE;

      break;
  }

  return FALSE;
}
