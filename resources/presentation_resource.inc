<?php

/**
 * Presentation viewer.
 */
function ca_presenter_service_presentation_viewer($pid, $reference, $x = NULL) {
  global $user;

  // Reference
  if ($reference != '') {
    $email = ca_presenter_email_get_email_by_reference($reference);

    if (!$email->eid && !$email->nid) {
      watchdog('ca_presenter_emails', 'Could not find email for presentation -' . $pid . ' and reference: '. $reference );
      return array('error' => t('Can\'t find email.'));
    }

    $presentations = ca_presenter_email_get_presentations($email);

    if (count($presentations) == 0) {
      $presentations = array();
      foreach(explode(',',$email->presentations) as $pid) {
        $presentations[] = node_load($pid);
      }
      if(count($presentations) <= 0)
      return array('error' => t('Can\'t find presentation.'));
    }

    $found = false;
    foreach ($presentations as $presentation) {
      if ($presentation->nid == $pid) {
        $found = true;
        break;
      }
    }

    if (!$found) {
      return array('error' => t('Can\'t find presentation ID.'));
    }

    $presentation_status = ca_presenter_presentation_get_status($presentation);

    if ($presentation_status['primary'] != 'PUBLISHED_1' && $presentation_status['primary'] != 'PUBLISHED_2') {
      return array('error' => t('Presentation not published.'));
    }
  } else {
    if (!ca_presenter_presentation_access('view', $pid)) {
      return array('error' => t('You have no access to view this presentation.'));
    }

    $presentation = ca_presenter_presentation_get_presentation($pid);
    $presentation_status = ca_presenter_presentation_get_status($presentation);

    if ($presentation_status['primary'] == 'BUSY') {
      return array('error' => t('This presentation is busy at this moment. Please try again within some minutes.'));
    }
  }
  //

  $slides = ca_presenter_presentation_get_slides($presentation);

  $tag_field_name = variable_get('ca_presenter_tag_field', '');
  $tags = array();

  if ($tag_field_name != '') {
    foreach ($slides as $slide) {
      $tag_field = $slide->$tag_field_name;
      $tag_field_value = $tag_field[0]['value'];

      if ($tag_field_value != '') {
        $tags[$slide->nid] = $tag_field_value;
      }
    }
  }

  // Event
  $email_address = NULL;
  if ($reference != '') {
      $encrypted_email_address = $x;

      if ($encrypted_email_address != '') {
        $email_address = mcrypt_ecb(MCRYPT_RIJNDAEL_256, md5($email->eid . ':' . $email->reference), base64_decode(strtr($encrypted_email_address, '-_,', '+/=')), MCRYPT_DECRYPT);

        if ($email_address != '') {
          ca_presenter_email_add_event($email, $presentation, NULL, 'PRESENTATION_VIEW', $email_address, time());
        }
      }
  }
  //

  // Stats
  if ($reference != '' && $email->field_email_notifications['und'][0]['value'] == 1) {
    $send_notification = FALSE;
    $website_id = variable_get('ca_presenter_stats_website_id', '');
    $cookie_names = array_keys($_COOKIE);

    if (count($cookie_names) > 0) {
      foreach ($cookie_names as $cookie_name) {
        if (substr($cookie_name, 0, 7 + strlen($website_id)) == '_pk_id_' . $website_id) {
          list($visitor_id, $created_timestamp, $visit_count, $current_visit_timestamp, $last_visit_timestamp) = explode('.', $_COOKIE[$cookie_name]);
        }
      }
    }

    if ($visitor_id != '') {
      $_slides = ca_presenter_presentation_get_slides($presentation);
      $sid = $_slides[0]->nid;
      $url = variable_get('ca_presenter_stats_server_host', '');
      $url .= "?module=API&method=Live.getLastVisitsDetails";
      $url .= "&idSite=" . variable_get('ca_presenter_stats_website_id', '') . "&period=year&date=today";
      $url .= "&segment=visitorId==" . $visitor_id . ";customVariableValue1==" . $reference . ";customVariableValue2==" . $pid;
      if($sid)
        $url .= ";customVariableValue3==" . $sid;
      $url .= "&filter_limit=100";
      $url .= "&format=PHP";
      $url .= "&token_auth=" . variable_get('ca_presenter_stats_api_key', '');

      $fetched = file_get_contents($url);
      $content = unserialize($fetched);

      if (is_array($content) && count($content) == 0) {
        // No visits
        $send_notification = TRUE;
      } else if (!isset($content['result'])) {
        // New visit
        $last_visit = $content[0];

        if ((time() - $last_visit['lastActionTimestamp']) > 1800) {
          $send_notification = TRUE;
        }
      }
    } else {
      // Visitor doesn't exist
      $send_notification = TRUE;
    }

    if ($send_notification) {
      $html_body = '<p>Hi ' . $email->from_name . ",</p>" .
                   '<p>A new visitor looked at the presentation.</p>' .
                   '<p>Presentation details:</p>' .
                   '<table border="0" cellpadding="0" cellspacing="0"><tr><td style="padding-right: 10px;">Title:&nbsp;</td><td>' . $presentation->title . '</td></tr><tr><td style="padding-right: 10px;">Sent to:&nbsp;</td><td style="padding-right: 10px;">' . $email->to_name . '</td></tr><tr><td style="padding-right: 10px;">Sent date:&nbsp;</td><td>' . date('d M y - H:i', $email->created). '</td></tr></table>' .
                   '<p>Please <a href="'. url('emails2/' . $email->eid . '/presentation/' . $pid . '/stats', array('absolute' => TRUE)) . '">click here</a> for more details.</p>' .
                   '<p>Thanks,<br />The Companyapp team</p>';

      $text_body = "Hi ". $email->from_name . ",\n\n" .
                   "A new visitor looked at the presentation.\n\n" .
                   "Presentation details:\n\n" .
                   "Title:\t\t" . $presentation->title . "\nSent to:\t" . $email->to_name . "\nSent date:\t" . date('d M y - H:i', $email->created) ."\n\n" .
                   "Please follow this URL for more details - " . url('emails2/' . $email->eid . '/presentation/' . $pid . '/stats', array('absolute' => TRUE)) . "\n\n" .
                   "Thanks,\nThe Companyapp team";

      $mail = new PHPMailer();
      $mail->IsSMTP();
      $mail->Host = 'localhost';
      $mail->CharSet = 'UTF-8';

      $smtp_username = variable_get('ca_presenter_mail_smtp_username', '');
      $smtp_password = variable_get('ca_presenter_mail_smtp_password', '');

      if ($smtp_username != '' && $smtp_password != '') {
        $mail->SMTPAuth = true;
        $mail->Username = $smtp_username;
        $mail->Password = $smtp_password;
      }

      $mail->IsHTML(true);

      $mail->From = 'no-reply@companyapp.co.uk';
      $mail->FromName = 'Companyapp presenter';
      $mail->Subject = 'Notification - ' . $email->to_name;
      $mail->Body = $html_body;
      $mail->AltBody = $text_body;

      $mail->AddAddress($email->reply_email);

      $result = $mail->Send();

      if (!$result) {
      }
    }
  }
  //
  //drupal_add_js(drupal_get_path('module', 'ca_presenter') . '/piwik.js');
  //return theme('ca_presenter_pages_presentation_viewer', $presentation, $tags, $slides, $reference);
  return array('status' => 'OK');
}

function ca_presenter_service_presentation_index($access_code) {
  global $user;
  global $published_full;
  global $published_web;
  global $base_root;
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'presentation')
    ->execute();
  $presentations = node_load_multiple(array_keys($result['node']));

  $data = array();

  if (count($presentations) <= 0 || !$presentation) {
    return array();
  }
  foreach ($presentations as $presentation) {
    $presentation_path = ca_presenter_presentation_get_path($presentation->nid);
    $status = taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']);
    $node_user = user_load($presentation->uid);
    
    if($status->tid == $published_full->tid || $status->tid == $published_web->tid) {
      $thumbnail_path = ca_presenter_presentation_get_thumbnail_path($presentation);

      if (file_exists($thumbnail_path)) {
        $thumbnail_data = file_get_contents($thumbnail_path);
        $thumbnail_data = base64_encode($thumbnail_data);
      }

      $item = array(
        'id' => $presentation->nid,
        'title' => $presentation->title,
        'description' => $presentation->body,
        'categories' => taxonomy_term_load($presentation->field_taxonomy['und'][0]['tid'])->name,
        'author' => $node_user->name,
        'group' => ca_presenter_presentation_get_group($presentation),
        'thumbnail' => $thumbnail_data,
        'created' => $presentation->created,
      );

      $data[] = $item;
    }
  }

  return $data;
}

function ca_presenter_service_presentation_download($access_code, $pid) {
  global $base_url;
  global $subdomain;
  $download_url = url('/presentations/'.(int)$pid.'/download',array('absolute' => TRUE));
  $file_path = drupal_realpath('private://'.$subdomain.'/ca_presenter/presentations/'.(int)$pid.'.zip');
  $archive_size = filesize($file_path);

  return array(
    'download_url' => $download_url,
    'download_size' => $archive_size,
  );
}

function ca_presenter_service_presentation_update($access_code, $pid, $remote_files) {
  global $subdomain;
  $presentation = node_load($pid);
  $presentation_path = ca_presenter_presentation_get_path($pid);
  $presentation_path = drupal_realpath('private://' . $subdomain . '/ca_presenter/presentations/' . $presentation->nid);
  
  $server_files = glob(drupal_realpath($presentation_path). '/');

  $update_files = array();
  $delete_files = array();

  $download_size = 0;

  // Append and update files
  if (count($server_files > 0)) {
    foreach ($server_files as $server_file_id => $server_file) {
      if ($server_file['path'] == 'files.json') continue;

      if (!isset($remote_files[$server_file_id])) {
          $update_files[$server_file_id] = $server_file;
          $update_files[$server_file_id]['download_url'] = ca_presenter_presentation_get_file_download_url($pid, $server_file['path']);
          $update_files[$server_file_id]['path'] = $server_file['path'];

          $download_size += $update_files[$server_file_id]['size'];
      } else {
        if ($remote_files[$server_file_id]['modified'] != $server_file['modified'] || $remote_files[$server_file_id]['size'] != $server_file['size']) {
          $update_files[$server_file_id] = $server_file;
          $update_files[$server_file_id]['download_url'] = ca_presenter_presentation_get_file_download_url($pid, $server_file['path']);
          $update_files[$server_file_id]['path'] = $server_file['path'];

          $download_size += $update_files[$server_file_id]['size'];
        }
      }
    }
  }

  // Delete files
  if (count($remote_files) > 0) {
    foreach ($remote_files as $remote_file_id => $remote_file) {
      if ($remote_files['path'] == 'files.json') continue;

      if (!isset($server_files[$remote_file_id])) {
        $delete_files[$remote_file_id] = $remote_file;
      }
    }
  }

  // Files metadata
  if (count($update_files) > 0 || count($delete_files) > 0) {
    $file_id = md5('files.json');

    $update_files[$file_id] = $server_files[$file_id];
    $update_files[$file_id]['download_url'] = url('/presentations/' . $pid . '/download/file/'.$file_path, array('absolute' => TRUE));
    $update_files[$file_id]['path'] = $server_files[$file_id]['path'];

    $download_size += $update_files[$file_id]['size'];
  }

  return array(
    'download_size' => $download_size,
    'update_files' => $update_files,
    'delete_files' => $delete_files,
  );
}

/**
 * Access.
 */
function ca_presenter_service_presentation_access($permission, $args = array()) {
  global $user;
  $account = user_load($user->uid);
  if(isset($account->profile_cms) && $account->profile_cms == "1") {
    return FALSE;
  }
  return TRUE;

  if ($user->uid != 1) {
    $access_code = $args[0];
    $device_enabled = variable_get('ca_presenter_device_key', 1);
    if ($device_enabled == 1) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->entityCondition('bundle', 'device_key');
      $query->fieldCondition('field_device_key_access_code', 'value', $access_code, '=');
      $query->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');
      $result = $query->execute();
      $device = node_load_multiple(array_keys($result['node']));
      if (count($device) > 1) {
        return services_error(t('Invalid access code.'), 406);
      }
      $is_there_any_active = FALSE;
      foreach($device as $d) {
        if($d->status == 1) {
          $is_there_any_active = TRUE;
          $d->field_device_key_app_info[LANGUAGE_NONE][0]['value'] = serialize($args[1]);
        }
      }
      if(!$is_there_any_active) {
        return services_error(t('Invalid access code.'), 406);
      }
    }
  }

  if (user_access('administer presentations')) {
    return TRUE;
  }

  if (!user_access('access presentations')) {
    return FALSE;
  }

  switch ($permission) {
    case 'index':
      return user_access('access presentations');
      break;
    case 'download':
    case 'update':
      $pid = $args[1];
      $presentation = ca_presenter_presentation_get_presentation($pid);
      if ($presentation && $presentation->status == 1 && ca_presenter_presentation_access('view', $pid)) {
        return TRUE;
      }
      break;
  }

  return FALSE;
}
