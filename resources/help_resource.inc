<?php

function ca_presenter_service_help_retrieve($app_type) {
  $query = new EntityFieldQuery();
  $result = $query->entityCondition('entity_type', 'node')
    ->entityCondition('bundle', 'help')
    ->execute();
  $nodes = node_load_multiple(array_keys($result['node']));
  $help_content = t('No help available');
  foreach($nodes as $n) {
    if ($app_type == 'desktop' && $n->field_help_mobile['und'][0]['value'] == "0") {
      $help_content = $n->field_help_body['und'][0]['value'];
    }
    if ($app_type == 'mobile' && $n->field_help_mobile['und'][0]['value'] == "1") {
      $help_content = $n->field_help_body['und'][0]['value'];
    }
  }

  return array(
    'content' => $help_content,
  );
}

/**
 * Access.
 */
function ca_presenter_service_help_access($permission, $args = array()) {
  global $user;

  if(!$user->uid) {
    return FALSE;
  }

  switch ($permission) {
    case 'retrieve':
      return TRUE;
      break;
  }

  return FALSE;
}
