<?php
  /**
   * Looking for PresenterSalesforce class? 
   * @see: ca_presenter_salesforce.info
   * @see: ca_presenter_salesforce.inc
   */

   function ca_presenter_perm() {
    return array(
      'use_salesforce' => array(
        'title' => t('Use SalesForce functionality'),
      )
    );
   }
  /**
   * Implements hook_menu().
   */
  function ca_presenter_salesforce_menu() {
    $items = array();
    $items['salesforce'] = array(
      'title' => 'CA  Salesforce Authentication',
      'description' => '',
      'page callback' => 'ca_presenter_salesforce_auth',
      'access arguments' => array('use_salesforce'),
      'type' => MENU_NORMAL_ITEM,
    );
    $items['salesforce/salesforce_oauth_callback'] = array(
      'title' => 'Salesforce oauth callback',
      'page callback' => 'ca_presenter_salesforce_oauth_callback',
      'access arguments' => array('use_salesforce'),
      'type' => MENU_CALLBACK,
    );
    $items['salesforce/autocomplete_contacts'] = array(
      'title' => 'Salesforce autocomplete contacts',
      'page callback' => 'ca_presenter_salesforce_autocomplete_contacts',
      'access arguments' => array('use_salesforce'),
      'type' => MENU_CALLBACK,
    );
    $items['admin/config/salesforce_extension_settings'] = array(
      'title' => 'Salesforce extension',
      'description' => 'Salesforce extension settings.',
      'position' => 'right',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('access administration pages'),
      'file path' => drupal_get_path('module', 'system'),
      'file' => 'system.admin.inc',
    );
    $items['admin/config/salesforce_extension_settings/authorize'] = array(
      'title' => 'Authorize',
      'description' => 'Authorize this user to communicate with Salesforce.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('ca_presenter_salesforce_oauth_form'),
      'access arguments' => array('administer salesforce'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 0,
    );
    $items['admin/config/salesforce_extension_settings/settings'] = array(
      'title' => 'Settings',
      'description' => 'Additional settings for the Salesforce Suite.',
      'page callback' => 'drupal_get_form',
      'page arguments' => array('ca_presenter_salesforce_settings_form'),
      'access arguments' => array('administer salesforce'),
      'type' => MENU_NORMAL_ITEM,
      'weight' => 1,
    );
    $items['admin/structure/salesforce_extension_settings'] = array(
      'title' => 'Salesforce',
      'description' => 'Configuration for Salesforce integration.',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('administer salesforce'),
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
    );
    $items['admin/structure/salesforce_extension_settings/mappings'] = array(
      'title' => 'Salesforce Mappings',
      'description' => 'Manage Salesforce mappings',
      'page callback' => 'system_admin_menu_block_page',
      'access arguments' => array('administer salesforce'),
      'file' => 'system.admin.inc',
      'file path' => drupal_get_path('module', 'system'),
    );
    return $items;
  }
  
  /**
   * Implement hook_form_alter
   * 
   * @param type $form
   * @param type $form_state
   * @param type $form_id
   */
  function ca_presenter_salesforce_form_alter(&$form, &$form_state, $form_id) {
    if($form_id == 'user_profile_form' && user_access('use_salesforce')) {
      $form['Salesforce'] = array(
        '#type' => 'fieldset',
        '#title' => 'Salesforce',
        '#weight' => 2
      );
      $form['Salesforce']['salesforce_authentication'] = array(
        '#type' => 'button',
        '#button_type' => 'submit',
        '#value' => t('Authenticate to SalesForce or refresh data manually'),
        '#attributes' => array('onclick' => 'window.location.href = "/salesforce";return false;')
      );
    }
    if($form_id == 'email_node_form') {
      $form['field_recipients_email']['#id'] = 'field_recipients_email';
      $form['field_recipients_email'][LANGUAGE_NONE][0]['value']['#autocomplete_path'] = 'salesforce/autocomplete_contacts';
    }
  }
  
  function ca_presenter_salesforce_autocomplete_contacts($string) {
    $string = explode(',', $string);
    $string = end($string);
    $matches = array();
    $query = db_select( 'node', 'n' )
      ->fields( 'n', array('title'));
    $query->leftJoin('field_data_field_crm_email','email','n.nid = email.entity_id');
    $query->addField('email', 'field_crm_email_value');
    $query->condition('title', '%' . db_like($string) . '%', 'LIKE');
    $query->condition('type', 'crm_contacts', '=');
    
    $result = $query->execute();

    // save the query to matches
    foreach ($result as $row) {
      $matches[$row->field_crm_email_value] = check_plain($row->title);
    }

    // Return the result to the form in json
    drupal_json_output($matches);
  }

  /**
   * Change the author of SF node to the actual user
   * 
   * @global object $user
   * @param object $node
   */
  function ca_presenter_salesforce_node_presave($node) {
    if($node->type == 'crm_contacts') {
      global $user;
      $node->uid = $user->uid;
    }
  }

  /**
   * Authentication with SF API
   */
  function ca_presenter_salesforce_auth() {
    $consumer_key = variable_get('salesforce_consumer_key', FALSE);
    $consumer_secret = variable_get('salesforce_consumer_secret', FALSE);
    $salesforce_endpoint = variable_get('salesforce_endpoint', 'https://login.salesforce.com');
    $salesforce = new PresenterSalesforce($consumer_key, $consumer_secret);
    $authCode = $salesforce->getAuthorizationCode();
  }

  /**
  * Callback for the oauth redirect URI.
  *
  * Exchanges an authorization code for an access token.
  */
  function ca_presenter_salesforce_oauth_callback() {
   // If no code is provided, return access denied.
   if (!isset($_GET['code'])) {
     return drupal_access_denied();
   }
   $salesforce = ca_presenter_salesforce_get_api();

   if ($salesforce->requestToken($_GET['code'])) {
     salesforce_set_message(t('Salesforce OAUTH2 authorization successful.'));

     // Rebuild the menu so the mappings links are now available.
     menu_rebuild();
   }
   else {
     salesforce_set_message(t('Salesforce OAUTH2 authorization failed, likely due to inadequate OAUTH scope. Ensure your app has scope %scope selected.', array('%scope' => 'Perform requests on your behalf at any time')), 'warning');
   }

   drupal_goto('/');
  }

  /**
  * Wrapper around the API constructor passing consume key and secret.
  *
  * @return PresenterSalesforce
  *   Returns a PresenterSalesforce class object.
  */
  function ca_presenter_salesforce_get_api() {
   return new PresenterSalesforce(
     variable_get('salesforce_consumer_key', ''),
     variable_get('salesforce_consumer_secret', '')
   );
  }

  /**
   * Check Salesforce authentication status
   * and do REST API pull
   * @return boolean
   */
  function ca_presenter_salesforce_pull() {
    $sfapi = ca_presenter_salesforce_get_api();
    variable_del('salesforce_pull_last_sync_Contact');
    if ($sfapi->isAuthorized() && salesforce_pull_check_throttle()) {
      ca_presenter_salesforce_pull_get_updated_records($sfapi);
      //salesforce_pull_process_deleted_records();
      // Store this request time for the throttle check.
      variable_set('salesforce_pull_last_sync', REQUEST_TIME);
      return TRUE;
    }
    // No pull happened.
    return FALSE;
  }

  /**
  * Webhook callback for salesforce pull. Returns status of 200 for successful
  * attempt or 403 for a failed pull attempt (SF not authorized, threshhold
  * reached, etc.
  */
  function ca_presenter_salesforce_pull_webhook() {
    if (ca_presenter_salesforce_pull()) {
      $code = '200';
      // Queue is populated, but not processed yet so we manually do some of what
      // drupal_cron_run() does to trigger processing of our pull queue.
      $queues = salesforce_cron_queue_info();
      $info = $queues[SALESFORCE_PULL_QUEUE];
      $callback = $info['worker callback'];
      $end = time() + (isset($info['time']) ? $info['time'] : 15);
      $queue = DrupalQueue::get(SALESFORCE_PULL_QUEUE);
      while (time() < $end && ($item = $queue->claimItem())) {
        try {
          call_user_func($callback, $item->data);
          $queue->deleteItem($item);
        }
        catch (Exception $e) {
          // In case of exception log it and leave the item in the queue
          // to be processed again later.
          watchdog_exception('salesforce_pull', $e);
        }
      }
    }
    else {
      $code = '403';
    }
    http_response_code($code);
  }

  /**
   * Doing SF pull
   * 
   * @param object $sfapi The Salesforce API
   * @return undefined
   */
  function ca_presenter_salesforce_pull_get_updated_records($sfapi = FALSE) {
    $queue = DrupalQueue::get(SALESFORCE_PULL_QUEUE);

    // Avoid overloading the processing queue and pass this time around if it's
    // over a configurable limit.
    if ($queue->numberOfItems() > variable_get('salesforce_pull_max_queue_size', 100000)) {
      return;
    }
    if($sfapi === FALSE)
      $sfapi = ca_presenter_salesforce_get_api();
    foreach (salesforce_mapping_get_mapped_objects() as $type) {
      $mapped_fields = array();
      $mapped_record_types = array();

      // Iterate over each field mapping to determine our query parameters.
      foreach (salesforce_mapping_load_multiple(array('salesforce_object_type' => $type)) as $mapping) {
        foreach ($mapping->field_mappings as $field_map) {
          // Exclude field mappings that are only drupal to SF.
          if (in_array($field_map['direction'], array(
            SALESFORCE_MAPPING_DIRECTION_SYNC,
            SALESFORCE_MAPPING_DIRECTION_SF_DRUPAL
          ))) {
            // Some field map types (Relation) store a collection of SF objects.
            if (is_array($field_map['salesforce_field']) && !isset($field_map['salesforce_field']['name'])) {
              foreach ($field_map['salesforce_field'] as $sf_field) {
                $mapped_fields[$sf_field['name']] = $sf_field['name'];
              }
            }
            // The rest of are just a name/value pair.
            else {
              $mapped_fields[$field_map['salesforce_field']['name']] = $field_map['salesforce_field']['name'];
            }
          }
        }

        if (!empty($mapped_fields) && $mapping->salesforce_record_type_default != SALESFORCE_MAPPING_DEFAULT_RECORD_TYPE) {
          foreach ($mapping->salesforce_record_types_allowed as $record_type) {
            if ($record_type) {
              $mapped_record_types[$record_type] = $record_type;
            }
          }
          // Add the RecordTypeId field so we can use it when processing the
          // queued SF objects.
          $mapped_fields['RecordTypeId'] = 'RecordTypeId';
        }
      }

      // There are no field mappings configured to pull data from Salesforce so
      // move on to the next mapped object. Prevents querying unmapped data.
      if (empty($mapped_fields)) {
        continue;
      }

      $soql = new SalesforceSelectQuery($type);
      // Convert field mappings to SOQL.
      $soql->fields = array_merge($mapped_fields, array(
        'Id' => 'Id',
        $mapping->pull_trigger_date => $mapping->pull_trigger_date
      ));

      // If no lastupdate, get all records, else get records since last pull.
      $sf_last_sync = variable_get('salesforce_pull_last_sync_' . $type, NULL);
      if ($sf_last_sync) {
        $last_sync = gmdate('Y-m-d\TH:i:s\Z', $sf_last_sync);
        $soql->addCondition($mapping->pull_trigger_date, $last_sync, '>');
      }

      // If Record Type is specified, restrict query.
      if (count($mapped_record_types) > 0) {
        $soql->addCondition('RecordTypeId', $mapped_record_types, 'IN');
      }

      // Execute query.
      $results = $sfapi->query($soql);
      $version_path = parse_url($sfapi->getApiEndPoint(), PHP_URL_PATH);

      if (!isset($results['errorCode'])) {
        // Write items to the queue.
        foreach ($results['records'] as $result) {
          $queue->createItem($result);
        }

        // Handle requests larger than the batch limit (usually 2000).
        $next_records_url = isset($results['nextRecordsUrl']) ?
          str_replace($version_path, '', $results['nextRecordsUrl']) :
          FALSE;
        while ($next_records_url) {
          $new_result = $sfapi->apiCall($next_records_url);
          if (!isset($new_result['errorCode'])) {
            // Write items to the queue.
            foreach ($new_result['records'] as $result) {
              $queue->createItem($result);
            }
          }
          $next_records_url = isset($new_result['nextRecordsUrl']) ?
            str_replace($version_path, '', $new_result['nextRecordsUrl']) : FALSE;
        }

        variable_set('salesforce_pull_last_sync_' . $type, REQUEST_TIME);
      }
      else {
        watchdog('Salesforce Pull', $results['errorCode'] . ':' . $results['message'], array(), WATCHDOG_ERROR);
      }
    }
  }
  
  //Administrative functions like settings, help etc...
  
  /**
  * Implements hook_help().
  */
 function ca_presenter_salesforce_help($path, $arg) {
   global $user;
   $account = user_load($user->uid);
   switch ($path) {
     case 'admin/structure/salesforce_extension_settings':
       $output = '';
       if (!module_exists('salesforce_mapping')) {
         $output .= '<p>' . t('In order to configure Salesforce Mappings, you must first enable the <a href="/admin/modules">Salesforce Mapping</a> module and at least one sync method (Push or Pull).') . '</p>';
       }
       if (!variable_get('salesforce_consumer_secret', NULL) || !variable_get('salesforce_consumer_key', NULL) || !$account->profile_sf_refresh_token) {
         $output .= '<p>' . t('You must !authorize in order to configure Salesforce Mappings.', array('!authorize' => l(t('authorize your account with Salesforce'), 'admin/config/salesforce/authorize'))) . '</p>';
       }
       return $output;

     case 'admin/help#salesforce_extension_settings':
       $output = '';
       $output .= '<h3>' . t('About') . '</h3>';
       $output .= '<p>' . t('This module suite implements a mapping functionality between Salesforce
   objects and Drupal entities. In other words, for each of your supported Drupal
   entities (e.g. node, user, or entities supported by extensions), you can
   assign Salesforce objects that will be created / updated when the entity is
   saved. For each such assignment, you choose which Drupal and Salesforce fields
   should be mapped to one another.') . '</p>';
       $output .= '<p>' . t('This suite also includes an API architecture which allows for additional
   modules to be easily plugged in (e.g. for webforms, contact form submits,
   etc).') . '</p>';

       $output .= '<p>' . t('For a more detailed description of each component module, see below.') . '</p>';
       $output .= '<h3>' . t('Requirements') . '</h3>';
       $output .= '<ol>';
       $output .= '<li>' . t('You need a Salesforce account. Developers can !register_here.', array('!register_here' => l(t('register here'), 'http://www.developerforce.com/events/regular/registration.php'))) . '</li>';
       $output .= '<li>' . t('You will need to create a remote application/connected app for authorization.') . '</li>';
       $output .= '<ul>';
       $output .= '<li>' . t('In Salesforce go to Your Name > Setup > Create > Apps then create a new Connected App. (Depending on your Salesforce instance, you may need to go to Your Name > Setup > Develop > Remote Access.)') . '</li>';
       $output .= '<li>' . t('Set the callback URL to: !url (SSL is required)', array(
         '!url' => '<code>' . url('salesforce/oauth_callback', array(
           'absolute' => TRUE,
           'https' => TRUE,
         )) .  '</code>')) . '</li>';
       $output .= '<li>' . t('Select at least "Perform requests on your behalf at any time" for OAuth Scope
   as well as the appropriate other scopes for your application. Note that "Full access" does not include the "Perform requests on your behalf at any time" scope! !info.', array('!info' => l(t('Additional information'), 'https://help.salesforce.com/help/doc/en/remoteaccess_about.htm'))) . '</li>';
       $output .= '<li>' . t('For more help see !salesforce.', array('!salesforce' => l(t('the salesforce.com documentation'), 'https://www.salesforce.com/us/developer/docs/api_rest/Content/quickstart_oauth.htm'))) . '</li>';
       $output .= '</ul>';
       $output .= '<li>' . t('Your site needs to be SSL enabled to authorize the remote application using OAUTH.') . '</li>';
       $output .= '<li>' . t('If using the SOAP API, PHP must be compiled with !SOAP and !SSL.',
         array(
           '!SOAP' => l(t('SOAP web services'), 'http://php.net/soap'),
           '!SSL' => l(t('OpenSSL support'), 'http://php.net/openssl'))
         ) . '</li>';
       $output .= '</ol>';
       $output .= '<h4>' . t('Required modules') . '</h4>';
       $output .= '<ul>';
       $output .= '<li>' . l(t('Entity API'), 'http://drupal.org/project/entity') . '</li>';
       $output .= '<li>' . l(t('Libraries, only for SOAP API'), 'http://drupal.org/project/libraries') . '</li>';
       $output .= '</ul>';
       $output .= '<h3>' . t('Modules') . '</h3>';
       $output .= '<h4>' . t('Salesforce (salesforce)') . '</h4>';
       $output .= '<p>' . t('OAUTH2 authorization and wrapper around the Salesforce REST API.') . '</p>';
       $output .= '<h4>' . t('Salesforce Mapping (salesforce_mapping)') . '</h4>';
       $output .= '<p>' . t('Map Drupal entities to Salesforce fields, including field level mapping.') . '</p>';
       $output .= '<h4>' . t('Salesforce Push (salesforce_push)') . '</h4>';
       $output .= '<p>' . t('Push Drupal entity updates into Salesforce.') . '</p>';
       $output .= '<h4>' . t('Salesforce Pull (salesforce_pull)') . '</h4>';
       $output .= '<p>' . t('Pull Salesforce object updates into Drupal on cron run. (Salesforce Outbound Notifications are not supported.)') . '</p>';
       $output .= '<h4>' . t('Salesforce Soap (salesforce_soap)') . '</h4>';
       $output .= '<p>' . t('Lightweight wrapper around the SOAP API, using the OAUTH access token, to fill in functional gaps missing in the REST API. Requires the Salesforce PHP Toolkit.') . '</p>';
       $output .= '<p>' . t('Example installation of the Salesforce PHP Toolkit using the provided Drush Make file:') . ' <code>drush make /path/to/salesforce/modules/salesforce_soap/salesforce_soap.make.example --no-core -y</code>' . '</p>';
       return $output;

     case 'admin/config/salesforce_extension_settings':
     case 'admin/config/salesforce_extension_settings/authorize':
       return '<p>' . t('Visit !help if you need help obtaining a consumer key and secret.', array('!help' => l(t('the Salesforce module help page'), 'admin/help/salesforce_extension_settings'))) . '</p>';
   }
 }
  
  /**
   * Settings form for Presenter Salesforce
   * @return string
   */
  function ca_presenter_salesforce_oauth_form() {
    $form = array();

    $consumer_key = variable_get('salesforce_consumer_key', FALSE);
    $consumer_secret = variable_get('salesforce_consumer_secret', FALSE);
    $salesforce_endpoint = variable_get('salesforce_endpoint', 'https://login.salesforce.com');

    $form['message'] = array(
      '#type' => 'item',
      '#markup' => t('Authorize this user to communicate with Salesforce by entering the consumer key and secret from a remote application. Clicking authorize will redirect you to Salesforce where you will be asked to grant access.'),
    );

    $form['salesforce_consumer_key'] = array(
      '#title' => t('Salesforce consumer key'),
      '#type' => 'textfield',
      '#description' => t('Consumer key of the Salesforce remote application you want to grant access to'),
      '#required' => TRUE,
      '#default_value' => $consumer_key,
    );
    $form['salesforce_consumer_secret'] = array(
      '#title' => t('Salesforce consumer secret'),
      '#type' => 'textfield',
      '#description' => t('Consumer secret of the Salesforce remote application you want to grant access to'),
      '#required' => TRUE,
      '#default_value' => $consumer_secret,
    );
    $form['advanced'] = array(
      '#title' => t('Advanced'),
      '#type' => 'fieldset',
      '#collapsible' => TRUE,
      '#collapsed' => TRUE,
    );
    $form['advanced']['salesforce_endpoint'] = array(
      '#title' => t('Salesforce endpoint'),
      '#type' => 'textfield',
      '#description' => t('Enter the URL of your Salesforce environment (for example, <code>https://test.salesforce.com</code>). <strong>Caution:</strong> Note that switching this setting after you have already synchronised data between your Drupal site and Salesforce will render any existing links between Salesforce objects and Drupal objects invalid!'),
      '#default_value' => $salesforce_endpoint,
    );
    $form['submit'] = array(
      '#value' => t('Authorize'),
      '#type' => 'submit',
    );

    // If we're authenticated, show a list of available REST resources.
    if ($consumer_key && $consumer_secret) {
      $sfapi = new PresenterSalesforce($consumer_key, $consumer_secret);
      // If fully configured, attempt to connect to Salesforce and return a list
      // of resources.
      if ($sfapi->isAuthorized()) {
        try {
          $resources = $sfapi->listResources();
          foreach ($resources as $key => $path) {
            $items[] = $key . ': ' . $path;
          }
          $form['resources'] = array(
            '#title' => t('Your Salesforce instance is authorized and has access to the following resources:'),
            '#type' => 'item',
            '#markup' => theme('item_list', array('items' => $items)),
          );
        }
        catch(SalesforceException $e) {
          salesforce_set_message($e->getMessage(), 'warning');
        }
      }
      else {
        salesforce_set_message(t('Salesforce needs to be authorized to connect to this website.'), 'error');
      }
    }

    return $form;
  }
  
 /**
  * Generate the Salesforce settings form.
  */
 function ca_presenter_salesforce_settings_form($form, &$form_state) {
   // Get the default or current setting.
   $api_version = variable_get('salesforce_api_version', array(
     "label" => "Spring '13",
     "url" => "/services/data/v27.0/",
     "version" => "27.0",
   ));
   $api_list = array($api_version);

   // If we're authenticated, get the full list of available versions.
   $salesforce = ca_presenter_salesforce_get_api();
   $instance = $salesforce->getInstanceUrl();
   if ($instance) {
     // Get all available API versions.
     $api_list = drupal_http_request($instance . "/services/data");
     $api_list = drupal_json_decode($api_list->data);
   }
   // Store api list for use in our validate callback.
   $form_state['api_list'] = $api_list;

   // Build the options array.
   $options = array();
   foreach ($api_list as $api) {
     $options[$api['version']] = $api['label'] . ' (' . $api['version'] . ')';
   }

   $form['salesforce_api_version'] = array(
     '#type' => 'select',
     '#title' => t('API Version'),
     '#description' => t('Select the version of the Salesforce API to use'),
     '#default_value' => $api_version['version'],
     '#options' => $options,
   );

   $form['#validate'][] = 'ca_presenter_salesforce_settings_form_validate';

   return system_settings_form($form);
 }
 
  /**
  * Validation handler for salesforce_settings_form().
  */
 function ca_presenter_salesforce_settings_form_validate($form, &$form_state) {
   // Convert version to the array of api info.
   $version = $form_state['values']['salesforce_api_version'];
   if ($version) {
     // Loop through available api versions to find a match.
     foreach ($form_state['api_list'] as $api) {
       if ($api['version'] == $version) {
         form_set_value($form['salesforce_api_version'], $api, $form_state);
         break;
       }
     }
   }
 }
 
  //@TODO: Implement pull in cron to work with user accounts
  /**
   * Implements hook_cron().
   */
  /*function ca_presenter_salesforce_cron() {
    $query = db_select('profile_field', 'pf');
    $query->leftJoin('profile_value','pv','pf.fid = pv.fid');
    $query->fields('pv', array('uid'));
    $query->condition('pf.name', 'profile_sf_access_token', '=');
    $query->condition('pv.value', '', '!=');
    $uids = $query->execute()->fetchAll();
    ca_presenter_salesforce_pull();
  }*/
?>