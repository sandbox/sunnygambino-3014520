<?php
/**
 * Salesforce class extension
 * Using user profile fields to authenticate to SF
 * Every methos is is an override of the original class
 */
class PresenterSalesforce extends Salesforce {
  /**
   * Check if authenticated to SF
   * @return boolean
   */
  public function isAuthorized() {
    return !empty($this->consumer_key) && !empty($this->consumer_secret) && $this->getRefreshToken();
  }

  /**
   * 
   * @global type $user
   * @return null | string The Refresh token
   */
  protected function getRefreshToken() {
    global $user;
    $account = user_load($user->uid);
    return (isset($account->profile_sf_refresh_token)) ? $account->profile_sf_refresh_token : NULL;
  }

  /**
   * Set tokens into profile fields
   * @throws SalesforceException
   */
  protected function refreshToken() {
    $refresh_token = $this->getRefreshToken();
    if (empty($refresh_token)) {
      throw new SalesforceException(t('There is no refresh token.'));
    }

    $data = drupal_http_build_query(array(
      'grant_type' => 'refresh_token',
      'refresh_token' => $refresh_token,
      'client_id' => $this->consumer_key,
      'client_secret' => $this->consumer_secret,
    ));

    $url = $this->login_url . '/services/oauth2/token';
    $headers = array(
      // This is an undocumented requirement on Salesforce's end.
      'Content-Type' => 'application/x-www-form-urlencoded',
    );
    $response = $this->httpRequest($url, $data, $headers, 'POST');

    if ($response->code != 200) {
      // @TODO: Deal with error better.
      throw new SalesforceException(t('Unable to get a Salesforce access token.'), $response->code);
    }

    $data = drupal_json_decode($response->data);

    if (isset($data['error'])) {
      throw new SalesforceException($data['error_description'], $data['error']);
    }

    $this->setAccessToken($data['access_token']);
    $this->setIdentity($data['id']);
    $this->setInstanceUrl($data['instance_url']);
  }

  /**
   * OAuth step 1: Redirect to Salesforce and request and authorization code.
  */
  public function getAuthorizationCode() {
    $url = $this->login_url . '/services/oauth2/authorize';
    $query = array(
      'redirect_uri' => $this->redirectUrl(),
      'response_type' => 'code',
      'client_id' => $this->consumer_key,
    );

    drupal_goto($url, array('query' => $query));
  }
  
  /**
   * Helper to build the redirect URL for OAUTH workflow.
   * Overrided to use custom menu point
   *
   * @return string
   *   Redirect URL.
   */
  protected function redirectUrl() {
    return url('salesforce/salesforce_oauth_callback', array(
      'absolute' => TRUE,
      'https' => TRUE,
    ));
  }
  
  /**
   * OAuth step 2: Exchange an authorization code for an access token.
   *
   * @param type $code
   * @return boolean
   * @throws SalesforceException
   */
  public function requestToken($code) {
    $data = drupal_http_build_query(array(
      'code' => $code,
      'grant_type' => 'authorization_code',
      'client_id' => $this->consumer_key,
      'client_secret' => $this->consumer_secret,
      'redirect_uri' => $this->redirectUrl(),
    ));

    $url = $this->login_url . '/services/oauth2/token';
    $headers = array(
      // This is an undocumented requirement on SF's end.
      'Content-Type' => 'application/x-www-form-urlencoded',
    );
    $response = $this->httpRequest($url, $data, $headers, 'POST');

    $data = drupal_json_decode($response->data);

    if ($response->code != 200) {
      $error = isset($data['error_description']) ? $data['error_description'] : $response->error;
      throw new SalesforceException($error, $response->code);
    }

    // Ensure all required attributes are returned. They can be omitted if the
    // OAUTH scope is inadequate.
    $required = array('refresh_token', 'access_token', 'id', 'instance_url');
    foreach ($required as $key) {
      if (!isset($data[$key])) {
        return FALSE;
      }
    }

    //@TODO: Save details belongs to USER!
    $this->setRefreshToken($data['refresh_token']);
    $this->setAccessToken($data['access_token']);
    $this->setIdentity($data['id']);
    $this->setInstanceUrl($data['instance_url']);
    //Update all SF object for the current user
    ca_presenter_salesforce_pull_webhook();
    return TRUE;
  }

  /**
   * Save refresh token to user profile
   * 
   * @global type $user
   * @param type $token
   */
  protected function setRefreshToken($token) {
    global $user;
    $account = user_load($user->uid);
    $new_account = array();
    $new_account['profile_sf_refresh_token'] = $token;
    $new_account['profile_sf_access_token'] = $account->profile_sf_access_token;
    $new_account['profile_sf_identity'] = $account->profile_sf_identity;
    $new_account['profile_sf_instance_url'] = $account->profile_sf_instance_url;
    user_save($account,(array)$new_account,'Salesforce');
  }

  /**
   * Save access token to user profile
   * 
   * @global type $user
   * @param type $token
   */
  protected function setAccessToken($token) {
    $_SESSION['salesforce_access_token'] = $token;
    global $user;
    $account = user_load($user->uid);
    $new_account = array();
    $new_account['profile_sf_access_token'] = $token;
    $new_account['profile_sf_refresh_token'] = $account->profile_sf_refresh_token;
    $new_account['profile_sf_identity'] = $account->profile_sf_identity;
    $new_account['profile_sf_instance_url'] = $account->profile_sf_instance_url;
    user_save($account,(array)$new_account,'Salesforce');
  }

  /**
   * Get access token from user profile
   * 
   * @global type $user
   * @return type
   */
  public function getAccessToken() {
    global $user;
    $account = user_load($user->uid);
    return $account->profile_sf_access_token;
  }

  /**
   * Save ID to user profile
   * 
   * @global type $user
   * @param type $id
   * @throws SalesforceException
   */
  protected function setIdentity($id) {
    $headers = array(
      'Authorization' => 'OAuth ' . $this->getAccessToken(),
      'Content-type' => 'application/json',
    );
    $response = $this->httpRequest($id, NULL, $headers);
    if ($response->code != 200) {
      throw new SalesforceException(t('Unable to access identity service.'), $response->code);
    }
    //$data = drupal_json_decode($response->data);
    $data = $response->data;
    global $user;
    $account = user_load($user->uid); 
    $new_account = array();
    $new_account['profile_sf_identity'] = $data;
    $new_account['profile_sf_refresh_token'] = $account->profile_sf_refresh_token;
    $new_account['profile_sf_access_token'] = $account->profile_sf_access_token;
    $new_account['profile_sf_instance_url'] = $account->profile_sf_instance_url;
    user_save($account,(array)$new_account,'Salesforce');
  }

  /**
   * Get ID from user profile
   * 
   * @global type $user
   * @return string
   */
  public function getIdentity() {
    global $user;
    $account = user_load($user->uid);
    return json_decode($account->profile_sf_identity,TRUE);
  }

  /**
   * Save instance URL to user profile
   * 
   * @global type $user
   * @param string $url
   */
  protected function setInstanceUrl($url) {
    global $user;
    $account = user_load($user->uid);
    $new_account = array();
    $new_account['profile_sf_instance_url'] = $url;
    $new_account['profile_sf_refresh_token'] = $account->profile_sf_refresh_token;
    $new_account['profile_sf_access_token'] = $account->profile_sf_access_token;
    $new_account['profile_sf_identity'] = $account->profile_sf_identity;
    user_save($account,(array)$new_account,'Salesforce');
  }

  /**
   * Get instance URL from user profile
   * 
   * @global type $user
   * @return string
   */
  public function getInstanceUrl() {
    global $user;
    $account = user_load($user->uid);
    return $account->profile_sf_instance_url;
  }
}