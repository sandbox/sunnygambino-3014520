<?php
class ca_presenter_handler_filter_anniversary extends views_handler_filter {
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  function exposed_form(&$form, &$form_state) {
    $anniversary = array(
      'All' => '- ' . t('All months') . ' -',
      '1' => t('January'),
      '2' => t('February'),
      '3' => t('March'),
      '4' => t('April'),
      '5' => t('May'),
      '6' => t('June'),
      '7' => t('July'),
      '8' => t('August'),
      '9' => t('September'),
      '10' => t('October'),
      '11' => t('November'),
      '12' => t('December'),
    );
    $form['anniversary'] = array(
      '#type' => 'select',
      '#title' => '',
      '#options' => $anniversary,
      '#default_value' => (isset($this->options['anniversary']) ? $this->options['anniversary'] : 'All'),
    );
  }

  function query() {
    global $user;
    $this->ensure_my_table();
    if (isset($this->value[0])) {
      $value = $this->value[0];
    } else {
      $value = 'all';
    }

    if($value === 'all')
      return;
    if($value !== 'all') {
        $this->query->add_where(
          $this->options['group'],
          db_and()->condition('profile_value_profile_anniversary.value', '%"month";s:'.strlen($value).':"'.$value.'"%', 'LIKE')
        );
    }
  }
}
