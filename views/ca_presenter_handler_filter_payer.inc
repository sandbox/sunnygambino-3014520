<?php
class ca_presenter_handler_filter_payer extends views_handler_filter {
  // You'll need an options form (just use the parent)
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  // You can use a views_form or an
  function exposed_form(&$form, &$form_state) {
    // Return a Form API select element
    // ... all, this-day, this-fornight, this-month
    
    $payers = db_query('select n.nid, n.title,r.field_reference_code_value from node n left join field_data_field_reference_code r on n.nid = r.entity_id where n.type=:node_type order by n.created desc', [
      'node_type' => 'payer'
    ]);   
    $payers = $payers->fetchAll();
    $p = array(
      'All' => '- ' . t('All payers'). ' -',
      'NULL' => '- ' . t('No payer'). ' -'
    );
    foreach($payers as $pk => $payer) {
      $p[$payer->nid] = $payer->title . ' ('.$payer->field_reference_code_value.')';
    }
    $form['payer'] = array(
      '#type' => 'select',
      '#title' => '',
      '#options' => $p,
      '#default_value' => (isset($this->options['payer']) ? $this->options['payer'] : 'All'),
    );
  }

  // The query to get your results based on your time period
  function query() {
    global $user;
    $this->ensure_my_table();

    // Get the value of the submitted filter
    if (isset($this->value[0])) {
      $value = $this->value[0];
    } else {
      $value = 'all';
    }

    if($value === 'all')
      return;
    if($value !== 'all' && $value !== 'NULL') {
        $this->query->add_where(
          $this->options['group'],
          db_and()->condition('profile_value_profile_payer.value', $value, '=')
        );
    } elseif($value == 'NULL') {
        $this->query->add_where(
          $this->options['group'],
          db_or()
          ->condition('profile_value_profile_payer.value',NULL , 'IS')
          ->condition('profile_value_profile_payer.value',0 , '=')
          ->condition('profile_value_profile_payer.value','' , '=')
        );
    }
  }
}
