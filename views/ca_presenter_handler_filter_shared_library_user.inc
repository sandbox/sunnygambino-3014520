<?php
class ca_presenter_handler_filter_shared_library_user extends views_handler_filter {
  // You'll need an options form (just use the parent)
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
  }

  // You can use a views_form or an
  function exposed_form(&$form, &$form_state) {
    // Return a Form API select element
    // ... all, this-day, this-fornight, this-month
    $form['shared_library_user'] = array(
      '#type' => 'select',
      '#title' => '',
      '#options' => array(
        'All' => '- ' . t('All Libraries') . ' -',
        'library' => t('Core Library'),
        'user' => t('User Library'),
        'shared' => t('Shared Library')
      ),
      '#default_value' => (isset($this->options['shared_library_user']) ? $this->options['shared_library_user'] : 'All'),
    );
  }

  // The query to get your results based on your time period
  function query() {
    global $user;
    $this->ensure_my_table();

    // Get the value of the submitted filter
    if (isset($this->value[0])) {
      $value = $this->value[0];
    } else {
      $value = 'all';
    }

    // A switch on the $value
    switch( $value ) {
      case 'all':
        return;
      case 'library':
        $this->query->add_where(
          $this->options['group'],
          db_and()->condition('node.status', '1', '=')
        );
        break;
      case 'shared':
        $this->query->add_where(
          $this->options['group'],
          db_and()->condition('users_node.uid', $user->uid, '!=')
        );
        break;
      case 'user':
        $this->query->add_where(
          $this->options['group'],
          db_and()->condition('users_node.uid', $user->uid, '=')
        );
        break;
      default:
        break;
    }
  }
}
