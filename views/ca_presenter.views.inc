<?php
function ca_presenter_views_data_alter(&$data) {
   if ( isset($data['node']) && !isset($data['node']['shared_library_user']) ) {
    $data['node']['shared_library_user'] = array(
      'real field' => array('status','uid'), // name of entity field to which filter applies
      'title' => t('Promoted to the library'),
      'help' => t('Promoted to the library'),
      'filter' => array(
        'handler' => 'ca_presenter_handler_filter_shared_library_user',
      ),
    );
  }
  if ( isset($data['users']) && !isset($data['users']['payer']) ) {
    $data['users']['payer'] = array(
      'real field' => array('profile_value_profile_payer_value'), // name of entity field to which filter applies
      'title' => t('Payer'),
      'help' => t('Payer'),
      'filter' => array(
        'handler' => 'ca_presenter_handler_filter_payer',
      ),
    );
  }
  if ( isset($data['users']) && !isset($data['users']['anniversary']) ) {
    $data['users']['anniversary'] = array(
      'real field' => array('profile_value_profile_anniversary_value'), // name of entity field to which filter applies
      'title' => t('Anniversary month'),
      'help' => t('Anniversary month'),
      'filter' => array(
        'handler' => 'ca_presenter_handler_filter_anniversary',
      ),
    );
  }
}

/*
  Exclude archived from the list
*/
function ca_presenter_views_query_alter(&$view, &$query) {
  //Remove archived from presentation list
  global $user;
  //Search slide free text search
  if (isset($view->name) && $view->name === 'presentations' && $view->current_display == 'page_8') {
    $search_for = arg(1);
    if($search_for == '') {
      return $query;
    }
    unset($query->where[0]);
    $query->add_where(0, 'title', '%' . db_like($search_for) . '%', 'LIKE');
  }
  if (isset($view->name) && $view->name === 'presentations' && $view->current_display == 'presentations_list') {
    global $published_archived;
    if(null == $published_archived) {
      drupal_set_message(t('Please setup CA Presenter module taxonomy settings by clicking this link') . ': '. l(t('CA Presenter module settings'),'admin/settings/ca_presenter',array('absolute' => TRUE)),'error');
      return $query;
    }
    if(isset($query->table_queue['field_data_field_primary_statuses_value_0'])) {
      foreach($query->table_queue['field_data_field_primary_statuses_value_0']['join']->extra as $join) {
        if($join['field'] == 'field_primary_statuses_tid' && $join['value'] == $published_archived->tid) {
          return $query;
        }
      }
    }
    $query->where[] = array(
      'conditions' => array(
        array(
          'field' => $query->tables['node']['field_data_field_primary_statuses']['alias'] . '.field_primary_statuses_tid',
          'value' => array($published_archived->tid),
          'operator' => '!='
        )
      ),
      'args' => array(),
      'type' => 'AND'
    );
  }
  //Categories: Terms page permissions
  if($view->name == 'categories' && $view->current_display == 'page_1') {
    //Get current taxonomy permissions
    $taxonomy_permissions = array();
    $user_permissions = user_role_permissions($user->roles);
    foreach($user_permissions as $rid => $permissions) {
      foreach($permissions as $machine_name => $boolean) {
        if(strpos($machine_name,'edit terms in') !== FALSE && $boolean === TRUE) {
          preg_match("/edit terms in (.*?)$/", $machine_name, $tid);
          $taxonomy_permissions[] = $tid[1];
        }
      }
    }
    if(count($taxonomy_permissions) <= 0) {
      return $query;
    }
    $taxonomy_permissions = array_unique($taxonomy_permissions);
    $query->where[0]['conditions'][] = array(
      //taxonomy_term_data.tid
      'field' => 'taxonomy_term_data.vid',
      'value' => $taxonomy_permissions,
      'operator' => 'IN'
    );
  }
  if($view->name == 'categories' && $view->current_display == 'page') {
    //Get current taxonomy permissions
    $taxonomy_permissions = array();
    $user_permissions = user_role_permissions($user->roles);
    foreach($user_permissions as $rid => $permissions) {
      foreach($permissions as $machine_name => $boolean) {
        if(strpos($machine_name,'edit terms in') !== FALSE && $boolean === TRUE) {
          preg_match("/edit terms in (.*?)$/", $machine_name, $tid);
          $taxonomy_permissions[] = $tid[1];
        }
      }
    }
    if(count($taxonomy_permissions) <= 0) {
      return $query;
    }
    $taxonomy_permissions = array_unique($taxonomy_permissions);
    $query->where[] = array(
      'conditions' => array(
        array(
          'field' => 'taxonomy_term_data.vid',
          'value' => $taxonomy_permissions,
          'operator' => 'IN'
        )
      ),
      'args' => array(),
      'type' => 'AND'
    );
  }
  if($view->name == 'presentations' && $view->current_display == 'page_2') {
    global $user;
    $pid = arg(1);
    $presentation = node_load($pid);
    $presentation_author = $presentation->uid;

    $presentation_wrapper = entity_metadata_wrapper('node', $presentation);
    
    $hide_drafts = array();
    $slides = $presentation_wrapper->field_slides->value();
    foreach($slides as $slide) {
      if($slide->status == 0) {
        //If it is a draft, and slide author not eq with current user id then hide it
        if($slide->uid == $user->uid) {
          continue;
        } else {
          $hide_drafts[] = $slide->nid;
        }
      }
    }
    if(count($hide_drafts) > 0) {
      $query->where[1]['conditions'][0]['value'] = $hide_drafts;
      $query->where[1]['conditions'][0]['operator'] = 'NOT IN';
    }
  }
}


/**
*  Implements hook_views_pre_render
*  @param $view
*
*  If you have access to a presentations,
*  grant access to view the slide.
*/
function ca_presenter_views_pre_render(&$view) {
  //On library slides get rid of rows what user have no access for
  if($view->name == 'presentations' && $view->current_display === 'library_slides_page') {
    global $user;
    foreach($view->result as $key => $result) {
      $pid = $result->nid;
      $presentation = node_load($pid);
      if(!node_access('view', $presentation, $user)) {
        unset($view->result[$key]);
      }
    }
  }
  //On slide search get rid of rows what user have no access for
  if($view->name == 'presentations' && $view->current_display === 'page_8') {
    global $user;
    foreach($view->result as $key => $result) {
      $sid = $result->nid;
      $query = new \EntityFieldQuery();
      $query->entityCondition('entity_type', 'node');
      $query->entityCondition('bundle', 'presentation');
      $query->fieldCondition('field_slides', 'target_id', $sid);
      $results = $query->execute();
      foreach($results['node'] as $node) {
        $pid = $node->nid;
      }
      if($pid*1 > 0) {
        $presentation = node_load($pid);
        if(!node_access('view',$presentation,$user)) {
          unset($view->result[$key]);
        }
      } else {
          unset($view->result[$key]);
      }
    }
  }
}

function ca_presenter_views_pre_view(&$view) {
  $view_name = $view->name . '_' . $view->current_display;
  if(!empty($_REQUEST)) {
    $_SESSION['views_pages'][$view_name] =  isset($_POST['page']) ? $_POST['page'] : null;
  }
  $view->set_current_page($_SESSION['views_pages'][$view_name]);
}
