<?php
/**
 * generate the slide example template with no thumbnail or preview
 * @var array $vars
 */
global $ca_presenter_module_path;
global $base_url;

?>

<?php if( isset($modular) ) : ?>

<div class="slide-template ' . $class . '">
  <div class="preview">
    <a href="javascript:void(0)" class="slide-example title" data-template="<?php print $template; ?>" data-pid="<?php print $pid; ?>">
      <h5><?php print $node->name; ?></h5>
    </a>
    <a style="pointer-events: none;" href="javascript:void(0)" class="slide-example image <?php print $class; ?>" data-template="<?php print $template; ?>" data-pid="<?php print $pid; ?>">
      <img src="<?php print $image; ?>" alt="" title="" />
    </a>
  </div>
  <div class="select">
    <a href="/presentations/<?php print $pid; ?>/add-slide/<?php print $template; ?>/p/<?php print $position; ?>"><i class="fa fa-plus"></i> <?php print t('Select'); ?></a>
  </div>
</div>

<?php else : ?>

  <div class="preview">
    <a href="javascript:void(0)" class="slide-example title" data-template="<?php print $template; ?>" data-pid="<?php print $pid; ?>">
      <h5><?php print $node_type; ?></h5>
    </a>
    <a style="pointer-events: none;" href="javascript:void(0)" class="slide-example image <?php print $class; ?>" data-template="<?php print $template; ?>" data-pid="<?php print $pid; ?>">
      <img src="<?php print $image; ?>" alt="" title="" />
    </a>
  </div>

<?php endif; ?>