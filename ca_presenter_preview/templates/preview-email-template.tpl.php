<?php
/**
 *  template for rendering email preview
 */
?>

<div id="popup_overlay"></div>
<div id="preview_popup">
  <iframe class="pagepreview-preview" src="/emails/preview"></iframe>
  <div id="popup_close">
    <i class="fa fa-times"></i>
  </div>
</div>
