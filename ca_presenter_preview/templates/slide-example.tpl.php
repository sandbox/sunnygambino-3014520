<?php
/**
 * generate the slide example template
 * @var array $vars
 */
global $ca_presenter_module_path;
global $base_url;

?>

<?php if( isset($modular) ) : ?>

<div class="slide-template <?php print $class; ?>">
  <div class="preview">
    <a href="javascript:void(0)" class="slide-example title" data-template="<?php print $template; ?>" data-sid="<?php print $slide_id; ?>" data-pid="<?php print $pid; ?>">
      <h5><?php print $node->name; ?></h5>
    </a>
    <a href="javascript:void(0)" class="slide-example image <?php print $class; ?>" data-template="<?php print $template; ?>" data-sid="<?php print $slide_id; ?>" data-pid="<?php print $pid; ?>">
      <span class="view-icon">
        <span class="icon-wrap">
          <i class="fa fa-play"></i>
        </span>
        <span class="text-wrap">
          <?php print t('See example'); ?>
        </span>
      </span>
      <img src="<?php print $image; ?>" alt="" title="" />
    </a>
  </div>
  <div class="select">
    <a href="/presentations/<?php print $pid; ?>/add-slide/<?php print $template; ?>/p/<?php print $position; ?>"><i class="fa fa-plus"></i> <?php print t('Select'); ?></a>
  </div>
  <div class="preview-frame" data-sid="<?php print $slide_id; ?>">
    <?php
      $preview_wrapper = theme('preview_node', ['pid' => $pid, 'template' => $template, 'modular' => TRUE, 'slide_id' => $slide_id, 'example_pid' => $example_pid ]);
      print $preview_wrapper;
    ?>
  </div>
</div>

<?php else : ?>

  <div class="preview">
    <a href="javascript:void(0)" class="slide-example title" data-template="<?php print $template; ?>" data-sid="<?php print $slide_id; ?>" data-pid="<?php print $pid; ?>">
      <h5><?php print $node_type; ?></h5>
    </a>
    <a href="javascript:void(0)" class="slide-example image <?php print $class; ?>" data-template="<?php print $template; ?>" data-sid="<?php print $slide_id; ?>" data-pid="<?php print $pid; ?>">
      <span class="view-icon">
        <span class="icon-wrap">
          <i class="fa fa-play"></i>
        </span>
        <span class="text-wrap">
          <?php print t('See example'); ?>
        </span>
      </span>
      <img src="<?php print $image; ?>" alt="" title="" />
    </a>
  </div>
  <div class="preview-frame" data-sid="<?php print $slide_id; ?>">
    <?php
      $preview_wrapper = theme('preview_node', ['pid' => $pid, 'template' => $template, 'modular' => TRUE, 'slide_id' => $slide_id, 'example_pid' => $example_pid ]);
      print $preview_wrapper;
    ?>
  </div>

<?php endif; ?>