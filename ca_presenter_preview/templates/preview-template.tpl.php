<?php
/**
 * generate the preview template
 * @var array $vars
 */
global $ca_presenter_module_path;
global $base_url;

$viewer_class = '';
if(isset($viewer_size) && $viewer_size == "1") {
  $viewer_class = "widescreen";
}
$loading = $base_url . '/' . $ca_presenter_module_path . '/ca_presenter_preview/css/ajax-loader.gif';

if( isset($modular) ) : ?>

<div class="outer-container preview-modal <?php echo $viewer_class ;?>">
  <div class="popup-close slide-example-close">
    <a href="#" data-slideid="<?php if(isset($slide_id)) : print $slide_id; endif; ?>">
      <i class="fa fa-times"></i>
    </a>
  </div>
  <div class="toggle-widescreen slide-example-toggle expand">
    <a href="#" data-slideid="<?php if(isset($slide_id)) : print $slide_id; endif; ?>">
      <i class="fa fa-arrows-h"></i>
    </a>
  </div>
  <div class="toggle-widescreen slide-example-toggle compress">
    <a href="#" data-slideid="<?php if(isset($slide_id)) : print $slide_id; endif; ?>">
      <i class="fa fa-arrows-h"></i>
    </a>
  </div>
  <div class="middle">
    <div id="" class="inner">
      <?php
        if(isset($example_pid)){
          $presentation = node_load($example_pid);
          $node_wrapper = entity_metadata_wrapper('node', $presentation);
          $slides = $node_wrapper->field_slides->value();

          print '<a id="prev-slide" data-slideid="' . $slide_id . '" class="owl-nav-buttons prev-slide-button" href="#"><i class="fa fa-chevron-left"></i></a>';
          print '<div data-slideid="' . $slide_id . '" class="owl-carousel">';

          foreach($slides as $slide){

            if($slide != NULL) {
              if($slide->type == $template){
                //print $slide->nid;
                print '<div id="frame-item-'. $slide->nid .'" class="item">';
                  print '<div class="frame-faker" data-id="'. $slide->nid . '" style="display: none;" data-src="/node/' . $slide->nid . '/' . $example_pid . '/?view=1&presentation_id='. $example_pid .'&slide_example=1"></div>';
                  print '<div class="loading-icon" data-id="'. $slide->nid . '"><img src="'. $loading . '" alt="loading-icon" /></div>';
                print '</div>';
              }
            }
          }

          print '</div>';
          print '<a id="next-slide" data-slideid="' . $slide_id . '" class="owl-nav-buttons next-slide-button" href="#"><i class="fa fa-chevron-right"></i></a>';
        }
      ?>
    </div>
  </div>
</div>

<?php else : ?>
<?php
 /**
  * This is the existing stuff
  *
  * I don't want to touch this as I don't know where it's being used
  */
?>
<div class="outer-container preview-modal slide-preview-container <?php echo $viewer_class ;?>">
  <div class="popup-close">
    <a id="close-slide-preview" href="#" data-slideid="<?php if(isset($slide_id)) : print $slide_id; endif; ?>">
      <i class="fa fa-times"></i>
    </a>
  </div>
  <div class="toggle-widescreen slide-preview-toggle expand">
    <a href="#" data-slideid="<?php if(isset($slide_id)) : print $slide_id; endif; ?>">
      <i class="fa fa-arrows-h"></i>
    </a>
  </div>
  <div class="toggle-widescreen slide-preview-toggle compress">
    <a href="#" data-slideid="<?php if(isset($slide_id)) : print $slide_id; endif; ?>">
      <i class="fa fa-arrows-h"></i>
    </a>
  </div>
  <div class="middle">
    <div id="" class="inner">
      <?php if (isset($node)) : ?>
        <iframe scrolling="no" id="preview-<?php print $node->type; ?>" class="slide-preview preview-iframe" src="/presentation-viewer/<?php print arg(1);?>/slide/<?php print $node->type; ?>/preview" onload="window.previewModal.frameLoaded(this);"></iframe>
      <?php else : ?>
        <iframe scrolling="no" id="slide_example_frame" class="slide-preview preview-iframe" src="" onload="window.previewModal.frameLoaded(this);"></iframe>
      <?php endif; ?>
    </div>
  </div>
</div>

<?php endif; ?>