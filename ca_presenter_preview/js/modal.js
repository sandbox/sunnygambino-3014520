/**
 *  Controller for slide previews
 *
 *  Abdul Sadık Yalçın
 */

popupModal = {};
popupModal.controller = function(){

    var self = this;
    this.carousel;

    this.elements = {
        outer: function(){
            return jQuery('.preview-modal');
        },
        inner: function(){
            return jQuery('.preview-modal .inner');
        },
        frame: function(){
            return jQuery('iframe.slide-preview');
        },
        toggleWidescreen: function(){
            return jQuery('.toggle-widescreen');
        },
        popupClose: function(){
            return jQuery('.popup-close');
        },
        scaledPopupClose: function(){
            return jQuery('.scaled-popup-close');
        }
    };

    this.init = function(){
        jQuery(window).on('resize', function(){
            self.scale();
        });

        this.scale();
    };

    this.frameLoaded = function(iframe){
        var iframeDoc = iframe.contentDocument ? iframe.contentDocument : (iframe.contentWindow ? iframe.contentWindow.document : iframe.document);
        var body = iframeDoc.body || null;
        if(body !== null){
            body.classList.add('widescreen');
        }
    };

    this.scale = function(){
      var $el = jQuery('.outer-container .inner');
      var elHeight = 768;
      var elWidth = 1366;
      var $wrapper = jQuery('body');

      function doResize(event, ui) {
        var scale, origin;

        scale = Math.min(
          ui.size.width / elWidth,
          ui.size.height / elHeight
        );

        if(scale > 1) {
          scale = 1;
        }

        $el.css({
          '-webkit-transform': "scale(" + scale + ")",
          '-ms-transform': "scale(" + scale + ")",
          'transform': "scale(" + scale + ")"
        });
      }

      var starterData = {
        size: {
          width: $wrapper.width() - 50,
          height: $wrapper.height() - 50
        }
      };
      doResize(null, starterData);
    };

    this.removeClass = function(){
        this.elements.outer().removeClass('scale no-scale scale65 scale70 scale75 scale80 scale85 scale90');
    };

};

jQuery.noConflict();
jQuery( document ).ready(function( $ ) {
    //console.log('preview initialised');
    window.previewModal = new window.popupModal.controller();
    previewModal.init();
});
