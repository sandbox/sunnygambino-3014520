<?php
/**
 * @file
 * live_presentation.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function live_presentation_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function live_presentation_node_info() {
  $items = array(
    'live_presentation' => array(
      'name' => t('Live presentation'),
      'base' => 'node_content',
      'description' => t('Start live presentation'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
