(function ($) {
Drupal.behaviors.live_presentation = {
  attach: function (context, settings) {
    this.livePresentation();
  },
  livePresentation: function() {
    //If we have live_presentation query string then start it
    var self = this;
    var q = self.queryString();
    if(q.live_presentation !== "" && typeof q.live_presentation !== "undefined") {
      self.isLivePresentation = true;
    }
    if(self.isLivePresentation === true) {
      //self.startLivePresentation();
    }
  },
  startLivePresentation: function() {
    var self = this;
    /**
      FALSE: triggered
      TRUE: Human click
    */
    var click_type = false;
    var q = self.queryString();
    $(window.document).on('click keyup',function(e) {
      //Send click events to the server
      console.log('event: ',e);
      console.log('e.isTrigger: ',e.isTrigger);
      if(e.isTrigger) {
        click_type = false;
      }
      if(typeof e.isTrigger === 'undefined') {
        click_type = true;
        $.ajax({
          url: '/live_presentation',
          type: 'post',
          data: {
            'live_presentation': {
              'message': {'type': e.type,'className':self.getXPath(e.target),'hash':q.live_presentation, 'pid':q.presentation_id, 'keyCode' : e.keyCode}
            }
          },
          dataType: 'json',
          success: function(response) {
            console.log('live presentation AJAX response: ',response);
          },
          error: function(error) {
            console.log('live presentation AJAX error: ',error);
          }
        });
      }
    });
    setInterval(function() {
      $.getJSON('/live_presentation?live_presentation='+q.live_presentation,function(data) {
        console.log('GET data : ',data);
        $.each(data,function(i,v) {
          if(v.message.length <= 0) {
            return false;
          }
          if(v.message.className.length > 0) {
              console.log('FROM DRUPAL : ',self.getElementByXpath(v.message.className));
              console.log('FROM DRUPAL : ',v.message.type);
              $(self.getElementByXpath(v.message.className)).trigger(v.message.type);
              console.log(v.message.className);
              console.log(v.message.type);
          }
          if(v.message.type == 'keyup') {
            console.log('keyup',v);
            var e = jQuery.Event("keydown");
            e.which = v.message.keyCode;
            e.keyCode = v.message.keyCode;
            e.target = $('body');
            $(window.document).trigger(e);
          }
          //After trigger delete the message
          $.ajax({
            url: '/live_presentation',
            type: 'post',
            data: {
              'live_presentation': {
                'message_id': i,
                'hash':q.live_presentation
              }
            },
            dataType: 'json',
            success: function(response) {
              console.log('live presentation AJAX response: ',response);
            },
            error: function(error) {
              console.log('live presentation AJAX error: ',error);
            }
          });
        });
      });
    },3000);
  },
  queryString : function () {
    var query_string = {};
    var query = window.top.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
      var pair = vars[i].split("=");
      if (typeof query_string[pair[0]] === "undefined") {
        query_string[pair[0]] = decodeURIComponent(pair[1]);
      } else if (typeof query_string[pair[0]] === "string") {
        var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
        query_string[pair[0]] = arr;
      } else {
        query_string[pair[0]].push(decodeURIComponent(pair[1]));
      }
    }
    return query_string;
  },
  /**
  * Helper function to get absolute CSS path
  */
  getXPath: function (node) {
    var comp, comps = [];
    var parent = null;
    var xpath = '';
    var getPos = function(node) {
        var position = 1, curNode;
        if (node.nodeType == Node.ATTRIBUTE_NODE) {
            return null;
        }
        for (curNode = node.previousSibling; curNode; curNode = curNode.previousSibling) {
            if (curNode.nodeName == node.nodeName) {
                ++position;
            }
        }
        return position;
     };

    if (node instanceof Document) {
        return '/';
    }

    for (; node && !(node instanceof Document); node = node.nodeType == Node.ATTRIBUTE_NODE ? node.ownerElement : node.parentNode) {
        comp = comps[comps.length] = {};
        switch (node.nodeType) {
            case Node.TEXT_NODE:
                comp.name = 'text()';
                break;
            case Node.ATTRIBUTE_NODE:
                comp.name = '@' + node.nodeName;
                break;
            case Node.PROCESSING_INSTRUCTION_NODE:
                comp.name = 'processing-instruction()';
                break;
            case Node.COMMENT_NODE:
                comp.name = 'comment()';
                break;
            case Node.ELEMENT_NODE:
                comp.name = node.nodeName;
                break;
        }
        comp.position = getPos(node);
    }

    for (var i = comps.length - 1; i >= 0; i--) {
        comp = comps[i];
        xpath += '/' + comp.name;
        if (comp.position != null) {
            xpath += '[' + comp.position + ']';
        }
    }
    return xpath;
  },
  getElementByXpath: function(path) {
    return window.document.evaluate(path, window.document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
  },
}
})(jQuery); 
