<?php
  /**
   * Global button form override.
   *
   * @param $form
   * @param $form_state
   *
   * @return mixed
   */
  function _all_form_after_build($form, &$form_state) {
    //Copy all action buttons to the top
    //On slide types we don't need delete buttons.
    $slide_types = get_slide_content_types();
    $slide_types = array_keys($slide_types);
    $forbidden_forms = [
      'views_form_presentations_page_2',
      'user_login',
      'user_pass',
      'node_delete_confirm'
    ];
    if (in_array($form['#form_id'], $forbidden_forms)) {
      return $form;
    }
  
    if (isset($form['actions']['delete'])) {
      $form['actions']['delete']['#weight'] = 1;
    }
    if (isset($form['actions']['preview'])) {
      $form['actions']['preview']['#weight'] = 3;
    }
    if (isset($form['actions']['submit'])) {
      $form['actions']['submit']['#weight'] = 4;
    }
    if (isset($form['actions'])) {
      $form['actions-copy'] = $form['actions'];
      $form['actions-copy']['#weight'] = -1000;
    }

    if (!isset($form['type'])) {
      return $form;
    }
    if (isset($form['type']['value']) && in_array($form['type']['#value'], $slide_types)) {
      unset($form['actions-copy']['delete']);
    }
    return $form;
  }

  function _global_to_all_form($form,$form_state,$form_id) {
    global $user;
    global $ca_presenter_module_path;
    global $base_url;
    global $subdomain;

    if(isset($form['language']) && isset($form['language']['#options'])) {
      $form['language']['#options']['und'] = t('Neutral');
    }
    $slide_types = get_slide_content_types();
    $slide_types = array_keys($slide_types);
    //Setup all forms after build callback 
    $form['#after_build'][] = '_all_form_after_build';
    //Hide node info panel if have no permission
    if (isset($form['#groups']['group_node_info']) && !user_access('administer site configuration')) {
      field_group_hide_field_groups($form, ['group_node_info']);
    }
    //Setup preview on slide node edit forms
    foreach ($slide_types as $type) {
      if ($form_id == $type . '_node_form') {
        $form['actions']['submit']['#submit'][0] = 'ca_presenter_node_form_submit';
        if( !isset($form_state['build_info']['args'][0]->nid) ){
          $pid = arg(1);
          $theme = ca_presenter_view_node_theme(null, null, $pid); // Get current theme
          $example_pid = variable_get($theme . '_example', 0); // Example presentation
          $node_type = node_type_get_name($type);

          $html = '
          <div class="header-form-section cf">
            <div class="slide-template" style="top: -45px;">';

          if( isset($example_pid) && $example_pid != 0 ) {
            $example_presentation = node_load($example_pid);
            $example_slide_ids = $example_presentation->field_slides['und'];
 
            foreach($example_slide_ids as $example_slide_id){
              $example_slide = node_load($example_slide_id['target_id']);
  
              if( $example_slide != NULL && $example_slide->type == $type ){
                $image = get_slide_thumbnail($example_slide->nid, $example_pid);
                $class = 'border';
                if($image == NULL){
                  $image = '/' . $ca_presenter_module_path . '/images/templates/' . $type . '.png';
                  $class = 'no-border';
                  $slide_wrapper = theme('slide_example_blank', [ 'class' => $class, 'template' => $type, 'image' => $image, 'pid' => $pid, 'node_type' => $node_type ]);
                } else {
                  $slide_wrapper = theme('slide_example', [ 'class' => $class, 'template' => $type, 'slide_id' => $example_slide->nid, 'image' => $image, 'pid' => $pid, 'example_slide' => $example_slide, 'example_pid' => $example_pid, 'node_type' => $node_type ]);
                }
              }
            }
          } else {
            $image = '/' . $ca_presenter_module_path . '/images/templates/' . $type . '.png';
            $class = 'no-border';
            $slide_wrapper = theme('slide_example_blank', [ 'class' => $class, 'template' => $type, 'image' => $image, 'pid' => $pid, 'node_type' => $node_type ]);
          }
 
          $html .= $slide_wrapper;
          $html .= '</div></div>';
          $form['#prefix'] = $html;
        }
        $form['#cancel_redirect_url'] = '/presentations/' . arg(1) . '/edit-slides';
      }
    }
    return array($form,$form_state);
  }
  /**
  *  Alter views exposed filer forms. Hide empty categories on presentation and document views.
  */
  function _views_exposed_form($form, $form_state, $form_id) {
    global $user;
    //Count presentations categories and statuses
    if(
       (
         $form_state['view']->name == 'presentations' &&
         $form_state['view']->current_display == 'presentations_list'
       ) ||
       (
         $form_state['view']->name == 'entity_selector' &&
         $form_state['view']->current_display == 'email_selector'
       ) ||
       (
         $form_state['view']->name == 'entity_selector' &&
         $form_state['view']->current_display == 'library_page_selector'
       ) ||
       (
         $form_state['view']->name == 'entity_selector' &&
         $form_state['view']->current_display == 'page_1'
       )
      ) {
      $account = user_load($user->uid);
      $ordered_options = array();
      $ordered_options = array('All' => t('- All -'));
      if(isset($form['field_taxonomy_tid']['#options']['All'])) {
        $ordered_options = array('All' => $form['field_taxonomy_tid']['#options']['All']);
      }
      foreach ($form['field_taxonomy_tid']['#options'] as $tid => &$value) {
        $query = db_select('field_data_field_taxonomy', 'f');
        $query->leftJoin('node', 'n', 'f.entity_id = n.nid');
        $query->condition('f.field_taxonomy_tid', $tid);
        $query->addExpression('n.nid', 'nid');
        $results = $query->execute()->fetchAll();
        $count = 0;
        foreach($results as $result) {
          if(@node_access('view', node_load($result->nid), $account)) {
            $count++;
          }
        }
        if($count === 0 && $tid !== 'All') {
          unset($form['field_taxonomy_tid']['#options'][$tid]);
        } elseif($tid !== 'All') {
          $ordered_options[$tid] = $form['field_taxonomy_tid']['#options'][$tid];
        }
        //$value = $value . ' (' . $count . ')';
      }
      $form['field_taxonomy_tid']['#options'] = $ordered_options;
    }

    //Count document categories and statuses
    if(
       (
         $form_state['view']->name == 'presentations' &&
         $form_state['view']->current_display == 'presentations_list'
       ) ||
       (
         $form_state['view']->name == 'entity_selector' &&
         $form_state['view']->current_display == 'email_selector'
       ) ||
       (
         $form_state['view']->name == 'entity_selector' &&
         $form_state['view']->current_display == 'library_page_selector'
       ) ||
       (
         $form_state['view']->name == 'entity_selector' &&
         $form_state['view']->current_display == 'page_1'
       )
      ) {
      $account = user_load($user->uid);
      if(isset($form['field_document_category_tid']['#options'])) {
        foreach ($form['field_document_category_tid']['#options'] as $tid => &$value) {
          $query = db_select('field_data_field_document_category', 'f');
          $query->leftJoin('node', 'n', 'f.entity_id = n.nid');
          $query->condition('f.field_document_category_tid', $tid);
          $query->addExpression('n.nid', 'nid');
          $results = $query->execute()->fetchAll();
          $count = 0;
          foreach($results as $result) {
            if(node_access('view', node_load($result->nid), $account)) {
              $count++;
            }
          }
          if($count === 0) {
            unset($form['field_document_category_tid']['#options'][$tid]);
          }
          //$value = $value . ' (' . $count . ')';
        }
      }
    }
    if(
	    (
		$form_state['view']->name == 'presentations' &&
	 	$form_state['view']->current_display == 'presentations_list'
	    ) ||
	    (
		$form_state['view']->name == 'documents_1' &&
	 	$form_state['view']->current_display == 'presentations_list'
	    ) &&
	 isset($form['language']) &&
	 isset($form['language']['#options'])
    ) {
	 unset($form['language']['#options']['***CURRENT_LANGUAGE***']);
	 unset($form['language']['#options']['***DEFAULT_LANGUAGE***']);
	 $form['language']['#options']['und'] = t('Neutral');
	 $form['language']['#options']['All'] = t('- All Languages -');
      }
    return array($form,$form_state);
  }

  function _messages_form($form,$form_state,$form_id) {
    global $user;
    $user_options = array();
    $all_roles = user_roles(TRUE);
    $current_roles = $user->roles;
    $current_permissions = user_role_permissions($current_roles);
    $permissions_to_see_users = array();
    foreach($all_roles as $rid => $role_name) {
      foreach($current_permissions as $crid => $carray) {
        $cperms = array_keys($carray);
        if(in_array($role_name . ' view content',$cperms)) {
          $permissions_to_see_users[$rid] = $role_name;
        }
      }
    }
    foreach($permissions_to_see_users as $rid => $role) {
      $query = 'SELECT ur.uid
        FROM {users_roles} AS ur
        WHERE ur.rid = :rid';
      $result = db_query($query, array(':rid' => $rid));
      $uids = $result->fetchCol();
      foreach($uids as $uid) {
        $u = user_load($uid);
        $user_options[$u->uid] = $u->name;
      }
    }
    if(!user_access('administer users')) {
      if(isset($form['field_message_users'])) {
        $form['field_message_users'][LANGUAGE_NONE]['#options'] = $user_options;
      }
      if(isset($form['field_message_users_target_id'])) {
        $form['field_message_users_target_id']['#options'] = array('All' => '- Any user -') + $user_options;
      }
      //Setup roles
      if(isset($form['field_roles'])) {
        $form['field_roles'][LANGUAGE_NONE]['#options'] = $permissions_to_see_users;
        $form['field_roles'][LANGUAGE_NONE]['#multiple'] = TRUE;
      }
    } else {
      //Setup roles
      if(isset($form['field_message_users'])) {
        unset($form['field_message_users'][LANGUAGE_NONE]['#options']['_none']);
      }
      if(isset($form['field_message_users_target_id'])) {
        unset($form['field_message_users_target_id']['#options']['_none']);
      }
      if(isset($form['field_roles'])) {
        $form['field_roles'][LANGUAGE_NONE]['#options'] = $all_roles;
        $form['field_roles'][LANGUAGE_NONE]['#multiple'] = TRUE;
      }
    }
    //Change save button name on messages page and hide preview
    if (isset($form['actions']['submit'])) {
      $form['actions']['submit']['#value'] = t('Send message');
      $form['actions']['submit']['#attributes']['class'] = ['class' => 'button-green'];
    }

    if (isset($form['actions']['preview'])) {
      $form['actions']['preview'] = FALSE;
    }
    return array($form,$form_state);
  }

  function _presentation_node_form($form,$form_state,$form_id) {
    //Theme selector
    $themes = list_themes();
    //Empty option element for select2
    global $forbidden_themes;
    $theme_options = array();
    $theme_options[''] = '-- ' . t('Select a theme') . ' --';
    foreach ($themes as $k => $theme) {
      if(module_exists('theme_permissions')) {
        if(user_access('use '. $theme->name . ' theme')) {
          $theme_options[$theme->name] = $theme->info['name'];
        }
      } elseif (strpos($theme->filename, 'sites/all/themes') !== FALSE && !in_array($theme->name, $forbidden_themes) && $theme->status == 1) {
        $theme_options[$theme->name] = $theme->info['name'];
      }
    }
    if (count($theme_options) == 1) {
      drupal_set_message(t('There are no themes enabled. Please enable some by gooing to the following link') . ': ' . l(t('Appearance settings'), 'admin/appearance', ['absolute' => TRUE]), 'error');
      return drupal_access_denied();
    }
    $form['field_theme_custom'] = [];
    if (isset($form['#node']->field_theme)) {
      $form['field_theme']['und'][0]['value']['#default_value'] = $form['#node']->field_theme[LANGUAGE_NONE][0]['value'];
    }
    $form['field_theme']['#attributes']['style'] = 'display:none;';
    $form['field_theme_custom']['#type'] = 'select';
    $form['field_theme_custom']['#title'] = t('Theme') . ' <span class="form-required" title="' . t('This field is required.') . '">*</span>';
    $form['field_theme_custom']['#options'] = $theme_options;
    $form['field_theme_custom']['#default_value'] = $form['field_theme']['und'][0]['value']['#default_value'];
    $form['field_theme_custom']['#select2']['placeholder'] = t('Select a theme');
    $form['field_theme_custom']['#select2']['allowClear'] = TRUE;
    unset($form['actions']['delete']);
    $form['#after_build'][] = '_all_form_after_build';
    //Path settings for friendly URLs
    unset($form['path']['#group']);
    $form['path']['#collapsed'] = TRUE;
    $form['path']['#title'] = t('Friendly URL settings');
    $form['path']['alias']['#description'] = t('Optionally specify an alternative URL by which this presentation can be accessed. For example, type "hotel-events" when creating a new events presentation. Use a relative path and don\'t add a trailing slash or the URL alias won\'t work.');
    return array($form,$form_state);
  }

  function _email_node_form($form,$form_state,$form_id) {
    global $ca_presenter_module_path;
    //Change save button name on emails page
    if (isset($form['actions']['submit'])) {
      $form['actions']['submit']['#value'] = t('Send email');
      $form['actions']['submit']['#attributes']['class'] = ['class' => 'button-green'];
    }
    return array($form,$form_state);
  }

  function _payer_node_form($form,$form_state,$firm_id) {
    if (isset($form['actions']['submit'])) {
      $form['actions']['submit']['#value'] = t('Add payer');
      $form['actions']['submit']['#attributes']['class'] = ['class' => 'button-green'];
    }

    if (isset($form['actions']['preview'])) {
      $form['actions']['preview'] = FALSE;
    }
    return array($form,$form_state);
  }

  function _nodeaccess_form($form,$form_state,$form_id) {
    foreach($form['rid'] as $key => $rid) {
      if(!user_access('assign '.$rid['name']['#value'].' role') && is_int($key)) {
        unset($form['rid'][$key]);
      }
    }
    $form['submit']['#value'] = 'Save Access';
    $form['submit']['#weight'] = 4;
    $form['#submit'][] = '_nodeaccess_form_submit';
    return array($form,$form_state);
  }

  function _nodeaccess_form_submit($form, &$form_state) {
    $presentation = node_load($form_state['values']['nid']);
    node_save($presentation);
  }

  function _document_node_form($form,$form_state,$form_id) {
    if (isset($form['actions']['preview'])) {
      $form['actions']['preview'] = FALSE;
    }
    if (isset($form['actions']['delete'])) {
      $form['actions']['delete'] = FALSE;
    }
    return array($form,$form_state);
  }
