<?php
/**
*  Check viewer access
*  @param 	session_id	PHP session iD
*  @param	domain		application's subdomain
*  @param	username	current user's username
*  @param	presentation_id	What presentation viewer should show?
*  @param	ref		Reference hash for emails, global hash or live presentation
*  @param	x		It used to be live presentation node ID
*/
function ca_presenter_service_presentation_session($session_id,$domain,$username,$presentation_id, $ref = NULL, $x = NULL) {
  //Anonymous user access with referece
  if(
      isset($presentation_id) &&
      $ref == sha1(drupal_get_hash_salt().$presentation_id)
    ) {
    return array('status' => 'OK');
  } else {
    $live_presentation = $ref;
    $live_presentation_nid = $x;
  }

  //Live presentation anonymous access
  if(
      isset($live_presentation) &&
      isset($live_presentation_nid)
    ) {
    //Load live presentation node
    $lpn = node_load($live_presentation_nid);
    $emails = $lpn->field_invitees[LANGUAGE_NONE][0]['value'];
    $emails = preg_split('/([\\s,;]+)/', $emails, -1);
    global $user;
    foreach($emails as $e) {
      if($live_presentation == sha1(drupal_get_hash_salt() . $e)) {
          return array('status' => 'OK');
      }
      if($live_presentation == sha1(drupal_get_hash_salt() . $user->uid)) {
          return array('status' => 'OK');
      }
    }
  }

  // if there is a valid email reference
  $valid_reference = CAViewer::check_presentation_reference($presentation_id, $ref);
  if ($valid_reference) {
    // hook in presentation notifications here
    module_load_include('inc', 'ca_presenter_tracking', 'includes/class');
    CAPresenterTracking::email_view($presentation_id, $ref, $eid, FALSE);
    return array('status' => 'OK');
  }
  global $user;
  if(isset($user) && (int)$user->uid > 0 && NULL == $ref && NULL == $live_presentation) {
    $query = db_select('sessions','s');
    $query
      ->fields('s',array('sid'))
      ->condition('s.sid',$session_id);

    $result = $query->execute();
    if($result->rowCount() === 0) {
      return array('error' => 'Invalid username');
    }
    $session_valid = FALSE;
    while($row = $result->fetchAssoc()) {
      if($session_id == $row['sid'])
        $session_valid = TRUE;
      if(is_array($row)) {
        $r[] = $row['sid'];
      }
    }
    if(!$session_valid && !$ref) {
      return array('error' => 'Invalid session');
    }
    if(!$presentation_id || $presentation_id == "") {
      return array('error' => 'Invalid presentation_id');
    }
    // if logged in
    if ($session_valid) {
      return array('status' => 'OK');
    }
  }

  return array('error' => 'Invalid request');
}

/**
 *
 * @return bool
 */
function ca_presenter_service_presentation_session_access() {
  return TRUE;
}
