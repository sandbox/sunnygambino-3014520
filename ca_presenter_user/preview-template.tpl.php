<?php
/**
 * generate the preview template
 * @var array $vars
 */
?>
<div class="outer-container preview-modal">
  <div class="popup-close">
    <a href="#">
      <i class="fa fa-times"></i>
    </a>
  </div>
  <div class="toggle-widescreen expand">
    <a href="#">
      <i class="fa fa-expand"></i>
    </a>
  </div>
  <div class="toggle-widescreen compress">
    <a href="#">
      <i class="fa fa-compress"></i>
    </a>
  </div>
  <div class="middle">
    <div id="modal-slide-preview" class="inner">
        <iframe scrolling="no" id="preview-<?php print $node->type; ?>" class="slide-preview preview-iframe" src="about:blank"></iframe>
    </div>
  </div>
</div>