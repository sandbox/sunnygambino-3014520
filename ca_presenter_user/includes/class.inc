<?php


class CAPresenterUser {

  const TERM_REPLACEMENT = '{{TERM}}';
  const VOCABULARY_MACHINE_NAME = 'organisational_structure';
  const USER_GROUPS = 'field_user_groups';

  static function VIEW_PERMISSION () {
    return 'view ' . self::TERM_REPLACEMENT . ' users';
  }

  static function  EDIT_PERMISSION () {
   return 'administer '. self::TERM_REPLACEMENT . ' users';
  }

  static function VIEW_ALL_GROUP_PRESENTATIONS () {
    return 'view ' . self::TERM_REPLACEMENT . ' content';
  }

  static function EDIT_ALL_GROUP_PRESENTATIONS () {
    return 'edit ' . self::TERM_REPLACEMENT . ' content';
  }

  /**
   * get the taxonomy terms for viewing and administering users
   * @return array $structure
   */
  static function get_structure() {
    $vocabulary = taxonomy_vocabulary_machine_name_load(self::VOCABULARY_MACHINE_NAME);
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    $structure = [];
    foreach ($terms as $term) {
      $structure[] = $term->name;
    }
    return $structure;
  }

  /**
   * gets the available user groups
   * @return array
   */
  static function get_all_user_groups($bypass_permission = FALSE) {
    global $user;
    $groups = [];
    $vocabulary = taxonomy_vocabulary_machine_name_load(self::VOCABULARY_MACHINE_NAME);
    $terms = entity_load('taxonomy_term', FALSE, array('vid' => $vocabulary->vid));
    foreach ($terms as $rid => $term) {
      if(user_access('administer '.$term->name.' users') || $user->uid == 1 || $bypass_permission == TRUE)
        $groups[$rid] = $term->name;
    }
    return $groups;
  }

  /**
   * gets the global user group rid
   * @return array
   */
  static function get_global_user_group () {
    $groups = self::get_all_user_groups(TRUE);
    foreach ($groups as $rid => $name) {
      if ($name === 'global') {
        return [$rid];
      }
    }
    return [];
  }

  /**
   * creates permissions for administering and viewing users
   */
  static function hook_permission () {
    $structures = self::get_structure();
    $permission_patterns = [
      self::VIEW_PERMISSION(),
      self::EDIT_PERMISSION(),
      self::VIEW_ALL_GROUP_PRESENTATIONS(),
      self::EDIT_ALL_GROUP_PRESENTATIONS()
    ];
    $permissions = [];
    foreach ($structures as $structure) {
      foreach ($permission_patterns as $permission_pattern) {
        $permission_name = str_replace(self::TERM_REPLACEMENT, $structure, $permission_pattern);
        $permissions[$permission_name] = ['title' => ucfirst($permission_name), 'module' => 'user'];
      }
    }
    return $permissions;
  }

  static function get_permission($group, $type) {
    $permission = NULL;
    $types = [
      'view_user' => self::VIEW_PERMISSION(),
      'administer_user' => self::EDIT_PERMISSION(),
      'view_presentations' => self::VIEW_ALL_GROUP_PRESENTATIONS(),
      'edit_presentations' => self::EDIT_ALL_GROUP_PRESENTATIONS()
    ];

    if ($group !== NULL && $type !== NULL && isset($types[$type])) {
      $permission = str_replace(self::TERM_REPLACEMENT, $group, $types[$type]);
    }

    return $permission;
  }

  /**
   * return the groups a user is a member of
   * @param $account
   * @return mixed
   */
  static function get_user_groups ($account) {
    $terms_ids = [];
    $groups = [];
    $user_wrapper = entity_metadata_wrapper('user', $account);
    try {

      $data = $user_wrapper->field_user_groups->value();

      foreach ($data as $term) {
        $groups[] = $term->name;
      }

      return $groups;

    } catch (Exception $e) {
      return [];
    }
  }


  /**
   * create edit permissions given a group
   * @param array $groups
   *
   * @return array
   */
  static function createEditPermissions ($groups = []) {
    $permissions = self::createPermissions($groups,
      CAPresenterUser::EDIT_PERMISSION());
    return $permissions;
  }

  /**
   *
   * create required view permissions given taxonomy groups
   * @param array $groups
   *
   * @return array
   */
  static function createViewPermissions ($groups = []) {
    $permissions = self::createPermissions($groups,
      CAPresenterUser::VIEW_PERMISSION());
    return $permissions;
  }

  /**
   * create permissions from groups
   * @param $groups
   * @param null $permission_type
   *
   * @return array
   */
  static function createPermissions($groups, $permission_type) {
    $permissions = [];
    foreach ($groups as $group) {
      $permissions[] = str_replace(CAPresenterUser::TERM_REPLACEMENT,
        $group, $permission_type);
    }
    return $permissions;
  }

  /**
   * checks to see if logged in user can edit another user
   * @param $uid
   *
   * @return bool
   */
  static function view_user_admin_permission_filter($uid) {
    global $user;
    $query = db_query('select 
        concat(\'administer \',td.name,\' users\') as permission
        from field_data_field_user_groups as ug
        join taxonomy_term_data as td
        on ug.field_user_groups_tid=td.tid
        where entity_id=:uid', ['uid' => $uid]);
    $user_group_permissions = $query->fetchAll();
    foreach ($user_group_permissions as $permission) {
      if (user_access($permission->permission)) {
        // return false if user can edit this user
        return FALSE;
      }
    }
    return TRUE;
  }

  /**
   * return role ids with a given permission
   * @param $permission
   *
   * @return array
   */
  static function get_roles_with_permission ($permission) {
    $query = db_select('role_permission', 'rp');
    $query->condition('permission', $permission, '=');
    $query->fields('rp', ['rid', 'permission']);
    $results = $query->execute()
      ->fetchAllKeyed();
    $rids = array_keys($results);
    return $rids;
  }

  /**
   * check to see if role alias is setup
   * @param $rid
   *
   * @return bool
   */
  static function is_role_setup_with_nodeaccess($rid) {
    $query = db_select('nodeaccess_role_alias', 'ra');
    $query->condition('rid', $rid, '=');
    $query->fields('ra', ['rid']);
    $results =$query->execute()->fetchAll();
    return (count($results) > 0);
  }

  /**
   * inserts role to node access alias table
   * @param $rid
   */
  static function add_role_to_nodeaccess ($rid) {
    $role = user_role_load($rid);
    if ($role) {
      $insert = db_insert('nodeaccess_role_alias')
        ->fields(['rid', 'name', 'weight'], [$rid, $role->name, 0])
        ->execute();
    }
  }

  /**
   * builds formatted array for giving grants of access to a node
   * @param array $rids
   *
   * @return array
   */
  static function build_access_grants ($rids = []) {
    $realm = 'nodeaccess_rid';
    $grants = [];
    $realm = 'nodeaccess_rid';
    foreach ($rids as $rid) {
      // check to see if role id has been added to node access alias table
      // and add if it has not
      $is_setup = self::is_role_setup_with_nodeaccess($rid);
      if (!$is_setup) {
        self::add_role_to_nodeaccess($rid);
      }

      $grants[] = [
        'gid' => $rid,
        'realm' => $realm,
        'grant_view' => 1,
        'grant_update' => 1,
        'grant_delete' => 0
      ];

    }
    return $grants;
  }

  /**
   * gets rids for a permission type
   * @param $permission_type
   *
   * @return array
   */
  static public function get_rids_for_user_group_permission($permission_type) {
    global $user;
    $field_groups = CAPresenterUser::get_user_groups($user);
    $permissions = [];

    // build the permissions we want to search for
    foreach ($field_groups as $group) {
      $permission = CAPresenterUser::get_permission($group, $permission_type);
      if ($permission !== NULL) {
        $permissions[] = $permission;
      }
    }

    // get the roles that have this permission
    $rids = [];
    foreach ($permissions as $permission) {
      $role_permissions = CAPresenterUser::get_roles_with_permission($permission);
      $rids = array_merge($rids, $role_permissions);
    }
    return $rids;
  }

}
