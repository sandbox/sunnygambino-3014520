(function ($) {
  Drupal.behaviors.payer_popup = {
      attach: function(context, settings) {
          var self = this;
          self.binder();
      },
      binder: function() {
          $('a#add_payer_link').unbind('click').bind('click',function(e) {
              e.preventDefault();
              var href = $(this).attr('href');
              $('#slide_example_frame').attr('src',href);
	      $('#slide_example_frame').attr('scrolling','yes');
              $('.preview-modal').css('opacity',1);
              $('.preview-modal').css('display','table');
              $('.slide-preview-container').addClass('widescreen');
          });
          $('.popup-close').bind('click',function(e) {
              $('#edit-add-payer').trigger('click');
              $('.preview-modal').css('opacity',0);
              $('.preview-modal').css('display','none');
          });
          $('.slide-preview-toggle a').on('click', function(event){
              event.preventDefault();

              var slideid = $(this).attr('data-slideid');
              $('.slide-preview-container').toggleClass('widescreen');

              if( $('.slide-preview-container').hasClass('widescreen') ){
                  toggleBG(slideid, true);
              } else {
                toggleBG(slideid);
              }
          });
      }
  }
})(jQuery);
