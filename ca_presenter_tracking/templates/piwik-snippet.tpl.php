<?php
/**
 * template for rendering snippet for piwik
 * @var array $js
 * @var $view_type
 * @var $sid
 * @var $website_id
 * @var $stats_url
 * @var $reference
 * @var $pid
 */
?>
<!-- Piwik -->

  var _paq = _paq || [];

  <?php if ($view_type === 'presentation' || $view_type === 'slide') : ?>
      _paq.push(['setCustomVariable', 1, "reference", "<?php print $reference; ?>", "page"]);
      _paq.push(['setCustomVariable', 2, "presentation", "<?php print $pid; ?>", "page"]);
      _paq.push(['setCustomVariable', 3, "view_type", "<?php print $view_type; ?>", "page"]);
      _paq.push(['setCrossDomainLinkingTimeout', 5]);
  <?php endif; ?>

  <?php if ($view_type === 'slide') : ?>
      _paq.push(['enableHeartBeatTimer', 5]);
      _paq.push(['setCustomVariable', 4, "slide", "<?php print $sid; ?>", "page"]);
      _paq.push(['setCustomDimension', 1, '<?php print $pid; ?>']);
      _paq.push(['setCustomDimension', 2, '<?php print $pid; ?>']);
  <?php endif; ?>

  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//<?php print $stats_url; ?>/";
    _paq.push(['setTrackerUrl', u+'piwik.php']);
    _paq.push(['setSiteId', '<?php print $website_id; ?>']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.type='text/javascript'; g.async=true; g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
  })();
<!-- End Piwik Code -->
