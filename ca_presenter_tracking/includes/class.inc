<?php

class CAPresenterTracking {

  const TRACKING_PATTERN = 'ca-viewer';

  const VIEW_PRESENTATION = 'presentation';

  const VIEW_SLIDE = 'slide';

  const VIEW_DOCUMENT = 'document';

  // date format for piwik reports
  const DATE_FORMAT = 'Y-m-d';

  const DATE_PAGE_FORMAT = 'G:i d-m-Y';

  // only checking the past year
  const MINUTES_IN_YEAR = 525600;

  const ACTION_PRESENTATION_OPENED = 'presentation_open';

  // custom variable keys - these are always the same

  const VAR_REFERENCE = 'reference';

  const VAR_VIEW_TYPE = 'view_type';

  const VAR_PRESENTATION = 'presentation';

  // link with piwik segmentation
  const CUSTOM_VAR_REFERENCE = 'customVariablePageValue1';

  const CUSTOM_VAR_PRESENTATION = 'customVariablePageValue2';

  const CUSTOM_VAR_VIEW_TYPE = 'customVariablePageValue3';

  const CUSTOM_VAR_SLIDE = 'customVariablePageValue4';

  static function API_TOKEN() {
    return variable_get('stats_api_key', '');
  }

  static function API_URL() {
    $protocol = (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off'
      || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://";
    return $protocol . variable_get('stats_server_host', '');
  }

  static function SITE_ID() {
    return variable_get('stats_website_id', '');
  }

  /**
   * adds tracking for presentation view
   *
   * @param string $pid
   * @param string $reference
   * @param NULL | string $eid
   * @param bool $is_document
   */
  static function email_view($pid, $reference, $eid = NULL, $is_document = FALSE) {
    global $base_url;
    global $subdomain;
    global $user;

    $presentation_node = node_load($pid);
    $nodes = CAViewer::get_email_node($reference, $eid);
    if ($nodes !== FALSE && $presentation_node !== FALSE) {
      foreach($nodes as $node) {
        $email_wrapper = entity_metadata_wrapper('node', $node);
        $to = $email_wrapper->field_reply_email->value();
        $title = $presentation_node->title;
        $reference = $email_wrapper->field_reference->value();

        // we send page tracking for documents when we actually download the document
        if (!$is_document) {
          CAPresenterTracking::create_page_view($reference, self::VIEW_PRESENTATION,
            $pid);
        }

        $notify_sender = ($email_wrapper->field_email_notifications->value()) ? TRUE : FALSE;
        // only want to send notification once per session
        /*if ($notify_sender && $to !== NULL && (!isset($_SESSION['presentations_notified'])
            || !in_array($reference . $pid, $_SESSION['presentations_notified']))) {*/
        if ($notify_sender && $to !== NULL) {

          $account = user_load($node->uid);
          $sender_name = $account->name;
          if(isset($account->profile_firstname) && $account->profile_firstname != '') {
            $sender_name = $account->profile_firstname;
          }
          drupal_mail('ca_presenter_emails', 'ca_presenter_presentation_viewed',
            $to, LANGUAGE_NONE, [
              'title' => $title,
              //'sender_name' => $email_wrapper->field_from_name->value(),
              'sender_name' => $sender_name,
              'sent_to' => $email_wrapper->field_recipients_email->value(),
              'sent_to_name' => $email_wrapper->field_recipients_name->value(),
              'sent_at' => date('d M Y - H:i', $node->created),
              'subdomain' => $subdomain,
              'site_name' => variable_get('site_name', ''),
              'site_url' => $base_url,
              'presentation_id' => $pid,
              'email_id' => $node->nid,
              'is_document' => $is_document,
              'subject' => $email_wrapper->field_email_subject->value(),
            ]);

          //$_SESSION['presentations_notified'][] = $reference . $pid;
        }
      }
    }
  }

  /**
   * adds tracking for slide view
   *
   * @param string $pid
   * @param string $sid
   * @param string $reference
   */
  static function slide_view($pid, $sid, $reference) {
    self::add_tracking([
      'view_type' => self::VIEW_SLIDE,
      'pid' => $pid,
      'reference' => $reference,
      'sid' => $sid,
    ]);
  }

  /**
   * Add piwik tracking snippet
   *
   * @param array $args
   */
  static function add_tracking($args = []) {
    $args += [
      'stats_url' => variable_get('ca_presenter_stats_server_host', ''),
      'website_id' => variable_get('ca_presenter_stats_website_id', ''),
    ];
    $js = theme('tracking-code', $args);

    drupal_add_js($js, [
      'type' => 'inline',
      'scope' => 'header',
      'weight' => -21,
      'requires_jquery' => FALSE,
    ]);
  }

  /**
   * creates a page view with the php piwik api
   * to register presentations being opened
   *
   * @param string $reference
   * @param string $view_type
   * @param string $nid
   */
  static function create_page_view($reference, $view_type, $nid) {
    global $base_url;

    // convert to a switch or change to a function if anymore view types needed
    $page_url = ($view_type === self::VIEW_DOCUMENT) ? $base_url . '/documents/'
      . $nid . '/download/' . $reference
      : $base_url . '/node/' . $nid;

    $custom_variables = [
      1 => [self::VAR_REFERENCE, $reference],
      2 => [self::VAR_PRESENTATION, $nid],
      3 => [self::VAR_VIEW_TYPE, $view_type],
    ];

    // http tracker for ensuring we have a view for opening the email
    $url = self::API_URL();
    $url .= '/piwik.php?';
    $url .= 'idsite=' . urlencode(self::SITE_ID());
    $url .= '&url=' . urlencode($page_url);
    $url .= '&action_name=' . urlencode(self::ACTION_PRESENTATION_OPENED);
    $url .= '&cvar=' . urlencode(json_encode($custom_variables));
    $url .= '&rand=' . rand();
    $url .= '&token_auth=' . self::API_TOKEN();
    $url .= '&rec=1';
    $fetched = file_get_contents($url);

  }

  /**
   * Provides visits by presentation id for a given email for past year
   *
   * @param string | null $reference
   * @param array $pids
   * @param string $view_type
   *
   * @return mixed
   */
  static function get_aggregate_visits_by_reference($reference = NULL, $pids = [],
                                                    $view_type) {
    $api_method = 'Live.getCounters';
    $methods = [];

    // get the total visits per reference by adding on a NULL value to $pids
    $pids[] = NULL;
    foreach ($pids as $pid) {
      $method = '';
      $method .= 'method=' . $api_method;
      $method .= '&idSite=' . self::SITE_ID();
      $method .= '&lastMinutes=' . self::MINUTES_IN_YEAR;
      $method .= '&segment=' . self::CUSTOM_VAR_REFERENCE . '==' . $reference;

      if ($pid !== NULL) {
        $method .= ';' . self::CUSTOM_VAR_PRESENTATION . '==' . $pid;
      }

      $method .= ';' . self::CUSTOM_VAR_VIEW_TYPE . '==' . $view_type;

      $methods[] = $method;
    }
    $url = CAPresenterTracking::API_URL();
    $url .= '/?module=API&method=API.getBulkRequest&format=json';
    foreach ($methods as $key => $method) {
      $url .= '&urls[' . $key . ']=' . urlencode($method);
    }
    $url .= '&token_auth=' . CAPresenterTracking::API_TOKEN();

    $fetched = file_get_contents($url);
    $content = json_decode($fetched, TRUE);
    // parse them by presentation id so we can access results easily
    $results = [];
    foreach ($content as $key => $data) {
      $data = array_pop($data);
      $pid = ($pids[$key] === NULL) ? 'total' : $pids[$key];
      $results[$pid] = $data['actions'];
    }
    return $results;
  }

  static function get_visit_data($reference, $view_type, $aid) {
    $api_module = 'API';
    $api_method = 'Live.getLastVisitsDetails'; //'VisitsSummary.get';
    $url = CAPresenterTracking::API_URL();
    $url .= '/?module=' . urlencode($api_module);
    $url .= '&method=' . urlencode($api_method);
    $url .= '&format=json';
    $url .= '&idSite=' . self::SITE_ID();
    $url .= '&period=year';
    $url .= '&date=' . urlencode(date(self::DATE_FORMAT, time()));
    $url .= '&segment=';
    $url .= urlencode(self::CUSTOM_VAR_REFERENCE . '==' . $reference . ';');
    $url .= urlencode(self::CUSTOM_VAR_VIEW_TYPE . '==' . $view_type . ';');
    $url .= urlencode(self::CUSTOM_VAR_PRESENTATION . '==' . $aid);
    $url .= urlencode(';dimension2=='.$aid);
    $url .= '&token_auth=' . CAPresenterTracking::API_TOKEN();
    $fetched = file_get_contents($url);
    $ret = json_decode($fetched, TRUE);

    $visit_times = [];
    foreach ($ret as $visitor) {
      foreach ($visitor['actionDetails'] as $action) {
        $visit_times[] = $action['timestamp'];
      }
    }
    $visits = [];
    if (!empty($visit_times)) {
      $first_visit = min($visit_times);
      $first_visit = date('G:i d-m-Y', $first_visit);
      $last_visit = max($visit_times);
      $last_visit = date('G:i d-m-Y', $last_visit);
      $visit_stats = CAPresenterTracking::get_aggregate_visits_by_reference(
        $reference, [$aid], $view_type);
      $num_visits = array_pop($visit_stats);
    }
    else {
      return $visits;
    }

    // actions are more appropriate as visits require
    // a new session or inactivity for half an hour
    $visits = [
      'first_visit' => $first_visit,
      'last_visit' => $last_visit,
      'views' => $num_visits,
    ];
    return $visits;
  }

  static function get_slide_data($reference,$pid = FALSE) {

    $api_module = 'API';
    //$api_method = 'Actions.getPageTitles'; //'VisitsSummary.get';
    $api_method = 'Live.getVisitorProfile'; //'VisitsSummary.get';
    $url = CAPresenterTracking::API_URL();
    $url .= '/?module=' . urlencode($api_module);
    $url .= '&method=' . urlencode($api_method);
    $url .= '&format=json';
    $url .= '&idSite=' . self::SITE_ID();
    $url .= '&period=year';
    $url .= '&date=' . urlencode(date(self::DATE_FORMAT, time()));
    $url .= '&segment=';
    $url .= urlencode(self::CUSTOM_VAR_REFERENCE . '==' . $reference . ';');
    $url .= urlencode(self::CUSTOM_VAR_VIEW_TYPE . '==' . self::VIEW_SLIDE);
    if($pid !== FALSE)
      $url .= urlencode(';dimension1=='.$pid);
    $url .= '&token_auth=' . CAPresenterTracking::API_TOKEN();
    $fetched = file_get_contents($url);
    $ret = json_decode($fetched, TRUE);

    $slides = array();
    //Filter out other presentations just in case
    foreach($ret['lastVisits'][0]['actionDetails'] as $key => $actionDetail) {     
      $pid = $actionDetail['customVariables'][2]['customVariablePageValue2'];
      if($pid !== arg(3)) {
        unset($ret['lastVisits'][0]['actionDetails'][$key]);
      }
    }
    //Build up out slides array based on pageid so we can count visits, avg time...
    foreach($ret['lastVisits'][0]['actionDetails'] as $key => $actionDetail) {
      if((int)@$actionDetail['timeSpent'] > 0) {
        $time = $actionDetail['timeSpent'];
      }
      if(null !== @$time) {
        $slides[$actionDetail['pageIdAction']][] = array(
          'label' => $actionDetail['pageTitle'],
          'time_spent' => $time
        );
      } else {
        $slides[$actionDetail['pageIdAction']][] = array(
          'label' => $actionDetail['pageTitle']
        );
      }
    }
    //Get summary of spent time on each slide for avg time
    foreach($slides as $pageId => $details) {
      $slides[$pageId]['visits'] = count($slides[$pageId]);
      $slides[$pageId]['sum_time'] = 0;  
      foreach($details as $detail) {
        $slides[$pageId]['sum_time'] = $slides[$pageId]['sum_time'] + @$detail['time_spent'];
      }
    }
    //Setup average time
    foreach($slides as $pageId => $details) {
      $slides[$pageId]['avg_time'] = array();
      foreach($details as $key => $detail) {
        if(null !== @$detail['time_spent'])
          $slides[$pageId]['avg_time'][] = $detail['time_spent'];
          if(null !== $detail['label']) {
            $slides[$pageId]['label'] = $detail['label'];
            unset($slides[$pageId][$key]);
          }
      }
      if($slides[$pageId]['sum_time'] <= 0) {
        unset($slides[$pageId]);
      }
      $count_times = count(@$slides[$pageId]['avg_time']);
      if($count_times > 0)
        $slides[$pageId]['avg_time'] = $slides[$pageId]['sum_time'] / $count_times;
    }

    $results = [];

    foreach ($slides as $slide) {
      // pretty formatted seconds on page
      $seconds = (int) $slide['avg_time'];
      $average_time_on_page = '';
      if ($seconds > 3600) {
        $average_time_on_page .=  intVal(date('H', $seconds)) . ' hours ';
      }
      if ($seconds > 60) {
        $average_time_on_page .= intVal(date('i', $seconds)) . ' minutes ';
      }
      $average_time_on_page .= intVal(date('s', $seconds)) . ' seconds';

      $results[] = [
        'title' => $slide['label'],
        'visits' => $slide['visits'],
        'avg_time' => $average_time_on_page,
      ];
    }
    return $results;
  }

  static function get_slide_detail_data($reference, $pid) {

    $api_module = 'API';
    $api_method = 'Live.getLastVisitsDetails';
    $url = CAPresenterTracking::API_URL();
    $url .= '/?module=' . urlencode($api_module);
    $url .= '&method=' . urlencode($api_method);
    $url .= '&format=json';
    $url .= '&idSite=' . self::SITE_ID();
    $url .= '&period=year';
    $url .= '&segment=';
    $url .= urlencode(self::CUSTOM_VAR_REFERENCE . '==' . $reference . ';');
    $url .= urlencode(self::CUSTOM_VAR_PRESENTATION . '==' . $pid . ';');
    $url .= urlencode(self::CUSTOM_VAR_VIEW_TYPE . '==' . self::VIEW_SLIDE);
    /*if($pid !== FALSE) {
      $url .= urlencode(';dimension1=='.$pid.';');
      $url .= urlencode(';dimension2=='.$pid);
    }*/
    $url .= "&filter_limit=100";
    $url .= '&token_auth=' . CAPresenterTracking::API_TOKEN();

    $fetched = file_get_contents($url);
    $ret = json_decode($fetched, TRUE);
    foreach($ret as $key => $r) {
      $visitDuration = 0;
      foreach($r['actionDetails'] as $key2 => $actionDetail) {
        if(!isset($actionDetail['timeSpent']) || (int)$actionDetail['timeSpent'] <= 0) {
          unset($ret[$key]['actionDetails'][$key2]);
        } 
        if(isset($actionDetail['customVariables'][2]['customVariablePageValue2']) && $actionDetail['customVariables'][2]['customVariablePageValue2'] != $pid) {
          unset($ret[$key]['actionDetails'][$key2]);
        }
        if((isset($actionDetail['timeSpent']) && (int)$actionDetail['timeSpent'] > 0) && (isset($actionDetail['customVariables'][2]['customVariablePageValue2']) && $actionDetail['customVariables'][2]['customVariablePageValue2'] == $pid)) {
          $visitDuration = $visitDuration + (int)$actionDetail['timeSpent'];
        }
      }
      // pretty formatted seconds on page
      $seconds = (int) $visitDuration;
      $average_time_on_page = '';
      if ($seconds > 3600) {
        $average_time_on_page .=  intVal(date('H', $seconds)) . ' hours ';
      }
      if ($seconds > 60) {
        $average_time_on_page .= intVal(date('i', $seconds)) . ' minutes ';
      }
      $average_time_on_page .= intVal(date('s', $seconds)) . ' seconds';
      $ret[$key]['visitDurationPretty'] = $average_time_on_page;
    }
    return $ret;
  }
}
