<?php

/**
 * services resource callback for messages index
 * @param string $start_time
 * @param string $page_index
 *
 * @return array $data
 */

function ca_presenter_api_messages_index() {
  global $user;

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'message')
    ->propertyOrderBy('created', 'DESC');

    /**
     * Pagination will have to be done on the controller on app as it will be a merged view: online/offlien
     */
    //->range($start, $items_per_page);

  $result = $query->execute();
  $data = ca_presenter_api_messages_build_list($result);

  if (count($data) <= 0) {
    return NULL;
  }

  return $data;
}

/**
 * builds messages list
 *
 * @params object result
 *
 * @return array $data
 */
function ca_presenter_api_messages_build_list($result){
  global $user;

  $readmessages = array();
  $unreadmessages = array();

  $messages = entity_load('node', array_keys($result['node']));

  if (count($messages) <= 0) {
    return NULL;
  }


  foreach ($messages as $message) {
    if(!node_access('view',$message)) {
      continue;
    }

    $hasread = false;
    $readfield = $message->field_users_read;
    $readusers = array();

    if( is_array($readfield[LANGUAGE_NONE]) ) {
      foreach ($readfield[LANGUAGE_NONE] as $target ) {
        $readusers[] = $target['target_id'];
      }
    }

    if( in_array($user->uid, $readusers) ) {
      $hasread = 1;
    } else {
      $hasread = 0;
    }

    $isdeleted = false;
    $deletefield = $message->field_users_deleted;
    $deletedusers = array();

    if( is_array($deletefield[LANGUAGE_NONE]) ) {
      foreach ($deletefield[LANGUAGE_NONE] as $target ) {
        $deletedusers[] = $target['target_id'];
      }
    }


    if( !in_array($user->uid, $deletedusers) ) {
      $data[] = array(
        'id' => $message->nid,
        'title' => ($message->title ? $message->title : NULL),
        'body' => ($message->field_body[LANGUAGE_NONE] ? $message->field_body[LANGUAGE_NONE][0]['value'] : NULL),
        'date_created' => $message->created,
        'date_created_formatted' => date('d/m/y G:i' , $message->created),
        'user' => user_load($message->uid),
        'readfield' => $readfield,
        'read' => $hasread
      );
    }
  }

  return $data;
}

/**
 * sets the status of a message
 *
 * @params int $id
 *
 */
function ca_presenter_api_message_update_status($id){
  global $user;

  $message = entity_load_single('node', $id);

  if($message){
      //@TODO Add watchdog
    if(count($message->field_message_view_count) == 0) {
      $message->field_message_view_count[LANGUAGE_NONE][0]['value'] = 1;
    } else {
      $count = $message->field_message_view_count[LANGUAGE_NONE][0]['value'];
      $count = (int)$count+1;
      $message->field_message_view_count[LANGUAGE_NONE][0]['value'] = $count;
    }

    $userids = array();
    $readfield = $message->field_users_read;

    if( isset($readfield) || is_array($readfield) ) {
      foreach($readfield[LANGUAGE_NONE] as $target){
        $userids[] = $target['target_id'];
      }

      // Check if it's already been read by user
      if( !in_array($user->uid, $userids) ) {
        $userids[] = $user->uid;
      }
    } else {
      $userids[] = $user->id;
    }

    $count = 0;
    foreach( $userids as $usertoread ) {
      $message->field_users_read[LANGUAGE_NONE][$count]['target_id'] = $usertoread;
      $count++;
    }

    node_save($message);

    return $data = array(
        'message' => 'success'
    );
  } else {
      return $data = array(
          'message' => 'Not found',
          'error' => 404
      );
  }
}

/**
 * deletes the message
 *
 * NOTE that this doesn't physically deletes the message but updates a field with the user id so it does not appear in the app
 *
 * @params int $id
 *
 */
function ca_presenter_api_message_delete($id){
  global $user;

  $message = entity_load_single('node', $id);

  if($message){
      //@TODO Add watchdog
    if(count($message->field_message_view_count) == 0) {
      $message->field_message_view_count[LANGUAGE_NONE][0]['value'] = 1;
    } else {
      $count = $message->field_message_view_count[LANGUAGE_NONE][0]['value'];
      $count = (int)$count+1;
      $message->field_message_view_count[LANGUAGE_NONE][0]['value'] = $count;
    }

    $userids = array();
    $deletefield = $message->field_users_deleted;

    if( isset($deletefield) || is_array($deletefield) ) {
      foreach($deletefield[LANGUAGE_NONE] as $target){
        $userids[] = $target['target_id'];
      }

      // Check if it's already been read by user
      if( !in_array($user->uid, $userids) ) {
        $userids[] = $user->uid;
      }
    } else {
      $userids[] = $user->id;
    }

    $count = 0;
    foreach( $userids as $userstodelete ) {
      $message->field_users_deleted[LANGUAGE_NONE][$count]['target_id'] = $userstodelete;
      $count++;
    }

    node_save($message);

    return $data = array(
        'message' => 'success'
    );
  } else {
      return $data = array(
          'message' => 'Not found',
          'error' => 404
      );
  }
}

/**
 * gets unread messages count
 *
 */
function ca_presenter_api_unread_messages(){
  global $user;

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'message')
    ->propertyOrderBy('created', 'DESC');

  $result = $query->execute();

  $messages = entity_load('node', array_keys($result['node']));
  $total = array();

  if (count($messages) <= 0) {
    return NULL;
  }

  foreach ($messages as $message ) {

    $isdeleted = false;
    $deletefield = $message->field_users_deleted;
    $deletedusers = array();

    if( is_array($deletefield[LANGUAGE_NONE]) ) {
      foreach ($deletefield[LANGUAGE_NONE] as $target ) {
        $deletedusers[] = $target['target_id'];
      }
    }

    $hasread = false;
    $readfield = $message->field_users_read;
    $userids = array();

    // Only count data if the message is not deleted!
    if( !in_array($user->uid, $deletedusers) ) {

      $total[] = $message;

      if( is_array($readfield[LANGUAGE_NONE]) ) {
        foreach ($readfield[LANGUAGE_NONE] as $target ) {
          $userids[] = $target['target_id'];
        }
      }

      if( in_array($user->uid, $userids) ) {
        $read[] = array(
          'id' => $message->nid
        );
      } else {
        $unread[] = array(
          'id' => $message->nid
        );
      }
    }
  }

  $unreadmessages = count($unread);
  if( $unreadmessages == 0 ) {
    $unreadmessages = '';
  }

  $data = array(
    'total' => count($total),
    'unread' => $unreadmessages,
    'read' => count($read)
  );

  return $data;
}