<?php
/**
 * services resource callback for documents index
 * @param int $id
 * @param string $title
 * @param string $category
 * @param string $author
 * @param string $group
 * @param string $page_index
 *
 * @return array $data
 */

function ca_presenter_api_document_index($ref_or_title, $category, $author, $group, $status, $page_index) {
  global $user;
  global $published_full;
  global $published_web;

  $items_per_page = 20;
  $start = 0;

  if($page_index !== 0) {
      $start = $page_index * $items_per_page;
  }

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'document')
    ->propertyOrderBy('created', 'DESC')
    ->range($start, $items_per_page);

    /**
     * Filter the results by field status
     */
    if( $status == 'full') {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_full->tid);
    } else if( $status == 'web' ) {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_web->tid);
    } else {
      $query->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));
    }

    /**
     * Filter the results by group if isset
     */
    if( isset($group) && $group != 'all' ){
      if($group == 'library'){
        $query->propertyCondition('status', 1, '=');
      }
      if($group == 'personal') {
        $query->propertyCondition('uid', $user->uid, '=');
      }
      if($grup == 'shared') {
        $query->propertyCondition('uid', $user->uid, '!=');
      }
    }

    /**
     * Search by node id or title if isset
     */
    if( is_numeric($ref_or_title) ) {
      $query->propertyCondition('nid', $ref_or_title);
    } else {
      $query->propertyCondition('title', '%' . $ref_or_title . '%', 'like');
    }

    /**
     * Search by author if isset
     */
    if( isset($author) && $author != '' ) {
      $presAuthor = user_load_by_name($author);

      $query->propertyCondition('uid', $presAuthor->uid, '=');
    }

    /**
     * Filter the results by category if isset
     */
    if( isset($category) && $category != '0'){
      $query->fieldCondition('field_document_category', 'tid', $category);
    }

    $result = $query->execute();
    $data = ca_presenter_api_documents_build_list($result);

    if (count($data) <= 0) {
      return NULL;
    }

    return $data;
}

/**
 * builds documents list
 *
 * @params object result
 *
 * @return array $data
 */
function ca_presenter_api_documents_build_list($result){
  global $user;
  global $published_full;
  global $published_web;
  global $base_root;
  $documents = entity_load('node', array_keys($result['node']));

  if (count($documents) <= 0) {
    return NULL;
  }

  $data = array();

  foreach ($documents as $document) {

    $categories = taxonomy_term_load($document->field_document_category['und'][0]['tid']);
    $pauthor = user_load($document->uid);
    $author = $pauthor->name;
    $file = $document->field_file['und'][0];
    $instance = get_instance_url();

    // @TODO
    $download_url = NULL;

    $data[] = array(
      'id' => $document->nid,
      'title' => $document->title,
      'description' => strip_tags(($document->body) ? $document->body['und'][0]['value'] : NULL),
      'category' => array(
        'id' => $categories->tid,
        'name' => $categories->name,
        'weight' => $categories->weight
      ),
      'author' => $author,
      'group' => ca_presenter_presentation_get_group($document),
      'date_created' => $document->created,
      'date_created_formatted' => date('d/m/y G:i', $document->created),
      'date_modified' => $document->changed,
      'date_modified_formatted' => date('d/m/y G:i' , $document->changed),
      'download_size' => $document->field_file['und'][0]['filesize'],
      'timestamp' => $document->field_file['und'][0]['timestamp'],
      'primary_status' => taxonomy_term_load($document->field_primary_statuses['und'][0]['tid']),
      'secondary_status' => taxonomy_term_load($document->field_secondary_statuses['und'][0]['tid']),
      'mimetype' => $file['filemime'],
      'file' => $file,
      'file_name' => $file['filename'],
      'extension' => mime2ext($file['filemime']),
      'view_url' => (mime2ext($file['filemime']) == 'pdf') ? 'https://' . $instance . '.companyapp.co.uk/system/files/' . str_replace('private://', '', $file['uri']) : false,
      'onlyweb' => ($document->field_primary_statuses['und'][0]['tid'] == $published_web->tid ? TRUE : FALSE), // IF THE STATUS IS == PUBLISHED WEB ? TRUE | needed in app
    );

  }

  return $data;

}

/**
 * builds a book for a document
 *
 * @params string id
 *
 * @return array $data
 */
function ca_presenter_api_document_book($id){
  global $user;
  global $published_full;
  global $published_web;


  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'document')
    ->propertyCondition('nid', $id)
    ->propertyOrderBy('created', 'DESC')
    ->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));

    $result = $query->execute();
    $data = ca_presenter_api_documents_build_list($result);

    if (count($data) <= 0) {
      return NULL;
    }

    return $data;
}

/**
 * checks for an update on a document
 *
 * @params string id
 * @params string date_changed
 *
 * @return Boolean $data
 */
function ca_presenter_api_documents_check_update($document, $timestamp) {
  global $user;
  global $published_full;
  global $published_web;

  $node = entity_load_single('node', $document['id']);
  $file = $node->field_file['und'][0];
  $node_timestamp = $node->field_file['und'][0]['timestamp'];

  $status = taxonomy_term_load($node->field_secondary_statuses['und'][0]['tid']);
  $status = strtolower($status->name); // for consistency

  $modified = $document['date_modified'];
  $has_update = false;

  if($node->field_primary_statuses['und'][0]['tid'] != $published_full->tid) {
    if($node->field_primary_statuses['und'][0]['tid'] == $published_web->tid){
      $data = array(
        'error' => true,
        'status' => 'Published Web',
        'code' => 401
      );
    } else {
      $data = array(
        'error' => true,
        'status' => 'Deleted',
        'code' => 403
      );
    }
  } else {
    if( isset($timestamp) && $timestamp != '' ){
      // Check if the file has changed
      if( $timestamp != $node_timestamp && $status == 'none' ) {
        $has_update = true;
      }
      // Check if node has changed and is published full
      if( $modified != $node->changed && $status == 'none' ) {
        $has_update = true;
      }
    }
  }

  $data = array(
    'has_update' => $has_update,
    'timestamp_retrived' => $timestamp,
    'timestamp_node' => $node_timestamp,
    'modified_retrived' => $modified,
    'modified_node' => $node->changed,
    'status' => $status,
    //'document' => $document,
    //'node' => $node,
  );

  return $data;
}


/**
 * checks for an update on all documents
 *
 * @params array documents
 *
 * @return array $data
 */
function ca_presenter_api_documents_sync($documents) {
  global $user;
  global $published_full;
  global $published_web;

  if (count($documents) <= 0) {
    return NULL;
  }

  $data = array();

  foreach($documents as $document) {
    $node = entity_load_single('node', $document['id']);
    $file = $node->field_file['und'][0];
    $node_timestamp = $node->field_file['und'][0]['timestamp'];

    $timestamp = $document['timestamp'];

    $status = taxonomy_term_load($node->field_secondary_statuses['und'][0]['tid']);
    $status = strtolower($status->name); // for consistency

    $modified = $document['date_modified'];
    $has_update = false;

    if($node->field_primary_statuses['und'][0]['tid'] != $published_full->tid) {
      if($node->field_primary_statuses['und'][0]['tid'] == $published_web->tid){
        $data[] = array(
          'id' => $document['id'],
          'error' => true,
          'status' => 'Published Web',
          'code' => 401
        );
      } else {
        $data[] = array(
          'id' => $document['id'],
          'error' => true,
          'status' => 'Deleted',
          'code' => 403
        );
      }
    } else {
      if( isset($timestamp) && $timestamp != '' ){
        // Check if the file has changed
        if( $timestamp != $node_timestamp && $status == 'none' ) {
          $has_update = true;
        }
        // Check if node has changed and is published full
        if( $modified != $node->changed && $status == 'none' ) {
          $has_update = true;
        }
      }

      $data[] = array(
        'id' => $document['id'],
        'has_update' => $has_update,
        'timestamp_retrived' => $timestamp,
        'timestamp_node' => $node_timestamp,
        'modified_retrived' => $modified,
        'modified_node' => $node->changed,
        'status' => $status,
        //'document' => $document,
        //'node' => $node,
      );
    }
  }

  return $data;

}

/**
 * get the number of documents available for the user
 *
 * @return array $documents
 */
function ca_presenter_api_document_list($ref_or_title, $category, $author, $group, $status) {
  global $user;
  global $published_full;
  global $published_web;

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'document')
    ->propertyOrderBy('created', 'DESC');

    /**
     * Filter the results by field status
     */
    if( $status == 'full') {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_full->tid);
    } else if( $status == 'web' ) {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_web->tid);
    } else {
      $query->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));
    }

    /**
     * Filter the results by group if isset
     */
    if( isset($group) && $group != 'all' ){
      if($group == 'library'){
        $query->propertyCondition('status', 1, '=');
      }
      if($group == 'personal') {
        $query->propertyCondition('uid', $user->uid, '=');
      }
      if($grup == 'shared') {
        $query->propertyCondition('uid', $user->uid, '!=');
      }
    }

    if( is_numeric($ref_or_title) ) {
      $query->propertyCondition('nid', $ref_or_title);
    } else {
      $query->propertyCondition('title', '%' . $ref_or_title . '%', 'like');
    }

    if( isset($author) && $author != '' ) {
      $docAuthor = user_load_by_name($author);

      $query->propertyCondition('uid', $docAuthor->uid);
    }

    if( isset($category) && $category != '0'){
      $query->fieldCondition('field_document_category', 'tid', $category);
    }

    $result = $query->execute();
    $documents = entity_load('node', array_keys($result['node']));

    return count($documents);
}


/**
 * mime to extension converter
 *
 * source: https://gist.github.com/alexcorvi/df8faecb59e86bee93411f6a7967df2c
 */
function mime2ext($mime){
  $all_mimes = '{"png":["image\/png","image\/x-png"],"bmp":["image\/bmp","image\/x-bmp","image\/x-bitmap","image\/x-xbitmap","image\/x-win-bitmap","image\/x-windows-bmp","image\/ms-bmp","image\/x-ms-bmp","application\/bmp","application\/x-bmp","application\/x-win-bitmap"],"gif":["image\/gif"],"jpeg":["image\/jpeg","image\/pjpeg"],"xspf":["application\/xspf+xml"],"vlc":["application\/videolan"],"wmv":["video\/x-ms-wmv","video\/x-ms-asf"],"au":["audio\/x-au"],"ac3":["audio\/ac3"],"flac":["audio\/x-flac"],"ogg":["audio\/ogg","video\/ogg","application\/ogg"],"kmz":["application\/vnd.google-earth.kmz"],"kml":["application\/vnd.google-earth.kml+xml"],"rtx":["text\/richtext"],"rtf":["text\/rtf"],"jar":["application\/java-archive","application\/x-java-application","application\/x-jar"],"zip":["application\/x-zip","application\/zip","application\/x-zip-compressed","application\/s-compressed","multipart\/x-zip"],"7zip":["application\/x-compressed"],"xml":["application\/xml","text\/xml"],"svg":["image\/svg+xml"],"3g2":["video\/3gpp2"],"3gp":["video\/3gp","video\/3gpp"],"mp4":["video\/mp4"],"m4a":["audio\/x-m4a"],"f4v":["video\/x-f4v"],"flv":["video\/x-flv"],"webm":["video\/webm"],"aac":["audio\/x-acc"],"m4u":["application\/vnd.mpegurl"],"pdf":["application\/pdf","application\/octet-stream"],"pptx":["application\/vnd.openxmlformats-officedocument.presentationml.presentation"],"ppt":["application\/powerpoint","application\/vnd.ms-powerpoint","application\/vnd.ms-office","application\/msword"],"docx":["application\/vnd.openxmlformats-officedocument.wordprocessingml.document"],"xlsx":["application\/vnd.openxmlformats-officedocument.spreadsheetml.sheet","application\/vnd.ms-excel"],"xl":["application\/excel"],"xls":["application\/msexcel","application\/x-msexcel","application\/x-ms-excel","application\/x-excel","application\/x-dos_ms_excel","application\/xls","application\/x-xls"],"xsl":["text\/xsl"],"mpeg":["video\/mpeg"],"mov":["video\/quicktime"],"avi":["video\/x-msvideo","video\/msvideo","video\/avi","application\/x-troff-msvideo"],"movie":["video\/x-sgi-movie"],"log":["text\/x-log"],"txt":["text\/plain"],"css":["text\/css"],"html":["text\/html"],"wav":["audio\/x-wav","audio\/wave","audio\/wav"],"xhtml":["application\/xhtml+xml"],"tar":["application\/x-tar"],"tgz":["application\/x-gzip-compressed"],"psd":["application\/x-photoshop","image\/vnd.adobe.photoshop"],"exe":["application\/x-msdownload"],"js":["application\/x-javascript"],"mp3":["audio\/mpeg","audio\/mpg","audio\/mpeg3","audio\/mp3"],"rar":["application\/x-rar","application\/rar","application\/x-rar-compressed"],"gzip":["application\/x-gzip"],"hqx":["application\/mac-binhex40","application\/mac-binhex","application\/x-binhex40","application\/x-mac-binhex40"],"cpt":["application\/mac-compactpro"],"bin":["application\/macbinary","application\/mac-binary","application\/x-binary","application\/x-macbinary"],"oda":["application\/oda"],"ai":["application\/postscript"],"smil":["application\/smil"],"mif":["application\/vnd.mif"],"wbxml":["application\/wbxml"],"wmlc":["application\/wmlc"],"dcr":["application\/x-director"],"dvi":["application\/x-dvi"],"gtar":["application\/x-gtar"],"php":["application\/x-httpd-php","application\/php","application\/x-php","text\/php","text\/x-php","application\/x-httpd-php-source"],"swf":["application\/x-shockwave-flash"],"sit":["application\/x-stuffit"],"z":["application\/x-compress"],"mid":["audio\/midi"],"aif":["audio\/x-aiff","audio\/aiff"],"ram":["audio\/x-pn-realaudio"],"rpm":["audio\/x-pn-realaudio-plugin"],"ra":["audio\/x-realaudio"],"rv":["video\/vnd.rn-realvideo"],"jp2":["image\/jp2","video\/mj2","image\/jpx","image\/jpm"],"tiff":["image\/tiff"],"eml":["message\/rfc822"],"pem":["application\/x-x509-user-cert","application\/x-pem-file"],"p10":["application\/x-pkcs10","application\/pkcs10"],"p12":["application\/x-pkcs12"],"p7a":["application\/x-pkcs7-signature"],"p7c":["application\/pkcs7-mime","application\/x-pkcs7-mime"],"p7r":["application\/x-pkcs7-certreqresp"],"p7s":["application\/pkcs7-signature"],"crt":["application\/x-x509-ca-cert","application\/pkix-cert"],"crl":["application\/pkix-crl","application\/pkcs-crl"],"pgp":["application\/pgp"],"gpg":["application\/gpg-keys"],"rsa":["application\/x-pkcs7"],"ics":["text\/calendar"],"zsh":["text\/x-scriptzsh"],"cdr":["application\/cdr","application\/coreldraw","application\/x-cdr","application\/x-coreldraw","image\/cdr","image\/x-cdr","zz-application\/zz-winassoc-cdr"],"wma":["audio\/x-ms-wma"],"vcf":["text\/x-vcard"],"srt":["text\/srt"],"vtt":["text\/vtt"],"ico":["image\/x-icon","image\/x-ico","image\/vnd.microsoft.icon"],"csv":["text\/x-comma-separated-values","text\/comma-separated-values","application\/vnd.msexcel"],"json":["application\/json","text\/json"]}';
  $all_mimes = json_decode($all_mimes,true);
  foreach ($all_mimes as $key => $value) {
    if(array_search($mime,$value) !== false) return $key;
  }
  return false;
}

/**
 * Gets the subdomain of the instance
 *
 * @return string $subdomain
 */
function get_instance_url(){
  global $user;
  global $base_url;

  $parsedUrl = parse_url($base_url);
  $host = explode('.', $parsedUrl['host']);
  $subdomain = $host[0];

  return $subdomain;
}