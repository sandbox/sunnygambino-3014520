<?php
/**
 * services resource callback for events such as app activity
 * @param array $events
 *
 * @return array
 */
function ca_presenter_api_events($events) {
  global $user;


  ob_start();
  var_dump($events);
  $r = ob_get_clean();
  watchdog('Events API', $r);

  if(is_string($events)) {
    $test_events = json_decode($events,TRUE);
    if(is_array($test_events) && count($test_events) != 0) {
      $events = $test_events;
    }
  }

  $event_ids = array();
  foreach ($events as $event) {

    $metadata = (!isset($event['metadata']) || $event['metadata'] == '') ? NULL : $event['metadata'];

    if ($metadata != NULL) {
      $array_data = json_decode($metadata, true);
      if(isset($array_data[0]))
        $array_data = $array_data[0]['data'];

      if(!$array_data) {
      	return array(
      	  'error' => t('Cannot decode metadata as JSON. Metadata is: '.$metadata),
      	);
      }

      if ($array_data == NULL) {
      	return array(
      	  'error' => t('Metadata is empty.'),
      	);
      }

      $flat_data = ca_presenter_event_array_flatten($array_data, array(), '', 0);

      if (!is_array($flat_data)) {
      	return array(
      	  'error' => t('Cannot flatten metadata array.'),
      	);
      }
    }

    $unique_id = $event['unique_id'];
    $type = $event['type'];
    $name = ($event['name'] == '') ? NULL : $event['name'];
    $presentation_id = (!isset($event['presentation_id']) || $event['presentation_id'] == '') ? NULL : $event['presentation_id'];
    $slide_id = (!isset($event['slide_id']) || $event['slide_id'] == '') ? NULL : $event['slide_id'];
    $document_id = (!isset($event['document_id']) || $event['document_id'] == '') ? NULL : $event['document_id'];
    $message_id = (!isset($event['message_id']) || $event['message_id'] == '') ? NULL : $event['message_id'];
    $view_id = (!isset($event['view_id']) || $event['view_id'] == '') ? NULL : $event['view_id'];
    $client_id = (!isset($event['client_id']) || $event['client_id'] == '') ? NULL : $event['client_id'];
    $client_name = (!isset($event['client_name']) || $event['client_name'] == '') ? NULL : $event['client_name'];
    $location_lat = (!isset($event['location_lat']) || $event['location_lat'] == '') ? NULL : $event['location_lat'];
    $location_lng = (!isset($event['location_lng']) || $event['location_lng'] == '') ? NULL : $event['location_lng'];
    $metadata = (!isset($event['metadata']) || $event['metadata'] == '') ? NULL : $event['metadata'];
    $viewer_mode = 0;
    $registered_time = $event['registered_time'];
    $received_time = time();

    $app_version = $event['app_version'];
    $app_platform = $event['app_platform'];
    $device_model = $event['device_model'];
    $device_details = $event['device_details'];

    if ($type == '' || (int)$registered_time == 0) {
      return array(
        'error' => 'Invalid data supplied. No type or regtime.',
      );
    }

    $new_event = new stdClass();
    $new_event->type = "event";
    node_object_prepare($new_event);
    $new_event->title = $name;

    if($name === NULL) {
      $new_event->title = $type;
    }

    $new_event->language = LANGUAGE_NONE;
    $new_event->uid = $user->uid;
    $new_event->status = 1;
    $new_event->promote = 0;

    if($presentation_id) {
      $new_event->field_presentation_id['und'][0]['target_id'] = $presentation_id;
    }

    if($document_id) {
      $new_event->field_document_id['und'][0]['target_id'] = $document_id;
    }

    if($slide_id) {
      $new_event->field_slide_id['und'][0]['target_id'] = $slide_id;
    }

    if($message_id) {
      $new_event->field_message_id['und'][0]['value'] = $message_id;
    }

    $new_event->field_metadata_value['und'][0]['value'] = $event['metadata'];
    $new_event->field_secondary_name['und'][0]['value'] = $type;
    $new_event->field_device_id['und'][0]['value'] = $device;
    $new_event->field_view_id['und'][0]['value'] = $view_id;
    $new_event->field_client_id['und'][0]['value'] = $client_id;
    $new_event->field_client_name['und'][0]['value'] = $client_name;
    $new_event->field_location_lat['und'][0]['value'] = $location_lat;
    $new_event->field_location_lng['und'][0]['value'] = $location_lng;
    $new_event->field_viewer_mode['und'][0]['value'] = $viewer_mode;
    $new_event->field_received['und'][0]['value'] = $registered_time;
    $new_event->field_user_id['und'][0]['value'] = $user->uid;

    $new_event->field_app_version['und'][0]['value'] = $app_version;
    $new_event->field_app_platform['und'][0]['value'] = $app_platform;
    $new_event->field_device_model['und'][0]['value'] = $device_model;
    $new_event->field_device_details['und'][0]['value'] = $device_details;



    node_save($new_event);
    $eid = $new_event->nid;

    if (!$eid) {
      return array(
        'error' => t('No event ID.'),
      );
    }
  }

  return array(
    'pushed' => $events,
  );
}
