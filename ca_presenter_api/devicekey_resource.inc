<?php
/**
 * services resource callback for checking validity of application key
 * @param string $device_key
 *
 * @return array
 */
function ca_presenter_api_device_key($device_key) {

  global $user;

  if($user->uid == 1) {
      $status = [
        'valid' => true,
        'message' => 'Valid device key',
        'code' => 200
      ];
      return $status;
  }

  $status = _ca_presenter_get_device_by_access_code($device_key);

  return $status;
}

/**
 * returns a status message on validity of application key
 * @param $user
 * @param $device_key
 *
 * @return array
 */
function _ca_presenter_get_device_by_access_code ($device_key, $account = NULL) {
  global $user;

  $device_enabled = variable_get('ca_presenter_device_key', 1);
  $account = ($account === NULL) ? $user : $account;
  $valid_access_code = FALSE;
  $message = 'Invalid device key';
  $code = 403;

  $data = array(
    'valid' => false,
    'message' => $message,
    'code' => $code
  );

  if ($device_enabled == 1) {
    if ($device_key !== NULL && isset($account->uid)) {
      $query = new EntityFieldQuery();
      $query->entityCondition('entity_type', 'node', '=')
        ->entityCondition('bundle', 'device_key', '=')
        ->propertyCondition('status', NODE_PUBLISHED)
        ->fieldCondition('field_device_key_user', 'target_id', $account->uid, '=')
        ->fieldCondition('field_device_key_access_code', 'value', $device_key, '=')
        ->addTag('DANGEROUS_ACCESS_CHECK_OPT_OUT');

      $results = $query->execute();
      $access = (count($results) > 0);
      if (!$access) {
        $query->propertyConditions[0]['value'] = NODE_NOT_PUBLISHED;
        $results = $query->execute();
        if (count($results) > 0) {
          $message = ($access) ? $message : 'Device key is not active';
          $code = 401;
        }
      } else {
        $message = 'Valid device key';
        $code = 200;
      }
    }

    $data = array(
      'valid' => $access,
      'message' => $message,
      'code' => $code
    );
  } else {
    // Device key functionality is OFF
    $data = array(
      'message' => 'Valid device key',
      'code' => 200
    );
  }

  return $data;
}

/**
 * Access.
 */
function ca_presenter_api_device_key_access($permission, $args = array()) {

}
