<?php

  function ca_presenter_api_get_slides($id) {
    global $user;

    $nid = $id[0];
    $node = entity_load_single('node', $nid);
    $slides = $node->field_slides['und'];

    $data = array();

    foreach ($slides as $slideref) {

      $slide = node_load($slideref['target_id']);

      $data[] = array(
        'id' => $slide->nid,
        'title' => ($slide->field_slide_main_title['und'][0]['safe_value'] ? $slide->field_slide_main_title['und'][0]['safe_value'] : $slide->title),
        'thumbnail' => ca_presenter_api_get_slide_thumbnail($nid, $slide->nid)
      );

    }

    return  $data;

  }

  function ca_presenter_api_get_slide_thumbnail($pid, $sid){
    global $subdomain;

    $image_uri = 'private://' . $subdomain . '/ca_presenter/presentations/' . $pid . '/__thumbnails/' . $sid . '.jpg';
    $image = drupal_realpath($image_uri);

    if($image){
      return file_create_url($image_uri);
    } else {
      return NULL;
    }
  }

?>

