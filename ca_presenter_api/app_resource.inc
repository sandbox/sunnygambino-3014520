<?php
/**
 * services resource callback for app information
 * @param int $id
 * @param string $title
 * @param string $category
 * @param string $author
 * @param string $group
 * @param int $per_page
 * @param int $offset
 *
 * @return array
 */

function ca_presenter_api_app_version() {

  $link = variable_get('ca_presenter_download_link');

  if(!$link){
    $link = 'http://downloads.companyapp.co.uk/presenter';
  }

  return array(
    'current' => variable_get('ca_presenter_app_current_version'),
    'link' => $link
  );

}