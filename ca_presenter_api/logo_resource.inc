<?php
/**
 * services resource callback for logo
 * @param int $id
 * @param string $title
 * @param string $category
 * @param string $author
 * @param string $group
 * @param int $per_page
 * @param int $offset
 *
 * @return array
 */

function ca_presenter_api_logo() {

  return array(
    'dark' => variable_get('ca_presenter_dark_logo'),
    'light' => variable_get('ca_presenter_light_logo'),
    'use' => variable_get('ca_presenter_use_logo')
  );

}