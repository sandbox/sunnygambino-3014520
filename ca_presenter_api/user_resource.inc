<?php
/**
 * services resource callback for user
 *
 * Returns the datails of currently logged in user
 *
 * This is a copy of system/connect - separated to allow skipping CSRF
 *
 * @return
 *   object with session id, session name and a user object.
 */

function ca_presenter_api_connect() {
  global $user;
  services_remove_user_data($user);

  $return = new stdClass();
  $return->sessid = session_id();
  $return->session_name = session_name();
  $return->user = $user;

  return $return;
}

?>