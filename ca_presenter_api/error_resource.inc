<?php
/**
 * services resource callback for errors
 *
 * Building this so we can track app errors and causes for maintanence
 *
 * @param array $error
 *
 */
function ca_presenter_api_error($error) {
  global $user;

  ob_start();
  var_dump($error);
  $r = ob_get_clean();
  watchdog('App Errors', $r);
}
