<?php
/**
 * services resource callback for presentations categories index
 *
 * @return array
 */
function ca_presenter_api_presentation_categories_index() {
  global $user;
  global $published_full;
  global $published_web;

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'presentation')
    ->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));

    $result = $query->execute();
    $presentations = entity_load('node', array_keys($result['node']));

    if (count($presentations) <= 0) {
      return NULL;
    }

    $data = array();

    $allCategories = array(
      'id' => '0',
      'name' => 'All categories',
      'weight' => '0'
    );

    foreach ($presentations as $presentation) {
      if( $presentation->field_taxonomy['und'] ){
        $prescategories = taxonomy_term_load($presentation->field_taxonomy['und'][0]['tid']);

        if( $prescategories->tid !== null ) {
          $item = array(
            'id' => $prescategories->tid,
            'name' => $prescategories->name,
            'weight' => $prescategories->weight
          );


          $data[] = $item;
        }
      }
    }

    $data = array_values(array_unique($data, SORT_REGULAR));

    usort($data, function ($a, $b) {
      $aname = strtolower($a['name']);
      $bname = strtolower($b['name']);

      return strcmp($aname, $bname);
    });

    array_unshift($data, $allCategories);

    return $data;
}