<?php
/**
 * services resource callback for presentations categories index
 *
 * @return array
 */
function ca_presenter_api_document_categories_index() {
  global $user;
  global $published_full;
  global $published_web;

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'document')
    ->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));

    $result = $query->execute();
    $documents = entity_load('node', array_keys($result['node']));

    if (count($documents) <= 0) {
      return NULL;
    }

    $data = array();

    $allCategories = array(
      'id' => '0',
      'name' => 'All categories',
      'weight' => '0'
    );

    foreach ($documents as $document) {
      if( $document->field_document_category['und'] ){
        $doccategories = taxonomy_term_load($document->field_document_category['und'][0]['tid']);

        if( $doccategories->tid !== null ) {
          $item = array(
            'id' => $doccategories->tid,
            'name' => $doccategories->name,
            'weight' => $doccategories->weight
          );


          $data[] = $item;
        }
      }
    }

    $data = array_values(array_unique($data, SORT_REGULAR));

    usort($data, function ($a, $b) {
      $aname = strtolower($a['name']);
      $bname = strtolower($b['name']);

      return strcmp($aname, $bname);
    });

    array_unshift($data, $allCategories);

    return $data;
}