<?php
/**
 * services resource callback for presentations index
 * @param int $id
 * @param string $title
 * @param string $category
 * @param string $author
 * @param string $group
 * @param string $page_index
 *
 * @return array $data
 */

function ca_presenter_api_presentation_index($ref_or_title, $category, $author, $group, $status, $page_index) {
  global $user;
  global $published_full;
  global $published_web;

  $items_per_page = 20;
  $start = 0;

  if($page_index !== 0) {
      $start = $page_index * $items_per_page;
  }

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'presentation')
    ->propertyOrderBy('created', 'DESC')
    ->range($start, $items_per_page);

    /**
     * Filter the results by field status
     */
    if( $status == 'full') {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_full->tid);
    } else if( $status == 'web' ) {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_web->tid);
    } else {
      $query->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));
    }

    /**
     * Filter the results by group if isset
     */
    if( isset($group) && $group != 'all' ){
      if($group == 'library'){
        $query->propertyCondition('status', 1, '=');
      }
      if($group == 'personal') {
        $query->propertyCondition('uid', $user->uid, '=');
      }
      if($grup == 'shared') {
        $query->propertyCondition('uid', $user->uid, '!=');
      }
    }

    /**
     * Search by node id or title if isset
     */
    if( is_numeric($ref_or_title) ) {
      $query->propertyCondition('nid', $ref_or_title);
    } else {
      $query->propertyCondition('title', '%' . $ref_or_title . '%', 'like');
    }

    /**
     * Search by author if isset
     */
    if( isset($author) && $author != '' ) {
      $presAuthor = user_load_by_name($author);

      $query->propertyCondition('uid', $presAuthor->uid, '=');
    }

    /**
     * Filter the results by category if isset
     */
    if( isset($category) && $category != '0'){
      $query->fieldCondition('field_taxonomy', 'tid', $category);
    }

    $result = $query->execute();
    $data = ca_presenter_api_presentations_build_list($result);




    if (count($data) <= 0) {
      return NULL;
    }

    return $data;
}

/**
 * builds presentations list
 *
 * @params object result
 *
 * @return array array
 */
function ca_presenter_api_presentations_build_list($result){
  global $user;
  global $published_full;
  global $published_web;
  global $base_root;
  $presentations = entity_load('node', array_keys($result['node']));

  if (count($presentations) <= 0) {
    return NULL;
  }

  $data = array();

  foreach ($presentations as $presentation) {

    $categories = taxonomy_term_load($presentation->field_taxonomy['und'][0]['tid']);
    $pauthor = user_load($presentation->uid);
    $author = $pauthor->name;
    //$thumbnail = (ca_presenter_presentation_get_thumbnail_path($presentation)) ? file_create_url(ca_presenter_presentation_get_thumbnail_path($presentation)) : NULL;
    $thumbnail = (ca_presenter_presentation_get_thumbnail_path($presentation)) ? ca_presenter_presentation_get_thumbnail_path($presentation) : NULL;

    //Add image style implementation
    $image_uri      = $thumbnail;
    $style          = 'app_thumbnails';
    $derivative_uri = image_style_path($style, $image_uri);
    $success        = file_exists($derivative_uri) || image_style_create_derivative(image_style_load($style), $image_uri, $derivative_uri);
    $new_image_url  = file_create_url($derivative_uri);
    $uri = image_style_url($style, $image_uri);
    $thumbnail = $uri;

    $instance = get_instance_url();

    $download_url = $base_root . '/presentations/' . $presentation->nid . '/download';
    // @TODO
    $view_url = NULL;

    $group = ca_presenter_presentation_get_group($presentation);

    if($group == 'personal'){
      $group = 'User Library';
    }

    if($group == 'library'){
      $group = 'Core Library';
    }

    if($group == 'personal'){
      $group = 'Shared Library';
    }

    $data[] = array(
      'id' => $presentation->nid,
      'title' => $presentation->title,
      'description' => strip_tags(($presentation->body) ? $presentation->body['und'][0]['value'] : NULL),
      'category' => array(
        'id' => $categories->tid,
        'name' => $categories->name,
        'weight' => $categories->weight
      ),
      'author' => $author,
      'group' => $group,
      'thumbnail' => $thumbnail,
      'date_created' => $presentation->created,
      'date_created_formatted' => date('d/m/y G:i', $presentation->created),
      'date_modified' => $presentation->changed,
      'date_modified_formatted' => date('d/m/y G:i' , $presentation->changed),
      'download_size' => ca_presenter_download_presentation_filesize($presentation->nid),
      'download_url' => $download_url,
      'archive_hash' => $presentation->field_archive_hash[LANGUAGE_NONE][0]['value'],
      'primary_status' => taxonomy_term_load($presentation->field_primary_statuses['und'][0]['tid']),
      'secondary_status' => taxonomy_term_load($presentation->field_secondary_statuses['und'][0]['tid']),
      'view_url' => 'domain=' . $instance . '&username=' . $user->name . '&session=' . session_id() . '&presentation_id=' . $presentation->nid,
      'viewer_size' => ($presentation->field_viewer_size ? $presentation->field_viewer_size['und'][0]['value'] : NULL),
      'onlyweb' => ($presentation->field_primary_statuses['und'][0]['tid'] == $published_web->tid ? TRUE : FALSE), // IF THE STATUS IS == PUBLISHED WEB ? TRUE | needed in app
      'slide_length' => count($presentation->field_slides['und'])
    );
  }
  return $data;

}

function get_instance_url(){
  global $base_url;

  $parsedUrl = parse_url($base_url);
  $host = explode('.', $parsedUrl['host']);
  $subdomain = $host[0];

  return $subdomain;
}


/**
 * checks for an update on a presentation
 *
 * @params string id
 * @params string date_changed
 *
 * @return Boolean
 */
function ca_presenter_api_presentations_check_update($id, $archive_hash) {
  global $user;
  global $published_full;
  global $published_web;

  $presentation = entity_load_single('node', $id);

  $data = array(
    'has_update' => false,
    'retrieved' => $archive_hash,
    'node' => $presentation->field_archive_hash[LANGUAGE_NONE][0]['value']
  );

  if($presentation->field_primary_statuses['und'][0]['tid'] != $published_full->tid) {
    if($presentation->field_primary_statuses['und'][0]['tid'] == $published_web->tid){
      $data = array(
        'error' => true,
        'status' => 'Published Web',
        'code' => 401
      );
    } else {
      $data = array(
        'error' => true,
        'status' => 'Deleted',
        'code' => 403
      );
    }
  } else {
    if( isset($archive_hash) && $archive_hash != '' && $archive_hash != $presentation->field_archive_hash[LANGUAGE_NONE][0]['value'] ) {
      $data = array(
        'has_update' => true,
        'retrieved' => $archive_hash,
        'node' => $presentation->field_archive_hash[LANGUAGE_NONE][0]['value']
      );
    }
  }

  return $data;
}


/**
 * checks for an update on all presentations
 *
 * @params array presentations
 *
 * @return array $data
 */
function ca_presenter_api_presentations_sync($presentations) {
  global $user;
  global $published_full;
  global $published_web;

  if (count($presentations) <= 0) {
    return NULL;
  }

  $data = array();

  foreach($presentations as $presentation) {

    if(!$presentation['id']){
      return;
    }

    $node = entity_load_single('node', $presentation['id']);

    if($node->field_primary_statuses['und'][0]['tid'] != $published_full->tid) {
      if($node->field_primary_statuses['und'][0]['tid'] == $published_web->tid){
        $data[] = array(
          'error' => true,
          'id' => $presentation['id'],
          'status' => 'Published Web',
          'code' => 401
        );
      } else {
        $data[] = array(
          'error' => true,
          'id' => $presentation['id'],
          'status' => 'Deleted',
          'code' => 403
        );
      }
    } else {
      if( isset($presentation['archive_hash'] ) && $presentation['archive_hash'] != ''  && $presentation['archive_hash'] != $node->field_archive_hash[LANGUAGE_NONE][0]['value'] ) {
        $data[] = array(
          'id' => $presentation['id'],
          'retrieved' => $presentation['archive_hash'],
          'node' => $node->field_archive_hash[LANGUAGE_NONE][0]['value'],
          'has_update' => true
        );
      } else {
        $data[] = array(
          'id' => $presentation['id'],
          'retrieved' => $presentation['archive_hash'],
          'node' => $node->field_archive_hash[LANGUAGE_NONE][0]['value'],
          'has_update' => false
        );
      }
    }
  }

  return $data;
}

/**
 * returns the hash of a presentation
 *
 * @return array
 */
function ca_presenter_api_get_presentation_hash($id){
  global $user;

  $presentation = entity_load_single('node', $id);

  return $presentation->field_archive_hash[LANGUAGE_NONE][0]['value'];
}

/**
 * get the number of presentations available for the user
 *
 * @return array
 */
function ca_presenter_api_presentation_list($ref_or_title, $category, $author, $group, $status) {
  global $user;
  global $published_full;
  global $published_web;

  $query = new EntityFieldQuery();
  $query
    ->entityCondition('entity_type', 'node', '=')
    ->entityCondition('bundle', 'presentation')
    ->propertyOrderBy('created', 'DESC');

    /**
     * Filter the results by field status
     */
    if( $status == 'full') {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_full->tid);
    } else if( $status == 'web' ) {
      $query->fieldCondition('field_primary_statuses', 'tid', $published_web->tid);
    } else {
      $query->fieldCondition('field_primary_statuses', 'tid', array($published_full->tid, $published_web->tid));
    }

    /**
     * Filter the results by group if isset
     */
    if( isset($group) && $group != 'all' ){
      if($group == 'library'){
        $query->propertyCondition('status', 1, '=');
      }
      if($group == 'personal') {
        $query->propertyCondition('uid', $user->uid, '=');
      }
      if($grup == 'shared') {
        $query->propertyCondition('uid', $user->uid, '!=');
      }
    }

    /**
     * Search by node id or title if isset
     */
    if( is_numeric($ref_or_title) ) {
      $query->propertyCondition('nid', $ref_or_title);
    } else {
      $query->propertyCondition('title', '%' . $ref_or_title . '%', 'like');
    }

    /**
     * Search by author if isset
     */
    if( isset($author) && $author != '' ) {
      $presAuthor = user_load_by_name($author);

      $query->propertyCondition('uid', $presAuthor->uid);
    }

    if( isset($category) && $category != '0'){
      $query->fieldCondition('field_taxonomy', 'tid', $category);
    }

    $result = $query->execute();
    $presentations = entity_load('node', array_keys($result['node']));

    return count($presentations);
}