<?php
  /**
   * 
   * This function is a node insert callback so please DO NOT MANUPULATE ANY OF
   * attributes from node object!
   * 
   * @param object $node
   * @return string
   */
  function ca_presenter_event_hook($node) {
    //Send resources send email
    if($node->type == 'event' && $node->title == 'SEND_RESOURCES') {
      $account = user_load($node->uid);
      $ca_presenter_emails_module_path = drupal_get_path('module', 'ca_presenter_emails');
      require_once($ca_presenter_emails_module_path . '/ca_presenter_emails.inc');
      $data = json_decode($node->field_metadata_value[LANGUAGE_NONE][0]['value'],TRUE);
      
      //Create email node      
      $email = new stdClass();
      $email->type = 'email';
      $email->title = t('Information from Companyapp');
      node_object_prepare($email);
      $email->uid = $account->uid;
      $email->body = '';
      $email->field_recipients_email[LANGUAGE_NONE][0]['value'] = implode(',',$data['emailAddress']);
      $email->field_recipients_name[LANGUAGE_NONE][0]['value'] = implode(',',$data['firstName']);
      if(isset($account->profile_firstname) && isset($account->profile_lastname))
        $email->field_from_name[LANGUAGE_NONE][0]['value'] = $account->profile_firstname . ' ' . $account->profile_lastname;
      else
        $email->field_from_name[LANGUAGE_NONE][0]['value'] = 'Companyapp';
      $email->field_reply_email[LANGUAGE_NONE][0]['value'] = 'noreply@companyapp.co.uk';
      $email->field_email_subject[LANGUAGE_NONE][0]['value'] = t('Information from Companyapp');
      $email->field_email_message[LANGUAGE_NONE][0]['value'] = t('Please find links below to the resources you requested. I hope you find them useful. Please come back to me with any questions.');
      $email->field_email_documents[LANGUAGE_NONE][0]['value'] = implode(',',$data['selectedDocumentIDs']);
      $email->field_email_presentations[LANGUAGE_NONE][0]['value'] = implode(',',$data['selectedPresentationIDs']);
      $email->field_email_note[LANGUAGE_NONE][0]['value'] = t('Sent by Companyapp ©AutoScript');
      node_save($email);
      //Archive, so we gonna know we sent it.
      /*$node->field_archived[LANGUAGE_NONE][0]['value'] = time();
      node_save($node);*/
      return 'send_email';
    }
  }
?>
